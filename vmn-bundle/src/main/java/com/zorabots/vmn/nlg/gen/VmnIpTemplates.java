/**
* GENERATED CODE
*/
package com.zorabots.vmn.nlg.gen;

public final class VmnIpTemplates extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.spit.TemplateCollection> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "spit#TemplateCollection";

	public VmnIpTemplates(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public VmnIpTemplates(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public VmnIpTemplates(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public VmnIpTemplates() {
		super();
	}

	@Override
	protected de.semvox.types.odp.spit.TemplateCollection createThing() {
		return getTemplateCollection();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.spit.TemplateCollection templateCollection0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("VmnIpTemplates");
		templateCollection0.setCommonName(string0);
	}

	de.semvox.types.odp.spit.TemplateCollection getTemplateCollection() {
		final de.semvox.types.odp.spit.TemplateCollection templateCollection0 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplateCollection();
		return templateCollection0;
	}

}