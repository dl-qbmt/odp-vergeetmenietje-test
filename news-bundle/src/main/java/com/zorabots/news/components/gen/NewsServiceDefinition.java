/**
* GENERATED CODE
*/
package com.zorabots.news.components.gen;

public final class NewsServiceDefinition extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.subcon.integration.ServiceDefinition> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "int#ServiceDefinition";

	public NewsServiceDefinition(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public NewsServiceDefinition(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public NewsServiceDefinition(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public NewsServiceDefinition() {
		super();
	}

	@Override
	protected de.semvox.types.subcon.integration.ServiceDefinition createThing() {
		return getServiceDefinition();
	}

	@Override
	protected void initialize(final de.semvox.types.subcon.integration.ServiceDefinition serviceDefinition0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("NewsService");
		serviceDefinition0.setCommonName(string0);
		String string1 = replaceEntities("com.zorabots.news.components");
		serviceDefinition0.setIntNamespace(string1);
		com.zorabots.ontologies.news.GetNews getNews1 = getGetNews1();
		serviceDefinition0.getIntSubscriptionsAsIntServiceRequest().put("GET_NEWS", getNews1);
	}

	de.semvox.types.subcon.integration.ServiceDefinition getServiceDefinition() {
		final de.semvox.types.subcon.integration.ServiceDefinition serviceDefinition0 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newServiceDefinition();
		return serviceDefinition0;
	}


	com.zorabots.ontologies.news.GetNews getGetNews1() {
		final com.zorabots.ontologies.news.GetNews getNews1 = com.zorabots.ontologies.news.factory.PatternFactoryNews.newGetNews();
		return getNews1;
	}
}