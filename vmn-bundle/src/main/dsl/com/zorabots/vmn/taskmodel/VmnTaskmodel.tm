taskmodel {
	events {
		presentable-event HUMIDITY_UPDATE {
			description "Humidity event"
			pattern {
				object vmn#HumidityUpdate;		
			}
			system-reaction {
				 object ia#Inform{
				 	property common#content ref: HUMIDITY_UPDATE;
				 }
			}
		}
		
		presentable-event TEMP_UPDATE {
			description "temp event"
			pattern {
				object vmn#TemperatureUpdate ;		
			}
			system-reaction {
				 object ia#Inform{
				 	property common#content ref: TEMP_UPDATE;
				 }
			}
		}
		presentable-event LIGHT_UPDATE {
			description "light event"
			pattern {
				object vmn#LightUpdate ;		
			}
			system-reaction {
				 object ia#Inform{
				 	property common#content ref: LIGHT_UPDATE;
				 }
			}
		}
		presentable-event NEWPOT {
			description "new pot event"
			pattern {
				object vmn#NewPotUpdate ;		
			}
			system-reaction {
				 object ia#Inform{
				 	property common#content ref: NEWPOT;
				 }
			}
		}
		
		presentable-event START {
			description "start event"
			pattern {
				object vmn#StartUpdate ;		
			}
			system-reaction {
				 object ia#Inform{
				 	property common#content ref: START;
				 }
			}
		}
		
		presentable-event WIFI {
			description "wifi event"
			pattern {
				object vmn#WifiUpdate ;		
			}
			system-reaction {
				 object ia#Inform{
				 	property common#content ref: WIFI;
				 }
			}
		}
		
		presentable-event TTS {
			description "tts event"
			pattern {
				object vmn#TtsUpdate ;		
			}
			system-reaction {
				 object ia#Inform{
				 	property common#content ref: TTS;
				 }
			}
		}
	}
	tasks {
	    executable-task GET_TEMPERATURE {
	      description "A task to get the weather report."
	      task-confirmation-directive never-confirm-execution
	      cancelation-directive none
	      interruption-directive cancel-on-interruption
	      execution direct-execution
	      use-hypothesis-fusion yes
	      pattern {
	        object vmn#GetTemperature ;
	      }
	    }
	    
	    executable-task GET_HUMIDITY {
	      description "A task to get the weather report."
	      task-confirmation-directive never-confirm-execution
	      cancelation-directive none
	      interruption-directive cancel-on-interruption
	      execution direct-execution
	      use-hypothesis-fusion yes
	      pattern {
	        object vmn#GetHumidity ;
	      }
	    }
	    
	    executable-task GET_LIGHT {
	      description "A task to get the weather report."
	      task-confirmation-directive never-confirm-execution
	      cancelation-directive none
	      interruption-directive cancel-on-interruption
	      execution direct-execution
	      use-hypothesis-fusion yes
	      pattern {
	        object vmn#GetLight ;
	      }
	    }
	    
	    executable-task GET_TIME {
	      description "A task to get the weather report."
	      task-confirmation-directive never-confirm-execution
	      cancelation-directive none
	      interruption-directive cancel-on-interruption
	      execution direct-execution
	      use-hypothesis-fusion yes
	      pattern {
	        object vmn#GetTime;
	      }
	    }
	    
	    executable-task GET_DATE {
	      description "A task to get the weather report."
	      task-confirmation-directive never-confirm-execution
	      cancelation-directive none
	      interruption-directive cancel-on-interruption
	      execution direct-execution
	      use-hypothesis-fusion yes
	      pattern {
	        object vmn#GetDate ;
	      }
	    }
	  
	  executable-task GET_PLANTNAME {
	      description "A task to get the weather report."
	      task-confirmation-directive never-confirm-execution
	      cancelation-directive none
	      interruption-directive cancel-on-interruption
	      execution direct-execution
	      use-hypothesis-fusion yes
	      pattern {
	        object vmn#GetPlantName ;
	      }
	    }
	    
	    executable-task GET_PLANTPARAMETERS {
	      description "A task to get the weather report."
	      task-confirmation-directive never-confirm-execution
	      cancelation-directive none
	      interruption-directive cancel-on-interruption
	      execution direct-execution
	      use-hypothesis-fusion yes
	      pattern {
	        object vmn#GetParameters ;
	      }
	    }
	    
	    executable-task GET_IP_ADDRESS {
	      description "A task to get the first IP address that is not a local loop address. "
	      task-confirmation-directive never-confirm-execution
	      cancelation-directive none
	      interruption-directive cancel-on-interruption
	      execution direct-execution
	      use-hypothesis-fusion yes
	      pattern {
	        object vmn#GetIpAddress ;
	      }
	    }
	    
	    executable-task GET_HI {
	      description "A task to say hello to the user. "
	      task-confirmation-directive never-confirm-execution
	      cancelation-directive none
	      interruption-directive cancel-on-interruption
	      execution direct-execution
	      use-hypothesis-fusion yes
	      pattern {
	        object vmn#GetHi ;
	      }
	    }
	    
	    executable-task GET_INTRODUCTION {
	      description "A task to start saying an introduction to the user. "
	      task-confirmation-directive never-confirm-execution
	      cancelation-directive none
	      interruption-directive cancel-on-interruption
	      execution direct-execution
	      use-hypothesis-fusion yes
	      pattern {
	        object vmn#GetIntroduction ;
	      }
	    }
	    
	    executable-task GET_LEAVE_ALONE {
	      description "A task to react to the user leaving the flowerpot "
	      task-confirmation-directive never-confirm-execution
	      cancelation-directive none
	      interruption-directive cancel-on-interruption
	      execution direct-execution
	      use-hypothesis-fusion yes
	      pattern {
	        object vmn#GetLeaveAlone ;
	      }
	    }
	    
	    executable-task GET_PLANT_DESCRIPTION {
	      description "A task to get the description of the plant. "
	      task-confirmation-directive never-confirm-execution
	      cancelation-directive none
	      interruption-directive cancel-on-interruption
	      execution direct-execution
	      use-hypothesis-fusion yes
	      pattern {
	        object vmn#GetPlantDescription ;
	      }
	    }
	  }
}