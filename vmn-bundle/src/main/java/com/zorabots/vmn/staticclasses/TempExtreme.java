package com.zorabots.vmn.staticclasses;

import com.zorabots.ontologies.vmn.factory.ThingFactoryVmn;

public final class TempExtreme {
	public static com.zorabots.ontologies.vmn.TempExtreme getTempExtremeFromServer(int i){
		if(i == 1){
			return getTempExtreme("HIGH");
		}else{
			return getTempExtreme("LOW");
		}
	}
	
	public static com.zorabots.ontologies.vmn.TempExtreme getTempExtreme(String tempextreme){
		switch(tempextreme){
		case "HIGH":
			return (com.zorabots.ontologies.vmn.TempExtreme)ThingFactoryVmn.newHot();
		case "LOW":
			return (com.zorabots.ontologies.vmn.TempExtreme)ThingFactoryVmn.newCold();
		
		}
		return null;
	}

}
