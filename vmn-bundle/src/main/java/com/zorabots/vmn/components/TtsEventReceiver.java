package com.zorabots.vmn.components;

import java.util.Map;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.simple.JSONObject;

import com.zorabots.ontologies.vmn.TtsUpdate;
import com.zorabots.ontologies.vmn.factory.ThingFactoryVmn;
import com.zorabots.socket.Client;
import com.zorabots.vmn.components.gen.TtsEventReceiverDefinition;
import com.zorabots.vmn.events.JsonParser;
import com.zorabots.vmn.routing.MQTTClient;

import de.semvox.commons.logging.Logger.LogLevel;
import de.semvox.subcon.integration.api.EventReceiver;
import de.semvox.subcon.integration.api.IpcFacade;
import de.semvox.types.common.Event;
import de.semvox.types.odp.multimodal.PresentEvent;
import de.semvox.types.odp.multimodal.factory.ThingFactoryMultimodal;
import de.semvox.types.subcon.integration.EventReceiverDefinition;
import de.semvox.types.subcon.integration.TokenEvent;

public class TtsEventReceiver implements EventReceiver {

	private final EventReceiverDefinition definition;
	private IpcFacade ipcFacade;

	public TtsEventReceiver(IpcFacade ipcFacade) throws Exception {
		definition = new TtsEventReceiverDefinition().loadThing();
		this.ipcFacade = ipcFacade;
	}

	@Override
	public EventReceiverDefinition getEventReceiverDefinition() {
		return definition;
	}

	@Override
	public void dispose() {

	}

	@Override
	public void handleEvent(Event event, Map<String, Object> boundVariables) {
		if (event instanceof TtsUpdate) {
			TtsUpdate ttsUpdate = (TtsUpdate) event;
			PresentEvent presentEvent = ThingFactoryMultimodal.newPresentEvent();
			presentEvent.setMmEvent(ttsUpdate);
			ipcFacade.invokeService(presentEvent, getClass().getCanonicalName());
		}
	}

	@Override
	public void handleToken(TokenEvent token, Map<String, Object> boundVariables) {
		// TODO Auto-generated method stub
	}
	
	public void messageHandle(String json){
		de.semvox.commons.logging.Logger.log(LogLevel.INFO, "json", json);
		JSONObject jsonObject = JsonParser.parseJson(json);
		
		String message = "";
		
		if (jsonObject.containsKey("language")){
			String language = (String) jsonObject.get("language");
			Client.getInstance().setLanguage(language);
		}
		
		if (jsonObject.containsKey("message")){
			message = (String) jsonObject.get("message");
		}
		
		TtsUpdate ttsUpdate = ThingFactoryVmn.newTtsUpdate();
		ttsUpdate.setCommonStringValue(message);
		ipcFacade.sendEvent((Event) ttsUpdate, "zelve");
	}
	
	public void setConfigHandle(String json){
		de.semvox.commons.logging.Logger.log(LogLevel.INFO, "json", json);
		JSONObject jsonObject = JsonParser.parseJson(json);
		Boolean somethingChanged = false;
		
		if (jsonObject.containsKey("pitch")){
			long pitch_long = (long) jsonObject.get("pitch");
			int pitch = (int) pitch_long;
			Client.getInstance().setPitch(pitch);
			somethingChanged = true;
		}
		
		if (jsonObject.containsKey("speechrate")){
			long speechrate_long = (long) jsonObject.get("speechrate");
			int speechrate = (int) speechrate_long;
			Client.getInstance().setSpeechRate(speechrate);
			somethingChanged = true;
		}
	}
	
	public void getConfigHandle(String json){
		de.semvox.commons.logging.Logger.log(LogLevel.INFO, "json", json);
		JSONObject jsonObject = JsonParser.parseJson(json);
		
		String key = "{default-key}";
		if(jsonObject.containsKey("key")){
			key = (String) jsonObject.get("key");
		}
		
		int pitch = Client.getInstance().getPitch();
		int speechrate = Client.getInstance().getSpeechRate();
		
		try {
			MQTTClient.getInstance().publish("vmnodp/response/" + key + "/tts/config", "{\"pitch\": " + pitch + ", \"speechrate\":" + speechrate + "}");
		} catch (MqttException e) {
			de.semvox.commons.logging.Logger.log(LogLevel.INFO, "MQTT ERROR", "A MQTT error was thrown while publishing a tts config response caused by a tts config get request. ");
			e.printStackTrace();
		}
	}
	
	public void getLanguageHandle(String json){
		de.semvox.commons.logging.Logger.log(LogLevel.INFO, "json", json);
		JSONObject jsonObject = JsonParser.parseJson(json);
		
		String key = "{default-key}";
		if(jsonObject.containsKey("key")){
			key = (String) jsonObject.get("key");
		}
		
		String language = Client.getInstance().getOriginalLanguage();
	
		try {
			MQTTClient.getInstance().publish("vmnodp/response/" + key + "/tts/language", "{\"language\": " + language + "}");
		} catch (MqttException e) {
			de.semvox.commons.logging.Logger.log(LogLevel.INFO, "MQTT ERROR", "A MQTT error was thrown while publishing a tts config response caused by a tts config get request. ");
			e.printStackTrace();
		}
	}
}
