package com.zorabots.vmn.staticclasses;

import com.zorabots.ontologies.vmn.NegativeQuestion;
import com.zorabots.ontologies.vmn.NeutralQuestion;
import com.zorabots.ontologies.vmn.PositiveQuestion;
import com.zorabots.ontologies.vmn.YesNoQuestionParamater;

public class ClosedQuestionParameters {

	public static void yesNoQuestion(YesNoQuestionParamater request, int level){
		
		if(request.getVmnYesNoQuestionType() instanceof NegativeQuestion){
			if(level == -1){
				request.setVmnYesNo(com.zorabots.vmn.staticclasses.YesNo.getYesNo("YES"));
				request.setVmnYesNoQuestionAnswer(YesNoAnswerType.getYesNoAnswerType("NEGBAD"));
			}else{
				request.setVmnYesNo(com.zorabots.vmn.staticclasses.YesNo.getYesNo("NO"));
				if(level == 0){
					request.setVmnYesNoQuestionAnswer(YesNoAnswerType.getYesNoAnswerType("GOOD"));
				}else if(level == 1){
					request.setVmnYesNoQuestionAnswer(YesNoAnswerType.getYesNoAnswerType("POSBAD"));
				}

			}
		}else if(request.getVmnYesNoQuestionType() instanceof PositiveQuestion){
			if(level == 1){
				request.setVmnYesNo(com.zorabots.vmn.staticclasses.YesNo.getYesNo("YES"));
				request.setVmnYesNoQuestionAnswer(YesNoAnswerType.getYesNoAnswerType("POSBAD"));
			}else{
				request.setVmnYesNo(com.zorabots.vmn.staticclasses.YesNo.getYesNo("NO"));
				if(level == 0){
					request.setVmnYesNoQuestionAnswer(YesNoAnswerType.getYesNoAnswerType("GOOD"));
				}else if(level == -1){
					request.setVmnYesNoQuestionAnswer(YesNoAnswerType.getYesNoAnswerType("NEGBAD"));
				}

			}
		}else if(request.getVmnYesNoQuestionType() instanceof NeutralQuestion){
			if(level == 0){
				request.setVmnYesNo(com.zorabots.vmn.staticclasses.YesNo.getYesNo("YES"));
				request.setVmnYesNoQuestionAnswer(YesNoAnswerType.getYesNoAnswerType("GOOD"));
			}else{
				request.setVmnYesNo(com.zorabots.vmn.staticclasses.YesNo.getYesNo("NO"));
				if(level == 1){
					request.setVmnYesNoQuestionAnswer(YesNoAnswerType.getYesNoAnswerType("POSBAD"));
				}else if(level == -1){
					request.setVmnYesNoQuestionAnswer(YesNoAnswerType.getYesNoAnswerType("NEGBAD"));
				}

			}
		}
			
		
	}
}
