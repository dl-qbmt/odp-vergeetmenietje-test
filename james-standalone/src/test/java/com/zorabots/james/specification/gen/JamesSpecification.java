/**
* GENERATED CODE
*/
package com.zorabots.james.specification.gen;

public final class JamesSpecification extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.specification.Specification> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "spec#Specification";

	public JamesSpecification(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public JamesSpecification(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public JamesSpecification(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public JamesSpecification() {
		super();
	}

	@Override
	protected de.semvox.types.odp.specification.Specification createThing() {
		return getSpecification();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.specification.Specification specification0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("JamesSpecification");
		specification0.setCommonName(string0);
		String string1 = replaceEntities("src/test/dsl/com/zorabots/james/specification/JamesSpecification.spec#JamesSpecification");
		specification0.setCommonId(string1);
		de.semvox.types.odp.specification.Scenario scenario1 = getScenario1();
		specification0.getSpecScenarios().add(scenario1);
	}

	de.semvox.types.odp.specification.Specification getSpecification() {
		final de.semvox.types.odp.specification.Specification specification0 = de.semvox.types.odp.specification.factory.PatternFactorySpecification.newSpecification();
		return specification0;
	}


	de.semvox.types.odp.specification.Scenario getScenario1() {
		final de.semvox.types.odp.specification.Scenario scenario1 = de.semvox.types.odp.specification.factory.PatternFactorySpecification.newScenario();
		String string2 = replaceEntities("Temperature");
		scenario1.setCommonName(string2);
		String string3 = replaceEntities("src/test/dsl/com/zorabots/james/specification/JamesSpecification.spec#JamesSpecification/Temperature");
		scenario1.setCommonId(string3);
		de.semvox.types.odp.specification.Intro intro2 = getIntro2();
		scenario1.setSpecIntro(intro2);
		de.semvox.types.odp.specification.Conversation conversation9 = getConversation9();
		scenario1.getSpecConversations().add(conversation9);
		return scenario1;
	}

	de.semvox.types.odp.specification.Intro getIntro2() {
		final de.semvox.types.odp.specification.Intro intro2 = de.semvox.types.odp.specification.factory.PatternFactorySpecification.newIntro();
		String string4 = replaceEntities("src/test/dsl/com/zorabots/james/specification/JamesSpecification.spec#JamesSpecification/Temperature");
		intro2.setCommonId(string4);
		de.semvox.types.odp.specification.TestSequence testSequence3 = getTestSequence3();
		intro2.getSpecElementsAsSpecSequence().add(testSequence3);
		return intro2;
	}

	de.semvox.types.odp.specification.TestSequence getTestSequence3() {
		final de.semvox.types.odp.specification.TestSequence testSequence3 = de.semvox.types.odp.specification.factory.PatternFactorySpecification.newTestSequence();
		String string5 = replaceEntities("src/test/dsl/com/zorabots/james/specification/JamesSpecification.spec#JamesSpecification/Temperature");
		testSequence3.setCommonId(string5);
		de.semvox.types.odp.testing.SendInputEvent sendInputEvent4 = getSendInputEvent4();
		testSequence3.getSpecElementsAsTestTestUnit().add(sendInputEvent4);
		de.semvox.types.odp.testing.MatchOutputEvent matchOutputEvent7 = getMatchOutputEvent7();
		testSequence3.getSpecElementsAsTestTestUnit().add(matchOutputEvent7);
		return testSequence3;
	}

	de.semvox.types.odp.testing.SendInputEvent getSendInputEvent4() {
		final de.semvox.types.odp.testing.SendInputEvent sendInputEvent4 = de.semvox.types.odp.testing.factory.PatternFactoryTesting.newSendInputEvent();
		de.semvox.types.subcon.integration.InitializeSystem initializeSystem5 = getInitializeSystem5();
		sendInputEvent4.setTestEvent(initializeSystem5);
		return sendInputEvent4;
	}

	de.semvox.types.subcon.integration.InitializeSystem getInitializeSystem5() {
		final de.semvox.types.subcon.integration.InitializeSystem initializeSystem5 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newInitializeSystem();
		de.semvox.types.subcon.integration.SystemInitialized systemInitialized6 = getSystemInitialized6();
		initializeSystem5.setIntSendOnComplete(systemInitialized6);
		return initializeSystem5;
	}

	de.semvox.types.subcon.integration.SystemInitialized getSystemInitialized6() {
		final de.semvox.types.subcon.integration.SystemInitialized systemInitialized6 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newSystemInitialized();
		return systemInitialized6;
	}

	de.semvox.types.odp.testing.MatchOutputEvent getMatchOutputEvent7() {
		final de.semvox.types.odp.testing.MatchOutputEvent matchOutputEvent7 = de.semvox.types.odp.testing.factory.PatternFactoryTesting.newMatchOutputEvent();
		de.semvox.types.subcon.integration.SystemInitialized systemInitialized8 = getSystemInitialized8();
		matchOutputEvent7.setTestEvent(systemInitialized8);
		return matchOutputEvent7;
	}

	de.semvox.types.subcon.integration.SystemInitialized getSystemInitialized8() {
		final de.semvox.types.subcon.integration.SystemInitialized systemInitialized8 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newSystemInitialized();
		return systemInitialized8;
	}

	de.semvox.types.odp.specification.Conversation getConversation9() {
		final de.semvox.types.odp.specification.Conversation conversation9 = de.semvox.types.odp.specification.factory.PatternFactorySpecification.newConversation();
		String string6 = replaceEntities("src/test/dsl/com/zorabots/james/specification/JamesSpecification.spec#JamesSpecification/Temperature/GetWeather");
		conversation9.setCommonId(string6);
		String string7 = replaceEntities("GetWeather");
		conversation9.setCommonName(string7);
		de.semvox.types.odp.specification.TestSequence testSequence10 = getTestSequence10();
		conversation9.getSpecElementsAsSpecSequence().add(testSequence10);
		return conversation9;
	}

	de.semvox.types.odp.specification.TestSequence getTestSequence10() {
		final de.semvox.types.odp.specification.TestSequence testSequence10 = de.semvox.types.odp.specification.factory.PatternFactorySpecification.newTestSequence();
		String string8 = replaceEntities("src/test/dsl/com/zorabots/james/specification/JamesSpecification.spec#JamesSpecification/Temperature/GetWeather");
		testSequence10.setCommonId(string8);
		de.semvox.types.odp.testing.SendInputEvent sendInputEvent11 = getSendInputEvent11();
		testSequence10.getSpecElementsAsTestTestUnit().add(sendInputEvent11);
		de.semvox.types.odp.specification.SendVoiceInput sendVoiceInput13 = getSendVoiceInput13();
		testSequence10.getSpecElementsAsTestTestUnit().add(sendVoiceInput13);
		de.semvox.types.odp.specification.MatchVoiceOutput matchVoiceOutput15 = getMatchVoiceOutput15();
		testSequence10.getSpecElementsAsTestTestUnit().add(matchVoiceOutput15);
		return testSequence10;
	}

	de.semvox.types.odp.testing.SendInputEvent getSendInputEvent11() {
		final de.semvox.types.odp.testing.SendInputEvent sendInputEvent11 = de.semvox.types.odp.testing.factory.PatternFactoryTesting.newSendInputEvent();
		de.semvox.types.odp.multimodal.PushToActivate pushToActivate12 = getPushToActivate12();
		sendInputEvent11.setTestEvent(pushToActivate12);
		return sendInputEvent11;
	}

	de.semvox.types.odp.multimodal.PushToActivate getPushToActivate12() {
		final de.semvox.types.odp.multimodal.PushToActivate pushToActivate12 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newPushToActivate();
		return pushToActivate12;
	}

	de.semvox.types.odp.specification.SendVoiceInput getSendVoiceInput13() {
		final de.semvox.types.odp.specification.SendVoiceInput sendVoiceInput13 = de.semvox.types.odp.specification.factory.PatternFactorySpecification.newSendVoiceInput();
		de.semvox.types.asr.GrammarResult grammarResult14 = getGrammarResult14();
		sendVoiceInput13.getAsrResults().add(grammarResult14);
		return sendVoiceInput13;
	}

	de.semvox.types.asr.GrammarResult getGrammarResult14() {
		final de.semvox.types.asr.GrammarResult grammarResult14 = de.semvox.types.asr.factory.PatternFactoryAsr.newGrammarResult();
		String string9 = replaceEntities("what is your temperature");
		grammarResult14.setAsrUtterance(string9);
		String string10 = replaceEntities("0.7");
		grammarResult14.setCommonConfidence(Double.parseDouble(string10));
		String string11 = replaceEntities("en-US");
		grammarResult14.setCommonLanguage(string11);
		return grammarResult14;
	}

	de.semvox.types.odp.specification.MatchVoiceOutput getMatchVoiceOutput15() {
		final de.semvox.types.odp.specification.MatchVoiceOutput matchVoiceOutput15 = de.semvox.types.odp.specification.factory.PatternFactorySpecification.newMatchVoiceOutput();
		String string12 = replaceEntities("<?xml version=\"1.0\"?><speak version=\"1.1\" xmlns=\"http://www.w3.org/2001/10/synthesis\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/2001/10/synthesis http://www.w3.org/TR/speech-synthesis11/synthesis.xsd\" xml:lang=\"en-US\">what is your temperature</speak>");
		matchVoiceOutput15.getSpecOutputAlternatives().add(string12);
		return matchVoiceOutput15;
	}
}