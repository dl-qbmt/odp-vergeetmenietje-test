/**
* GENERATED CODE
*/
package com.zorabots.weather.components.gen;

public final class RelativeMomentProviderDefinition extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.ltm.ContentProviderDefinition> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "ltm#ContentProviderDefinition";

	public RelativeMomentProviderDefinition(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public RelativeMomentProviderDefinition(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public RelativeMomentProviderDefinition(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public RelativeMomentProviderDefinition() {
		super();
	}

	@Override
	protected de.semvox.types.odp.ltm.ContentProviderDefinition createThing() {
		return getContentProviderDefinition();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.ltm.ContentProviderDefinition contentProviderDefinition0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("RelativeMomentProvider");
		contentProviderDefinition0.setCommonName(string0);
		String string1 = replaceEntities("com.zorabots.weather.components");
		contentProviderDefinition0.setIntNamespace(string1);
	}

	de.semvox.types.odp.ltm.ContentProviderDefinition getContentProviderDefinition() {
		final de.semvox.types.odp.ltm.ContentProviderDefinition contentProviderDefinition0 = de.semvox.types.odp.ltm.factory.PatternFactoryLtm.newContentProviderDefinition();
		return contentProviderDefinition0;
	}

}