package com.zorabots.vmn.components;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Map;

import com.zorabots.ontologies.vmn.GetIpAddress;
import com.zorabots.ontologies.vmn.IpAddress;
import com.zorabots.ontologies.vmn.factory.ThingFactoryVmn;
import com.zorabots.vmn.components.gen.VmnIpServiceDefinition;

import de.semvox.subcon.integration.api.Service;
import de.semvox.types.subcon.integration.ServiceDefinition;
import de.semvox.types.subcon.integration.ServiceRequest;
import de.semvox.types.subcon.integration.factory.ThingFactoryIntegration;

public class VmnIpService implements Service {

	private final ServiceDefinition definition;

	public VmnIpService() throws Exception {
		definition = new VmnIpServiceDefinition().loadThing();
	}

	@Override
	public ServiceDefinition getServiceDefinition() {
		return definition;
	}

	@Override
	public void dispose() {

	}

	@Override
	public ServiceRequest handleRequest(ServiceRequest request,
			Map<String, Object> boundVariables) {
		if (request instanceof GetIpAddress) {
			IpAddress vmnipaddress = ThingFactoryVmn.newIpAddress();

			try {
				InetAddress ipaddress = InetAddress.getLocalHost();
				String ipstring = "";
				String ipstringdotted = "";
				// Boolean foundip = false;

				for (Enumeration en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
					NetworkInterface intf = (NetworkInterface) en.nextElement();
					for (Enumeration enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
						InetAddress inetAddress = (InetAddress) enumIpAddr.nextElement();
						if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
							ipstring = inetAddress.getHostAddress().toString();
							ipstringdotted = ipstring.replace(".", ", punt, ");
							vmnipaddress.setCommonStringValue(ipstringdotted);
							// foundip = true;
							// break;
						}
					}
					/*
					 * if (foundip){ break; }
					 */
				}
				((GetIpAddress) request).setVmnIpAddress(vmnipaddress);
				request.setIntResponseStatus(ThingFactoryIntegration.newStatusSuccess());
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				System.out.println("Unknown Host Exception was thrown while trying to retrieve local IP address in service; ");
				e.printStackTrace();
			} catch (SocketException e) {
				// TODO Auto-generated catch block
				System.out.println("Unknown Host Exception was thrown while trying to retrieve local IP address in service; ");
				e.printStackTrace();
			}
		}
		// TODO Auto-generated method stub
		return request;
	}

}
