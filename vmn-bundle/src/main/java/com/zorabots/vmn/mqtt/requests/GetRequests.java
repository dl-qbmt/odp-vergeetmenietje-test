package com.zorabots.vmn.mqtt.requests;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;

import com.zorabots.vmn.routing.MQTTClient;
import com.zorabots.vmn.routing.MQTTRouter;

import de.semvox.commons.logging.Logger;
import de.semvox.commons.logging.Logger.LogLevel;

public class GetRequests extends Thread{

	private String reqTopic;
	private Object obj;
	private String methodname;
	private String message;
	private String recTopic;
	private Object request;
	
	public GetRequests(String reqTopic, String recTopic, String message, Object obj, String methodname, Object request){
		this.reqTopic = reqTopic;
		this.recTopic = recTopic;
		this.obj = obj;
		this.methodname = methodname;
		this.message = message;
		this.request = request;
	}
	
	public GetRequests(String reqTopic, String recTopic, String message, Object obj, Object request){
		this(reqTopic, recTopic, message, obj, "handle", request);
	}
	
	public GetRequests(String reqTopic, String recTopic, String message, Object obj, String methodName){
		this(reqTopic, recTopic, message, obj, methodName, null);
	}
	
	public void run(){
		MQTTRouter.getInstance().route(recTopic, "getRequest", this);
		sendRequest();
	}
	
	public void sendRequest(){
		try {
			MQTTClient.getInstance().publish(reqTopic, message);
		} catch (MqttPersistenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void getRequest(String recMessage){
		try {
			if(request == null){
				Method method = obj.getClass().getMethod(methodname, String.class);
				method.invoke(obj, recMessage);
			}else{
				Method method = obj.getClass().getMethod(methodname, String.class, Object.class);
				method.invoke(obj, recMessage, request);
			}
			
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			Logger.log(LogLevel.INFO, "GetRequest ERROR", e.toString());
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			Logger.log(LogLevel.INFO, "GetRequest ERROR", e.toString());
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			Logger.log(LogLevel.INFO, "GetRequest ERROR", e.toString());
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			Logger.log(LogLevel.INFO, "GetRequest ERROR", e.toString());
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			Logger.log(LogLevel.INFO, "GetRequest ERROR", e.toString());
			e.printStackTrace();
		}
	}
}
