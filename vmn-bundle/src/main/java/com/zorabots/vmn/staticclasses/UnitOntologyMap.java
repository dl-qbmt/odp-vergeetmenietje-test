package com.zorabots.vmn.staticclasses;

import com.zorabots.ontologies.vmn.factory.ThingFactoryVmn;

public final class UnitOntologyMap {
	public static com.zorabots.ontologies.vmn.Unit getUnitOntology(String unit) {
		switch (unit) {
		case "degrees":
			return (com.zorabots.ontologies.vmn.Unit) ThingFactoryVmn.newDegrees();
		case "lux":
			return (com.zorabots.ontologies.vmn.Unit) ThingFactoryVmn.newLux();
		case "percent":
			return (com.zorabots.ontologies.vmn.Unit) ThingFactoryVmn.newPercent();
		}
		return null;
	}
}
