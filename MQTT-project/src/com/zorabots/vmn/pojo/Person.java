package com.zorabots.vmn.pojo;

public class Person {
	private String name = "nick";
	private static Person person = null;
	
	public static Person getInstance(){
		if(person == null){
			person = new Person();
		}
		return person;
	}
	
	public String getName(){
		return name;
	}
}
