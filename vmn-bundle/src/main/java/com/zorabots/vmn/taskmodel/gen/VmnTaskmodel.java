/**
* GENERATED CODE
*/
package com.zorabots.vmn.taskmodel.gen;

public final class VmnTaskmodel extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.squint.TaskModel> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "squint#TaskModel";

	public VmnTaskmodel(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public VmnTaskmodel(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public VmnTaskmodel(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public VmnTaskmodel() {
		super();
	}

	@Override
	protected de.semvox.types.odp.squint.TaskModel createThing() {
		return getTaskModel();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.squint.TaskModel taskModel0, java.util.Map<String, Object> params) {

		de.semvox.types.odp.squint.EventHandlingPolicy eventHandlingPolicy1 = getEventHandlingPolicy1();
		taskModel0.getSquintEventHandlingPolicies().add(eventHandlingPolicy1);
		de.semvox.types.odp.squint.EventHandlingPolicy eventHandlingPolicy4 = getEventHandlingPolicy4();
		taskModel0.getSquintEventHandlingPolicies().add(eventHandlingPolicy4);
		de.semvox.types.odp.squint.EventHandlingPolicy eventHandlingPolicy7 = getEventHandlingPolicy7();
		taskModel0.getSquintEventHandlingPolicies().add(eventHandlingPolicy7);
		de.semvox.types.odp.squint.EventHandlingPolicy eventHandlingPolicy10 = getEventHandlingPolicy10();
		taskModel0.getSquintEventHandlingPolicies().add(eventHandlingPolicy10);
		de.semvox.types.odp.squint.EventHandlingPolicy eventHandlingPolicy13 = getEventHandlingPolicy13();
		taskModel0.getSquintEventHandlingPolicies().add(eventHandlingPolicy13);
		de.semvox.types.odp.squint.EventHandlingPolicy eventHandlingPolicy16 = getEventHandlingPolicy16();
		taskModel0.getSquintEventHandlingPolicies().add(eventHandlingPolicy16);
		de.semvox.types.odp.squint.EventHandlingPolicy eventHandlingPolicy19 = getEventHandlingPolicy19();
		taskModel0.getSquintEventHandlingPolicies().add(eventHandlingPolicy19);
		de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition22 = getTaskExecutionDefinition22();
		taskModel0.getSquintTaskDefinitions().add(taskExecutionDefinition22);
		de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition26 = getTaskExecutionDefinition26();
		taskModel0.getSquintTaskDefinitions().add(taskExecutionDefinition26);
		de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition30 = getTaskExecutionDefinition30();
		taskModel0.getSquintTaskDefinitions().add(taskExecutionDefinition30);
		de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition34 = getTaskExecutionDefinition34();
		taskModel0.getSquintTaskDefinitions().add(taskExecutionDefinition34);
		de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition38 = getTaskExecutionDefinition38();
		taskModel0.getSquintTaskDefinitions().add(taskExecutionDefinition38);
		de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition42 = getTaskExecutionDefinition42();
		taskModel0.getSquintTaskDefinitions().add(taskExecutionDefinition42);
		de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition46 = getTaskExecutionDefinition46();
		taskModel0.getSquintTaskDefinitions().add(taskExecutionDefinition46);
		de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition50 = getTaskExecutionDefinition50();
		taskModel0.getSquintTaskDefinitions().add(taskExecutionDefinition50);
		de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition54 = getTaskExecutionDefinition54();
		taskModel0.getSquintTaskDefinitions().add(taskExecutionDefinition54);
		de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition58 = getTaskExecutionDefinition58();
		taskModel0.getSquintTaskDefinitions().add(taskExecutionDefinition58);
		de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition62 = getTaskExecutionDefinition62();
		taskModel0.getSquintTaskDefinitions().add(taskExecutionDefinition62);
		de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition66 = getTaskExecutionDefinition66();
		taskModel0.getSquintTaskDefinitions().add(taskExecutionDefinition66);
	}

	de.semvox.types.odp.squint.TaskModel getTaskModel() {
		final de.semvox.types.odp.squint.TaskModel taskModel0 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newTaskModel();
		return taskModel0;
	}


	de.semvox.types.odp.squint.EventHandlingPolicy getEventHandlingPolicy1() {
		final de.semvox.types.odp.squint.EventHandlingPolicy eventHandlingPolicy1 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newEventHandlingPolicy();
		String string0 = replaceEntities("HUMIDITY_UPDATE");
		eventHandlingPolicy1.setCommonName(string0);
		String string1 = replaceEntities("Humidity event");
		eventHandlingPolicy1.setIntDescription(string1);
		eventHandlingPolicy1.markMmEventAsVariable("HUMIDITY_UPDATE", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.HumidityUpdate humidityUpdate2 = getHumidityUpdate2();
		eventHandlingPolicy1.setMmEvent(humidityUpdate2);
		de.semvox.types.odp.interaction.Inform inform3 = getInform3();
		eventHandlingPolicy1.getSquintSystemInitiativeActs().add(inform3);
		return eventHandlingPolicy1;
	}

	com.zorabots.ontologies.vmn.HumidityUpdate getHumidityUpdate2() {
		final com.zorabots.ontologies.vmn.HumidityUpdate humidityUpdate2 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidityUpdate();
		return humidityUpdate2;
	}

	de.semvox.types.odp.interaction.Inform getInform3() {
		final de.semvox.types.odp.interaction.Inform inform3 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		inform3.markCommonContentAsVariable("HUMIDITY_UPDATE", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		inform3.assignCommonContentPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return inform3;
	}

	de.semvox.types.odp.squint.EventHandlingPolicy getEventHandlingPolicy4() {
		final de.semvox.types.odp.squint.EventHandlingPolicy eventHandlingPolicy4 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newEventHandlingPolicy();
		String string2 = replaceEntities("TEMP_UPDATE");
		eventHandlingPolicy4.setCommonName(string2);
		String string3 = replaceEntities("temp event");
		eventHandlingPolicy4.setIntDescription(string3);
		eventHandlingPolicy4.markMmEventAsVariable("TEMP_UPDATE", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.TemperatureUpdate temperatureUpdate5 = getTemperatureUpdate5();
		eventHandlingPolicy4.setMmEvent(temperatureUpdate5);
		de.semvox.types.odp.interaction.Inform inform6 = getInform6();
		eventHandlingPolicy4.getSquintSystemInitiativeActs().add(inform6);
		return eventHandlingPolicy4;
	}

	com.zorabots.ontologies.vmn.TemperatureUpdate getTemperatureUpdate5() {
		final com.zorabots.ontologies.vmn.TemperatureUpdate temperatureUpdate5 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperatureUpdate();
		return temperatureUpdate5;
	}

	de.semvox.types.odp.interaction.Inform getInform6() {
		final de.semvox.types.odp.interaction.Inform inform6 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		inform6.markCommonContentAsVariable("TEMP_UPDATE", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		inform6.assignCommonContentPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return inform6;
	}

	de.semvox.types.odp.squint.EventHandlingPolicy getEventHandlingPolicy7() {
		final de.semvox.types.odp.squint.EventHandlingPolicy eventHandlingPolicy7 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newEventHandlingPolicy();
		String string4 = replaceEntities("LIGHT_UPDATE");
		eventHandlingPolicy7.setCommonName(string4);
		String string5 = replaceEntities("light event");
		eventHandlingPolicy7.setIntDescription(string5);
		eventHandlingPolicy7.markMmEventAsVariable("LIGHT_UPDATE", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.LightUpdate lightUpdate8 = getLightUpdate8();
		eventHandlingPolicy7.setMmEvent(lightUpdate8);
		de.semvox.types.odp.interaction.Inform inform9 = getInform9();
		eventHandlingPolicy7.getSquintSystemInitiativeActs().add(inform9);
		return eventHandlingPolicy7;
	}

	com.zorabots.ontologies.vmn.LightUpdate getLightUpdate8() {
		final com.zorabots.ontologies.vmn.LightUpdate lightUpdate8 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLightUpdate();
		return lightUpdate8;
	}

	de.semvox.types.odp.interaction.Inform getInform9() {
		final de.semvox.types.odp.interaction.Inform inform9 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		inform9.markCommonContentAsVariable("LIGHT_UPDATE", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		inform9.assignCommonContentPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return inform9;
	}

	de.semvox.types.odp.squint.EventHandlingPolicy getEventHandlingPolicy10() {
		final de.semvox.types.odp.squint.EventHandlingPolicy eventHandlingPolicy10 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newEventHandlingPolicy();
		String string6 = replaceEntities("NEWPOT");
		eventHandlingPolicy10.setCommonName(string6);
		String string7 = replaceEntities("new pot event");
		eventHandlingPolicy10.setIntDescription(string7);
		eventHandlingPolicy10.markMmEventAsVariable("NEWPOT", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.NewPotUpdate newPotUpdate11 = getNewPotUpdate11();
		eventHandlingPolicy10.setMmEvent(newPotUpdate11);
		de.semvox.types.odp.interaction.Inform inform12 = getInform12();
		eventHandlingPolicy10.getSquintSystemInitiativeActs().add(inform12);
		return eventHandlingPolicy10;
	}

	com.zorabots.ontologies.vmn.NewPotUpdate getNewPotUpdate11() {
		final com.zorabots.ontologies.vmn.NewPotUpdate newPotUpdate11 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNewPotUpdate();
		return newPotUpdate11;
	}

	de.semvox.types.odp.interaction.Inform getInform12() {
		final de.semvox.types.odp.interaction.Inform inform12 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		inform12.markCommonContentAsVariable("NEWPOT", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		inform12.assignCommonContentPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return inform12;
	}

	de.semvox.types.odp.squint.EventHandlingPolicy getEventHandlingPolicy13() {
		final de.semvox.types.odp.squint.EventHandlingPolicy eventHandlingPolicy13 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newEventHandlingPolicy();
		String string8 = replaceEntities("START");
		eventHandlingPolicy13.setCommonName(string8);
		String string9 = replaceEntities("start event");
		eventHandlingPolicy13.setIntDescription(string9);
		eventHandlingPolicy13.markMmEventAsVariable("START", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.StartUpdate startUpdate14 = getStartUpdate14();
		eventHandlingPolicy13.setMmEvent(startUpdate14);
		de.semvox.types.odp.interaction.Inform inform15 = getInform15();
		eventHandlingPolicy13.getSquintSystemInitiativeActs().add(inform15);
		return eventHandlingPolicy13;
	}

	com.zorabots.ontologies.vmn.StartUpdate getStartUpdate14() {
		final com.zorabots.ontologies.vmn.StartUpdate startUpdate14 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newStartUpdate();
		return startUpdate14;
	}

	de.semvox.types.odp.interaction.Inform getInform15() {
		final de.semvox.types.odp.interaction.Inform inform15 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		inform15.markCommonContentAsVariable("START", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		inform15.assignCommonContentPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return inform15;
	}

	de.semvox.types.odp.squint.EventHandlingPolicy getEventHandlingPolicy16() {
		final de.semvox.types.odp.squint.EventHandlingPolicy eventHandlingPolicy16 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newEventHandlingPolicy();
		String string10 = replaceEntities("WIFI");
		eventHandlingPolicy16.setCommonName(string10);
		String string11 = replaceEntities("wifi event");
		eventHandlingPolicy16.setIntDescription(string11);
		eventHandlingPolicy16.markMmEventAsVariable("WIFI", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.WifiUpdate wifiUpdate17 = getWifiUpdate17();
		eventHandlingPolicy16.setMmEvent(wifiUpdate17);
		de.semvox.types.odp.interaction.Inform inform18 = getInform18();
		eventHandlingPolicy16.getSquintSystemInitiativeActs().add(inform18);
		return eventHandlingPolicy16;
	}

	com.zorabots.ontologies.vmn.WifiUpdate getWifiUpdate17() {
		final com.zorabots.ontologies.vmn.WifiUpdate wifiUpdate17 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newWifiUpdate();
		return wifiUpdate17;
	}

	de.semvox.types.odp.interaction.Inform getInform18() {
		final de.semvox.types.odp.interaction.Inform inform18 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		inform18.markCommonContentAsVariable("WIFI", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		inform18.assignCommonContentPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return inform18;
	}

	de.semvox.types.odp.squint.EventHandlingPolicy getEventHandlingPolicy19() {
		final de.semvox.types.odp.squint.EventHandlingPolicy eventHandlingPolicy19 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newEventHandlingPolicy();
		String string12 = replaceEntities("TTS");
		eventHandlingPolicy19.setCommonName(string12);
		String string13 = replaceEntities("tts event");
		eventHandlingPolicy19.setIntDescription(string13);
		eventHandlingPolicy19.markMmEventAsVariable("TTS", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.TtsUpdate ttsUpdate20 = getTtsUpdate20();
		eventHandlingPolicy19.setMmEvent(ttsUpdate20);
		de.semvox.types.odp.interaction.Inform inform21 = getInform21();
		eventHandlingPolicy19.getSquintSystemInitiativeActs().add(inform21);
		return eventHandlingPolicy19;
	}

	com.zorabots.ontologies.vmn.TtsUpdate getTtsUpdate20() {
		final com.zorabots.ontologies.vmn.TtsUpdate ttsUpdate20 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTtsUpdate();
		return ttsUpdate20;
	}

	de.semvox.types.odp.interaction.Inform getInform21() {
		final de.semvox.types.odp.interaction.Inform inform21 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		inform21.markCommonContentAsVariable("TTS", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		inform21.assignCommonContentPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return inform21;
	}

	de.semvox.types.odp.squint.TaskExecutionDefinition getTaskExecutionDefinition22() {
		final de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition22 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newTaskExecutionDefinition();
		String string14 = replaceEntities("A task to get the weather report.");
		taskExecutionDefinition22.setIntDescription(string14);
		String string15 = replaceEntities("true");
		taskExecutionDefinition22.setSquintUseHypothesisFusion(Boolean.parseBoolean(string15));
		String string16 = replaceEntities("GET_TEMPERATURE");
		taskExecutionDefinition22.setCommonName(string16);
		taskExecutionDefinition22.markMmTaskAsVariable("GET_TEMPERATURE", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.GetTemperature getTemperature23 = getGetTemperature23();
		taskExecutionDefinition22.setMmTask(getTemperature23);
		de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution24 = getNeverConfirmExecution24();
		taskExecutionDefinition22.setSquintTaskConfirmationDirective(neverConfirmExecution24);
		String string17 = replaceEntities("true");
		taskExecutionDefinition22.setSquintAllowCorrectLastStepIntoClosedSelectionSpaces(Boolean.parseBoolean(string17));
		taskExecutionDefinition22.assignSquintCancelationDirectivePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption25 = getCancelTaskOnInterruption25();
		taskExecutionDefinition22.setSquintInterruptionDirective(cancelTaskOnInterruption25);
		String string18 = replaceEntities("com/zorabots/vmn/taskmodel/VmnTaskmodel#GET_TEMPERATURE");
		taskExecutionDefinition22.setCommonId(string18);
		return taskExecutionDefinition22;
	}

	com.zorabots.ontologies.vmn.GetTemperature getGetTemperature23() {
		final com.zorabots.ontologies.vmn.GetTemperature getTemperature23 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetTemperature();
		return getTemperature23;
	}

	de.semvox.types.odp.squint.NeverConfirmExecution getNeverConfirmExecution24() {
		final de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution24 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newNeverConfirmExecution();
		return neverConfirmExecution24;
	}

	de.semvox.types.odp.squint.CancelTaskOnInterruption getCancelTaskOnInterruption25() {
		final de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption25 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newCancelTaskOnInterruption();
		return cancelTaskOnInterruption25;
	}

	de.semvox.types.odp.squint.TaskExecutionDefinition getTaskExecutionDefinition26() {
		final de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition26 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newTaskExecutionDefinition();
		String string19 = replaceEntities("A task to get the weather report.");
		taskExecutionDefinition26.setIntDescription(string19);
		String string20 = replaceEntities("true");
		taskExecutionDefinition26.setSquintUseHypothesisFusion(Boolean.parseBoolean(string20));
		String string21 = replaceEntities("GET_HUMIDITY");
		taskExecutionDefinition26.setCommonName(string21);
		taskExecutionDefinition26.markMmTaskAsVariable("GET_HUMIDITY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.GetHumidity getHumidity27 = getGetHumidity27();
		taskExecutionDefinition26.setMmTask(getHumidity27);
		de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution28 = getNeverConfirmExecution28();
		taskExecutionDefinition26.setSquintTaskConfirmationDirective(neverConfirmExecution28);
		String string22 = replaceEntities("true");
		taskExecutionDefinition26.setSquintAllowCorrectLastStepIntoClosedSelectionSpaces(Boolean.parseBoolean(string22));
		taskExecutionDefinition26.assignSquintCancelationDirectivePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption29 = getCancelTaskOnInterruption29();
		taskExecutionDefinition26.setSquintInterruptionDirective(cancelTaskOnInterruption29);
		String string23 = replaceEntities("com/zorabots/vmn/taskmodel/VmnTaskmodel#GET_HUMIDITY");
		taskExecutionDefinition26.setCommonId(string23);
		return taskExecutionDefinition26;
	}

	com.zorabots.ontologies.vmn.GetHumidity getGetHumidity27() {
		final com.zorabots.ontologies.vmn.GetHumidity getHumidity27 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetHumidity();
		return getHumidity27;
	}

	de.semvox.types.odp.squint.NeverConfirmExecution getNeverConfirmExecution28() {
		final de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution28 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newNeverConfirmExecution();
		return neverConfirmExecution28;
	}

	de.semvox.types.odp.squint.CancelTaskOnInterruption getCancelTaskOnInterruption29() {
		final de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption29 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newCancelTaskOnInterruption();
		return cancelTaskOnInterruption29;
	}

	de.semvox.types.odp.squint.TaskExecutionDefinition getTaskExecutionDefinition30() {
		final de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition30 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newTaskExecutionDefinition();
		String string24 = replaceEntities("A task to get the weather report.");
		taskExecutionDefinition30.setIntDescription(string24);
		String string25 = replaceEntities("true");
		taskExecutionDefinition30.setSquintUseHypothesisFusion(Boolean.parseBoolean(string25));
		String string26 = replaceEntities("GET_LIGHT");
		taskExecutionDefinition30.setCommonName(string26);
		taskExecutionDefinition30.markMmTaskAsVariable("GET_LIGHT", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.GetLight getLight31 = getGetLight31();
		taskExecutionDefinition30.setMmTask(getLight31);
		de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution32 = getNeverConfirmExecution32();
		taskExecutionDefinition30.setSquintTaskConfirmationDirective(neverConfirmExecution32);
		String string27 = replaceEntities("true");
		taskExecutionDefinition30.setSquintAllowCorrectLastStepIntoClosedSelectionSpaces(Boolean.parseBoolean(string27));
		taskExecutionDefinition30.assignSquintCancelationDirectivePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption33 = getCancelTaskOnInterruption33();
		taskExecutionDefinition30.setSquintInterruptionDirective(cancelTaskOnInterruption33);
		String string28 = replaceEntities("com/zorabots/vmn/taskmodel/VmnTaskmodel#GET_LIGHT");
		taskExecutionDefinition30.setCommonId(string28);
		return taskExecutionDefinition30;
	}

	com.zorabots.ontologies.vmn.GetLight getGetLight31() {
		final com.zorabots.ontologies.vmn.GetLight getLight31 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetLight();
		return getLight31;
	}

	de.semvox.types.odp.squint.NeverConfirmExecution getNeverConfirmExecution32() {
		final de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution32 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newNeverConfirmExecution();
		return neverConfirmExecution32;
	}

	de.semvox.types.odp.squint.CancelTaskOnInterruption getCancelTaskOnInterruption33() {
		final de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption33 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newCancelTaskOnInterruption();
		return cancelTaskOnInterruption33;
	}

	de.semvox.types.odp.squint.TaskExecutionDefinition getTaskExecutionDefinition34() {
		final de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition34 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newTaskExecutionDefinition();
		String string29 = replaceEntities("A task to get the weather report.");
		taskExecutionDefinition34.setIntDescription(string29);
		String string30 = replaceEntities("true");
		taskExecutionDefinition34.setSquintUseHypothesisFusion(Boolean.parseBoolean(string30));
		String string31 = replaceEntities("GET_TIME");
		taskExecutionDefinition34.setCommonName(string31);
		taskExecutionDefinition34.markMmTaskAsVariable("GET_TIME", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.GetTime getTime35 = getGetTime35();
		taskExecutionDefinition34.setMmTask(getTime35);
		de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution36 = getNeverConfirmExecution36();
		taskExecutionDefinition34.setSquintTaskConfirmationDirective(neverConfirmExecution36);
		String string32 = replaceEntities("true");
		taskExecutionDefinition34.setSquintAllowCorrectLastStepIntoClosedSelectionSpaces(Boolean.parseBoolean(string32));
		taskExecutionDefinition34.assignSquintCancelationDirectivePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption37 = getCancelTaskOnInterruption37();
		taskExecutionDefinition34.setSquintInterruptionDirective(cancelTaskOnInterruption37);
		String string33 = replaceEntities("com/zorabots/vmn/taskmodel/VmnTaskmodel#GET_TIME");
		taskExecutionDefinition34.setCommonId(string33);
		return taskExecutionDefinition34;
	}

	com.zorabots.ontologies.vmn.GetTime getGetTime35() {
		final com.zorabots.ontologies.vmn.GetTime getTime35 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetTime();
		return getTime35;
	}

	de.semvox.types.odp.squint.NeverConfirmExecution getNeverConfirmExecution36() {
		final de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution36 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newNeverConfirmExecution();
		return neverConfirmExecution36;
	}

	de.semvox.types.odp.squint.CancelTaskOnInterruption getCancelTaskOnInterruption37() {
		final de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption37 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newCancelTaskOnInterruption();
		return cancelTaskOnInterruption37;
	}

	de.semvox.types.odp.squint.TaskExecutionDefinition getTaskExecutionDefinition38() {
		final de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition38 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newTaskExecutionDefinition();
		String string34 = replaceEntities("A task to get the weather report.");
		taskExecutionDefinition38.setIntDescription(string34);
		String string35 = replaceEntities("true");
		taskExecutionDefinition38.setSquintUseHypothesisFusion(Boolean.parseBoolean(string35));
		String string36 = replaceEntities("GET_DATE");
		taskExecutionDefinition38.setCommonName(string36);
		taskExecutionDefinition38.markMmTaskAsVariable("GET_DATE", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.GetDate getDate39 = getGetDate39();
		taskExecutionDefinition38.setMmTask(getDate39);
		de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution40 = getNeverConfirmExecution40();
		taskExecutionDefinition38.setSquintTaskConfirmationDirective(neverConfirmExecution40);
		String string37 = replaceEntities("true");
		taskExecutionDefinition38.setSquintAllowCorrectLastStepIntoClosedSelectionSpaces(Boolean.parseBoolean(string37));
		taskExecutionDefinition38.assignSquintCancelationDirectivePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption41 = getCancelTaskOnInterruption41();
		taskExecutionDefinition38.setSquintInterruptionDirective(cancelTaskOnInterruption41);
		String string38 = replaceEntities("com/zorabots/vmn/taskmodel/VmnTaskmodel#GET_DATE");
		taskExecutionDefinition38.setCommonId(string38);
		return taskExecutionDefinition38;
	}

	com.zorabots.ontologies.vmn.GetDate getGetDate39() {
		final com.zorabots.ontologies.vmn.GetDate getDate39 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetDate();
		return getDate39;
	}

	de.semvox.types.odp.squint.NeverConfirmExecution getNeverConfirmExecution40() {
		final de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution40 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newNeverConfirmExecution();
		return neverConfirmExecution40;
	}

	de.semvox.types.odp.squint.CancelTaskOnInterruption getCancelTaskOnInterruption41() {
		final de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption41 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newCancelTaskOnInterruption();
		return cancelTaskOnInterruption41;
	}

	de.semvox.types.odp.squint.TaskExecutionDefinition getTaskExecutionDefinition42() {
		final de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition42 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newTaskExecutionDefinition();
		String string39 = replaceEntities("A task to get the weather report.");
		taskExecutionDefinition42.setIntDescription(string39);
		String string40 = replaceEntities("true");
		taskExecutionDefinition42.setSquintUseHypothesisFusion(Boolean.parseBoolean(string40));
		String string41 = replaceEntities("GET_PLANTNAME");
		taskExecutionDefinition42.setCommonName(string41);
		taskExecutionDefinition42.markMmTaskAsVariable("GET_PLANTNAME", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.GetPlantName getPlantName43 = getGetPlantName43();
		taskExecutionDefinition42.setMmTask(getPlantName43);
		de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution44 = getNeverConfirmExecution44();
		taskExecutionDefinition42.setSquintTaskConfirmationDirective(neverConfirmExecution44);
		String string42 = replaceEntities("true");
		taskExecutionDefinition42.setSquintAllowCorrectLastStepIntoClosedSelectionSpaces(Boolean.parseBoolean(string42));
		taskExecutionDefinition42.assignSquintCancelationDirectivePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption45 = getCancelTaskOnInterruption45();
		taskExecutionDefinition42.setSquintInterruptionDirective(cancelTaskOnInterruption45);
		String string43 = replaceEntities("com/zorabots/vmn/taskmodel/VmnTaskmodel#GET_PLANTNAME");
		taskExecutionDefinition42.setCommonId(string43);
		return taskExecutionDefinition42;
	}

	com.zorabots.ontologies.vmn.GetPlantName getGetPlantName43() {
		final com.zorabots.ontologies.vmn.GetPlantName getPlantName43 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetPlantName();
		return getPlantName43;
	}

	de.semvox.types.odp.squint.NeverConfirmExecution getNeverConfirmExecution44() {
		final de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution44 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newNeverConfirmExecution();
		return neverConfirmExecution44;
	}

	de.semvox.types.odp.squint.CancelTaskOnInterruption getCancelTaskOnInterruption45() {
		final de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption45 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newCancelTaskOnInterruption();
		return cancelTaskOnInterruption45;
	}

	de.semvox.types.odp.squint.TaskExecutionDefinition getTaskExecutionDefinition46() {
		final de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition46 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newTaskExecutionDefinition();
		String string44 = replaceEntities("A task to get the weather report.");
		taskExecutionDefinition46.setIntDescription(string44);
		String string45 = replaceEntities("true");
		taskExecutionDefinition46.setSquintUseHypothesisFusion(Boolean.parseBoolean(string45));
		String string46 = replaceEntities("GET_PLANTPARAMETERS");
		taskExecutionDefinition46.setCommonName(string46);
		taskExecutionDefinition46.markMmTaskAsVariable("GET_PLANTPARAMETERS", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.GetParameters getParameters47 = getGetParameters47();
		taskExecutionDefinition46.setMmTask(getParameters47);
		de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution48 = getNeverConfirmExecution48();
		taskExecutionDefinition46.setSquintTaskConfirmationDirective(neverConfirmExecution48);
		String string47 = replaceEntities("true");
		taskExecutionDefinition46.setSquintAllowCorrectLastStepIntoClosedSelectionSpaces(Boolean.parseBoolean(string47));
		taskExecutionDefinition46.assignSquintCancelationDirectivePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption49 = getCancelTaskOnInterruption49();
		taskExecutionDefinition46.setSquintInterruptionDirective(cancelTaskOnInterruption49);
		String string48 = replaceEntities("com/zorabots/vmn/taskmodel/VmnTaskmodel#GET_PLANTPARAMETERS");
		taskExecutionDefinition46.setCommonId(string48);
		return taskExecutionDefinition46;
	}

	com.zorabots.ontologies.vmn.GetParameters getGetParameters47() {
		final com.zorabots.ontologies.vmn.GetParameters getParameters47 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetParameters();
		return getParameters47;
	}

	de.semvox.types.odp.squint.NeverConfirmExecution getNeverConfirmExecution48() {
		final de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution48 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newNeverConfirmExecution();
		return neverConfirmExecution48;
	}

	de.semvox.types.odp.squint.CancelTaskOnInterruption getCancelTaskOnInterruption49() {
		final de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption49 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newCancelTaskOnInterruption();
		return cancelTaskOnInterruption49;
	}

	de.semvox.types.odp.squint.TaskExecutionDefinition getTaskExecutionDefinition50() {
		final de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition50 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newTaskExecutionDefinition();
		String string49 = replaceEntities("A task to get the first IP address that is not a local loop address. ");
		taskExecutionDefinition50.setIntDescription(string49);
		String string50 = replaceEntities("true");
		taskExecutionDefinition50.setSquintUseHypothesisFusion(Boolean.parseBoolean(string50));
		String string51 = replaceEntities("GET_IP_ADDRESS");
		taskExecutionDefinition50.setCommonName(string51);
		taskExecutionDefinition50.markMmTaskAsVariable("GET_IP_ADDRESS", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.GetIpAddress getIpAddress51 = getGetIpAddress51();
		taskExecutionDefinition50.setMmTask(getIpAddress51);
		de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution52 = getNeverConfirmExecution52();
		taskExecutionDefinition50.setSquintTaskConfirmationDirective(neverConfirmExecution52);
		String string52 = replaceEntities("true");
		taskExecutionDefinition50.setSquintAllowCorrectLastStepIntoClosedSelectionSpaces(Boolean.parseBoolean(string52));
		taskExecutionDefinition50.assignSquintCancelationDirectivePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption53 = getCancelTaskOnInterruption53();
		taskExecutionDefinition50.setSquintInterruptionDirective(cancelTaskOnInterruption53);
		String string53 = replaceEntities("com/zorabots/vmn/taskmodel/VmnTaskmodel#GET_IP_ADDRESS");
		taskExecutionDefinition50.setCommonId(string53);
		return taskExecutionDefinition50;
	}

	com.zorabots.ontologies.vmn.GetIpAddress getGetIpAddress51() {
		final com.zorabots.ontologies.vmn.GetIpAddress getIpAddress51 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetIpAddress();
		return getIpAddress51;
	}

	de.semvox.types.odp.squint.NeverConfirmExecution getNeverConfirmExecution52() {
		final de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution52 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newNeverConfirmExecution();
		return neverConfirmExecution52;
	}

	de.semvox.types.odp.squint.CancelTaskOnInterruption getCancelTaskOnInterruption53() {
		final de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption53 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newCancelTaskOnInterruption();
		return cancelTaskOnInterruption53;
	}

	de.semvox.types.odp.squint.TaskExecutionDefinition getTaskExecutionDefinition54() {
		final de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition54 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newTaskExecutionDefinition();
		String string54 = replaceEntities("A task to say hello to the user. ");
		taskExecutionDefinition54.setIntDescription(string54);
		String string55 = replaceEntities("true");
		taskExecutionDefinition54.setSquintUseHypothesisFusion(Boolean.parseBoolean(string55));
		String string56 = replaceEntities("GET_HI");
		taskExecutionDefinition54.setCommonName(string56);
		taskExecutionDefinition54.markMmTaskAsVariable("GET_HI", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.GetHi getHi55 = getGetHi55();
		taskExecutionDefinition54.setMmTask(getHi55);
		de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution56 = getNeverConfirmExecution56();
		taskExecutionDefinition54.setSquintTaskConfirmationDirective(neverConfirmExecution56);
		String string57 = replaceEntities("true");
		taskExecutionDefinition54.setSquintAllowCorrectLastStepIntoClosedSelectionSpaces(Boolean.parseBoolean(string57));
		taskExecutionDefinition54.assignSquintCancelationDirectivePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption57 = getCancelTaskOnInterruption57();
		taskExecutionDefinition54.setSquintInterruptionDirective(cancelTaskOnInterruption57);
		String string58 = replaceEntities("com/zorabots/vmn/taskmodel/VmnTaskmodel#GET_HI");
		taskExecutionDefinition54.setCommonId(string58);
		return taskExecutionDefinition54;
	}

	com.zorabots.ontologies.vmn.GetHi getGetHi55() {
		final com.zorabots.ontologies.vmn.GetHi getHi55 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetHi();
		return getHi55;
	}

	de.semvox.types.odp.squint.NeverConfirmExecution getNeverConfirmExecution56() {
		final de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution56 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newNeverConfirmExecution();
		return neverConfirmExecution56;
	}

	de.semvox.types.odp.squint.CancelTaskOnInterruption getCancelTaskOnInterruption57() {
		final de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption57 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newCancelTaskOnInterruption();
		return cancelTaskOnInterruption57;
	}

	de.semvox.types.odp.squint.TaskExecutionDefinition getTaskExecutionDefinition58() {
		final de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition58 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newTaskExecutionDefinition();
		String string59 = replaceEntities("A task to start saying an introduction to the user. ");
		taskExecutionDefinition58.setIntDescription(string59);
		String string60 = replaceEntities("true");
		taskExecutionDefinition58.setSquintUseHypothesisFusion(Boolean.parseBoolean(string60));
		String string61 = replaceEntities("GET_INTRODUCTION");
		taskExecutionDefinition58.setCommonName(string61);
		taskExecutionDefinition58.markMmTaskAsVariable("GET_INTRODUCTION", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.GetIntroduction getIntroduction59 = getGetIntroduction59();
		taskExecutionDefinition58.setMmTask(getIntroduction59);
		de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution60 = getNeverConfirmExecution60();
		taskExecutionDefinition58.setSquintTaskConfirmationDirective(neverConfirmExecution60);
		String string62 = replaceEntities("true");
		taskExecutionDefinition58.setSquintAllowCorrectLastStepIntoClosedSelectionSpaces(Boolean.parseBoolean(string62));
		taskExecutionDefinition58.assignSquintCancelationDirectivePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption61 = getCancelTaskOnInterruption61();
		taskExecutionDefinition58.setSquintInterruptionDirective(cancelTaskOnInterruption61);
		String string63 = replaceEntities("com/zorabots/vmn/taskmodel/VmnTaskmodel#GET_INTRODUCTION");
		taskExecutionDefinition58.setCommonId(string63);
		return taskExecutionDefinition58;
	}

	com.zorabots.ontologies.vmn.GetIntroduction getGetIntroduction59() {
		final com.zorabots.ontologies.vmn.GetIntroduction getIntroduction59 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetIntroduction();
		return getIntroduction59;
	}

	de.semvox.types.odp.squint.NeverConfirmExecution getNeverConfirmExecution60() {
		final de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution60 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newNeverConfirmExecution();
		return neverConfirmExecution60;
	}

	de.semvox.types.odp.squint.CancelTaskOnInterruption getCancelTaskOnInterruption61() {
		final de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption61 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newCancelTaskOnInterruption();
		return cancelTaskOnInterruption61;
	}

	de.semvox.types.odp.squint.TaskExecutionDefinition getTaskExecutionDefinition62() {
		final de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition62 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newTaskExecutionDefinition();
		String string64 = replaceEntities("A task to react to the user leaving the flowerpot ");
		taskExecutionDefinition62.setIntDescription(string64);
		String string65 = replaceEntities("true");
		taskExecutionDefinition62.setSquintUseHypothesisFusion(Boolean.parseBoolean(string65));
		String string66 = replaceEntities("GET_LEAVE_ALONE");
		taskExecutionDefinition62.setCommonName(string66);
		taskExecutionDefinition62.markMmTaskAsVariable("GET_LEAVE_ALONE", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.GetLeaveAlone getLeaveAlone63 = getGetLeaveAlone63();
		taskExecutionDefinition62.setMmTask(getLeaveAlone63);
		de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution64 = getNeverConfirmExecution64();
		taskExecutionDefinition62.setSquintTaskConfirmationDirective(neverConfirmExecution64);
		String string67 = replaceEntities("true");
		taskExecutionDefinition62.setSquintAllowCorrectLastStepIntoClosedSelectionSpaces(Boolean.parseBoolean(string67));
		taskExecutionDefinition62.assignSquintCancelationDirectivePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption65 = getCancelTaskOnInterruption65();
		taskExecutionDefinition62.setSquintInterruptionDirective(cancelTaskOnInterruption65);
		String string68 = replaceEntities("com/zorabots/vmn/taskmodel/VmnTaskmodel#GET_LEAVE_ALONE");
		taskExecutionDefinition62.setCommonId(string68);
		return taskExecutionDefinition62;
	}

	com.zorabots.ontologies.vmn.GetLeaveAlone getGetLeaveAlone63() {
		final com.zorabots.ontologies.vmn.GetLeaveAlone getLeaveAlone63 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetLeaveAlone();
		return getLeaveAlone63;
	}

	de.semvox.types.odp.squint.NeverConfirmExecution getNeverConfirmExecution64() {
		final de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution64 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newNeverConfirmExecution();
		return neverConfirmExecution64;
	}

	de.semvox.types.odp.squint.CancelTaskOnInterruption getCancelTaskOnInterruption65() {
		final de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption65 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newCancelTaskOnInterruption();
		return cancelTaskOnInterruption65;
	}

	de.semvox.types.odp.squint.TaskExecutionDefinition getTaskExecutionDefinition66() {
		final de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition66 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newTaskExecutionDefinition();
		String string69 = replaceEntities("A task to get the description of the plant. ");
		taskExecutionDefinition66.setIntDescription(string69);
		String string70 = replaceEntities("true");
		taskExecutionDefinition66.setSquintUseHypothesisFusion(Boolean.parseBoolean(string70));
		String string71 = replaceEntities("GET_PLANT_DESCRIPTION");
		taskExecutionDefinition66.setCommonName(string71);
		taskExecutionDefinition66.markMmTaskAsVariable("GET_PLANT_DESCRIPTION", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.vmn.GetPlantDescription getPlantDescription67 = getGetPlantDescription67();
		taskExecutionDefinition66.setMmTask(getPlantDescription67);
		de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution68 = getNeverConfirmExecution68();
		taskExecutionDefinition66.setSquintTaskConfirmationDirective(neverConfirmExecution68);
		String string72 = replaceEntities("true");
		taskExecutionDefinition66.setSquintAllowCorrectLastStepIntoClosedSelectionSpaces(Boolean.parseBoolean(string72));
		taskExecutionDefinition66.assignSquintCancelationDirectivePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption69 = getCancelTaskOnInterruption69();
		taskExecutionDefinition66.setSquintInterruptionDirective(cancelTaskOnInterruption69);
		String string73 = replaceEntities("com/zorabots/vmn/taskmodel/VmnTaskmodel#GET_PLANT_DESCRIPTION");
		taskExecutionDefinition66.setCommonId(string73);
		return taskExecutionDefinition66;
	}

	com.zorabots.ontologies.vmn.GetPlantDescription getGetPlantDescription67() {
		final com.zorabots.ontologies.vmn.GetPlantDescription getPlantDescription67 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetPlantDescription();
		return getPlantDescription67;
	}

	de.semvox.types.odp.squint.NeverConfirmExecution getNeverConfirmExecution68() {
		final de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution68 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newNeverConfirmExecution();
		return neverConfirmExecution68;
	}

	de.semvox.types.odp.squint.CancelTaskOnInterruption getCancelTaskOnInterruption69() {
		final de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption69 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newCancelTaskOnInterruption();
		return cancelTaskOnInterruption69;
	}
}