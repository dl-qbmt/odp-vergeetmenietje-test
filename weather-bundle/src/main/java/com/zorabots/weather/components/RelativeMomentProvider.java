package com.zorabots.weather.components;

import com.zorabots.weather.components.gen.RelativeMomentProviderDefinition;

import de.semvox.odp.ltm.DefaultStaticSlotContentProvider;
import de.semvox.subcon.data.util.JavaResourceLoader;

public class RelativeMomentProvider extends DefaultStaticSlotContentProvider {

	private static final String DATA_DIRECTORY = "com/zorabots/weather/relativemoments/gen"; // TODO fill in the data directory

	public RelativeMomentProvider() throws Exception {
		super(new RelativeMomentProviderDefinition().loadThing(),
				JavaResourceLoader.loadResources(DATA_DIRECTORY).values());
	}

}
