taskmodel {
	tasks{
		executable-task GET_WEATHER_FORECAST {
		      description "A task to get the news of the plant. "
		      task-confirmation-directive never-confirm-execution
		      cancelation-directive none
		      interruption-directive cancel-on-interruption
		      execution direct-execution
		      use-hypothesis-fusion yes
		      pattern {
		        object weather#GetWeatherForecast ;
		      }
	  }
	  
	  executable-task GET_WEATHER {
		      description "A task to get the news of the plant. "
		      task-confirmation-directive never-confirm-execution
		      cancelation-directive none
		      interruption-directive cancel-on-interruption
		      execution direct-execution
		      use-hypothesis-fusion yes
		      pattern {
		        object weather#GetWeather ;
		      }
	  }
	}
}