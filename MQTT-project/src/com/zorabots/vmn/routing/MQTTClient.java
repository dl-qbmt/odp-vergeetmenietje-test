package com.zorabots.vmn.routing;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;

import com.zorabots.vmn.pojo.MethodInvoker;

import de.semvox.commons.logging.Logger;
import de.semvox.commons.logging.Logger.LogLevel;

public class MQTTClient implements MqttCallback {

	private static MQTTClient mqttClient = null;
	private boolean isRunning = false;
	
	MqttClient myClient;
	MqttConnectOptions connOpt;

	static final String BROKER_URL = "tcp://127.0.0.1:1883";
	static final String DEV_BROKER_URL = "tcp://192.168.0.154:1883";
	static final String clientId = "java-mqtt";
	
	/*static final String M2MIO_DOMAIN = "<Insert m2m.io domain here>";
	static final String M2MIO_STUFF = "things";
	static final String M2MIO_THING = "<Unique device ID>";
	static final String M2MIO_USERNAME = "<m2m.io username>";
	static final String M2MIO_PASSWORD_MD5 = "<m2m.io password (MD5 sum of password)>";
	*/

	// the following two flags control whether this example is a publisher, a subscriber or both
	static final Boolean subscriber = true;
	static final Boolean publisher = true;
	
	private MQTTRouter mqttRouter;
	
	
	private MQTTClient() {
	
	}
	
	public static MQTTClient getInstance(){
		if(mqttClient == null){
			mqttClient = new MQTTClient();
		}
		return mqttClient;
	}

	/**
	 * 
	 * connectionLost
	 * This callback is invoked upon losing the MQTT connection.
	 * 
	 */
	@Override
	public void connectionLost(Throwable t) {
		System.out.println("Connection lost!");
		this.isRunning = false;
		this.runClient();
		// code to reconnect to the broker would go here if desired
	}



	/**
	 * 
	 * runClient
	 * The main functionality of this simple example.
	 * Create a MQTT client, connect to broker, pub/sub, disconnect.
	 * 
	 */
	public void runClient() {
		if(!isRunning){
			isRunning = true;
			// setup MQTT Client
			String clientID = clientId;
			connOpt = new MqttConnectOptions();
			
			connOpt.setCleanSession(true);
			/*connOpt.setUserName(M2MIO_USERNAME);
			connOpt.setPassword(M2MIO_PASSWORD_MD5.toCharArray());*/
			
			// Connect to Broker
			try {
				myClient = new MqttClient(BROKER_URL, clientID);
				myClient.setCallback(this);
				myClient.connect(connOpt);
				System.out.println("Connected to MQTT server on " + BROKER_URL);
				Logger.log(LogLevel.INFO, "MQTT connect", "Connected to MQTT server on " + BROKER_URL);
			} catch (MqttException e) {
				Logger.log(LogLevel.INFO, "MQTT CONNECT ERROR", "Not able to connect to MQTT server on " + BROKER_URL);
				e.printStackTrace();
				Logger.log(LogLevel.INFO, "MQTT CONNECT", "Attempting to connect to MQTT server on " + DEV_BROKER_URL);
		  		try{
		  			myClient = new MqttClient(DEV_BROKER_URL, clientID);
					myClient.setCallback(this);
					myClient.connect(connOpt);
					System.out.println("Connected to MQTT server on " + DEV_BROKER_URL);
					Logger.log(LogLevel.INFO, "MQTT connect", "Connected to MQTT server on " + DEV_BROKER_URL);
		  		}
		  		catch(MqttException e2){
		  			Logger.log(LogLevel.INFO, "MQTT CONNECT ERROR", "Not able to connect to MQTT server on " + DEV_BROKER_URL);
		  			System.out.println("Failed to connect to the MQTT server on  IP " + DEV_BROKER_URL);
					e2.printStackTrace();
					System.exit(-1);
		  		}
			}
			
			
		}
	}
	
	public void subscribe(String topic){
		if (subscriber) {
			try {
				int subQoS = 0;
				myClient.subscribe(topic, subQoS);
			} catch (Exception e) {
				System.out.println("Failed to subscribe on topic " + topic);
				Logger.log(LogLevel.INFO, "MQTT subscribe", "Failed to subscribe on topic " + topic);
				e.printStackTrace();
			}
		}
	}
	
	public void publish(String topic, String message) throws MqttPersistenceException, MqttException{
		MqttMessage mqttMessage = new MqttMessage(message.getBytes());
		myClient.publish(topic, mqttMessage);
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void messageArrived(String arg0, MqttMessage arg1) throws Exception {
		System.out.println("-------------------------------------------------");
		System.out.println("| Topic:" + arg0);
		System.out.println("| Message: " + new String(arg1.getPayload()));
		System.out.println("-------------------------------------------------");
		Logger.log(LogLevel.INFO, "test", new String(arg1.getPayload()));
		mqttRouter = MQTTRouter.getInstance();
		HashMap<String, ArrayList<MethodInvoker>> map = mqttRouter.getRouting();
		
		//Logger.log(LogLevel.INFO, "Resolver", map.keySet().toString());
		
		//resolve given mqtt topic using the regex expressions in the route map. 
		for(String key : map.keySet()){	
			if(arg0.matches(key)){
				//Logger.log(LogLevel.INFO, "Routingmap", "Topic found in routing map!!!");
				for(MethodInvoker m : map.get(key)){
					try{
					Logger.log(LogLevel.INFO, "Invoker", "Invoking " + m.getM().toString() + " from " + m.getO().toString() + " with " + new String(arg1.getPayload()) + " as arguments!!!");	
					m.getM().invoke(m.getO(), new String(arg1.getPayload()));
					Logger.log(LogLevel.INFO, "Invoker", "Invoked " + m.getM().toString() + " from " + m.getO().toString() + " with " + new String(arg1.getPayload()) + " as arguments!!!");
					}
					catch (Exception e){
					Logger.log(LogLevel.INFO, "Invoker Error", m.getM().toString() + " from " + m.getO().toString() + " not invokable. ");
					}
				}
				break;
			}
		}
		
		/*
		if(routeFound){
			Logger.log(LogLevel.INFO, "Routingmap", "Topic found in routing map!!!");
			for(MethodInvoker m : map.get(arg0)){
				try{
				Logger.log(LogLevel.INFO, "Invoker", "Invoking " + m.getM().toString() + " from " + m.getO().toString() + " with " + new String(arg1.getPayload()) + " as arguments!!!");	
				m.getM().invoke(m.getO(), new String(arg1.getPayload()));
				Logger.log(LogLevel.INFO, "Invoker", "Invoked " + m.getM().toString() + " from " + m.getO().toString() + " with " + new String(arg1.getPayload()) + " as arguments!!!");
				}
				catch (Exception e){
				Logger.log(LogLevel.INFO, "Invoker Error", m.getM().toString() + " from " + m.getO().toString() + " not invokable. ");
				}
			}
		}*/
	}
	
	
}
