/**
* GENERATED CODE
*/
package com.zorabots.vmn.nlg.gen;

public final class VmnUrgencyTemplates extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.spit.TemplateCollection> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "spit#TemplateCollection";

	public VmnUrgencyTemplates(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public VmnUrgencyTemplates(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public VmnUrgencyTemplates(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public VmnUrgencyTemplates() {
		super();
	}

	@Override
	protected de.semvox.types.odp.spit.TemplateCollection createThing() {
		return getTemplateCollection();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.spit.TemplateCollection templateCollection0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("VmnUrgencyTemplates");
		templateCollection0.setCommonName(string0);
		de.semvox.types.odp.spit.Template template1 = getTemplate1();
		templateCollection0.getSpitElements().add(template1);
		de.semvox.types.odp.spit.Template template11 = getTemplate11();
		templateCollection0.getSpitElements().add(template11);
		de.semvox.types.odp.spit.Template template21 = getTemplate21();
		templateCollection0.getSpitElements().add(template21);
		de.semvox.types.odp.spit.Template template31 = getTemplate31();
		templateCollection0.getSpitElements().add(template31);
		de.semvox.types.odp.spit.Template template41 = getTemplate41();
		templateCollection0.getSpitElements().add(template41);
	}

	de.semvox.types.odp.spit.TemplateCollection getTemplateCollection() {
		final de.semvox.types.odp.spit.TemplateCollection templateCollection0 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplateCollection();
		return templateCollection0;
	}


	de.semvox.types.odp.spit.Template getTemplate1() {
		final de.semvox.types.odp.spit.Template template1 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string1 = replaceEntities("Urgency_High");
		template1.setCommonName(string1);
		de.semvox.types.odp.spit.Switch switch2 = getSwitch2();
		template1.setSpitAction(switch2);
		de.semvox.types.odp.interaction.GenerationContext generationContext9 = getGenerationContext9();
		template1.setSpitOnContext(generationContext9);
		com.zorabots.ontologies.vmn.Error error10 = getError10();
		template1.setSpitOnInput(error10);
		return template1;
	}

	de.semvox.types.odp.spit.Switch getSwitch2() {
		final de.semvox.types.odp.spit.Switch switch2 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case3 = getCase3();
			switch2.getSpitCases().add(case3);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case6 = getCase6();
			switch2.getSpitCases().add(case6);
		}
		return switch2;
	}

	de.semvox.types.odp.spit.Case getCase3() {
		final de.semvox.types.odp.spit.Case case3 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition4 = getStringCondition4();
		case3.getSpitConditions().add(stringCondition4);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs5 = getGenerateStringAndOutputs5();
		case3.setSpitAction(generateStringAndOutputs5);
		return case3;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition4() {
		final de.semvox.types.odp.spit.StringCondition stringCondition4 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string2 = replaceEntities("lang");
		stringCondition4.setAlgVariableReference(string2);
		String string3 = replaceEntities("en-US");
		stringCondition4.setCommonStringValue(string3);
		return stringCondition4;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs5() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs5 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs5.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string4 = replaceEntities("very urgent");
		generateStringAndOutputs5.getSpitAlternatives().add(string4);
		String string5 = replaceEntities("high urgency");
		generateStringAndOutputs5.getSpitAlternatives().add(string5);
		return generateStringAndOutputs5;
	}

	de.semvox.types.odp.spit.Case getCase6() {
		final de.semvox.types.odp.spit.Case case6 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition7 = getStringCondition7();
		case6.getSpitConditions().add(stringCondition7);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs8 = getGenerateStringAndOutputs8();
		case6.setSpitAction(generateStringAndOutputs8);
		return case6;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition7() {
		final de.semvox.types.odp.spit.StringCondition stringCondition7 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string6 = replaceEntities("lang");
		stringCondition7.setAlgVariableReference(string6);
		String string7 = replaceEntities("nl-BE");
		stringCondition7.setCommonStringValue(string7);
		return stringCondition7;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs8() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs8 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs8.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string8 = replaceEntities("zeer dringend");
		generateStringAndOutputs8.getSpitAlternatives().add(string8);
		return generateStringAndOutputs8;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext9() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext9 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext9.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext9.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext9.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext9.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext9.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext9.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext9;
	}

	com.zorabots.ontologies.vmn.Error getError10() {
		final com.zorabots.ontologies.vmn.Error error10 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newError();
		return error10;
	}

	de.semvox.types.odp.spit.Template getTemplate11() {
		final de.semvox.types.odp.spit.Template template11 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string9 = replaceEntities("Urgency_Medium");
		template11.setCommonName(string9);
		de.semvox.types.odp.spit.Switch switch12 = getSwitch12();
		template11.setSpitAction(switch12);
		de.semvox.types.odp.interaction.GenerationContext generationContext19 = getGenerationContext19();
		template11.setSpitOnContext(generationContext19);
		com.zorabots.ontologies.vmn.Alert alert20 = getAlert20();
		template11.setSpitOnInput(alert20);
		return template11;
	}

	de.semvox.types.odp.spit.Switch getSwitch12() {
		final de.semvox.types.odp.spit.Switch switch12 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case13 = getCase13();
			switch12.getSpitCases().add(case13);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case16 = getCase16();
			switch12.getSpitCases().add(case16);
		}
		return switch12;
	}

	de.semvox.types.odp.spit.Case getCase13() {
		final de.semvox.types.odp.spit.Case case13 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition14 = getStringCondition14();
		case13.getSpitConditions().add(stringCondition14);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs15 = getGenerateStringAndOutputs15();
		case13.setSpitAction(generateStringAndOutputs15);
		return case13;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition14() {
		final de.semvox.types.odp.spit.StringCondition stringCondition14 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string10 = replaceEntities("lang");
		stringCondition14.setAlgVariableReference(string10);
		String string11 = replaceEntities("en-US");
		stringCondition14.setCommonStringValue(string11);
		return stringCondition14;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs15() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs15 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs15.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string12 = replaceEntities("too");
		generateStringAndOutputs15.getSpitAlternatives().add(string12);
		return generateStringAndOutputs15;
	}

	de.semvox.types.odp.spit.Case getCase16() {
		final de.semvox.types.odp.spit.Case case16 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition17 = getStringCondition17();
		case16.getSpitConditions().add(stringCondition17);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs18 = getGenerateStringAndOutputs18();
		case16.setSpitAction(generateStringAndOutputs18);
		return case16;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition17() {
		final de.semvox.types.odp.spit.StringCondition stringCondition17 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string13 = replaceEntities("lang");
		stringCondition17.setAlgVariableReference(string13);
		String string14 = replaceEntities("nl-BE");
		stringCondition17.setCommonStringValue(string14);
		return stringCondition17;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs18() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs18 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs18.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string15 = replaceEntities("te");
		generateStringAndOutputs18.getSpitAlternatives().add(string15);
		return generateStringAndOutputs18;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext19() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext19 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext19.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext19.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext19.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext19.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext19.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext19.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext19;
	}

	com.zorabots.ontologies.vmn.Alert getAlert20() {
		final com.zorabots.ontologies.vmn.Alert alert20 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newAlert();
		return alert20;
	}

	de.semvox.types.odp.spit.Template getTemplate21() {
		final de.semvox.types.odp.spit.Template template21 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string16 = replaceEntities("Urgency_Low");
		template21.setCommonName(string16);
		de.semvox.types.odp.spit.Switch switch22 = getSwitch22();
		template21.setSpitAction(switch22);
		de.semvox.types.odp.interaction.GenerationContext generationContext29 = getGenerationContext29();
		template21.setSpitOnContext(generationContext29);
		com.zorabots.ontologies.vmn.Warning warning30 = getWarning30();
		template21.setSpitOnInput(warning30);
		return template21;
	}

	de.semvox.types.odp.spit.Switch getSwitch22() {
		final de.semvox.types.odp.spit.Switch switch22 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case23 = getCase23();
			switch22.getSpitCases().add(case23);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case26 = getCase26();
			switch22.getSpitCases().add(case26);
		}
		return switch22;
	}

	de.semvox.types.odp.spit.Case getCase23() {
		final de.semvox.types.odp.spit.Case case23 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition24 = getStringCondition24();
		case23.getSpitConditions().add(stringCondition24);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs25 = getGenerateStringAndOutputs25();
		case23.setSpitAction(generateStringAndOutputs25);
		return case23;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition24() {
		final de.semvox.types.odp.spit.StringCondition stringCondition24 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string17 = replaceEntities("lang");
		stringCondition24.setAlgVariableReference(string17);
		String string18 = replaceEntities("en-US");
		stringCondition24.setCommonStringValue(string18);
		return stringCondition24;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs25() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs25 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs25.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string19 = replaceEntities("quite");
		generateStringAndOutputs25.getSpitAlternatives().add(string19);
		return generateStringAndOutputs25;
	}

	de.semvox.types.odp.spit.Case getCase26() {
		final de.semvox.types.odp.spit.Case case26 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition27 = getStringCondition27();
		case26.getSpitConditions().add(stringCondition27);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs28 = getGenerateStringAndOutputs28();
		case26.setSpitAction(generateStringAndOutputs28);
		return case26;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition27() {
		final de.semvox.types.odp.spit.StringCondition stringCondition27 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string20 = replaceEntities("lang");
		stringCondition27.setAlgVariableReference(string20);
		String string21 = replaceEntities("nl-BE");
		stringCondition27.setCommonStringValue(string21);
		return stringCondition27;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs28() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs28 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs28.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string22 = replaceEntities("nogal");
		generateStringAndOutputs28.getSpitAlternatives().add(string22);
		return generateStringAndOutputs28;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext29() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext29 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext29.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext29.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext29.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext29.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext29.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext29.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext29;
	}

	com.zorabots.ontologies.vmn.Warning getWarning30() {
		final com.zorabots.ontologies.vmn.Warning warning30 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newWarning();
		return warning30;
	}

	de.semvox.types.odp.spit.Template getTemplate31() {
		final de.semvox.types.odp.spit.Template template31 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string23 = replaceEntities("Urgency_Kinda");
		template31.setCommonName(string23);
		de.semvox.types.odp.spit.Switch switch32 = getSwitch32();
		template31.setSpitAction(switch32);
		de.semvox.types.odp.interaction.GenerationContext generationContext39 = getGenerationContext39();
		template31.setSpitOnContext(generationContext39);
		com.zorabots.ontologies.vmn.Kinda kinda40 = getKinda40();
		template31.setSpitOnInput(kinda40);
		return template31;
	}

	de.semvox.types.odp.spit.Switch getSwitch32() {
		final de.semvox.types.odp.spit.Switch switch32 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case33 = getCase33();
			switch32.getSpitCases().add(case33);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case36 = getCase36();
			switch32.getSpitCases().add(case36);
		}
		return switch32;
	}

	de.semvox.types.odp.spit.Case getCase33() {
		final de.semvox.types.odp.spit.Case case33 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition34 = getStringCondition34();
		case33.getSpitConditions().add(stringCondition34);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs35 = getGenerateStringAndOutputs35();
		case33.setSpitAction(generateStringAndOutputs35);
		return case33;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition34() {
		final de.semvox.types.odp.spit.StringCondition stringCondition34 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string24 = replaceEntities("lang");
		stringCondition34.setAlgVariableReference(string24);
		String string25 = replaceEntities("en-US");
		stringCondition34.setCommonStringValue(string25);
		return stringCondition34;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs35() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs35 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs35.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string26 = replaceEntities("kind of");
		generateStringAndOutputs35.getSpitAlternatives().add(string26);
		return generateStringAndOutputs35;
	}

	de.semvox.types.odp.spit.Case getCase36() {
		final de.semvox.types.odp.spit.Case case36 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition37 = getStringCondition37();
		case36.getSpitConditions().add(stringCondition37);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs38 = getGenerateStringAndOutputs38();
		case36.setSpitAction(generateStringAndOutputs38);
		return case36;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition37() {
		final de.semvox.types.odp.spit.StringCondition stringCondition37 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string27 = replaceEntities("lang");
		stringCondition37.setAlgVariableReference(string27);
		String string28 = replaceEntities("nl-BE");
		stringCondition37.setCommonStringValue(string28);
		return stringCondition37;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs38() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs38 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs38.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string29 = replaceEntities("");
		generateStringAndOutputs38.getSpitAlternatives().add(string29);
		return generateStringAndOutputs38;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext39() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext39 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext39.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext39.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext39.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext39.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext39.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext39.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext39;
	}

	com.zorabots.ontologies.vmn.Kinda getKinda40() {
		final com.zorabots.ontologies.vmn.Kinda kinda40 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newKinda();
		return kinda40;
	}

	de.semvox.types.odp.spit.Template getTemplate41() {
		final de.semvox.types.odp.spit.Template template41 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string30 = replaceEntities("Urgency_Very");
		template41.setCommonName(string30);
		de.semvox.types.odp.spit.Switch switch42 = getSwitch42();
		template41.setSpitAction(switch42);
		de.semvox.types.odp.interaction.GenerationContext generationContext49 = getGenerationContext49();
		template41.setSpitOnContext(generationContext49);
		com.zorabots.ontologies.vmn.Very very50 = getVery50();
		template41.setSpitOnInput(very50);
		return template41;
	}

	de.semvox.types.odp.spit.Switch getSwitch42() {
		final de.semvox.types.odp.spit.Switch switch42 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case43 = getCase43();
			switch42.getSpitCases().add(case43);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case46 = getCase46();
			switch42.getSpitCases().add(case46);
		}
		return switch42;
	}

	de.semvox.types.odp.spit.Case getCase43() {
		final de.semvox.types.odp.spit.Case case43 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition44 = getStringCondition44();
		case43.getSpitConditions().add(stringCondition44);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs45 = getGenerateStringAndOutputs45();
		case43.setSpitAction(generateStringAndOutputs45);
		return case43;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition44() {
		final de.semvox.types.odp.spit.StringCondition stringCondition44 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string31 = replaceEntities("lang");
		stringCondition44.setAlgVariableReference(string31);
		String string32 = replaceEntities("en-US");
		stringCondition44.setCommonStringValue(string32);
		return stringCondition44;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs45() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs45 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs45.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string33 = replaceEntities("very");
		generateStringAndOutputs45.getSpitAlternatives().add(string33);
		return generateStringAndOutputs45;
	}

	de.semvox.types.odp.spit.Case getCase46() {
		final de.semvox.types.odp.spit.Case case46 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition47 = getStringCondition47();
		case46.getSpitConditions().add(stringCondition47);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs48 = getGenerateStringAndOutputs48();
		case46.setSpitAction(generateStringAndOutputs48);
		return case46;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition47() {
		final de.semvox.types.odp.spit.StringCondition stringCondition47 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string34 = replaceEntities("lang");
		stringCondition47.setAlgVariableReference(string34);
		String string35 = replaceEntities("nl-BE");
		stringCondition47.setCommonStringValue(string35);
		return stringCondition47;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs48() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs48 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs48.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string36 = replaceEntities("dringend");
		generateStringAndOutputs48.getSpitAlternatives().add(string36);
		return generateStringAndOutputs48;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext49() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext49 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext49.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext49.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext49.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext49.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext49.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext49.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext49;
	}

	com.zorabots.ontologies.vmn.Very getVery50() {
		final com.zorabots.ontologies.vmn.Very very50 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newVery();
		return very50;
	}
}