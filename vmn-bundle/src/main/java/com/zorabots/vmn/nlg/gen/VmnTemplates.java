/**
* GENERATED CODE
*/
package com.zorabots.vmn.nlg.gen;

public final class VmnTemplates extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.spit.TemplateCollection> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "spit#TemplateCollection";

	public VmnTemplates(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public VmnTemplates(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public VmnTemplates(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public VmnTemplates() {
		super();
	}

	@Override
	protected de.semvox.types.odp.spit.TemplateCollection createThing() {
		return getTemplateCollection();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.spit.TemplateCollection templateCollection0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("VmnTemplates");
		templateCollection0.setCommonName(string0);
		de.semvox.subcon.data.Thing include1 = getThing1();
		templateCollection0.getSpitImports().add((de.semvox.types.odp.spit.TemplateCollection)include1);
		de.semvox.subcon.data.Thing include2 = getThing2();
		templateCollection0.getSpitImports().add((de.semvox.types.odp.spit.TemplateCollection)include2);
		de.semvox.subcon.data.Thing include3 = getThing3();
		templateCollection0.getSpitImports().add((de.semvox.types.odp.spit.TemplateCollection)include3);
		de.semvox.subcon.data.Thing include4 = getThing4();
		templateCollection0.getSpitImports().add((de.semvox.types.odp.spit.TemplateCollection)include4);
		de.semvox.types.odp.spit.Template template5 = getTemplate5();
		templateCollection0.getSpitElements().add(template5);
		de.semvox.types.odp.spit.Template template19 = getTemplate19();
		templateCollection0.getSpitElements().add(template19);
		de.semvox.types.odp.spit.Template template29 = getTemplate29();
		templateCollection0.getSpitElements().add(template29);
		de.semvox.types.odp.spit.Template template42 = getTemplate42();
		templateCollection0.getSpitElements().add(template42);
		de.semvox.types.odp.spit.Template template55 = getTemplate55();
		templateCollection0.getSpitElements().add(template55);
		de.semvox.types.odp.spit.Template template70 = getTemplate70();
		templateCollection0.getSpitElements().add(template70);
		de.semvox.types.odp.spit.Template template86 = getTemplate86();
		templateCollection0.getSpitElements().add(template86);
		de.semvox.types.odp.spit.Template template102 = getTemplate102();
		templateCollection0.getSpitElements().add(template102);
		de.semvox.types.odp.spit.Template template118 = getTemplate118();
		templateCollection0.getSpitElements().add(template118);
		de.semvox.types.odp.spit.Template template130 = getTemplate130();
		templateCollection0.getSpitElements().add(template130);
		de.semvox.types.odp.spit.Template template145 = getTemplate145();
		templateCollection0.getSpitElements().add(template145);
		de.semvox.types.odp.spit.Template template161 = getTemplate161();
		templateCollection0.getSpitElements().add(template161);
		de.semvox.types.odp.spit.Template template177 = getTemplate177();
		templateCollection0.getSpitElements().add(template177);
		de.semvox.types.odp.spit.Template template193 = getTemplate193();
		templateCollection0.getSpitElements().add(template193);
		de.semvox.types.odp.spit.Template template205 = getTemplate205();
		templateCollection0.getSpitElements().add(template205);
		de.semvox.types.odp.spit.Template template220 = getTemplate220();
		templateCollection0.getSpitElements().add(template220);
		de.semvox.types.odp.spit.Template template236 = getTemplate236();
		templateCollection0.getSpitElements().add(template236);
		de.semvox.types.odp.spit.Template template252 = getTemplate252();
		templateCollection0.getSpitElements().add(template252);
		de.semvox.types.odp.spit.Template template268 = getTemplate268();
		templateCollection0.getSpitElements().add(template268);
		de.semvox.types.odp.spit.Template template280 = getTemplate280();
		templateCollection0.getSpitElements().add(template280);
		de.semvox.types.odp.spit.Template template294 = getTemplate294();
		templateCollection0.getSpitElements().add(template294);
		de.semvox.types.odp.spit.Template template308 = getTemplate308();
		templateCollection0.getSpitElements().add(template308);
		de.semvox.types.odp.spit.Template template322 = getTemplate322();
		templateCollection0.getSpitElements().add(template322);
		de.semvox.types.odp.spit.Template template336 = getTemplate336();
		templateCollection0.getSpitElements().add(template336);
		de.semvox.types.odp.spit.Template template350 = getTemplate350();
		templateCollection0.getSpitElements().add(template350);
		de.semvox.types.odp.spit.Template template364 = getTemplate364();
		templateCollection0.getSpitElements().add(template364);
		de.semvox.types.odp.spit.Template template378 = getTemplate378();
		templateCollection0.getSpitElements().add(template378);
		de.semvox.types.odp.spit.Template template392 = getTemplate392();
		templateCollection0.getSpitElements().add(template392);
	}

	de.semvox.types.odp.spit.TemplateCollection getTemplateCollection() {
		final de.semvox.types.odp.spit.TemplateCollection templateCollection0 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplateCollection();
		return templateCollection0;
	}


	de.semvox.subcon.data.Thing getThing1() {
		java.util.Map<java.lang.String, java.lang.String> newEntities = new java.util.HashMap<java.lang.String, java.lang.String>(entities);
		de.semvox.subcon.data.Thing include1 = new com.zorabots.vmn.nlg.gen.VmnEventTemplates(getParameterProvider(), newEntities).loadThing(false);
		return include1;
	}

	de.semvox.subcon.data.Thing getThing2() {
		java.util.Map<java.lang.String, java.lang.String> newEntities = new java.util.HashMap<java.lang.String, java.lang.String>(entities);
		de.semvox.subcon.data.Thing include2 = new com.zorabots.vmn.nlg.gen.VmnUrgencyTemplates(getParameterProvider(), newEntities).loadThing(false);
		return include2;
	}

	de.semvox.subcon.data.Thing getThing3() {
		java.util.Map<java.lang.String, java.lang.String> newEntities = new java.util.HashMap<java.lang.String, java.lang.String>(entities);
		de.semvox.subcon.data.Thing include3 = new com.zorabots.vmn.nlg.gen.VmnYesNoTemplates(getParameterProvider(), newEntities).loadThing(false);
		return include3;
	}

	de.semvox.subcon.data.Thing getThing4() {
		java.util.Map<java.lang.String, java.lang.String> newEntities = new java.util.HashMap<java.lang.String, java.lang.String>(entities);
		de.semvox.subcon.data.Thing include4 = new com.zorabots.vmn.nlg.gen.VmnUnitTemplates(getParameterProvider(), newEntities).loadThing(false);
		return include4;
	}

	de.semvox.types.odp.spit.Template getTemplate5() {
		final de.semvox.types.odp.spit.Template template5 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string1 = replaceEntities("ReportOnTask_getHi");
		template5.setCommonName(string1);
		de.semvox.types.odp.spit.Switch switch6 = getSwitch6();
		template5.setSpitAction(switch6);
		de.semvox.types.odp.interaction.GenerationContext generationContext13 = getGenerationContext13();
		template5.setSpitOnContext(generationContext13);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask14 = getReportOnTask14();
		template5.setSpitOnInput(reportOnTask14);
		return template5;
	}

	de.semvox.types.odp.spit.Switch getSwitch6() {
		final de.semvox.types.odp.spit.Switch switch6 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case7 = getCase7();
			switch6.getSpitCases().add(case7);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case10 = getCase10();
			switch6.getSpitCases().add(case10);
		}
		return switch6;
	}

	de.semvox.types.odp.spit.Case getCase7() {
		final de.semvox.types.odp.spit.Case case7 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition8 = getStringCondition8();
		case7.getSpitConditions().add(stringCondition8);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs9 = getGenerateStringAndOutputs9();
		case7.setSpitAction(generateStringAndOutputs9);
		return case7;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition8() {
		final de.semvox.types.odp.spit.StringCondition stringCondition8 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string2 = replaceEntities("lang");
		stringCondition8.setAlgVariableReference(string2);
		String string3 = replaceEntities("en-US");
		stringCondition8.setCommonStringValue(string3);
		return stringCondition8;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs9() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs9 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs9.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string4 = replaceEntities("hi ?($NAME)");
		generateStringAndOutputs9.getSpitAlternatives().add(string4);
		return generateStringAndOutputs9;
	}

	de.semvox.types.odp.spit.Case getCase10() {
		final de.semvox.types.odp.spit.Case case10 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition11 = getStringCondition11();
		case10.getSpitConditions().add(stringCondition11);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs12 = getGenerateStringAndOutputs12();
		case10.setSpitAction(generateStringAndOutputs12);
		return case10;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition11() {
		final de.semvox.types.odp.spit.StringCondition stringCondition11 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string5 = replaceEntities("lang");
		stringCondition11.setAlgVariableReference(string5);
		String string6 = replaceEntities("nl-BE");
		stringCondition11.setCommonStringValue(string6);
		return stringCondition11;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs12() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs12 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs12.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string7 = replaceEntities("dag ?($NAME)");
		generateStringAndOutputs12.getSpitAlternatives().add(string7);
		return generateStringAndOutputs12;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext13() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext13 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext13.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext13.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext13.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext13.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext13.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext13.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext13;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask14() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask14 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released15 = getReleased15();
		reportOnTask14.setMmTaskStatus(released15);
		return reportOnTask14;
	}

	de.semvox.types.odp.multimodal.Released getReleased15() {
		final de.semvox.types.odp.multimodal.Released released15 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.GetHi getHi16 = getGetHi16();
		released15.setMmTaskExecutionResult(getHi16);
		return released15;
	}

	com.zorabots.ontologies.vmn.GetHi getGetHi16() {
		final com.zorabots.ontologies.vmn.GetHi getHi16 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetHi();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess17 = getStatusSuccess17();
		getHi16.setIntResponseStatus(statusSuccess17);
		com.zorabots.ontologies.vmn.User user18 = getUser18();
		getHi16.setVmnUser(user18);
		return getHi16;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess17() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess17 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess17;
	}

	com.zorabots.ontologies.vmn.User getUser18() {
		final com.zorabots.ontologies.vmn.User user18 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newUser();
		user18.markVmnNameAsVariable("NAME", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		user18.assignVmnNamePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return user18;
	}

	de.semvox.types.odp.spit.Template getTemplate19() {
		final de.semvox.types.odp.spit.Template template19 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string8 = replaceEntities("Repeat");
		template19.setCommonName(string8);
		de.semvox.types.odp.spit.Switch switch20 = getSwitch20();
		template19.setSpitAction(switch20);
		de.semvox.types.odp.interaction.GenerationContext generationContext27 = getGenerationContext27();
		template19.setSpitOnContext(generationContext27);
		de.semvox.types.odp.interaction.Repeat repeat28 = getRepeat28();
		template19.setSpitOnInput(repeat28);
		return template19;
	}

	de.semvox.types.odp.spit.Switch getSwitch20() {
		final de.semvox.types.odp.spit.Switch switch20 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case21 = getCase21();
			switch20.getSpitCases().add(case21);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case24 = getCase24();
			switch20.getSpitCases().add(case24);
		}
		return switch20;
	}

	de.semvox.types.odp.spit.Case getCase21() {
		final de.semvox.types.odp.spit.Case case21 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition22 = getStringCondition22();
		case21.getSpitConditions().add(stringCondition22);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs23 = getGenerateStringAndOutputs23();
		case21.setSpitAction(generateStringAndOutputs23);
		return case21;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition22() {
		final de.semvox.types.odp.spit.StringCondition stringCondition22 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string9 = replaceEntities("lang");
		stringCondition22.setAlgVariableReference(string9);
		String string10 = replaceEntities("en-US");
		stringCondition22.setCommonStringValue(string10);
		return stringCondition22;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs23() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs23 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs23.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string11 = replaceEntities("I said, !($LAST_OUTPUT)");
		generateStringAndOutputs23.getSpitAlternatives().add(string11);
		return generateStringAndOutputs23;
	}

	de.semvox.types.odp.spit.Case getCase24() {
		final de.semvox.types.odp.spit.Case case24 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition25 = getStringCondition25();
		case24.getSpitConditions().add(stringCondition25);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs26 = getGenerateStringAndOutputs26();
		case24.setSpitAction(generateStringAndOutputs26);
		return case24;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition25() {
		final de.semvox.types.odp.spit.StringCondition stringCondition25 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string12 = replaceEntities("lang");
		stringCondition25.setAlgVariableReference(string12);
		String string13 = replaceEntities("nl-BE");
		stringCondition25.setCommonStringValue(string13);
		return stringCondition25;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs26() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs26 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs26.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string14 = replaceEntities("ik zei, !($LAST_OUTPUT)");
		generateStringAndOutputs26.getSpitAlternatives().add(string14);
		return generateStringAndOutputs26;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext27() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext27 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext27.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext27.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext27.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext27.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext27.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext27.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext27;
	}

	de.semvox.types.odp.interaction.Repeat getRepeat28() {
		final de.semvox.types.odp.interaction.Repeat repeat28 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRepeat();
		repeat28.markCommonContentAsVariable("LAST_OUTPUT", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		repeat28.assignCommonContentPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return repeat28;
	}

	de.semvox.types.odp.spit.Template getTemplate29() {
		final de.semvox.types.odp.spit.Template template29 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string15 = replaceEntities("ReportOnTask_getIntroduction");
		template29.setCommonName(string15);
		de.semvox.types.odp.spit.Switch switch30 = getSwitch30();
		template29.setSpitAction(switch30);
		de.semvox.types.odp.interaction.GenerationContext generationContext37 = getGenerationContext37();
		template29.setSpitOnContext(generationContext37);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask38 = getReportOnTask38();
		template29.setSpitOnInput(reportOnTask38);
		return template29;
	}

	de.semvox.types.odp.spit.Switch getSwitch30() {
		final de.semvox.types.odp.spit.Switch switch30 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case31 = getCase31();
			switch30.getSpitCases().add(case31);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case34 = getCase34();
			switch30.getSpitCases().add(case34);
		}
		return switch30;
	}

	de.semvox.types.odp.spit.Case getCase31() {
		final de.semvox.types.odp.spit.Case case31 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition32 = getStringCondition32();
		case31.getSpitConditions().add(stringCondition32);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs33 = getGenerateStringAndOutputs33();
		case31.setSpitAction(generateStringAndOutputs33);
		return case31;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition32() {
		final de.semvox.types.odp.spit.StringCondition stringCondition32 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string16 = replaceEntities("lang");
		stringCondition32.setAlgVariableReference(string16);
		String string17 = replaceEntities("en-US");
		stringCondition32.setCommonStringValue(string17);
		return stringCondition32;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs33() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs33 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs33.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string18 = replaceEntities("Hi, my name is Billy, I am a smart vase, I help to take care of plants as good as possible, the advice on plants I give is based on a big database, while the people take care of plants, i take care of the people using memos, news articles and weather forecasts. ");
		generateStringAndOutputs33.getSpitAlternatives().add(string18);
		return generateStringAndOutputs33;
	}

	de.semvox.types.odp.spit.Case getCase34() {
		final de.semvox.types.odp.spit.Case case34 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition35 = getStringCondition35();
		case34.getSpitConditions().add(stringCondition35);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs36 = getGenerateStringAndOutputs36();
		case34.setSpitAction(generateStringAndOutputs36);
		return case34;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition35() {
		final de.semvox.types.odp.spit.StringCondition stringCondition35 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string19 = replaceEntities("lang");
		stringCondition35.setAlgVariableReference(string19);
		String string20 = replaceEntities("nl-BE");
		stringCondition35.setCommonStringValue(string20);
		return stringCondition35;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs36() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs36 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs36.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string21 = replaceEntities("Hallo, Mijn naam is Billy, Ik ben een slimme vaas, Ik help om planten zo goed mogelijk te verzorgen, de raad die ik geef is gebaseerd op een uitgebreide planten database, Terwijl de mensen planten verzorgen, zorg ik ook voor de mensen, met behulp van memos, nieuwsberichten en weerberichten. ");
		generateStringAndOutputs36.getSpitAlternatives().add(string21);
		return generateStringAndOutputs36;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext37() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext37 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext37.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext37.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext37.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext37.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext37.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext37.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext37;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask38() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask38 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released39 = getReleased39();
		reportOnTask38.setMmTaskStatus(released39);
		return reportOnTask38;
	}

	de.semvox.types.odp.multimodal.Released getReleased39() {
		final de.semvox.types.odp.multimodal.Released released39 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.GetIntroduction getIntroduction40 = getGetIntroduction40();
		released39.setMmTaskExecutionResult(getIntroduction40);
		return released39;
	}

	com.zorabots.ontologies.vmn.GetIntroduction getGetIntroduction40() {
		final com.zorabots.ontologies.vmn.GetIntroduction getIntroduction40 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetIntroduction();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess41 = getStatusSuccess41();
		getIntroduction40.setIntResponseStatus(statusSuccess41);
		return getIntroduction40;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess41() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess41 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess41;
	}

	de.semvox.types.odp.spit.Template getTemplate42() {
		final de.semvox.types.odp.spit.Template template42 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string22 = replaceEntities("ReportOnTask_getLeaveAlone");
		template42.setCommonName(string22);
		de.semvox.types.odp.spit.Switch switch43 = getSwitch43();
		template42.setSpitAction(switch43);
		de.semvox.types.odp.interaction.GenerationContext generationContext50 = getGenerationContext50();
		template42.setSpitOnContext(generationContext50);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask51 = getReportOnTask51();
		template42.setSpitOnInput(reportOnTask51);
		return template42;
	}

	de.semvox.types.odp.spit.Switch getSwitch43() {
		final de.semvox.types.odp.spit.Switch switch43 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case44 = getCase44();
			switch43.getSpitCases().add(case44);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case47 = getCase47();
			switch43.getSpitCases().add(case47);
		}
		return switch43;
	}

	de.semvox.types.odp.spit.Case getCase44() {
		final de.semvox.types.odp.spit.Case case44 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition45 = getStringCondition45();
		case44.getSpitConditions().add(stringCondition45);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs46 = getGenerateStringAndOutputs46();
		case44.setSpitAction(generateStringAndOutputs46);
		return case44;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition45() {
		final de.semvox.types.odp.spit.StringCondition stringCondition45 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string23 = replaceEntities("lang");
		stringCondition45.setAlgVariableReference(string23);
		String string24 = replaceEntities("en-US");
		stringCondition45.setCommonStringValue(string24);
		return stringCondition45;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs46() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs46 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs46.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string25 = replaceEntities("Ok, if there is something wrong with the plant i will call you. ");
		generateStringAndOutputs46.getSpitAlternatives().add(string25);
		return generateStringAndOutputs46;
	}

	de.semvox.types.odp.spit.Case getCase47() {
		final de.semvox.types.odp.spit.Case case47 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition48 = getStringCondition48();
		case47.getSpitConditions().add(stringCondition48);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs49 = getGenerateStringAndOutputs49();
		case47.setSpitAction(generateStringAndOutputs49);
		return case47;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition48() {
		final de.semvox.types.odp.spit.StringCondition stringCondition48 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string26 = replaceEntities("lang");
		stringCondition48.setAlgVariableReference(string26);
		String string27 = replaceEntities("nl-BE");
		stringCondition48.setCommonStringValue(string27);
		return stringCondition48;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs49() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs49 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs49.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string28 = replaceEntities("Geen probleem, als er iets is met de plant roep ik wel. ");
		generateStringAndOutputs49.getSpitAlternatives().add(string28);
		return generateStringAndOutputs49;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext50() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext50 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext50.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext50.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext50.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext50.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext50.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext50.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext50;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask51() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask51 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released52 = getReleased52();
		reportOnTask51.setMmTaskStatus(released52);
		return reportOnTask51;
	}

	de.semvox.types.odp.multimodal.Released getReleased52() {
		final de.semvox.types.odp.multimodal.Released released52 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.GetLeaveAlone getLeaveAlone53 = getGetLeaveAlone53();
		released52.setMmTaskExecutionResult(getLeaveAlone53);
		return released52;
	}

	com.zorabots.ontologies.vmn.GetLeaveAlone getGetLeaveAlone53() {
		final com.zorabots.ontologies.vmn.GetLeaveAlone getLeaveAlone53 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetLeaveAlone();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess54 = getStatusSuccess54();
		getLeaveAlone53.setIntResponseStatus(statusSuccess54);
		return getLeaveAlone53;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess54() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess54 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess54;
	}

	de.semvox.types.odp.spit.Template getTemplate55() {
		final de.semvox.types.odp.spit.Template template55 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string29 = replaceEntities("ReportOnTask_getTemperature");
		template55.setCommonName(string29);
		de.semvox.types.odp.spit.Switch switch56 = getSwitch56();
		template55.setSpitAction(switch56);
		de.semvox.types.odp.interaction.GenerationContext generationContext63 = getGenerationContext63();
		template55.setSpitOnContext(generationContext63);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask64 = getReportOnTask64();
		template55.setSpitOnInput(reportOnTask64);
		return template55;
	}

	de.semvox.types.odp.spit.Switch getSwitch56() {
		final de.semvox.types.odp.spit.Switch switch56 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case57 = getCase57();
			switch56.getSpitCases().add(case57);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case60 = getCase60();
			switch56.getSpitCases().add(case60);
		}
		return switch56;
	}

	de.semvox.types.odp.spit.Case getCase57() {
		final de.semvox.types.odp.spit.Case case57 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition58 = getStringCondition58();
		case57.getSpitConditions().add(stringCondition58);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs59 = getGenerateStringAndOutputs59();
		case57.setSpitAction(generateStringAndOutputs59);
		return case57;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition58() {
		final de.semvox.types.odp.spit.StringCondition stringCondition58 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string30 = replaceEntities("lang");
		stringCondition58.setAlgVariableReference(string30);
		String string31 = replaceEntities("en-US");
		stringCondition58.setCommonStringValue(string31);
		return stringCondition58;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs59() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs59 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs59.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string32 = replaceEntities("?(currently) my temperature is ?(approximately) ?($TEMP) !($UNIT)");
		generateStringAndOutputs59.getSpitAlternatives().add(string32);
		return generateStringAndOutputs59;
	}

	de.semvox.types.odp.spit.Case getCase60() {
		final de.semvox.types.odp.spit.Case case60 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition61 = getStringCondition61();
		case60.getSpitConditions().add(stringCondition61);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs62 = getGenerateStringAndOutputs62();
		case60.setSpitAction(generateStringAndOutputs62);
		return case60;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition61() {
		final de.semvox.types.odp.spit.StringCondition stringCondition61 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string33 = replaceEntities("lang");
		stringCondition61.setAlgVariableReference(string33);
		String string34 = replaceEntities("nl-BE");
		stringCondition61.setCommonStringValue(string34);
		return stringCondition61;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs62() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs62 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs62.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string35 = replaceEntities("Mijn ?(huidige) temperatuur bedraagt ?($TEMP) !($UNIT)");
		generateStringAndOutputs62.getSpitAlternatives().add(string35);
		return generateStringAndOutputs62;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext63() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext63 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext63.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext63.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext63.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext63.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext63.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext63.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext63;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask64() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask64 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released65 = getReleased65();
		reportOnTask64.setMmTaskStatus(released65);
		return reportOnTask64;
	}

	de.semvox.types.odp.multimodal.Released getReleased65() {
		final de.semvox.types.odp.multimodal.Released released65 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.GetTemperature getTemperature66 = getGetTemperature66();
		released65.setMmTaskExecutionResult(getTemperature66);
		return released65;
	}

	com.zorabots.ontologies.vmn.GetTemperature getGetTemperature66() {
		final com.zorabots.ontologies.vmn.GetTemperature getTemperature66 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetTemperature();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess67 = getStatusSuccess67();
		getTemperature66.setIntResponseStatus(statusSuccess67);
		com.zorabots.ontologies.vmn.TemperatureReport temperatureReport68 = getTemperatureReport68();
		getTemperature66.setVmnTemperatureReport(temperatureReport68);
		return getTemperature66;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess67() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess67 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess67;
	}

	com.zorabots.ontologies.vmn.TemperatureReport getTemperatureReport68() {
		final com.zorabots.ontologies.vmn.TemperatureReport temperatureReport68 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperatureReport();
		com.zorabots.ontologies.vmn.Temperature temperature69 = getTemperature69();
		temperatureReport68.setVmnTemperature(temperature69);
		return temperatureReport68;
	}

	com.zorabots.ontologies.vmn.Temperature getTemperature69() {
		final com.zorabots.ontologies.vmn.Temperature temperature69 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperature();
		temperature69.markCommonStringValueAsVariable("TEMP", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		temperature69.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		temperature69.markVmnUnitAsVariable("UNIT", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		temperature69.assignVmnUnitPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return temperature69;
	}

	de.semvox.types.odp.spit.Template getTemplate70() {
		final de.semvox.types.odp.spit.Template template70 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string36 = replaceEntities("ReportOnTask_getTemperatureYesNo_Good");
		template70.setCommonName(string36);
		de.semvox.types.odp.spit.Switch switch71 = getSwitch71();
		template70.setSpitAction(switch71);
		de.semvox.types.odp.interaction.GenerationContext generationContext78 = getGenerationContext78();
		template70.setSpitOnContext(generationContext78);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask79 = getReportOnTask79();
		template70.setSpitOnInput(reportOnTask79);
		return template70;
	}

	de.semvox.types.odp.spit.Switch getSwitch71() {
		final de.semvox.types.odp.spit.Switch switch71 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case72 = getCase72();
			switch71.getSpitCases().add(case72);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case75 = getCase75();
			switch71.getSpitCases().add(case75);
		}
		return switch71;
	}

	de.semvox.types.odp.spit.Case getCase72() {
		final de.semvox.types.odp.spit.Case case72 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition73 = getStringCondition73();
		case72.getSpitConditions().add(stringCondition73);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs74 = getGenerateStringAndOutputs74();
		case72.setSpitAction(generateStringAndOutputs74);
		return case72;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition73() {
		final de.semvox.types.odp.spit.StringCondition stringCondition73 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string37 = replaceEntities("lang");
		stringCondition73.setAlgVariableReference(string37);
		String string38 = replaceEntities("en-US");
		stringCondition73.setCommonStringValue(string38);
		return stringCondition73;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs74() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs74 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs74.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string39 = replaceEntities("!($YESNO), I'm fine, thank you for asking");
		generateStringAndOutputs74.getSpitAlternatives().add(string39);
		String string40 = replaceEntities("!($YESNO), the temperature is ok in here, ?(don't you agree?)");
		generateStringAndOutputs74.getSpitAlternatives().add(string40);
		return generateStringAndOutputs74;
	}

	de.semvox.types.odp.spit.Case getCase75() {
		final de.semvox.types.odp.spit.Case case75 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition76 = getStringCondition76();
		case75.getSpitConditions().add(stringCondition76);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs77 = getGenerateStringAndOutputs77();
		case75.setSpitAction(generateStringAndOutputs77);
		return case75;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition76() {
		final de.semvox.types.odp.spit.StringCondition stringCondition76 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string41 = replaceEntities("lang");
		stringCondition76.setAlgVariableReference(string41);
		String string42 = replaceEntities("nl-BE");
		stringCondition76.setCommonStringValue(string42);
		return stringCondition76;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs77() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs77 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs77.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string43 = replaceEntities("!($YESNO), de temperatuur is hier (best ok|prima). ");
		generateStringAndOutputs77.getSpitAlternatives().add(string43);
		String string44 = replaceEntities("!($YESNO), de temperatuur is hier (best ok|prima). ?(Vind je ook niet?)");
		generateStringAndOutputs77.getSpitAlternatives().add(string44);
		return generateStringAndOutputs77;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext78() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext78 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext78.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext78.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext78.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext78.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext78.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext78.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext78;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask79() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask79 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released80 = getReleased80();
		reportOnTask79.setMmTaskStatus(released80);
		return reportOnTask79;
	}

	de.semvox.types.odp.multimodal.Released getReleased80() {
		final de.semvox.types.odp.multimodal.Released released80 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.YesNoTemperature yesNoTemperature81 = getYesNoTemperature81();
		released80.setMmTaskExecutionResult(yesNoTemperature81);
		return released80;
	}

	com.zorabots.ontologies.vmn.YesNoTemperature getYesNoTemperature81() {
		final com.zorabots.ontologies.vmn.YesNoTemperature yesNoTemperature81 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newYesNoTemperature();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess82 = getStatusSuccess82();
		yesNoTemperature81.setIntResponseStatus(statusSuccess82);
		yesNoTemperature81.markVmnYesNoAsVariable("YESNO", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		yesNoTemperature81.assignVmnYesNoPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.GoodAnswer goodAnswer83 = getGoodAnswer83();
		yesNoTemperature81.setVmnYesNoQuestionAnswer(goodAnswer83);
		com.zorabots.ontologies.vmn.TemperatureReport temperatureReport84 = getTemperatureReport84();
		yesNoTemperature81.setVmnTemperatureReport(temperatureReport84);
		return yesNoTemperature81;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess82() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess82 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess82;
	}

	com.zorabots.ontologies.vmn.GoodAnswer getGoodAnswer83() {
		final com.zorabots.ontologies.vmn.GoodAnswer goodAnswer83 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGoodAnswer();
		return goodAnswer83;
	}

	com.zorabots.ontologies.vmn.TemperatureReport getTemperatureReport84() {
		final com.zorabots.ontologies.vmn.TemperatureReport temperatureReport84 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperatureReport();
		com.zorabots.ontologies.vmn.Temperature temperature85 = getTemperature85();
		temperatureReport84.setVmnTemperature(temperature85);
		return temperatureReport84;
	}

	com.zorabots.ontologies.vmn.Temperature getTemperature85() {
		final com.zorabots.ontologies.vmn.Temperature temperature85 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperature();
		return temperature85;
	}

	de.semvox.types.odp.spit.Template getTemplate86() {
		final de.semvox.types.odp.spit.Template template86 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string45 = replaceEntities("ReportOnTask_getTemperatureYesNo_Pos");
		template86.setCommonName(string45);
		de.semvox.types.odp.spit.Switch switch87 = getSwitch87();
		template86.setSpitAction(switch87);
		de.semvox.types.odp.interaction.GenerationContext generationContext94 = getGenerationContext94();
		template86.setSpitOnContext(generationContext94);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask95 = getReportOnTask95();
		template86.setSpitOnInput(reportOnTask95);
		return template86;
	}

	de.semvox.types.odp.spit.Switch getSwitch87() {
		final de.semvox.types.odp.spit.Switch switch87 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case88 = getCase88();
			switch87.getSpitCases().add(case88);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case91 = getCase91();
			switch87.getSpitCases().add(case91);
		}
		return switch87;
	}

	de.semvox.types.odp.spit.Case getCase88() {
		final de.semvox.types.odp.spit.Case case88 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition89 = getStringCondition89();
		case88.getSpitConditions().add(stringCondition89);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs90 = getGenerateStringAndOutputs90();
		case88.setSpitAction(generateStringAndOutputs90);
		return case88;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition89() {
		final de.semvox.types.odp.spit.StringCondition stringCondition89 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string46 = replaceEntities("lang");
		stringCondition89.setAlgVariableReference(string46);
		String string47 = replaceEntities("en-US");
		stringCondition89.setCommonStringValue(string47);
		return stringCondition89;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs90() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs90 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs90.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string48 = replaceEntities("!($YESNO), it is warm out here. I want to cool down. ");
		generateStringAndOutputs90.getSpitAlternatives().add(string48);
		String string49 = replaceEntities("!($YESNO), it is hot, I need to cool down, Can you help me out?");
		generateStringAndOutputs90.getSpitAlternatives().add(string49);
		return generateStringAndOutputs90;
	}

	de.semvox.types.odp.spit.Case getCase91() {
		final de.semvox.types.odp.spit.Case case91 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition92 = getStringCondition92();
		case91.getSpitConditions().add(stringCondition92);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs93 = getGenerateStringAndOutputs93();
		case91.setSpitAction(generateStringAndOutputs93);
		return case91;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition92() {
		final de.semvox.types.odp.spit.StringCondition stringCondition92 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string50 = replaceEntities("lang");
		stringCondition92.setAlgVariableReference(string50);
		String string51 = replaceEntities("nl-BE");
		stringCondition92.setCommonStringValue(string51);
		return stringCondition92;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs93() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs93 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs93.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string52 = replaceEntities("!($YESNO), Ik heb het te warm. Kun je me op een koelere plaats zetten? ");
		generateStringAndOutputs93.getSpitAlternatives().add(string52);
		String string53 = replaceEntities("!($YESNO), het is hier onaangenaam warm. Ik moet afkoelen. Kun je me helpen? ");
		generateStringAndOutputs93.getSpitAlternatives().add(string53);
		return generateStringAndOutputs93;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext94() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext94 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext94.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext94.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext94.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext94.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext94.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext94.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext94;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask95() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask95 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released96 = getReleased96();
		reportOnTask95.setMmTaskStatus(released96);
		return reportOnTask95;
	}

	de.semvox.types.odp.multimodal.Released getReleased96() {
		final de.semvox.types.odp.multimodal.Released released96 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.YesNoTemperature yesNoTemperature97 = getYesNoTemperature97();
		released96.setMmTaskExecutionResult(yesNoTemperature97);
		return released96;
	}

	com.zorabots.ontologies.vmn.YesNoTemperature getYesNoTemperature97() {
		final com.zorabots.ontologies.vmn.YesNoTemperature yesNoTemperature97 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newYesNoTemperature();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess98 = getStatusSuccess98();
		yesNoTemperature97.setIntResponseStatus(statusSuccess98);
		com.zorabots.ontologies.vmn.PositiveBadAnswer positiveBadAnswer99 = getPositiveBadAnswer99();
		yesNoTemperature97.setVmnYesNoQuestionAnswer(positiveBadAnswer99);
		yesNoTemperature97.markVmnYesNoAsVariable("YESNO", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		yesNoTemperature97.assignVmnYesNoPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.TemperatureReport temperatureReport100 = getTemperatureReport100();
		yesNoTemperature97.setVmnTemperatureReport(temperatureReport100);
		return yesNoTemperature97;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess98() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess98 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess98;
	}

	com.zorabots.ontologies.vmn.PositiveBadAnswer getPositiveBadAnswer99() {
		final com.zorabots.ontologies.vmn.PositiveBadAnswer positiveBadAnswer99 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newPositiveBadAnswer();
		return positiveBadAnswer99;
	}

	com.zorabots.ontologies.vmn.TemperatureReport getTemperatureReport100() {
		final com.zorabots.ontologies.vmn.TemperatureReport temperatureReport100 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperatureReport();
		com.zorabots.ontologies.vmn.Temperature temperature101 = getTemperature101();
		temperatureReport100.setVmnTemperature(temperature101);
		return temperatureReport100;
	}

	com.zorabots.ontologies.vmn.Temperature getTemperature101() {
		final com.zorabots.ontologies.vmn.Temperature temperature101 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperature();
		return temperature101;
	}

	de.semvox.types.odp.spit.Template getTemplate102() {
		final de.semvox.types.odp.spit.Template template102 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string54 = replaceEntities("ReportOnTask_getTemperatureYesNo_Neg");
		template102.setCommonName(string54);
		de.semvox.types.odp.spit.Switch switch103 = getSwitch103();
		template102.setSpitAction(switch103);
		de.semvox.types.odp.interaction.GenerationContext generationContext110 = getGenerationContext110();
		template102.setSpitOnContext(generationContext110);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask111 = getReportOnTask111();
		template102.setSpitOnInput(reportOnTask111);
		return template102;
	}

	de.semvox.types.odp.spit.Switch getSwitch103() {
		final de.semvox.types.odp.spit.Switch switch103 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case104 = getCase104();
			switch103.getSpitCases().add(case104);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case107 = getCase107();
			switch103.getSpitCases().add(case107);
		}
		return switch103;
	}

	de.semvox.types.odp.spit.Case getCase104() {
		final de.semvox.types.odp.spit.Case case104 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition105 = getStringCondition105();
		case104.getSpitConditions().add(stringCondition105);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs106 = getGenerateStringAndOutputs106();
		case104.setSpitAction(generateStringAndOutputs106);
		return case104;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition105() {
		final de.semvox.types.odp.spit.StringCondition stringCondition105 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string55 = replaceEntities("lang");
		stringCondition105.setAlgVariableReference(string55);
		String string56 = replaceEntities("en-US");
		stringCondition105.setCommonStringValue(string56);
		return stringCondition105;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs106() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs106 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs106.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string57 = replaceEntities("!($YESNO), I'm cold. I need to be at a warmer place. ");
		generateStringAndOutputs106.getSpitAlternatives().add(string57);
		String string58 = replaceEntities("!($YESNO), It's cold out here. I should be somewhere warmer. ");
		generateStringAndOutputs106.getSpitAlternatives().add(string58);
		return generateStringAndOutputs106;
	}

	de.semvox.types.odp.spit.Case getCase107() {
		final de.semvox.types.odp.spit.Case case107 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition108 = getStringCondition108();
		case107.getSpitConditions().add(stringCondition108);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs109 = getGenerateStringAndOutputs109();
		case107.setSpitAction(generateStringAndOutputs109);
		return case107;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition108() {
		final de.semvox.types.odp.spit.StringCondition stringCondition108 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string59 = replaceEntities("lang");
		stringCondition108.setAlgVariableReference(string59);
		String string60 = replaceEntities("nl-BE");
		stringCondition108.setCommonStringValue(string60);
		return stringCondition108;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs109() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs109 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs109.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string61 = replaceEntities("!($YESNO), het is hier best koud.. vind je ook niet?.. je zet me best op een warmere plaats. ");
		generateStringAndOutputs109.getSpitAlternatives().add(string61);
		String string62 = replaceEntities("!($YESNO), ik heb het koud.. kan je me op een warmere plaats zetten? ");
		generateStringAndOutputs109.getSpitAlternatives().add(string62);
		return generateStringAndOutputs109;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext110() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext110 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext110.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext110.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext110.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext110.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext110.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext110.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext110;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask111() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask111 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released112 = getReleased112();
		reportOnTask111.setMmTaskStatus(released112);
		return reportOnTask111;
	}

	de.semvox.types.odp.multimodal.Released getReleased112() {
		final de.semvox.types.odp.multimodal.Released released112 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.YesNoTemperature yesNoTemperature113 = getYesNoTemperature113();
		released112.setMmTaskExecutionResult(yesNoTemperature113);
		return released112;
	}

	com.zorabots.ontologies.vmn.YesNoTemperature getYesNoTemperature113() {
		final com.zorabots.ontologies.vmn.YesNoTemperature yesNoTemperature113 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newYesNoTemperature();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess114 = getStatusSuccess114();
		yesNoTemperature113.setIntResponseStatus(statusSuccess114);
		com.zorabots.ontologies.vmn.NegativeBadAnswer negativeBadAnswer115 = getNegativeBadAnswer115();
		yesNoTemperature113.setVmnYesNoQuestionAnswer(negativeBadAnswer115);
		yesNoTemperature113.markVmnYesNoAsVariable("YESNO", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		yesNoTemperature113.assignVmnYesNoPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.TemperatureReport temperatureReport116 = getTemperatureReport116();
		yesNoTemperature113.setVmnTemperatureReport(temperatureReport116);
		return yesNoTemperature113;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess114() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess114 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess114;
	}

	com.zorabots.ontologies.vmn.NegativeBadAnswer getNegativeBadAnswer115() {
		final com.zorabots.ontologies.vmn.NegativeBadAnswer negativeBadAnswer115 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNegativeBadAnswer();
		return negativeBadAnswer115;
	}

	com.zorabots.ontologies.vmn.TemperatureReport getTemperatureReport116() {
		final com.zorabots.ontologies.vmn.TemperatureReport temperatureReport116 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperatureReport();
		com.zorabots.ontologies.vmn.Temperature temperature117 = getTemperature117();
		temperatureReport116.setVmnTemperature(temperature117);
		return temperatureReport116;
	}

	com.zorabots.ontologies.vmn.Temperature getTemperature117() {
		final com.zorabots.ontologies.vmn.Temperature temperature117 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperature();
		return temperature117;
	}

	de.semvox.types.odp.spit.Template getTemplate118() {
		final de.semvox.types.odp.spit.Template template118 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string63 = replaceEntities("ReportOnTask_StatusAccepted_GetWeather");
		template118.setCommonName(string63);
		de.semvox.types.odp.spit.Switch switch119 = getSwitch119();
		template118.setSpitAction(switch119);
		de.semvox.types.odp.interaction.GenerationContext generationContext126 = getGenerationContext126();
		template118.setSpitOnContext(generationContext126);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask127 = getReportOnTask127();
		template118.setSpitOnInput(reportOnTask127);
		return template118;
	}

	de.semvox.types.odp.spit.Switch getSwitch119() {
		final de.semvox.types.odp.spit.Switch switch119 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case120 = getCase120();
			switch119.getSpitCases().add(case120);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case123 = getCase123();
			switch119.getSpitCases().add(case123);
		}
		return switch119;
	}

	de.semvox.types.odp.spit.Case getCase120() {
		final de.semvox.types.odp.spit.Case case120 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition121 = getStringCondition121();
		case120.getSpitConditions().add(stringCondition121);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs122 = getGenerateStringAndOutputs122();
		case120.setSpitAction(generateStringAndOutputs122);
		return case120;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition121() {
		final de.semvox.types.odp.spit.StringCondition stringCondition121 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string64 = replaceEntities("lang");
		stringCondition121.setAlgVariableReference(string64);
		String string65 = replaceEntities("en-US");
		stringCondition121.setCommonStringValue(string65);
		return stringCondition121;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs122() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs122 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs122.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string66 = replaceEntities("");
		generateStringAndOutputs122.getSpitAlternatives().add(string66);
		return generateStringAndOutputs122;
	}

	de.semvox.types.odp.spit.Case getCase123() {
		final de.semvox.types.odp.spit.Case case123 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition124 = getStringCondition124();
		case123.getSpitConditions().add(stringCondition124);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs125 = getGenerateStringAndOutputs125();
		case123.setSpitAction(generateStringAndOutputs125);
		return case123;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition124() {
		final de.semvox.types.odp.spit.StringCondition stringCondition124 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string67 = replaceEntities("lang");
		stringCondition124.setAlgVariableReference(string67);
		String string68 = replaceEntities("nl-BE");
		stringCondition124.setCommonStringValue(string68);
		return stringCondition124;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs125() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs125 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs125.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string69 = replaceEntities("");
		generateStringAndOutputs125.getSpitAlternatives().add(string69);
		return generateStringAndOutputs125;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext126() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext126 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext126.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext126.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext126.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext126.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext126.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext126.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext126;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask127() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask127 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Accepted accepted128 = getAccepted128();
		reportOnTask127.setMmTaskStatus(accepted128);
		com.zorabots.ontologies.vmn.GetTemperature getTemperature129 = getGetTemperature129();
		reportOnTask127.setCommonContent(getTemperature129);
		return reportOnTask127;
	}

	de.semvox.types.odp.multimodal.Accepted getAccepted128() {
		final de.semvox.types.odp.multimodal.Accepted accepted128 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newAccepted();
		return accepted128;
	}

	com.zorabots.ontologies.vmn.GetTemperature getGetTemperature129() {
		final com.zorabots.ontologies.vmn.GetTemperature getTemperature129 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetTemperature();
		getTemperature129.markMmTaskIdAsVariable("taskId", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		getTemperature129.assignMmTaskIdPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return getTemperature129;
	}

	de.semvox.types.odp.spit.Template getTemplate130() {
		final de.semvox.types.odp.spit.Template template130 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string70 = replaceEntities("ReportOnTask_getHumidity");
		template130.setCommonName(string70);
		de.semvox.types.odp.spit.Switch switch131 = getSwitch131();
		template130.setSpitAction(switch131);
		de.semvox.types.odp.interaction.GenerationContext generationContext138 = getGenerationContext138();
		template130.setSpitOnContext(generationContext138);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask139 = getReportOnTask139();
		template130.setSpitOnInput(reportOnTask139);
		return template130;
	}

	de.semvox.types.odp.spit.Switch getSwitch131() {
		final de.semvox.types.odp.spit.Switch switch131 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case132 = getCase132();
			switch131.getSpitCases().add(case132);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case135 = getCase135();
			switch131.getSpitCases().add(case135);
		}
		return switch131;
	}

	de.semvox.types.odp.spit.Case getCase132() {
		final de.semvox.types.odp.spit.Case case132 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition133 = getStringCondition133();
		case132.getSpitConditions().add(stringCondition133);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs134 = getGenerateStringAndOutputs134();
		case132.setSpitAction(generateStringAndOutputs134);
		return case132;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition133() {
		final de.semvox.types.odp.spit.StringCondition stringCondition133 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string71 = replaceEntities("lang");
		stringCondition133.setAlgVariableReference(string71);
		String string72 = replaceEntities("en-US");
		stringCondition133.setCommonStringValue(string72);
		return stringCondition133;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs134() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs134 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs134.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string73 = replaceEntities("My soil has a humidity of ?($HUM) !($UNIT).");
		generateStringAndOutputs134.getSpitAlternatives().add(string73);
		return generateStringAndOutputs134;
	}

	de.semvox.types.odp.spit.Case getCase135() {
		final de.semvox.types.odp.spit.Case case135 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition136 = getStringCondition136();
		case135.getSpitConditions().add(stringCondition136);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs137 = getGenerateStringAndOutputs137();
		case135.setSpitAction(generateStringAndOutputs137);
		return case135;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition136() {
		final de.semvox.types.odp.spit.StringCondition stringCondition136 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string74 = replaceEntities("lang");
		stringCondition136.setAlgVariableReference(string74);
		String string75 = replaceEntities("nl-BE");
		stringCondition136.setCommonStringValue(string75);
		return stringCondition136;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs137() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs137 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs137.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string76 = replaceEntities("Mijn bodem heeft een vochtigheid van ?($HUM) !($UNIT).");
		generateStringAndOutputs137.getSpitAlternatives().add(string76);
		return generateStringAndOutputs137;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext138() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext138 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext138.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext138.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext138.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext138.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext138.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext138.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext138;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask139() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask139 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released140 = getReleased140();
		reportOnTask139.setMmTaskStatus(released140);
		return reportOnTask139;
	}

	de.semvox.types.odp.multimodal.Released getReleased140() {
		final de.semvox.types.odp.multimodal.Released released140 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.GetHumidity getHumidity141 = getGetHumidity141();
		released140.setMmTaskExecutionResult(getHumidity141);
		return released140;
	}

	com.zorabots.ontologies.vmn.GetHumidity getGetHumidity141() {
		final com.zorabots.ontologies.vmn.GetHumidity getHumidity141 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetHumidity();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess142 = getStatusSuccess142();
		getHumidity141.setIntResponseStatus(statusSuccess142);
		com.zorabots.ontologies.vmn.HumidityReport humidityReport143 = getHumidityReport143();
		getHumidity141.setVmnHumidityReport(humidityReport143);
		return getHumidity141;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess142() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess142 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess142;
	}

	com.zorabots.ontologies.vmn.HumidityReport getHumidityReport143() {
		final com.zorabots.ontologies.vmn.HumidityReport humidityReport143 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidityReport();
		com.zorabots.ontologies.vmn.Humidity humidity144 = getHumidity144();
		humidityReport143.setVmnHumidity(humidity144);
		return humidityReport143;
	}

	com.zorabots.ontologies.vmn.Humidity getHumidity144() {
		final com.zorabots.ontologies.vmn.Humidity humidity144 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidity();
		humidity144.markCommonStringValueAsVariable("HUM", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		humidity144.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		humidity144.markVmnUnitAsVariable("UNIT", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		humidity144.assignVmnUnitPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return humidity144;
	}

	de.semvox.types.odp.spit.Template getTemplate145() {
		final de.semvox.types.odp.spit.Template template145 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string77 = replaceEntities("ReportOnTask_getHumidityYesNo_Good");
		template145.setCommonName(string77);
		de.semvox.types.odp.spit.Switch switch146 = getSwitch146();
		template145.setSpitAction(switch146);
		de.semvox.types.odp.interaction.GenerationContext generationContext153 = getGenerationContext153();
		template145.setSpitOnContext(generationContext153);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask154 = getReportOnTask154();
		template145.setSpitOnInput(reportOnTask154);
		return template145;
	}

	de.semvox.types.odp.spit.Switch getSwitch146() {
		final de.semvox.types.odp.spit.Switch switch146 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case147 = getCase147();
			switch146.getSpitCases().add(case147);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case150 = getCase150();
			switch146.getSpitCases().add(case150);
		}
		return switch146;
	}

	de.semvox.types.odp.spit.Case getCase147() {
		final de.semvox.types.odp.spit.Case case147 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition148 = getStringCondition148();
		case147.getSpitConditions().add(stringCondition148);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs149 = getGenerateStringAndOutputs149();
		case147.setSpitAction(generateStringAndOutputs149);
		return case147;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition148() {
		final de.semvox.types.odp.spit.StringCondition stringCondition148 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string78 = replaceEntities("lang");
		stringCondition148.setAlgVariableReference(string78);
		String string79 = replaceEntities("en-US");
		stringCondition148.setCommonStringValue(string79);
		return stringCondition148;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs149() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs149 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs149.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string80 = replaceEntities("!($YESNO), the amount of water in my soil is good");
		generateStringAndOutputs149.getSpitAlternatives().add(string80);
		String string81 = replaceEntities("!($YESNO), I have enough water, thank you for asking, don't forget to drink water yourself, ?(water is very important for your body.)");
		generateStringAndOutputs149.getSpitAlternatives().add(string81);
		return generateStringAndOutputs149;
	}

	de.semvox.types.odp.spit.Case getCase150() {
		final de.semvox.types.odp.spit.Case case150 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition151 = getStringCondition151();
		case150.getSpitConditions().add(stringCondition151);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs152 = getGenerateStringAndOutputs152();
		case150.setSpitAction(generateStringAndOutputs152);
		return case150;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition151() {
		final de.semvox.types.odp.spit.StringCondition stringCondition151 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string82 = replaceEntities("lang");
		stringCondition151.setAlgVariableReference(string82);
		String string83 = replaceEntities("nl-BE");
		stringCondition151.setCommonStringValue(string83);
		return stringCondition151;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs152() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs152 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs152.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string84 = replaceEntities("!($YESNO), de hoeveelheid water die ik nu heb is ok");
		generateStringAndOutputs152.getSpitAlternatives().add(string84);
		String string85 = replaceEntities("!($YESNO), ik heb genoeg water, dank je, vergeet niet zelf genoeg te drinken, ?(genoeg drinken is belangrijk voor je lichaam.)");
		generateStringAndOutputs152.getSpitAlternatives().add(string85);
		return generateStringAndOutputs152;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext153() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext153 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext153.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext153.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext153.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext153.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext153.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext153.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext153;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask154() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask154 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released155 = getReleased155();
		reportOnTask154.setMmTaskStatus(released155);
		return reportOnTask154;
	}

	de.semvox.types.odp.multimodal.Released getReleased155() {
		final de.semvox.types.odp.multimodal.Released released155 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.YesNoHumidity yesNoHumidity156 = getYesNoHumidity156();
		released155.setMmTaskExecutionResult(yesNoHumidity156);
		return released155;
	}

	com.zorabots.ontologies.vmn.YesNoHumidity getYesNoHumidity156() {
		final com.zorabots.ontologies.vmn.YesNoHumidity yesNoHumidity156 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newYesNoHumidity();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess157 = getStatusSuccess157();
		yesNoHumidity156.setIntResponseStatus(statusSuccess157);
		yesNoHumidity156.markVmnYesNoAsVariable("YESNO", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		yesNoHumidity156.assignVmnYesNoPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.GoodAnswer goodAnswer158 = getGoodAnswer158();
		yesNoHumidity156.setVmnYesNoQuestionAnswer(goodAnswer158);
		com.zorabots.ontologies.vmn.HumidityReport humidityReport159 = getHumidityReport159();
		yesNoHumidity156.setVmnHumidityReport(humidityReport159);
		return yesNoHumidity156;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess157() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess157 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess157;
	}

	com.zorabots.ontologies.vmn.GoodAnswer getGoodAnswer158() {
		final com.zorabots.ontologies.vmn.GoodAnswer goodAnswer158 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGoodAnswer();
		return goodAnswer158;
	}

	com.zorabots.ontologies.vmn.HumidityReport getHumidityReport159() {
		final com.zorabots.ontologies.vmn.HumidityReport humidityReport159 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidityReport();
		com.zorabots.ontologies.vmn.Humidity humidity160 = getHumidity160();
		humidityReport159.setVmnHumidity(humidity160);
		return humidityReport159;
	}

	com.zorabots.ontologies.vmn.Humidity getHumidity160() {
		final com.zorabots.ontologies.vmn.Humidity humidity160 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidity();
		return humidity160;
	}

	de.semvox.types.odp.spit.Template getTemplate161() {
		final de.semvox.types.odp.spit.Template template161 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string86 = replaceEntities("ReportOnTask_getHumidityYesNo_Pos");
		template161.setCommonName(string86);
		de.semvox.types.odp.spit.Switch switch162 = getSwitch162();
		template161.setSpitAction(switch162);
		de.semvox.types.odp.interaction.GenerationContext generationContext169 = getGenerationContext169();
		template161.setSpitOnContext(generationContext169);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask170 = getReportOnTask170();
		template161.setSpitOnInput(reportOnTask170);
		return template161;
	}

	de.semvox.types.odp.spit.Switch getSwitch162() {
		final de.semvox.types.odp.spit.Switch switch162 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case163 = getCase163();
			switch162.getSpitCases().add(case163);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case166 = getCase166();
			switch162.getSpitCases().add(case166);
		}
		return switch162;
	}

	de.semvox.types.odp.spit.Case getCase163() {
		final de.semvox.types.odp.spit.Case case163 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition164 = getStringCondition164();
		case163.getSpitConditions().add(stringCondition164);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs165 = getGenerateStringAndOutputs165();
		case163.setSpitAction(generateStringAndOutputs165);
		return case163;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition164() {
		final de.semvox.types.odp.spit.StringCondition stringCondition164 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string87 = replaceEntities("lang");
		stringCondition164.setAlgVariableReference(string87);
		String string88 = replaceEntities("en-US");
		stringCondition164.setCommonStringValue(string88);
		return stringCondition164;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs165() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs165 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs165.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string89 = replaceEntities("!($YESNO), I'm almost drowning. Could you give me less water next time?");
		generateStringAndOutputs165.getSpitAlternatives().add(string89);
		String string90 = replaceEntities("!($YESNO), there is too much water in my soil. Please give me less water next time.");
		generateStringAndOutputs165.getSpitAlternatives().add(string90);
		return generateStringAndOutputs165;
	}

	de.semvox.types.odp.spit.Case getCase166() {
		final de.semvox.types.odp.spit.Case case166 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition167 = getStringCondition167();
		case166.getSpitConditions().add(stringCondition167);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs168 = getGenerateStringAndOutputs168();
		case166.setSpitAction(generateStringAndOutputs168);
		return case166;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition167() {
		final de.semvox.types.odp.spit.StringCondition stringCondition167 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string91 = replaceEntities("lang");
		stringCondition167.setAlgVariableReference(string91);
		String string92 = replaceEntities("nl-BE");
		stringCondition167.setCommonStringValue(string92);
		return stringCondition167;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs168() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs168 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs168.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string93 = replaceEntities("!($YESNO), ik verdrink bijna. Kun je me de volgende keer minder water geven?");
		generateStringAndOutputs168.getSpitAlternatives().add(string93);
		String string94 = replaceEntities("!($YESNO), je hebt me te veel water gegeven. Geef me de volgende keer minder water. ");
		generateStringAndOutputs168.getSpitAlternatives().add(string94);
		return generateStringAndOutputs168;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext169() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext169 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext169.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext169.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext169.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext169.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext169.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext169.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext169;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask170() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask170 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released171 = getReleased171();
		reportOnTask170.setMmTaskStatus(released171);
		return reportOnTask170;
	}

	de.semvox.types.odp.multimodal.Released getReleased171() {
		final de.semvox.types.odp.multimodal.Released released171 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.YesNoHumidity yesNoHumidity172 = getYesNoHumidity172();
		released171.setMmTaskExecutionResult(yesNoHumidity172);
		return released171;
	}

	com.zorabots.ontologies.vmn.YesNoHumidity getYesNoHumidity172() {
		final com.zorabots.ontologies.vmn.YesNoHumidity yesNoHumidity172 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newYesNoHumidity();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess173 = getStatusSuccess173();
		yesNoHumidity172.setIntResponseStatus(statusSuccess173);
		com.zorabots.ontologies.vmn.PositiveBadAnswer positiveBadAnswer174 = getPositiveBadAnswer174();
		yesNoHumidity172.setVmnYesNoQuestionAnswer(positiveBadAnswer174);
		yesNoHumidity172.markVmnYesNoAsVariable("YESNO", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		yesNoHumidity172.assignVmnYesNoPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.HumidityReport humidityReport175 = getHumidityReport175();
		yesNoHumidity172.setVmnHumidityReport(humidityReport175);
		return yesNoHumidity172;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess173() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess173 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess173;
	}

	com.zorabots.ontologies.vmn.PositiveBadAnswer getPositiveBadAnswer174() {
		final com.zorabots.ontologies.vmn.PositiveBadAnswer positiveBadAnswer174 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newPositiveBadAnswer();
		return positiveBadAnswer174;
	}

	com.zorabots.ontologies.vmn.HumidityReport getHumidityReport175() {
		final com.zorabots.ontologies.vmn.HumidityReport humidityReport175 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidityReport();
		com.zorabots.ontologies.vmn.Humidity humidity176 = getHumidity176();
		humidityReport175.setVmnHumidity(humidity176);
		return humidityReport175;
	}

	com.zorabots.ontologies.vmn.Humidity getHumidity176() {
		final com.zorabots.ontologies.vmn.Humidity humidity176 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidity();
		return humidity176;
	}

	de.semvox.types.odp.spit.Template getTemplate177() {
		final de.semvox.types.odp.spit.Template template177 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string95 = replaceEntities("ReportOnTask_getHumidityYesNo_Neg");
		template177.setCommonName(string95);
		de.semvox.types.odp.spit.Switch switch178 = getSwitch178();
		template177.setSpitAction(switch178);
		de.semvox.types.odp.interaction.GenerationContext generationContext185 = getGenerationContext185();
		template177.setSpitOnContext(generationContext185);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask186 = getReportOnTask186();
		template177.setSpitOnInput(reportOnTask186);
		return template177;
	}

	de.semvox.types.odp.spit.Switch getSwitch178() {
		final de.semvox.types.odp.spit.Switch switch178 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case179 = getCase179();
			switch178.getSpitCases().add(case179);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case182 = getCase182();
			switch178.getSpitCases().add(case182);
		}
		return switch178;
	}

	de.semvox.types.odp.spit.Case getCase179() {
		final de.semvox.types.odp.spit.Case case179 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition180 = getStringCondition180();
		case179.getSpitConditions().add(stringCondition180);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs181 = getGenerateStringAndOutputs181();
		case179.setSpitAction(generateStringAndOutputs181);
		return case179;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition180() {
		final de.semvox.types.odp.spit.StringCondition stringCondition180 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string96 = replaceEntities("lang");
		stringCondition180.setAlgVariableReference(string96);
		String string97 = replaceEntities("en-US");
		stringCondition180.setCommonStringValue(string97);
		return stringCondition180;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs181() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs181 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs181.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string98 = replaceEntities("!($YESNO), I'm thirsty, I need water. Can you give me water?(, please?)");
		generateStringAndOutputs181.getSpitAlternatives().add(string98);
		String string99 = replaceEntities("!($YESNO), my soil is too dry. Could you give me some water?");
		generateStringAndOutputs181.getSpitAlternatives().add(string99);
		return generateStringAndOutputs181;
	}

	de.semvox.types.odp.spit.Case getCase182() {
		final de.semvox.types.odp.spit.Case case182 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition183 = getStringCondition183();
		case182.getSpitConditions().add(stringCondition183);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs184 = getGenerateStringAndOutputs184();
		case182.setSpitAction(generateStringAndOutputs184);
		return case182;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition183() {
		final de.semvox.types.odp.spit.StringCondition stringCondition183 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string100 = replaceEntities("lang");
		stringCondition183.setAlgVariableReference(string100);
		String string101 = replaceEntities("nl-BE");
		stringCondition183.setCommonStringValue(string101);
		return stringCondition183;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs184() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs184 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs184.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string102 = replaceEntities("!($YESNO), ik heb dorst. Zou je me zo snel mogelijk water kunnen geven? ");
		generateStringAndOutputs184.getSpitAlternatives().add(string102);
		String string103 = replaceEntities("!($YESNO), Ik heb dringend meer water nodig. Kan je me water geven? ");
		generateStringAndOutputs184.getSpitAlternatives().add(string103);
		return generateStringAndOutputs184;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext185() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext185 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext185.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext185.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext185.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext185.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext185.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext185.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext185;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask186() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask186 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released187 = getReleased187();
		reportOnTask186.setMmTaskStatus(released187);
		return reportOnTask186;
	}

	de.semvox.types.odp.multimodal.Released getReleased187() {
		final de.semvox.types.odp.multimodal.Released released187 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.YesNoHumidity yesNoHumidity188 = getYesNoHumidity188();
		released187.setMmTaskExecutionResult(yesNoHumidity188);
		return released187;
	}

	com.zorabots.ontologies.vmn.YesNoHumidity getYesNoHumidity188() {
		final com.zorabots.ontologies.vmn.YesNoHumidity yesNoHumidity188 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newYesNoHumidity();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess189 = getStatusSuccess189();
		yesNoHumidity188.setIntResponseStatus(statusSuccess189);
		com.zorabots.ontologies.vmn.NegativeBadAnswer negativeBadAnswer190 = getNegativeBadAnswer190();
		yesNoHumidity188.setVmnYesNoQuestionAnswer(negativeBadAnswer190);
		yesNoHumidity188.markVmnYesNoAsVariable("YESNO", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		yesNoHumidity188.assignVmnYesNoPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.HumidityReport humidityReport191 = getHumidityReport191();
		yesNoHumidity188.setVmnHumidityReport(humidityReport191);
		return yesNoHumidity188;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess189() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess189 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess189;
	}

	com.zorabots.ontologies.vmn.NegativeBadAnswer getNegativeBadAnswer190() {
		final com.zorabots.ontologies.vmn.NegativeBadAnswer negativeBadAnswer190 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNegativeBadAnswer();
		return negativeBadAnswer190;
	}

	com.zorabots.ontologies.vmn.HumidityReport getHumidityReport191() {
		final com.zorabots.ontologies.vmn.HumidityReport humidityReport191 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidityReport();
		com.zorabots.ontologies.vmn.Humidity humidity192 = getHumidity192();
		humidityReport191.setVmnHumidity(humidity192);
		return humidityReport191;
	}

	com.zorabots.ontologies.vmn.Humidity getHumidity192() {
		final com.zorabots.ontologies.vmn.Humidity humidity192 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidity();
		return humidity192;
	}

	de.semvox.types.odp.spit.Template getTemplate193() {
		final de.semvox.types.odp.spit.Template template193 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string104 = replaceEntities("ReportOnTask_StatusAccepted_GetHumidity");
		template193.setCommonName(string104);
		de.semvox.types.odp.spit.Switch switch194 = getSwitch194();
		template193.setSpitAction(switch194);
		de.semvox.types.odp.interaction.GenerationContext generationContext201 = getGenerationContext201();
		template193.setSpitOnContext(generationContext201);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask202 = getReportOnTask202();
		template193.setSpitOnInput(reportOnTask202);
		return template193;
	}

	de.semvox.types.odp.spit.Switch getSwitch194() {
		final de.semvox.types.odp.spit.Switch switch194 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case195 = getCase195();
			switch194.getSpitCases().add(case195);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case198 = getCase198();
			switch194.getSpitCases().add(case198);
		}
		return switch194;
	}

	de.semvox.types.odp.spit.Case getCase195() {
		final de.semvox.types.odp.spit.Case case195 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition196 = getStringCondition196();
		case195.getSpitConditions().add(stringCondition196);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs197 = getGenerateStringAndOutputs197();
		case195.setSpitAction(generateStringAndOutputs197);
		return case195;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition196() {
		final de.semvox.types.odp.spit.StringCondition stringCondition196 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string105 = replaceEntities("lang");
		stringCondition196.setAlgVariableReference(string105);
		String string106 = replaceEntities("en-US");
		stringCondition196.setCommonStringValue(string106);
		return stringCondition196;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs197() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs197 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs197.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string107 = replaceEntities("");
		generateStringAndOutputs197.getSpitAlternatives().add(string107);
		return generateStringAndOutputs197;
	}

	de.semvox.types.odp.spit.Case getCase198() {
		final de.semvox.types.odp.spit.Case case198 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition199 = getStringCondition199();
		case198.getSpitConditions().add(stringCondition199);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs200 = getGenerateStringAndOutputs200();
		case198.setSpitAction(generateStringAndOutputs200);
		return case198;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition199() {
		final de.semvox.types.odp.spit.StringCondition stringCondition199 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string108 = replaceEntities("lang");
		stringCondition199.setAlgVariableReference(string108);
		String string109 = replaceEntities("nl-BE");
		stringCondition199.setCommonStringValue(string109);
		return stringCondition199;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs200() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs200 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs200.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string110 = replaceEntities("");
		generateStringAndOutputs200.getSpitAlternatives().add(string110);
		return generateStringAndOutputs200;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext201() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext201 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext201.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext201.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext201.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext201.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext201.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext201.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext201;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask202() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask202 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Accepted accepted203 = getAccepted203();
		reportOnTask202.setMmTaskStatus(accepted203);
		com.zorabots.ontologies.vmn.GetHumidity getHumidity204 = getGetHumidity204();
		reportOnTask202.setCommonContent(getHumidity204);
		return reportOnTask202;
	}

	de.semvox.types.odp.multimodal.Accepted getAccepted203() {
		final de.semvox.types.odp.multimodal.Accepted accepted203 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newAccepted();
		return accepted203;
	}

	com.zorabots.ontologies.vmn.GetHumidity getGetHumidity204() {
		final com.zorabots.ontologies.vmn.GetHumidity getHumidity204 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetHumidity();
		getHumidity204.markMmTaskIdAsVariable("taskId", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		getHumidity204.assignMmTaskIdPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return getHumidity204;
	}

	de.semvox.types.odp.spit.Template getTemplate205() {
		final de.semvox.types.odp.spit.Template template205 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string111 = replaceEntities("ReportOnTask_getLight");
		template205.setCommonName(string111);
		de.semvox.types.odp.spit.Switch switch206 = getSwitch206();
		template205.setSpitAction(switch206);
		de.semvox.types.odp.interaction.GenerationContext generationContext213 = getGenerationContext213();
		template205.setSpitOnContext(generationContext213);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask214 = getReportOnTask214();
		template205.setSpitOnInput(reportOnTask214);
		return template205;
	}

	de.semvox.types.odp.spit.Switch getSwitch206() {
		final de.semvox.types.odp.spit.Switch switch206 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case207 = getCase207();
			switch206.getSpitCases().add(case207);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case210 = getCase210();
			switch206.getSpitCases().add(case210);
		}
		return switch206;
	}

	de.semvox.types.odp.spit.Case getCase207() {
		final de.semvox.types.odp.spit.Case case207 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition208 = getStringCondition208();
		case207.getSpitConditions().add(stringCondition208);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs209 = getGenerateStringAndOutputs209();
		case207.setSpitAction(generateStringAndOutputs209);
		return case207;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition208() {
		final de.semvox.types.odp.spit.StringCondition stringCondition208 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string112 = replaceEntities("lang");
		stringCondition208.setAlgVariableReference(string112);
		String string113 = replaceEntities("en-US");
		stringCondition208.setCommonStringValue(string113);
		return stringCondition208;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs209() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs209 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs209.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string114 = replaceEntities("the ?(amount of )light i'm getting is ?($LIGHT) !($UNIT).");
		generateStringAndOutputs209.getSpitAlternatives().add(string114);
		return generateStringAndOutputs209;
	}

	de.semvox.types.odp.spit.Case getCase210() {
		final de.semvox.types.odp.spit.Case case210 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition211 = getStringCondition211();
		case210.getSpitConditions().add(stringCondition211);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs212 = getGenerateStringAndOutputs212();
		case210.setSpitAction(generateStringAndOutputs212);
		return case210;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition211() {
		final de.semvox.types.odp.spit.StringCondition stringCondition211 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string115 = replaceEntities("lang");
		stringCondition211.setAlgVariableReference(string115);
		String string116 = replaceEntities("nl-BE");
		stringCondition211.setCommonStringValue(string116);
		return stringCondition211;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs212() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs212 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs212.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string117 = replaceEntities("de licht intensiteit die ik nu opvang bedraagt ?($LIGHT) !($UNIT). ");
		generateStringAndOutputs212.getSpitAlternatives().add(string117);
		String string118 = replaceEntities("de licht hoeveelheid die ik hier opmeet is ?($LIGHT) !($UNIT). ");
		generateStringAndOutputs212.getSpitAlternatives().add(string118);
		return generateStringAndOutputs212;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext213() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext213 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext213.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext213.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext213.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext213.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext213.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext213.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext213;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask214() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask214 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released215 = getReleased215();
		reportOnTask214.setMmTaskStatus(released215);
		return reportOnTask214;
	}

	de.semvox.types.odp.multimodal.Released getReleased215() {
		final de.semvox.types.odp.multimodal.Released released215 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.GetLight getLight216 = getGetLight216();
		released215.setMmTaskExecutionResult(getLight216);
		return released215;
	}

	com.zorabots.ontologies.vmn.GetLight getGetLight216() {
		final com.zorabots.ontologies.vmn.GetLight getLight216 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetLight();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess217 = getStatusSuccess217();
		getLight216.setIntResponseStatus(statusSuccess217);
		com.zorabots.ontologies.vmn.LightReport lightReport218 = getLightReport218();
		getLight216.setVmnLightReport(lightReport218);
		return getLight216;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess217() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess217 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess217;
	}

	com.zorabots.ontologies.vmn.LightReport getLightReport218() {
		final com.zorabots.ontologies.vmn.LightReport lightReport218 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLightReport();
		com.zorabots.ontologies.vmn.Light light219 = getLight219();
		lightReport218.setVmnLight(light219);
		return lightReport218;
	}

	com.zorabots.ontologies.vmn.Light getLight219() {
		final com.zorabots.ontologies.vmn.Light light219 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLight();
		light219.markCommonStringValueAsVariable("LIGHT", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		light219.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		light219.markVmnUnitAsVariable("UNIT", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		light219.assignVmnUnitPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return light219;
	}

	de.semvox.types.odp.spit.Template getTemplate220() {
		final de.semvox.types.odp.spit.Template template220 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string119 = replaceEntities("ReportOnTask_getLightYesNo_Good");
		template220.setCommonName(string119);
		de.semvox.types.odp.spit.Switch switch221 = getSwitch221();
		template220.setSpitAction(switch221);
		de.semvox.types.odp.interaction.GenerationContext generationContext228 = getGenerationContext228();
		template220.setSpitOnContext(generationContext228);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask229 = getReportOnTask229();
		template220.setSpitOnInput(reportOnTask229);
		return template220;
	}

	de.semvox.types.odp.spit.Switch getSwitch221() {
		final de.semvox.types.odp.spit.Switch switch221 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case222 = getCase222();
			switch221.getSpitCases().add(case222);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case225 = getCase225();
			switch221.getSpitCases().add(case225);
		}
		return switch221;
	}

	de.semvox.types.odp.spit.Case getCase222() {
		final de.semvox.types.odp.spit.Case case222 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition223 = getStringCondition223();
		case222.getSpitConditions().add(stringCondition223);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs224 = getGenerateStringAndOutputs224();
		case222.setSpitAction(generateStringAndOutputs224);
		return case222;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition223() {
		final de.semvox.types.odp.spit.StringCondition stringCondition223 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string120 = replaceEntities("lang");
		stringCondition223.setAlgVariableReference(string120);
		String string121 = replaceEntities("en-US");
		stringCondition223.setCommonStringValue(string121);
		return stringCondition223;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs224() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs224 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs224.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string122 = replaceEntities("!($YESNO), the amount of light i'm getting is good?(, thank you for asking.) ");
		generateStringAndOutputs224.getSpitAlternatives().add(string122);
		String string123 = replaceEntities("!($YESNO), over here the amount of light is fine, don't worry");
		generateStringAndOutputs224.getSpitAlternatives().add(string123);
		return generateStringAndOutputs224;
	}

	de.semvox.types.odp.spit.Case getCase225() {
		final de.semvox.types.odp.spit.Case case225 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition226 = getStringCondition226();
		case225.getSpitConditions().add(stringCondition226);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs227 = getGenerateStringAndOutputs227();
		case225.setSpitAction(generateStringAndOutputs227);
		return case225;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition226() {
		final de.semvox.types.odp.spit.StringCondition stringCondition226 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string124 = replaceEntities("lang");
		stringCondition226.setAlgVariableReference(string124);
		String string125 = replaceEntities("nl-BE");
		stringCondition226.setCommonStringValue(string125);
		return stringCondition226;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs227() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs227 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs227.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string126 = replaceEntities("!($YESNO), de hoeveelheid licht is perfect. ?(Bedankt voor je bezorgdheid.) ");
		generateStringAndOutputs227.getSpitAlternatives().add(string126);
		String string127 = replaceEntities("!($YESNO), maak je geen zorgen, de licht hoeveelheid is prima hier. ");
		generateStringAndOutputs227.getSpitAlternatives().add(string127);
		return generateStringAndOutputs227;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext228() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext228 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext228.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext228.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext228.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext228.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext228.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext228.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext228;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask229() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask229 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released230 = getReleased230();
		reportOnTask229.setMmTaskStatus(released230);
		return reportOnTask229;
	}

	de.semvox.types.odp.multimodal.Released getReleased230() {
		final de.semvox.types.odp.multimodal.Released released230 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.YesNoLight yesNoLight231 = getYesNoLight231();
		released230.setMmTaskExecutionResult(yesNoLight231);
		return released230;
	}

	com.zorabots.ontologies.vmn.YesNoLight getYesNoLight231() {
		final com.zorabots.ontologies.vmn.YesNoLight yesNoLight231 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newYesNoLight();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess232 = getStatusSuccess232();
		yesNoLight231.setIntResponseStatus(statusSuccess232);
		yesNoLight231.markVmnYesNoAsVariable("YESNO", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		yesNoLight231.assignVmnYesNoPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.GoodAnswer goodAnswer233 = getGoodAnswer233();
		yesNoLight231.setVmnYesNoQuestionAnswer(goodAnswer233);
		com.zorabots.ontologies.vmn.LightReport lightReport234 = getLightReport234();
		yesNoLight231.setVmnLightReport(lightReport234);
		return yesNoLight231;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess232() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess232 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess232;
	}

	com.zorabots.ontologies.vmn.GoodAnswer getGoodAnswer233() {
		final com.zorabots.ontologies.vmn.GoodAnswer goodAnswer233 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGoodAnswer();
		return goodAnswer233;
	}

	com.zorabots.ontologies.vmn.LightReport getLightReport234() {
		final com.zorabots.ontologies.vmn.LightReport lightReport234 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLightReport();
		com.zorabots.ontologies.vmn.Light light235 = getLight235();
		lightReport234.setVmnLight(light235);
		return lightReport234;
	}

	com.zorabots.ontologies.vmn.Light getLight235() {
		final com.zorabots.ontologies.vmn.Light light235 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLight();
		return light235;
	}

	de.semvox.types.odp.spit.Template getTemplate236() {
		final de.semvox.types.odp.spit.Template template236 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string128 = replaceEntities("ReportOnTask_getLightYesNo_Pos");
		template236.setCommonName(string128);
		de.semvox.types.odp.spit.Switch switch237 = getSwitch237();
		template236.setSpitAction(switch237);
		de.semvox.types.odp.interaction.GenerationContext generationContext244 = getGenerationContext244();
		template236.setSpitOnContext(generationContext244);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask245 = getReportOnTask245();
		template236.setSpitOnInput(reportOnTask245);
		return template236;
	}

	de.semvox.types.odp.spit.Switch getSwitch237() {
		final de.semvox.types.odp.spit.Switch switch237 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case238 = getCase238();
			switch237.getSpitCases().add(case238);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case241 = getCase241();
			switch237.getSpitCases().add(case241);
		}
		return switch237;
	}

	de.semvox.types.odp.spit.Case getCase238() {
		final de.semvox.types.odp.spit.Case case238 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition239 = getStringCondition239();
		case238.getSpitConditions().add(stringCondition239);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs240 = getGenerateStringAndOutputs240();
		case238.setSpitAction(generateStringAndOutputs240);
		return case238;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition239() {
		final de.semvox.types.odp.spit.StringCondition stringCondition239 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string129 = replaceEntities("lang");
		stringCondition239.setAlgVariableReference(string129);
		String string130 = replaceEntities("en-US");
		stringCondition239.setCommonStringValue(string130);
		return stringCondition239;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs240() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs240 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs240.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string131 = replaceEntities("!($YESNO), I need to be at a darker place. Can you help me out?");
		generateStringAndOutputs240.getSpitAlternatives().add(string131);
		String string132 = replaceEntities("!($YESNO), it is too bright out here. Can you move me to a darker place?");
		generateStringAndOutputs240.getSpitAlternatives().add(string132);
		return generateStringAndOutputs240;
	}

	de.semvox.types.odp.spit.Case getCase241() {
		final de.semvox.types.odp.spit.Case case241 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition242 = getStringCondition242();
		case241.getSpitConditions().add(stringCondition242);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs243 = getGenerateStringAndOutputs243();
		case241.setSpitAction(generateStringAndOutputs243);
		return case241;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition242() {
		final de.semvox.types.odp.spit.StringCondition stringCondition242 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string133 = replaceEntities("lang");
		stringCondition242.setAlgVariableReference(string133);
		String string134 = replaceEntities("nl-BE");
		stringCondition242.setCommonStringValue(string134);
		return stringCondition242;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs243() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs243 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs243.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string135 = replaceEntities("!($YESNO), ik zou op een meer donkere plaats moeten staan. Kun je me verplaatsen? ");
		generateStringAndOutputs243.getSpitAlternatives().add(string135);
		String string136 = replaceEntities("!($YESNO), het is hier te helder. Kun je me op een meer verduisterde plaats zetten?");
		generateStringAndOutputs243.getSpitAlternatives().add(string136);
		return generateStringAndOutputs243;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext244() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext244 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext244.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext244.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext244.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext244.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext244.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext244.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext244;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask245() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask245 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released246 = getReleased246();
		reportOnTask245.setMmTaskStatus(released246);
		return reportOnTask245;
	}

	de.semvox.types.odp.multimodal.Released getReleased246() {
		final de.semvox.types.odp.multimodal.Released released246 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.YesNoLight yesNoLight247 = getYesNoLight247();
		released246.setMmTaskExecutionResult(yesNoLight247);
		return released246;
	}

	com.zorabots.ontologies.vmn.YesNoLight getYesNoLight247() {
		final com.zorabots.ontologies.vmn.YesNoLight yesNoLight247 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newYesNoLight();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess248 = getStatusSuccess248();
		yesNoLight247.setIntResponseStatus(statusSuccess248);
		com.zorabots.ontologies.vmn.PositiveBadAnswer positiveBadAnswer249 = getPositiveBadAnswer249();
		yesNoLight247.setVmnYesNoQuestionAnswer(positiveBadAnswer249);
		yesNoLight247.markVmnYesNoAsVariable("YESNO", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		yesNoLight247.assignVmnYesNoPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.LightReport lightReport250 = getLightReport250();
		yesNoLight247.setVmnLightReport(lightReport250);
		return yesNoLight247;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess248() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess248 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess248;
	}

	com.zorabots.ontologies.vmn.PositiveBadAnswer getPositiveBadAnswer249() {
		final com.zorabots.ontologies.vmn.PositiveBadAnswer positiveBadAnswer249 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newPositiveBadAnswer();
		return positiveBadAnswer249;
	}

	com.zorabots.ontologies.vmn.LightReport getLightReport250() {
		final com.zorabots.ontologies.vmn.LightReport lightReport250 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLightReport();
		com.zorabots.ontologies.vmn.Light light251 = getLight251();
		lightReport250.setVmnLight(light251);
		return lightReport250;
	}

	com.zorabots.ontologies.vmn.Light getLight251() {
		final com.zorabots.ontologies.vmn.Light light251 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLight();
		return light251;
	}

	de.semvox.types.odp.spit.Template getTemplate252() {
		final de.semvox.types.odp.spit.Template template252 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string137 = replaceEntities("ReportOnTask_getLightYesNo_Neg");
		template252.setCommonName(string137);
		de.semvox.types.odp.spit.Switch switch253 = getSwitch253();
		template252.setSpitAction(switch253);
		de.semvox.types.odp.interaction.GenerationContext generationContext260 = getGenerationContext260();
		template252.setSpitOnContext(generationContext260);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask261 = getReportOnTask261();
		template252.setSpitOnInput(reportOnTask261);
		return template252;
	}

	de.semvox.types.odp.spit.Switch getSwitch253() {
		final de.semvox.types.odp.spit.Switch switch253 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case254 = getCase254();
			switch253.getSpitCases().add(case254);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case257 = getCase257();
			switch253.getSpitCases().add(case257);
		}
		return switch253;
	}

	de.semvox.types.odp.spit.Case getCase254() {
		final de.semvox.types.odp.spit.Case case254 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition255 = getStringCondition255();
		case254.getSpitConditions().add(stringCondition255);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs256 = getGenerateStringAndOutputs256();
		case254.setSpitAction(generateStringAndOutputs256);
		return case254;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition255() {
		final de.semvox.types.odp.spit.StringCondition stringCondition255 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string138 = replaceEntities("lang");
		stringCondition255.setAlgVariableReference(string138);
		String string139 = replaceEntities("en-US");
		stringCondition255.setCommonStringValue(string139);
		return stringCondition255;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs256() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs256 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs256.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string140 = replaceEntities("!($YESNO), I need to be at a brighter place. Can you help me out? ");
		generateStringAndOutputs256.getSpitAlternatives().add(string140);
		String string141 = replaceEntities("!($YESNO), it is too dark here. Can you move me to a brighter place?");
		generateStringAndOutputs256.getSpitAlternatives().add(string141);
		return generateStringAndOutputs256;
	}

	de.semvox.types.odp.spit.Case getCase257() {
		final de.semvox.types.odp.spit.Case case257 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition258 = getStringCondition258();
		case257.getSpitConditions().add(stringCondition258);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs259 = getGenerateStringAndOutputs259();
		case257.setSpitAction(generateStringAndOutputs259);
		return case257;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition258() {
		final de.semvox.types.odp.spit.StringCondition stringCondition258 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string142 = replaceEntities("lang");
		stringCondition258.setAlgVariableReference(string142);
		String string143 = replaceEntities("nl-BE");
		stringCondition258.setCommonStringValue(string143);
		return stringCondition258;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs259() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs259 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs259.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string144 = replaceEntities("!($YESNO), ik zou op een meer heldere plaats moeten staan. Kun je me verplaatsen? ");
		generateStringAndOutputs259.getSpitAlternatives().add(string144);
		String string145 = replaceEntities("!($YESNO), het is hier te donker. Kun je me naar een meer heldere plek verplaatsen? ");
		generateStringAndOutputs259.getSpitAlternatives().add(string145);
		return generateStringAndOutputs259;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext260() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext260 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext260.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext260.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext260.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext260.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext260.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext260.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext260;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask261() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask261 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released262 = getReleased262();
		reportOnTask261.setMmTaskStatus(released262);
		return reportOnTask261;
	}

	de.semvox.types.odp.multimodal.Released getReleased262() {
		final de.semvox.types.odp.multimodal.Released released262 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.YesNoLight yesNoLight263 = getYesNoLight263();
		released262.setMmTaskExecutionResult(yesNoLight263);
		return released262;
	}

	com.zorabots.ontologies.vmn.YesNoLight getYesNoLight263() {
		final com.zorabots.ontologies.vmn.YesNoLight yesNoLight263 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newYesNoLight();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess264 = getStatusSuccess264();
		yesNoLight263.setIntResponseStatus(statusSuccess264);
		com.zorabots.ontologies.vmn.NegativeBadAnswer negativeBadAnswer265 = getNegativeBadAnswer265();
		yesNoLight263.setVmnYesNoQuestionAnswer(negativeBadAnswer265);
		yesNoLight263.markVmnYesNoAsVariable("YESNO", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		yesNoLight263.assignVmnYesNoPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.LightReport lightReport266 = getLightReport266();
		yesNoLight263.setVmnLightReport(lightReport266);
		return yesNoLight263;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess264() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess264 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess264;
	}

	com.zorabots.ontologies.vmn.NegativeBadAnswer getNegativeBadAnswer265() {
		final com.zorabots.ontologies.vmn.NegativeBadAnswer negativeBadAnswer265 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNegativeBadAnswer();
		return negativeBadAnswer265;
	}

	com.zorabots.ontologies.vmn.LightReport getLightReport266() {
		final com.zorabots.ontologies.vmn.LightReport lightReport266 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLightReport();
		com.zorabots.ontologies.vmn.Light light267 = getLight267();
		lightReport266.setVmnLight(light267);
		return lightReport266;
	}

	com.zorabots.ontologies.vmn.Light getLight267() {
		final com.zorabots.ontologies.vmn.Light light267 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLight();
		return light267;
	}

	de.semvox.types.odp.spit.Template getTemplate268() {
		final de.semvox.types.odp.spit.Template template268 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string146 = replaceEntities("ReportOnTask_StatusAccepted_GetLight");
		template268.setCommonName(string146);
		de.semvox.types.odp.spit.Switch switch269 = getSwitch269();
		template268.setSpitAction(switch269);
		de.semvox.types.odp.interaction.GenerationContext generationContext276 = getGenerationContext276();
		template268.setSpitOnContext(generationContext276);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask277 = getReportOnTask277();
		template268.setSpitOnInput(reportOnTask277);
		return template268;
	}

	de.semvox.types.odp.spit.Switch getSwitch269() {
		final de.semvox.types.odp.spit.Switch switch269 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case270 = getCase270();
			switch269.getSpitCases().add(case270);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case273 = getCase273();
			switch269.getSpitCases().add(case273);
		}
		return switch269;
	}

	de.semvox.types.odp.spit.Case getCase270() {
		final de.semvox.types.odp.spit.Case case270 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition271 = getStringCondition271();
		case270.getSpitConditions().add(stringCondition271);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs272 = getGenerateStringAndOutputs272();
		case270.setSpitAction(generateStringAndOutputs272);
		return case270;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition271() {
		final de.semvox.types.odp.spit.StringCondition stringCondition271 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string147 = replaceEntities("lang");
		stringCondition271.setAlgVariableReference(string147);
		String string148 = replaceEntities("en-US");
		stringCondition271.setCommonStringValue(string148);
		return stringCondition271;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs272() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs272 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs272.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string149 = replaceEntities("");
		generateStringAndOutputs272.getSpitAlternatives().add(string149);
		return generateStringAndOutputs272;
	}

	de.semvox.types.odp.spit.Case getCase273() {
		final de.semvox.types.odp.spit.Case case273 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition274 = getStringCondition274();
		case273.getSpitConditions().add(stringCondition274);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs275 = getGenerateStringAndOutputs275();
		case273.setSpitAction(generateStringAndOutputs275);
		return case273;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition274() {
		final de.semvox.types.odp.spit.StringCondition stringCondition274 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string150 = replaceEntities("lang");
		stringCondition274.setAlgVariableReference(string150);
		String string151 = replaceEntities("nl-BE");
		stringCondition274.setCommonStringValue(string151);
		return stringCondition274;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs275() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs275 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs275.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string152 = replaceEntities("");
		generateStringAndOutputs275.getSpitAlternatives().add(string152);
		return generateStringAndOutputs275;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext276() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext276 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext276.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext276.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext276.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext276.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext276.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext276.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext276;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask277() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask277 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Accepted accepted278 = getAccepted278();
		reportOnTask277.setMmTaskStatus(accepted278);
		com.zorabots.ontologies.vmn.GetLight getLight279 = getGetLight279();
		reportOnTask277.setCommonContent(getLight279);
		return reportOnTask277;
	}

	de.semvox.types.odp.multimodal.Accepted getAccepted278() {
		final de.semvox.types.odp.multimodal.Accepted accepted278 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newAccepted();
		return accepted278;
	}

	com.zorabots.ontologies.vmn.GetLight getGetLight279() {
		final com.zorabots.ontologies.vmn.GetLight getLight279 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetLight();
		getLight279.markMmTaskIdAsVariable("taskId", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		getLight279.assignMmTaskIdPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return getLight279;
	}

	de.semvox.types.odp.spit.Template getTemplate280() {
		final de.semvox.types.odp.spit.Template template280 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string153 = replaceEntities("ReportOnTask_getTime");
		template280.setCommonName(string153);
		de.semvox.types.odp.spit.Switch switch281 = getSwitch281();
		template280.setSpitAction(switch281);
		de.semvox.types.odp.interaction.GenerationContext generationContext288 = getGenerationContext288();
		template280.setSpitOnContext(generationContext288);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask289 = getReportOnTask289();
		template280.setSpitOnInput(reportOnTask289);
		return template280;
	}

	de.semvox.types.odp.spit.Switch getSwitch281() {
		final de.semvox.types.odp.spit.Switch switch281 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case282 = getCase282();
			switch281.getSpitCases().add(case282);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case285 = getCase285();
			switch281.getSpitCases().add(case285);
		}
		return switch281;
	}

	de.semvox.types.odp.spit.Case getCase282() {
		final de.semvox.types.odp.spit.Case case282 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition283 = getStringCondition283();
		case282.getSpitConditions().add(stringCondition283);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs284 = getGenerateStringAndOutputs284();
		case282.setSpitAction(generateStringAndOutputs284);
		return case282;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition283() {
		final de.semvox.types.odp.spit.StringCondition stringCondition283 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string154 = replaceEntities("lang");
		stringCondition283.setAlgVariableReference(string154);
		String string155 = replaceEntities("en-US");
		stringCondition283.setCommonStringValue(string155);
		return stringCondition283;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs284() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs284 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs284.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string156 = replaceEntities("Its ?($HOUR):?($MIN) .");
		generateStringAndOutputs284.getSpitAlternatives().add(string156);
		return generateStringAndOutputs284;
	}

	de.semvox.types.odp.spit.Case getCase285() {
		final de.semvox.types.odp.spit.Case case285 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition286 = getStringCondition286();
		case285.getSpitConditions().add(stringCondition286);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs287 = getGenerateStringAndOutputs287();
		case285.setSpitAction(generateStringAndOutputs287);
		return case285;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition286() {
		final de.semvox.types.odp.spit.StringCondition stringCondition286 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string157 = replaceEntities("lang");
		stringCondition286.setAlgVariableReference(string157);
		String string158 = replaceEntities("nl-BE");
		stringCondition286.setCommonStringValue(string158);
		return stringCondition286;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs287() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs287 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs287.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string159 = replaceEntities("Het is ?($HOUR) uur ?($MIN)");
		generateStringAndOutputs287.getSpitAlternatives().add(string159);
		return generateStringAndOutputs287;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext288() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext288 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext288.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext288.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext288.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext288.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext288.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext288.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext288;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask289() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask289 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released290 = getReleased290();
		reportOnTask289.setMmTaskStatus(released290);
		return reportOnTask289;
	}

	de.semvox.types.odp.multimodal.Released getReleased290() {
		final de.semvox.types.odp.multimodal.Released released290 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.GetTime getTime291 = getGetTime291();
		released290.setMmTaskExecutionResult(getTime291);
		return released290;
	}

	com.zorabots.ontologies.vmn.GetTime getGetTime291() {
		final com.zorabots.ontologies.vmn.GetTime getTime291 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetTime();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess292 = getStatusSuccess292();
		getTime291.setIntResponseStatus(statusSuccess292);
		de.semvox.types.time.Time time293 = getTime293();
		getTime291.setTimeTime(time293);
		return getTime291;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess292() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess292 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess292;
	}

	de.semvox.types.time.Time getTime293() {
		final de.semvox.types.time.Time time293 = de.semvox.types.time.factory.PatternFactoryTime.newTime();
		time293.markTimeHourAsVariable("HOUR", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		time293.assignTimeHourPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		time293.markTimeMinuteAsVariable("MIN", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		time293.assignTimeMinutePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		time293.markTimeSecondAsVariable("SEC", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		time293.assignTimeSecondPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return time293;
	}

	de.semvox.types.odp.spit.Template getTemplate294() {
		final de.semvox.types.odp.spit.Template template294 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string160 = replaceEntities("ReportOnTask_getDate");
		template294.setCommonName(string160);
		de.semvox.types.odp.spit.Switch switch295 = getSwitch295();
		template294.setSpitAction(switch295);
		de.semvox.types.odp.interaction.GenerationContext generationContext302 = getGenerationContext302();
		template294.setSpitOnContext(generationContext302);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask303 = getReportOnTask303();
		template294.setSpitOnInput(reportOnTask303);
		return template294;
	}

	de.semvox.types.odp.spit.Switch getSwitch295() {
		final de.semvox.types.odp.spit.Switch switch295 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case296 = getCase296();
			switch295.getSpitCases().add(case296);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case299 = getCase299();
			switch295.getSpitCases().add(case299);
		}
		return switch295;
	}

	de.semvox.types.odp.spit.Case getCase296() {
		final de.semvox.types.odp.spit.Case case296 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition297 = getStringCondition297();
		case296.getSpitConditions().add(stringCondition297);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs298 = getGenerateStringAndOutputs298();
		case296.setSpitAction(generateStringAndOutputs298);
		return case296;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition297() {
		final de.semvox.types.odp.spit.StringCondition stringCondition297 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string161 = replaceEntities("lang");
		stringCondition297.setAlgVariableReference(string161);
		String string162 = replaceEntities("en-US");
		stringCondition297.setCommonStringValue(string162);
		return stringCondition297;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs298() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs298 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs298.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string163 = replaceEntities("?(today its) ?($MONTH)/?($DAYOM)/?($YEAR). ");
		generateStringAndOutputs298.getSpitAlternatives().add(string163);
		return generateStringAndOutputs298;
	}

	de.semvox.types.odp.spit.Case getCase299() {
		final de.semvox.types.odp.spit.Case case299 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition300 = getStringCondition300();
		case299.getSpitConditions().add(stringCondition300);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs301 = getGenerateStringAndOutputs301();
		case299.setSpitAction(generateStringAndOutputs301);
		return case299;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition300() {
		final de.semvox.types.odp.spit.StringCondition stringCondition300 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string164 = replaceEntities("lang");
		stringCondition300.setAlgVariableReference(string164);
		String string165 = replaceEntities("nl-BE");
		stringCondition300.setCommonStringValue(string165);
		return stringCondition300;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs301() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs301 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs301.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string166 = replaceEntities("?(we zijn) ?($DAYOM)/?($MONTH)/?($YEAR). ");
		generateStringAndOutputs301.getSpitAlternatives().add(string166);
		return generateStringAndOutputs301;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext302() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext302 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext302.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext302.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext302.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext302.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext302.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext302.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext302;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask303() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask303 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released304 = getReleased304();
		reportOnTask303.setMmTaskStatus(released304);
		return reportOnTask303;
	}

	de.semvox.types.odp.multimodal.Released getReleased304() {
		final de.semvox.types.odp.multimodal.Released released304 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.GetDate getDate305 = getGetDate305();
		released304.setMmTaskExecutionResult(getDate305);
		return released304;
	}

	com.zorabots.ontologies.vmn.GetDate getGetDate305() {
		final com.zorabots.ontologies.vmn.GetDate getDate305 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetDate();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess306 = getStatusSuccess306();
		getDate305.setIntResponseStatus(statusSuccess306);
		de.semvox.types.time.Date date307 = getDate307();
		getDate305.setTimeDate(date307);
		return getDate305;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess306() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess306 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess306;
	}

	de.semvox.types.time.Date getDate307() {
		final de.semvox.types.time.Date date307 = de.semvox.types.time.factory.PatternFactoryTime.newDate();
		date307.markTimeDayOfMonthAsVariable("DAYOM", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		date307.assignTimeDayOfMonthPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		date307.markTimeMonthAsVariable("MONTH", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		date307.assignTimeMonthPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		date307.markTimeYearAsVariable("YEAR", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		date307.assignTimeYearPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return date307;
	}

	de.semvox.types.odp.spit.Template getTemplate308() {
		final de.semvox.types.odp.spit.Template template308 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string167 = replaceEntities("ReportOnTask_getIpAddress");
		template308.setCommonName(string167);
		de.semvox.types.odp.spit.Switch switch309 = getSwitch309();
		template308.setSpitAction(switch309);
		de.semvox.types.odp.interaction.GenerationContext generationContext316 = getGenerationContext316();
		template308.setSpitOnContext(generationContext316);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask317 = getReportOnTask317();
		template308.setSpitOnInput(reportOnTask317);
		return template308;
	}

	de.semvox.types.odp.spit.Switch getSwitch309() {
		final de.semvox.types.odp.spit.Switch switch309 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case310 = getCase310();
			switch309.getSpitCases().add(case310);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case313 = getCase313();
			switch309.getSpitCases().add(case313);
		}
		return switch309;
	}

	de.semvox.types.odp.spit.Case getCase310() {
		final de.semvox.types.odp.spit.Case case310 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition311 = getStringCondition311();
		case310.getSpitConditions().add(stringCondition311);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs312 = getGenerateStringAndOutputs312();
		case310.setSpitAction(generateStringAndOutputs312);
		return case310;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition311() {
		final de.semvox.types.odp.spit.StringCondition stringCondition311 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string168 = replaceEntities("lang");
		stringCondition311.setAlgVariableReference(string168);
		String string169 = replaceEntities("en-US");
		stringCondition311.setCommonStringValue(string169);
		return stringCondition311;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs312() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs312 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs312.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string170 = replaceEntities("My IP address is ?($IPADDR)");
		generateStringAndOutputs312.getSpitAlternatives().add(string170);
		return generateStringAndOutputs312;
	}

	de.semvox.types.odp.spit.Case getCase313() {
		final de.semvox.types.odp.spit.Case case313 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition314 = getStringCondition314();
		case313.getSpitConditions().add(stringCondition314);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs315 = getGenerateStringAndOutputs315();
		case313.setSpitAction(generateStringAndOutputs315);
		return case313;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition314() {
		final de.semvox.types.odp.spit.StringCondition stringCondition314 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string171 = replaceEntities("lang");
		stringCondition314.setAlgVariableReference(string171);
		String string172 = replaceEntities("nl-BE");
		stringCondition314.setCommonStringValue(string172);
		return stringCondition314;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs315() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs315 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs315.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string173 = replaceEntities("Mijn IP adres is ?($IPADDR)");
		generateStringAndOutputs315.getSpitAlternatives().add(string173);
		return generateStringAndOutputs315;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext316() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext316 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext316.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext316.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext316.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext316.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext316.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext316.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext316;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask317() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask317 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released318 = getReleased318();
		reportOnTask317.setMmTaskStatus(released318);
		return reportOnTask317;
	}

	de.semvox.types.odp.multimodal.Released getReleased318() {
		final de.semvox.types.odp.multimodal.Released released318 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.GetIpAddress getIpAddress319 = getGetIpAddress319();
		released318.setMmTaskExecutionResult(getIpAddress319);
		return released318;
	}

	com.zorabots.ontologies.vmn.GetIpAddress getGetIpAddress319() {
		final com.zorabots.ontologies.vmn.GetIpAddress getIpAddress319 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetIpAddress();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess320 = getStatusSuccess320();
		getIpAddress319.setIntResponseStatus(statusSuccess320);
		com.zorabots.ontologies.vmn.IpAddress ipAddress321 = getIpAddress321();
		getIpAddress319.setVmnIpAddress(ipAddress321);
		return getIpAddress319;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess320() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess320 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess320;
	}

	com.zorabots.ontologies.vmn.IpAddress getIpAddress321() {
		final com.zorabots.ontologies.vmn.IpAddress ipAddress321 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newIpAddress();
		ipAddress321.markCommonStringValueAsVariable("IPADDR", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		ipAddress321.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return ipAddress321;
	}

	de.semvox.types.odp.spit.Template getTemplate322() {
		final de.semvox.types.odp.spit.Template template322 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string174 = replaceEntities("ReportOnTask_getPlantName");
		template322.setCommonName(string174);
		de.semvox.types.odp.spit.Switch switch323 = getSwitch323();
		template322.setSpitAction(switch323);
		de.semvox.types.odp.interaction.GenerationContext generationContext330 = getGenerationContext330();
		template322.setSpitOnContext(generationContext330);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask331 = getReportOnTask331();
		template322.setSpitOnInput(reportOnTask331);
		return template322;
	}

	de.semvox.types.odp.spit.Switch getSwitch323() {
		final de.semvox.types.odp.spit.Switch switch323 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case324 = getCase324();
			switch323.getSpitCases().add(case324);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case327 = getCase327();
			switch323.getSpitCases().add(case327);
		}
		return switch323;
	}

	de.semvox.types.odp.spit.Case getCase324() {
		final de.semvox.types.odp.spit.Case case324 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition325 = getStringCondition325();
		case324.getSpitConditions().add(stringCondition325);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs326 = getGenerateStringAndOutputs326();
		case324.setSpitAction(generateStringAndOutputs326);
		return case324;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition325() {
		final de.semvox.types.odp.spit.StringCondition stringCondition325 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string175 = replaceEntities("lang");
		stringCondition325.setAlgVariableReference(string175);
		String string176 = replaceEntities("en-US");
		stringCondition325.setCommonStringValue(string176);
		return stringCondition325;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs326() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs326 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs326.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string177 = replaceEntities("Right now, I'm taking care of a ?($NAME)");
		generateStringAndOutputs326.getSpitAlternatives().add(string177);
		return generateStringAndOutputs326;
	}

	de.semvox.types.odp.spit.Case getCase327() {
		final de.semvox.types.odp.spit.Case case327 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition328 = getStringCondition328();
		case327.getSpitConditions().add(stringCondition328);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs329 = getGenerateStringAndOutputs329();
		case327.setSpitAction(generateStringAndOutputs329);
		return case327;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition328() {
		final de.semvox.types.odp.spit.StringCondition stringCondition328 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string178 = replaceEntities("lang");
		stringCondition328.setAlgVariableReference(string178);
		String string179 = replaceEntities("nl-BE");
		stringCondition328.setCommonStringValue(string179);
		return stringCondition328;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs329() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs329 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs329.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string180 = replaceEntities("Ik zorg nu voor een ?($NAME)");
		generateStringAndOutputs329.getSpitAlternatives().add(string180);
		return generateStringAndOutputs329;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext330() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext330 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext330.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext330.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext330.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext330.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext330.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext330.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext330;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask331() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask331 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released332 = getReleased332();
		reportOnTask331.setMmTaskStatus(released332);
		return reportOnTask331;
	}

	de.semvox.types.odp.multimodal.Released getReleased332() {
		final de.semvox.types.odp.multimodal.Released released332 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.GetPlantName getPlantName333 = getGetPlantName333();
		released332.setMmTaskExecutionResult(getPlantName333);
		return released332;
	}

	com.zorabots.ontologies.vmn.GetPlantName getGetPlantName333() {
		final com.zorabots.ontologies.vmn.GetPlantName getPlantName333 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetPlantName();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess334 = getStatusSuccess334();
		getPlantName333.setIntResponseStatus(statusSuccess334);
		com.zorabots.ontologies.vmn.Plant plant335 = getPlant335();
		getPlantName333.setVmnPlant(plant335);
		return getPlantName333;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess334() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess334 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess334;
	}

	com.zorabots.ontologies.vmn.Plant getPlant335() {
		final com.zorabots.ontologies.vmn.Plant plant335 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newPlant();
		plant335.markVmnPlantNameAsVariable("NAME", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant335.assignVmnPlantNamePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return plant335;
	}

	de.semvox.types.odp.spit.Template getTemplate336() {
		final de.semvox.types.odp.spit.Template template336 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string181 = replaceEntities("ReportOnTask_getParamters");
		template336.setCommonName(string181);
		de.semvox.types.odp.spit.Switch switch337 = getSwitch337();
		template336.setSpitAction(switch337);
		de.semvox.types.odp.interaction.GenerationContext generationContext344 = getGenerationContext344();
		template336.setSpitOnContext(generationContext344);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask345 = getReportOnTask345();
		template336.setSpitOnInput(reportOnTask345);
		return template336;
	}

	de.semvox.types.odp.spit.Switch getSwitch337() {
		final de.semvox.types.odp.spit.Switch switch337 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case338 = getCase338();
			switch337.getSpitCases().add(case338);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case341 = getCase341();
			switch337.getSpitCases().add(case341);
		}
		return switch337;
	}

	de.semvox.types.odp.spit.Case getCase338() {
		final de.semvox.types.odp.spit.Case case338 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition339 = getStringCondition339();
		case338.getSpitConditions().add(stringCondition339);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs340 = getGenerateStringAndOutputs340();
		case338.setSpitAction(generateStringAndOutputs340);
		return case338;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition339() {
		final de.semvox.types.odp.spit.StringCondition stringCondition339 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string182 = replaceEntities("lang");
		stringCondition339.setAlgVariableReference(string182);
		String string183 = replaceEntities("en-US");
		stringCondition339.setCommonStringValue(string183);
		return stringCondition339;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs340() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs340 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs340.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string184 = replaceEntities("(The name of my plant is ?($NAME).|My plantname is ?($NAME).) The optimal humidity is ?($HUMIDITY), the optimal light intensity is ?($LIGHT) and the optimal temperature is ?($TEMP). ");
		generateStringAndOutputs340.getSpitAlternatives().add(string184);
		return generateStringAndOutputs340;
	}

	de.semvox.types.odp.spit.Case getCase341() {
		final de.semvox.types.odp.spit.Case case341 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition342 = getStringCondition342();
		case341.getSpitConditions().add(stringCondition342);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs343 = getGenerateStringAndOutputs343();
		case341.setSpitAction(generateStringAndOutputs343);
		return case341;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition342() {
		final de.semvox.types.odp.spit.StringCondition stringCondition342 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string185 = replaceEntities("lang");
		stringCondition342.setAlgVariableReference(string185);
		String string186 = replaceEntities("nl-BE");
		stringCondition342.setCommonStringValue(string186);
		return stringCondition342;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs343() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs343 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs343.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string187 = replaceEntities("(Mijn plantnaam is ?($NAME).|De naam van de plant die nu is ingevoegd is ?($NAME).) De optimale vochtigheidsgraad is ?($HUMIDITY), de optimale licht intensiteit is ?($LIGHT) en de optimale temperatuur is ?($TEMP). ");
		generateStringAndOutputs343.getSpitAlternatives().add(string187);
		return generateStringAndOutputs343;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext344() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext344 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext344.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext344.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext344.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext344.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext344.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext344.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext344;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask345() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask345 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released346 = getReleased346();
		reportOnTask345.setMmTaskStatus(released346);
		return reportOnTask345;
	}

	de.semvox.types.odp.multimodal.Released getReleased346() {
		final de.semvox.types.odp.multimodal.Released released346 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.GetParameters getParameters347 = getGetParameters347();
		released346.setMmTaskExecutionResult(getParameters347);
		return released346;
	}

	com.zorabots.ontologies.vmn.GetParameters getGetParameters347() {
		final com.zorabots.ontologies.vmn.GetParameters getParameters347 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetParameters();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess348 = getStatusSuccess348();
		getParameters347.setIntResponseStatus(statusSuccess348);
		com.zorabots.ontologies.vmn.Plant plant349 = getPlant349();
		getParameters347.setVmnPlant(plant349);
		return getParameters347;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess348() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess348 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess348;
	}

	com.zorabots.ontologies.vmn.Plant getPlant349() {
		final com.zorabots.ontologies.vmn.Plant plant349 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newPlant();
		plant349.markVmnPlantNameAsVariable("NAME", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant349.assignVmnPlantNamePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		plant349.markVmnOptimalHumidityAsVariable("HUMIDITY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant349.assignVmnOptimalHumidityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		plant349.markVmnOptimalLightAsVariable("LIGHT", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant349.assignVmnOptimalLightPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		plant349.markVmnOptimalTemperatureAsVariable("TEMP", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant349.assignVmnOptimalTemperaturePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return plant349;
	}

	de.semvox.types.odp.spit.Template getTemplate350() {
		final de.semvox.types.odp.spit.Template template350 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string188 = replaceEntities("ReportOnTask_getOptimalPlantHumidity");
		template350.setCommonName(string188);
		de.semvox.types.odp.spit.Switch switch351 = getSwitch351();
		template350.setSpitAction(switch351);
		de.semvox.types.odp.interaction.GenerationContext generationContext358 = getGenerationContext358();
		template350.setSpitOnContext(generationContext358);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask359 = getReportOnTask359();
		template350.setSpitOnInput(reportOnTask359);
		return template350;
	}

	de.semvox.types.odp.spit.Switch getSwitch351() {
		final de.semvox.types.odp.spit.Switch switch351 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case352 = getCase352();
			switch351.getSpitCases().add(case352);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case355 = getCase355();
			switch351.getSpitCases().add(case355);
		}
		return switch351;
	}

	de.semvox.types.odp.spit.Case getCase352() {
		final de.semvox.types.odp.spit.Case case352 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition353 = getStringCondition353();
		case352.getSpitConditions().add(stringCondition353);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs354 = getGenerateStringAndOutputs354();
		case352.setSpitAction(generateStringAndOutputs354);
		return case352;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition353() {
		final de.semvox.types.odp.spit.StringCondition stringCondition353 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string189 = replaceEntities("lang");
		stringCondition353.setAlgVariableReference(string189);
		String string190 = replaceEntities("en-US");
		stringCondition353.setCommonStringValue(string190);
		return stringCondition353;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs354() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs354 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs354.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string191 = replaceEntities("for ?($NAME) the optimal humidity is ?($HUMIDITY)");
		generateStringAndOutputs354.getSpitAlternatives().add(string191);
		String string192 = replaceEntities("the optimal humidity for ?($NAME) is ?($HUMIDITY)");
		generateStringAndOutputs354.getSpitAlternatives().add(string192);
		return generateStringAndOutputs354;
	}

	de.semvox.types.odp.spit.Case getCase355() {
		final de.semvox.types.odp.spit.Case case355 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition356 = getStringCondition356();
		case355.getSpitConditions().add(stringCondition356);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs357 = getGenerateStringAndOutputs357();
		case355.setSpitAction(generateStringAndOutputs357);
		return case355;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition356() {
		final de.semvox.types.odp.spit.StringCondition stringCondition356 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string193 = replaceEntities("lang");
		stringCondition356.setAlgVariableReference(string193);
		String string194 = replaceEntities("nl-BE");
		stringCondition356.setCommonStringValue(string194);
		return stringCondition356;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs357() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs357 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs357.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string195 = replaceEntities("de optimale vochtigheidsgraad voor ?($NAME) (is|bedraagt) ?($HUMIDITY)");
		generateStringAndOutputs357.getSpitAlternatives().add(string195);
		String string196 = replaceEntities("voor ?($NAME) (is|bedraagt) de optimale vochtigheidsgraad ?($HUMIDITY)");
		generateStringAndOutputs357.getSpitAlternatives().add(string196);
		return generateStringAndOutputs357;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext358() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext358 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext358.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext358.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext358.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext358.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext358.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext358.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext358;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask359() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask359 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released360 = getReleased360();
		reportOnTask359.setMmTaskStatus(released360);
		return reportOnTask359;
	}

	de.semvox.types.odp.multimodal.Released getReleased360() {
		final de.semvox.types.odp.multimodal.Released released360 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.GetPlantHumidityParamater getPlantHumidityParamater361 = getGetPlantHumidityParamater361();
		released360.setMmTaskExecutionResult(getPlantHumidityParamater361);
		return released360;
	}

	com.zorabots.ontologies.vmn.GetPlantHumidityParamater getGetPlantHumidityParamater361() {
		final com.zorabots.ontologies.vmn.GetPlantHumidityParamater getPlantHumidityParamater361 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetPlantHumidityParamater();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess362 = getStatusSuccess362();
		getPlantHumidityParamater361.setIntResponseStatus(statusSuccess362);
		com.zorabots.ontologies.vmn.Plant plant363 = getPlant363();
		getPlantHumidityParamater361.setVmnPlant(plant363);
		return getPlantHumidityParamater361;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess362() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess362 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess362;
	}

	com.zorabots.ontologies.vmn.Plant getPlant363() {
		final com.zorabots.ontologies.vmn.Plant plant363 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newPlant();
		plant363.markVmnPlantNameAsVariable("NAME", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant363.assignVmnPlantNamePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		plant363.markVmnOptimalHumidityAsVariable("HUMIDITY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant363.assignVmnOptimalHumidityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		plant363.markVmnOptimalLightAsVariable("LIGHT", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant363.assignVmnOptimalLightPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		plant363.markVmnOptimalTemperatureAsVariable("TEMP", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant363.assignVmnOptimalTemperaturePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return plant363;
	}

	de.semvox.types.odp.spit.Template getTemplate364() {
		final de.semvox.types.odp.spit.Template template364 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string197 = replaceEntities("ReportOnTask_getOptimalPlantHumidity");
		template364.setCommonName(string197);
		de.semvox.types.odp.spit.Switch switch365 = getSwitch365();
		template364.setSpitAction(switch365);
		de.semvox.types.odp.interaction.GenerationContext generationContext372 = getGenerationContext372();
		template364.setSpitOnContext(generationContext372);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask373 = getReportOnTask373();
		template364.setSpitOnInput(reportOnTask373);
		return template364;
	}

	de.semvox.types.odp.spit.Switch getSwitch365() {
		final de.semvox.types.odp.spit.Switch switch365 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case366 = getCase366();
			switch365.getSpitCases().add(case366);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case369 = getCase369();
			switch365.getSpitCases().add(case369);
		}
		return switch365;
	}

	de.semvox.types.odp.spit.Case getCase366() {
		final de.semvox.types.odp.spit.Case case366 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition367 = getStringCondition367();
		case366.getSpitConditions().add(stringCondition367);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs368 = getGenerateStringAndOutputs368();
		case366.setSpitAction(generateStringAndOutputs368);
		return case366;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition367() {
		final de.semvox.types.odp.spit.StringCondition stringCondition367 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string198 = replaceEntities("lang");
		stringCondition367.setAlgVariableReference(string198);
		String string199 = replaceEntities("en-US");
		stringCondition367.setCommonStringValue(string199);
		return stringCondition367;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs368() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs368 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs368.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string200 = replaceEntities("for ?($NAME) the optimal light intensity is ?($LIGHT)");
		generateStringAndOutputs368.getSpitAlternatives().add(string200);
		String string201 = replaceEntities("the optimal light intensity for ?($NAME) is ?($LIGHT)");
		generateStringAndOutputs368.getSpitAlternatives().add(string201);
		return generateStringAndOutputs368;
	}

	de.semvox.types.odp.spit.Case getCase369() {
		final de.semvox.types.odp.spit.Case case369 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition370 = getStringCondition370();
		case369.getSpitConditions().add(stringCondition370);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs371 = getGenerateStringAndOutputs371();
		case369.setSpitAction(generateStringAndOutputs371);
		return case369;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition370() {
		final de.semvox.types.odp.spit.StringCondition stringCondition370 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string202 = replaceEntities("lang");
		stringCondition370.setAlgVariableReference(string202);
		String string203 = replaceEntities("nl-BE");
		stringCondition370.setCommonStringValue(string203);
		return stringCondition370;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs371() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs371 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs371.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string204 = replaceEntities("de optimale licht (intensiteit|hoeveelheid) voor ?($NAME) (is|bedraagt) ?($LIGHT)");
		generateStringAndOutputs371.getSpitAlternatives().add(string204);
		String string205 = replaceEntities("voor ?($NAME) (is|bedraagt) de optimale licht (intensiteit|hoeveelheid) ?($LIGHT)");
		generateStringAndOutputs371.getSpitAlternatives().add(string205);
		return generateStringAndOutputs371;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext372() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext372 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext372.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext372.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext372.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext372.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext372.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext372.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext372;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask373() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask373 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released374 = getReleased374();
		reportOnTask373.setMmTaskStatus(released374);
		return reportOnTask373;
	}

	de.semvox.types.odp.multimodal.Released getReleased374() {
		final de.semvox.types.odp.multimodal.Released released374 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.GetPlantLightParamater getPlantLightParamater375 = getGetPlantLightParamater375();
		released374.setMmTaskExecutionResult(getPlantLightParamater375);
		return released374;
	}

	com.zorabots.ontologies.vmn.GetPlantLightParamater getGetPlantLightParamater375() {
		final com.zorabots.ontologies.vmn.GetPlantLightParamater getPlantLightParamater375 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetPlantLightParamater();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess376 = getStatusSuccess376();
		getPlantLightParamater375.setIntResponseStatus(statusSuccess376);
		com.zorabots.ontologies.vmn.Plant plant377 = getPlant377();
		getPlantLightParamater375.setVmnPlant(plant377);
		return getPlantLightParamater375;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess376() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess376 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess376;
	}

	com.zorabots.ontologies.vmn.Plant getPlant377() {
		final com.zorabots.ontologies.vmn.Plant plant377 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newPlant();
		plant377.markVmnPlantNameAsVariable("NAME", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant377.assignVmnPlantNamePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		plant377.markVmnOptimalHumidityAsVariable("HUMIDITY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant377.assignVmnOptimalHumidityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		plant377.markVmnOptimalLightAsVariable("LIGHT", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant377.assignVmnOptimalLightPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		plant377.markVmnOptimalTemperatureAsVariable("TEMP", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant377.assignVmnOptimalTemperaturePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return plant377;
	}

	de.semvox.types.odp.spit.Template getTemplate378() {
		final de.semvox.types.odp.spit.Template template378 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string206 = replaceEntities("ReportOnTask_getOptimalPlantHumidity");
		template378.setCommonName(string206);
		de.semvox.types.odp.spit.Switch switch379 = getSwitch379();
		template378.setSpitAction(switch379);
		de.semvox.types.odp.interaction.GenerationContext generationContext386 = getGenerationContext386();
		template378.setSpitOnContext(generationContext386);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask387 = getReportOnTask387();
		template378.setSpitOnInput(reportOnTask387);
		return template378;
	}

	de.semvox.types.odp.spit.Switch getSwitch379() {
		final de.semvox.types.odp.spit.Switch switch379 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case380 = getCase380();
			switch379.getSpitCases().add(case380);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case383 = getCase383();
			switch379.getSpitCases().add(case383);
		}
		return switch379;
	}

	de.semvox.types.odp.spit.Case getCase380() {
		final de.semvox.types.odp.spit.Case case380 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition381 = getStringCondition381();
		case380.getSpitConditions().add(stringCondition381);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs382 = getGenerateStringAndOutputs382();
		case380.setSpitAction(generateStringAndOutputs382);
		return case380;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition381() {
		final de.semvox.types.odp.spit.StringCondition stringCondition381 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string207 = replaceEntities("lang");
		stringCondition381.setAlgVariableReference(string207);
		String string208 = replaceEntities("en-US");
		stringCondition381.setCommonStringValue(string208);
		return stringCondition381;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs382() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs382 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs382.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string209 = replaceEntities("for ?($NAME) the optimal temperature (is|bedraagt) ?($TEMP)");
		generateStringAndOutputs382.getSpitAlternatives().add(string209);
		String string210 = replaceEntities("the optimal temperature for ?($NAME) (is|bedraagt) ?($TEMP)");
		generateStringAndOutputs382.getSpitAlternatives().add(string210);
		return generateStringAndOutputs382;
	}

	de.semvox.types.odp.spit.Case getCase383() {
		final de.semvox.types.odp.spit.Case case383 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition384 = getStringCondition384();
		case383.getSpitConditions().add(stringCondition384);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs385 = getGenerateStringAndOutputs385();
		case383.setSpitAction(generateStringAndOutputs385);
		return case383;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition384() {
		final de.semvox.types.odp.spit.StringCondition stringCondition384 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string211 = replaceEntities("lang");
		stringCondition384.setAlgVariableReference(string211);
		String string212 = replaceEntities("nl-BE");
		stringCondition384.setCommonStringValue(string212);
		return stringCondition384;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs385() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs385 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs385.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string213 = replaceEntities("de optimale temperatuur voor ?($NAME) (is|bedraagt) ?($TEMP)");
		generateStringAndOutputs385.getSpitAlternatives().add(string213);
		String string214 = replaceEntities("voor ?($NAME) (is|bedraagt) de optimale temperatuur ?($TEMP)");
		generateStringAndOutputs385.getSpitAlternatives().add(string214);
		return generateStringAndOutputs385;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext386() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext386 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext386.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext386.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext386.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext386.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext386.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext386.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext386;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask387() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask387 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released388 = getReleased388();
		reportOnTask387.setMmTaskStatus(released388);
		return reportOnTask387;
	}

	de.semvox.types.odp.multimodal.Released getReleased388() {
		final de.semvox.types.odp.multimodal.Released released388 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.GetPlantTemperatureParamter getPlantTemperatureParamter389 = getGetPlantTemperatureParamter389();
		released388.setMmTaskExecutionResult(getPlantTemperatureParamter389);
		return released388;
	}

	com.zorabots.ontologies.vmn.GetPlantTemperatureParamter getGetPlantTemperatureParamter389() {
		final com.zorabots.ontologies.vmn.GetPlantTemperatureParamter getPlantTemperatureParamter389 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetPlantTemperatureParamter();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess390 = getStatusSuccess390();
		getPlantTemperatureParamter389.setIntResponseStatus(statusSuccess390);
		com.zorabots.ontologies.vmn.Plant plant391 = getPlant391();
		getPlantTemperatureParamter389.setVmnPlant(plant391);
		return getPlantTemperatureParamter389;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess390() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess390 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess390;
	}

	com.zorabots.ontologies.vmn.Plant getPlant391() {
		final com.zorabots.ontologies.vmn.Plant plant391 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newPlant();
		plant391.markVmnPlantNameAsVariable("NAME", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant391.assignVmnPlantNamePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		plant391.markVmnOptimalHumidityAsVariable("HUMIDITY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant391.assignVmnOptimalHumidityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		plant391.markVmnOptimalLightAsVariable("LIGHT", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant391.assignVmnOptimalLightPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		plant391.markVmnOptimalTemperatureAsVariable("TEMP", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant391.assignVmnOptimalTemperaturePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return plant391;
	}

	de.semvox.types.odp.spit.Template getTemplate392() {
		final de.semvox.types.odp.spit.Template template392 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string215 = replaceEntities("ReportOnTask_getPlantDescription");
		template392.setCommonName(string215);
		de.semvox.types.odp.spit.Switch switch393 = getSwitch393();
		template392.setSpitAction(switch393);
		de.semvox.types.odp.interaction.GenerationContext generationContext400 = getGenerationContext400();
		template392.setSpitOnContext(generationContext400);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask401 = getReportOnTask401();
		template392.setSpitOnInput(reportOnTask401);
		return template392;
	}

	de.semvox.types.odp.spit.Switch getSwitch393() {
		final de.semvox.types.odp.spit.Switch switch393 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case394 = getCase394();
			switch393.getSpitCases().add(case394);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case397 = getCase397();
			switch393.getSpitCases().add(case397);
		}
		return switch393;
	}

	de.semvox.types.odp.spit.Case getCase394() {
		final de.semvox.types.odp.spit.Case case394 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition395 = getStringCondition395();
		case394.getSpitConditions().add(stringCondition395);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs396 = getGenerateStringAndOutputs396();
		case394.setSpitAction(generateStringAndOutputs396);
		return case394;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition395() {
		final de.semvox.types.odp.spit.StringCondition stringCondition395 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string216 = replaceEntities("lang");
		stringCondition395.setAlgVariableReference(string216);
		String string217 = replaceEntities("en-US");
		stringCondition395.setCommonStringValue(string217);
		return stringCondition395;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs396() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs396 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs396.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string218 = replaceEntities("?($DESCRIPT)");
		generateStringAndOutputs396.getSpitAlternatives().add(string218);
		return generateStringAndOutputs396;
	}

	de.semvox.types.odp.spit.Case getCase397() {
		final de.semvox.types.odp.spit.Case case397 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition398 = getStringCondition398();
		case397.getSpitConditions().add(stringCondition398);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs399 = getGenerateStringAndOutputs399();
		case397.setSpitAction(generateStringAndOutputs399);
		return case397;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition398() {
		final de.semvox.types.odp.spit.StringCondition stringCondition398 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string219 = replaceEntities("lang");
		stringCondition398.setAlgVariableReference(string219);
		String string220 = replaceEntities("nl-BE");
		stringCondition398.setCommonStringValue(string220);
		return stringCondition398;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs399() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs399 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs399.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string221 = replaceEntities("?($DESCRIPT)");
		generateStringAndOutputs399.getSpitAlternatives().add(string221);
		return generateStringAndOutputs399;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext400() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext400 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext400.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext400.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext400.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext400.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext400.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext400.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext400;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask401() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask401 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released402 = getReleased402();
		reportOnTask401.setMmTaskStatus(released402);
		return reportOnTask401;
	}

	de.semvox.types.odp.multimodal.Released getReleased402() {
		final de.semvox.types.odp.multimodal.Released released402 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.vmn.GetPlantDescription getPlantDescription403 = getGetPlantDescription403();
		released402.setMmTaskExecutionResult(getPlantDescription403);
		return released402;
	}

	com.zorabots.ontologies.vmn.GetPlantDescription getGetPlantDescription403() {
		final com.zorabots.ontologies.vmn.GetPlantDescription getPlantDescription403 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetPlantDescription();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess404 = getStatusSuccess404();
		getPlantDescription403.setIntResponseStatus(statusSuccess404);
		com.zorabots.ontologies.vmn.Plant plant405 = getPlant405();
		getPlantDescription403.setVmnPlant(plant405);
		return getPlantDescription403;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess404() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess404 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess404;
	}

	com.zorabots.ontologies.vmn.Plant getPlant405() {
		final com.zorabots.ontologies.vmn.Plant plant405 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newPlant();
		plant405.markVmnDescriptionAsVariable("DESCRIPT", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant405.assignVmnDescriptionPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return plant405;
	}
}