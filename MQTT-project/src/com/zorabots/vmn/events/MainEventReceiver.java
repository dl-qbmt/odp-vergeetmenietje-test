package com.zorabots.vmn.events;

public class MainEventReceiver {

	private static MainEventReceiver eventReceiver = null;
	
	private MainEventReceiver(){
		
	}
	
	public static MainEventReceiver getInstance(){
		if(eventReceiver == null){
			eventReceiver = new MainEventReceiver();
		}
		return eventReceiver;
	}
	
	public void handle(String event){
		
	}
}
