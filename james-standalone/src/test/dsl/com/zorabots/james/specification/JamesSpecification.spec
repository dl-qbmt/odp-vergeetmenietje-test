specification JamesSpecification {
	scenario Temperature {
    intro {
      sequence {
        initialize-system
				
      } //sequence
      
    }
    conversation GetWeather {
  sequence {
    push-to-activate
    send-voice-input en-US {
      "what is your temperature":grammar ():0.7
    }
    match-voice-output ssml (en-US) {
      "what is your temperature"
    }
  } //sequence
} //conversation GetWeather
  }
}