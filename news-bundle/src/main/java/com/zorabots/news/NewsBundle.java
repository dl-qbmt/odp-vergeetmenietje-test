package com.zorabots.news;

import java.util.ArrayList;
import java.util.List;

import com.zorabots.news.components.NewsService;
import com.zorabots.news.nlg.gen.NewsTemplates;
import com.zorabots.news.nlu.gen.NewsGrammar;
import com.zorabots.news.taskmodel.gen.NewsTaskmodel;

import de.semvox.odp.multimodal.LanguageProvider;
import de.semvox.odp.s3.Platform;
import de.semvox.odp.s3.bundle.AbstractDialogBundle;
import de.semvox.types.odp.spijk.StandaloneGrammar;
import de.semvox.types.odp.spit.TemplateCollection;
import de.semvox.types.odp.squint.TaskModel;

public class NewsBundle extends AbstractDialogBundle {

	@Override
	protected void onLoad(Platform platform) throws Exception {
	    // initialize bundle resources
        
        // initialize grammar
        List<StandaloneGrammar> grammars = getGrammars(platform);
        
        // initialize template collection
        List<TemplateCollection> templates = getTemplates(platform);
        
        // initialize taskmodel
        TaskModel taskModel = new NewsTaskmodel().loadThing();
        
        loadBundleResources(grammars, taskModel, templates, platform);
	 
		// TODO implement onLoad
        platform.registerComponent(new NewsService(platform.getIpcFacade()));
		
	}
	
	protected List<StandaloneGrammar> getGrammars(Platform platform) {
	 	LanguageProvider langProvider = platform.getLanguageProvider();
	 
        List<StandaloneGrammar> grammars = new ArrayList<StandaloneGrammar>();
        grammars.add(new NewsGrammar(langProvider).loadThing());

        return grammars;
    }

    protected List<TemplateCollection> getTemplates(Platform platform) {
	    LanguageProvider langProvider = platform.getLanguageProvider();
    
        List<TemplateCollection> templates = new ArrayList<TemplateCollection>();
        templates.add(new NewsTemplates(langProvider).loadThing());

        return templates;
    }
	
	@Override
    public void changeLanguage(String language, Platform platform) throws Exception {
        reloadGrammars(getGrammars(platform), platform);
        reloadTemplates(getTemplates(platform), platform);
    }

	@Override
	protected void onUnload(Platform arg0) {
		// TODO implement onUnLoad

	}
}