package com.zorabots.vmn.staticclasses;

import com.zorabots.ontologies.vmn.factory.ThingFactoryVmn;

public class YesNo {

	public static com.zorabots.ontologies.vmn.YesNo getYesNo(String str){
		switch(str){
		case "YES":
			return (com.zorabots.ontologies.vmn.YesNo)ThingFactoryVmn.newYes();
		case "NO":
			return (com.zorabots.ontologies.vmn.YesNo)ThingFactoryVmn.newNo();
		}
		return null;
	}
}
