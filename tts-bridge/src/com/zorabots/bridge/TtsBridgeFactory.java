package com.zorabots.bridge;

import de.semvox.tts.bridge.TtsBridge.PrepareVoiceOutputCallback;
import de.semvox.tts.bridge.TtsBridge.TtsCallback;

public class TtsBridgeFactory implements de.semvox.tts.bridge.TtsBridgeFactory{

	 private TtsConfiguration configuration;

	    public TtsBridgeFactory(TtsConfiguration configuration) {
	        this.configuration = configuration;
	    }

	    @Override
	    public TtsBridge newTtsBridge(TtsCallback ttsCallback, PrepareVoiceOutputCallback prepareVoiceOutputCallback) {
	        return new TtsBridge(ttsCallback, prepareVoiceOutputCallback, configuration);
	    }


}
