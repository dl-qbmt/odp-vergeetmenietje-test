package com.zorabots.vmn.pojo;

import java.util.HashMap;

public class SharedUnits {
	private static SharedUnits instance = null;
	public HashMap<String, String> map;
	
	private SharedUnits(){
		map = new HashMap<String, String>();
		map.put("temperature", "degrees");
		map.put("light", "lux");
		map.put("humidity", "percent");
	}
	
	public static SharedUnits getInstance() {
	      if(instance == null) {
	         instance = new SharedUnits();
	      }
	      return instance;
	}
}
