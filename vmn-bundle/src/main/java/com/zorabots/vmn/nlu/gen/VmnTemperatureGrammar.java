/**
* GENERATED CODE
*/
package com.zorabots.vmn.nlu.gen;

public final class VmnTemperatureGrammar extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.spijk.StandaloneGrammar> implements de.semvox.commons.Generated {

	private java.util.Map<java.lang.String, de.semvox.subcon.data.Thing> idRefMap = new java.util.HashMap<java.lang.String, de.semvox.subcon.data.Thing>();

	public static java.lang.String TYPE_NAME = "spijk#StandaloneGrammar";

	public VmnTemperatureGrammar(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public VmnTemperatureGrammar(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public VmnTemperatureGrammar(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public VmnTemperatureGrammar() {
		super();
	}

	@Override
	protected de.semvox.types.odp.spijk.StandaloneGrammar createThing() {
		return getStandaloneGrammar();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.spijk.StandaloneGrammar standaloneGrammar0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("VmnTemperatureGrammar");
		standaloneGrammar0.setCommonName(string0);
		de.semvox.types.asr.MainGrammar mainGrammar1 = getMainGrammar1();
		standaloneGrammar0.setAsrGrammarType(mainGrammar1);
		standaloneGrammar0.assignSpijkImportsPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.spijk.Rule rule2 = getRule2();
		standaloneGrammar0.getSpijkElements().add(rule2);
		de.semvox.types.odp.spijk.Rule rule8 = getRule8();
		standaloneGrammar0.getSpijkElements().add(rule8);
		de.semvox.types.odp.spijk.Rule rule15 = getRule15();
		standaloneGrammar0.getSpijkElements().add(rule15);
		de.semvox.types.odp.spijk.Rule rule22 = getRule22();
		standaloneGrammar0.getSpijkElements().add(rule22);
	}

	de.semvox.types.odp.spijk.StandaloneGrammar getStandaloneGrammar() {
		final de.semvox.types.odp.spijk.StandaloneGrammar standaloneGrammar0 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newStandaloneGrammar();
		return standaloneGrammar0;
	}


	de.semvox.types.asr.MainGrammar getMainGrammar1() {
		final de.semvox.types.asr.MainGrammar mainGrammar1 = de.semvox.types.asr.factory.PatternFactoryAsr.newMainGrammar();
		return mainGrammar1;
	}

	de.semvox.types.odp.spijk.Rule getRule2() {
		final de.semvox.types.odp.spijk.Rule rule2 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string1 = replaceEntities("RequestTask_GetTemperature");
		rule2.setCommonName(string1);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern3 = getPattern3();
			rule2.getSpijkPatterns().add(pattern3);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern4 = getPattern4();
			rule2.getSpijkPatterns().add(pattern4);
		}
		de.semvox.types.odp.spijk.References references5 = getReferences5();
		rule2.setSpijkReferences(references5);
		de.semvox.types.odp.interaction.RequestTask requestTask6 = getRequestTask6();
		rule2.setMmInterpretation(requestTask6);
		return rule2;
	}

	de.semvox.types.odp.spijk.Pattern getPattern3() {
		final de.semvox.types.odp.spijk.Pattern pattern3 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string2 = replaceEntities("en-US");
		pattern3.setCommonLanguage(string2);
		String string3 = replaceEntities("what is (your|the) temperature");
		pattern3.getSpijkUtterances().add(string3);
		String string4 = replaceEntities("how (warm|cold) are you");
		pattern3.getSpijkUtterances().add(string4);
		return pattern3;
	}

	de.semvox.types.odp.spijk.Pattern getPattern4() {
		final de.semvox.types.odp.spijk.Pattern pattern4 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string5 = replaceEntities("nl-BE");
		pattern4.setCommonLanguage(string5);
		String string6 = replaceEntities("(wat|hoe) is je temperatuur");
		pattern4.getSpijkUtterances().add(string6);
		String string7 = replaceEntities("hoeveel bedraagt je temperatuur");
		pattern4.getSpijkUtterances().add(string7);
		String string8 = replaceEntities("hoe warm (heb je ?(het)|is het)");
		pattern4.getSpijkUtterances().add(string8);
		return pattern4;
	}

	de.semvox.types.odp.spijk.References getReferences5() {
		final de.semvox.types.odp.spijk.References references5 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string9 = replaceEntities("");
		references5.setSpijkValue(string9);
		return references5;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask6() {
		final de.semvox.types.odp.interaction.RequestTask requestTask6 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetTemperature", requestTask6);
		com.zorabots.ontologies.vmn.GetTemperature getTemperature7 = getGetTemperature7();
		requestTask6.setCommonContent(getTemperature7);
		return requestTask6;
	}

	com.zorabots.ontologies.vmn.GetTemperature getGetTemperature7() {
		final com.zorabots.ontologies.vmn.GetTemperature getTemperature7 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetTemperature();
		return getTemperature7;
	}

	de.semvox.types.odp.spijk.Rule getRule8() {
		final de.semvox.types.odp.spijk.Rule rule8 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string10 = replaceEntities("RequestTask_GetTemperature_YesNo_Pos");
		rule8.setCommonName(string10);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern9 = getPattern9();
			rule8.getSpijkPatterns().add(pattern9);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern10 = getPattern10();
			rule8.getSpijkPatterns().add(pattern10);
		}
		de.semvox.types.odp.spijk.References references11 = getReferences11();
		rule8.setSpijkReferences(references11);
		de.semvox.types.odp.interaction.RequestTask requestTask12 = getRequestTask12();
		rule8.setMmInterpretation(requestTask12);
		return rule8;
	}

	de.semvox.types.odp.spijk.Pattern getPattern9() {
		final de.semvox.types.odp.spijk.Pattern pattern9 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string11 = replaceEntities("en-US");
		pattern9.setCommonLanguage(string11);
		String string12 = replaceEntities("is (your|the) temperature too high");
		pattern9.getSpijkUtterances().add(string12);
		String string13 = replaceEntities("are you too (warm|hot)");
		pattern9.getSpijkUtterances().add(string13);
		return pattern9;
	}

	de.semvox.types.odp.spijk.Pattern getPattern10() {
		final de.semvox.types.odp.spijk.Pattern pattern10 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string14 = replaceEntities("nl-BE");
		pattern10.setCommonLanguage(string14);
		String string15 = replaceEntities("is je temperatuur ?(niet) te hoog");
		pattern10.getSpijkUtterances().add(string15);
		String string16 = replaceEntities("heb je ?(het) ?(niet) te warm");
		pattern10.getSpijkUtterances().add(string16);
		String string17 = replaceEntities("is het ?(niet) te warm");
		pattern10.getSpijkUtterances().add(string17);
		String string18 = replaceEntities("heeft (je|de) plant niet te warm");
		pattern10.getSpijkUtterances().add(string18);
		return pattern10;
	}

	de.semvox.types.odp.spijk.References getReferences11() {
		final de.semvox.types.odp.spijk.References references11 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string19 = replaceEntities("");
		references11.setSpijkValue(string19);
		return references11;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask12() {
		final de.semvox.types.odp.interaction.RequestTask requestTask12 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetTemperature_YesNo_Pos", requestTask12);
		com.zorabots.ontologies.vmn.YesNoTemperature yesNoTemperature13 = getYesNoTemperature13();
		requestTask12.setCommonContent(yesNoTemperature13);
		return requestTask12;
	}

	com.zorabots.ontologies.vmn.YesNoTemperature getYesNoTemperature13() {
		final com.zorabots.ontologies.vmn.YesNoTemperature yesNoTemperature13 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newYesNoTemperature();
		com.zorabots.ontologies.vmn.PositiveQuestion positiveQuestion14 = getPositiveQuestion14();
		yesNoTemperature13.setVmnYesNoQuestionType(positiveQuestion14);
		return yesNoTemperature13;
	}

	com.zorabots.ontologies.vmn.PositiveQuestion getPositiveQuestion14() {
		final com.zorabots.ontologies.vmn.PositiveQuestion positiveQuestion14 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newPositiveQuestion();
		return positiveQuestion14;
	}

	de.semvox.types.odp.spijk.Rule getRule15() {
		final de.semvox.types.odp.spijk.Rule rule15 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string20 = replaceEntities("RequestTask_GetTemperature_YesNo_Neutral");
		rule15.setCommonName(string20);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern16 = getPattern16();
			rule15.getSpijkPatterns().add(pattern16);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern17 = getPattern17();
			rule15.getSpijkPatterns().add(pattern17);
		}
		de.semvox.types.odp.spijk.References references18 = getReferences18();
		rule15.setSpijkReferences(references18);
		de.semvox.types.odp.interaction.RequestTask requestTask19 = getRequestTask19();
		rule15.setMmInterpretation(requestTask19);
		return rule15;
	}

	de.semvox.types.odp.spijk.Pattern getPattern16() {
		final de.semvox.types.odp.spijk.Pattern pattern16 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string21 = replaceEntities("en-US");
		pattern16.setCommonLanguage(string21);
		String string22 = replaceEntities("is (your|the) temperature (ok|well suited) ?(for you)");
		pattern16.getSpijkUtterances().add(string22);
		String string23 = replaceEntities("are you ?(still) (warm|cold) enough");
		pattern16.getSpijkUtterances().add(string23);
		String string24 = replaceEntities("is it warm enough");
		pattern16.getSpijkUtterances().add(string24);
		return pattern16;
	}

	de.semvox.types.odp.spijk.Pattern getPattern17() {
		final de.semvox.types.odp.spijk.Pattern pattern17 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string25 = replaceEntities("nl-BE");
		pattern17.setCommonLanguage(string25);
		String string26 = replaceEntities("is je temperatuur ?(nog) ok");
		pattern17.getSpijkUtterances().add(string26);
		String string27 = replaceEntities("is het ?(nog) warm genoeg");
		pattern17.getSpijkUtterances().add(string27);
		String string28 = replaceEntities("?(heb je) warm genoeg");
		pattern17.getSpijkUtterances().add(string28);
		String string29 = replaceEntities("heeft (je|de) plant warm genoeg");
		pattern17.getSpijkUtterances().add(string29);
		return pattern17;
	}

	de.semvox.types.odp.spijk.References getReferences18() {
		final de.semvox.types.odp.spijk.References references18 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string30 = replaceEntities("");
		references18.setSpijkValue(string30);
		return references18;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask19() {
		final de.semvox.types.odp.interaction.RequestTask requestTask19 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetTemperature_YesNo_Neutral", requestTask19);
		com.zorabots.ontologies.vmn.YesNoTemperature yesNoTemperature20 = getYesNoTemperature20();
		requestTask19.setCommonContent(yesNoTemperature20);
		return requestTask19;
	}

	com.zorabots.ontologies.vmn.YesNoTemperature getYesNoTemperature20() {
		final com.zorabots.ontologies.vmn.YesNoTemperature yesNoTemperature20 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newYesNoTemperature();
		com.zorabots.ontologies.vmn.NeutralQuestion neutralQuestion21 = getNeutralQuestion21();
		yesNoTemperature20.setVmnYesNoQuestionType(neutralQuestion21);
		return yesNoTemperature20;
	}

	com.zorabots.ontologies.vmn.NeutralQuestion getNeutralQuestion21() {
		final com.zorabots.ontologies.vmn.NeutralQuestion neutralQuestion21 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNeutralQuestion();
		return neutralQuestion21;
	}

	de.semvox.types.odp.spijk.Rule getRule22() {
		final de.semvox.types.odp.spijk.Rule rule22 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string31 = replaceEntities("RequestTask_GetTemperature_YesNo_Negative");
		rule22.setCommonName(string31);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern23 = getPattern23();
			rule22.getSpijkPatterns().add(pattern23);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern24 = getPattern24();
			rule22.getSpijkPatterns().add(pattern24);
		}
		de.semvox.types.odp.spijk.References references25 = getReferences25();
		rule22.setSpijkReferences(references25);
		de.semvox.types.odp.interaction.RequestTask requestTask26 = getRequestTask26();
		rule22.setMmInterpretation(requestTask26);
		return rule22;
	}

	de.semvox.types.odp.spijk.Pattern getPattern23() {
		final de.semvox.types.odp.spijk.Pattern pattern23 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string32 = replaceEntities("en-US");
		pattern23.setCommonLanguage(string32);
		String string33 = replaceEntities("is (your|the) temperature too low");
		pattern23.getSpijkUtterances().add(string33);
		String string34 = replaceEntities("are you too cold");
		pattern23.getSpijkUtterances().add(string34);
		return pattern23;
	}

	de.semvox.types.odp.spijk.Pattern getPattern24() {
		final de.semvox.types.odp.spijk.Pattern pattern24 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string35 = replaceEntities("nl-BE");
		pattern24.setCommonLanguage(string35);
		String string36 = replaceEntities("is je temperatuur te laag");
		pattern24.getSpijkUtterances().add(string36);
		String string37 = replaceEntities("heb je ?(het) ?(niet) te koud");
		pattern24.getSpijkUtterances().add(string37);
		String string38 = replaceEntities("is het niet te koud");
		pattern24.getSpijkUtterances().add(string38);
		String string39 = replaceEntities("heeft (je|de) plant te koud");
		pattern24.getSpijkUtterances().add(string39);
		return pattern24;
	}

	de.semvox.types.odp.spijk.References getReferences25() {
		final de.semvox.types.odp.spijk.References references25 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string40 = replaceEntities("");
		references25.setSpijkValue(string40);
		return references25;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask26() {
		final de.semvox.types.odp.interaction.RequestTask requestTask26 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetTemperature_YesNo_Negative", requestTask26);
		com.zorabots.ontologies.vmn.YesNoTemperature yesNoTemperature27 = getYesNoTemperature27();
		requestTask26.setCommonContent(yesNoTemperature27);
		return requestTask26;
	}

	com.zorabots.ontologies.vmn.YesNoTemperature getYesNoTemperature27() {
		final com.zorabots.ontologies.vmn.YesNoTemperature yesNoTemperature27 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newYesNoTemperature();
		com.zorabots.ontologies.vmn.NegativeQuestion negativeQuestion28 = getNegativeQuestion28();
		yesNoTemperature27.setVmnYesNoQuestionType(negativeQuestion28);
		return yesNoTemperature27;
	}

	com.zorabots.ontologies.vmn.NegativeQuestion getNegativeQuestion28() {
		final com.zorabots.ontologies.vmn.NegativeQuestion negativeQuestion28 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNegativeQuestion();
		return negativeQuestion28;
	}
}