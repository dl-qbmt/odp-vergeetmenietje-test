package com.zorabots.bridge;

import de.semvox.commons.config.Configuration;
import de.semvox.tts.bridge.TtsBridge;

public class TtsConfiguration extends de.semvox.tts.bridge.TtsConfiguration{

	 /**
     * Defines how long the {@link TtsBridge#startSpeaking(String, String, de.semvox.types.tts.Voice, boolean)} 
     * method will wait to simulate the voice output. 
     */
    public static final String KEY_TTS_MOCK_UTTERANCE_LENGTH = "tts.mock.utterance-length";

    public TtsConfiguration(Configuration config) {
        super(config);
    }

    /**
     * Getter {@link QbmtTtsConfiguration#KEY_TTS_MOCK_UTTERANCE_LENGTH}
     * 
     * @return Duration in milliseconds 
     */
    public int mockUtteranceLength() {
        return getConfig().getInteger(KEY_TTS_MOCK_UTTERANCE_LENGTH, 2000); // ms
    }
}
