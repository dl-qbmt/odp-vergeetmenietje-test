package com.zorabots.vmn.components;

import java.util.Map;

import org.eclipse.paho.client.mqttv3.MqttException;

import com.zorabots.vmn.components.gen.RecognitionEventReceiverDefinition;
import com.zorabots.vmn.routing.MQTTClient;

import de.semvox.commons.logging.Logger;
import de.semvox.commons.logging.Logger.LogLevel;
import de.semvox.subcon.integration.api.EventReceiver;
import de.semvox.types.asr.NoMatch;
import de.semvox.types.asr.RecognitionSuccessful;
import de.semvox.types.common.Event;
import de.semvox.types.subcon.integration.EventReceiverDefinition;
import de.semvox.types.subcon.integration.TokenEvent;

public class RecognitionEventReceiver implements EventReceiver {

	private final EventReceiverDefinition definition;

	public RecognitionEventReceiver() throws Exception {
		definition = new RecognitionEventReceiverDefinition().loadThing();
	}

	@Override
	public EventReceiverDefinition getEventReceiverDefinition() {
		return definition;
	}

	@Override
	public void dispose() {

	}

	@Override
	public void handleEvent(Event event, Map<String, Object> boundVariables) {
		/*
		if (event instanceof StartOfSpeech){
			try {
				MQTTClient.getInstance().publish("vmnengine/push/odp/recognition", "{\"hearingvoice\": true}");
			} catch (MqttException e) {
				// TODO Auto-generated catch block
				Logger.log(LogLevel.INFO, "VMN Bundle", "StartOfSpeech ERROR fired!!!!!!!");
				e.printStackTrace();
			}
		}*/
		/*
		if (event instanceof EndOfSpeech){
			try {
				MQTTClient.getInstance().publish("vmnengine/push/odp/processing", "{\"priority\": 0, \"processing\": true}");
				Logger.log(LogLevel.INFO, "VMN Bundle", "End Of Speech detected, priority 0 blue led blinking sent through MQTT!!!!!!!");
			} catch (MqttException e) {
				// TODO Auto-generated catch block
				Logger.log(LogLevel.INFO, "VMN Bundle", "EndOfSpeech ERROR fired!!!!!!!");
				e.printStackTrace();
			}
		}
		*/
		if (event instanceof RecognitionSuccessful){
			Logger.log(LogLevel.INFO, "VMN Bundle", "RecognitionSuccessful detected!!!!!!!");
			
			if(((RecognitionSuccessful) event).hasAsrResults()){
				try {
					MQTTClient.getInstance().publish("vmnengine/push/odp/processing", "{\"priority\": 1, \"processing\": true}");
					Logger.log(LogLevel.INFO, "VMN Bundle", "RecognitionSuccessful WITH ASR RESULTS detected, priority 1 blue led blinking sent through MQTT!!!!!!!");
				} catch (MqttException e) {
					// TODO Auto-generated catch block
					Logger.log(LogLevel.INFO, "VMN Bundle", "Recognition Successful ERROR fired!!!!!!!");
					e.printStackTrace();	
				}
			}
		}
		
		
		if (event instanceof NoMatch){
			try {
 				MQTTClient.getInstance().publish("vmnengine/push/odp/processing", "{\"processing\": false}");
 				Logger.log(LogLevel.INFO, "VMN bundle", "No match, MQTT ODP processing false sent to engine!!!!!!!");
 			} catch (MqttException e) {
 				// TODO Auto-generated catch block
 				Logger.log(LogLevel.INFO, "VMN bundle", "MQTT No match ODP ERROR fired!!!!!!!");
 				e.printStackTrace();
 			}
		}

	}

	@Override
	public void handleToken(TokenEvent token, Map<String, Object> boundVariables) {
		// TODO Auto-generated method stub

	}

}
