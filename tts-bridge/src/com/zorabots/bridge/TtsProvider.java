package com.zorabots.bridge;

import de.semvox.commons.config.Configuration;

public class TtsProvider implements de.semvox.tts.bridge.TtsProvider{

	public TtsProvider() {
		// TODO Auto-generated constructor stub
	}
	
    @Override
    public TtsBridgeFactory newTtsBridgeFactory(Configuration configuration) {
        return new TtsBridgeFactory(new TtsConfiguration(configuration));
    }
	
	
}
