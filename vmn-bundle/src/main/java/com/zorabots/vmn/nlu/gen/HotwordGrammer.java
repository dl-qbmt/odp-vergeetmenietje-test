/**
* GENERATED CODE
*/
package com.zorabots.vmn.nlu.gen;

public final class HotwordGrammer extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.spijk.StandaloneGrammar> implements de.semvox.commons.Generated {

	private java.util.Map<java.lang.String, de.semvox.subcon.data.Thing> idRefMap = new java.util.HashMap<java.lang.String, de.semvox.subcon.data.Thing>();

	public static java.lang.String TYPE_NAME = "spijk#StandaloneGrammar";

	public HotwordGrammer(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public HotwordGrammer(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public HotwordGrammer(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public HotwordGrammer() {
		super();
	}

	@Override
	protected de.semvox.types.odp.spijk.StandaloneGrammar createThing() {
		return getStandaloneGrammar();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.spijk.StandaloneGrammar standaloneGrammar0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("HotwordGrammer");
		standaloneGrammar0.setCommonName(string0);
		de.semvox.types.asr.HotwordGrammar hotwordGrammar1 = getHotwordGrammar1();
		standaloneGrammar0.setAsrGrammarType(hotwordGrammar1);
		standaloneGrammar0.assignSpijkImportsPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.spijk.Rule rule2 = getRule2();
		standaloneGrammar0.getSpijkElements().add(rule2);
	}

	de.semvox.types.odp.spijk.StandaloneGrammar getStandaloneGrammar() {
		final de.semvox.types.odp.spijk.StandaloneGrammar standaloneGrammar0 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newStandaloneGrammar();
		return standaloneGrammar0;
	}


	de.semvox.types.asr.HotwordGrammar getHotwordGrammar1() {
		final de.semvox.types.asr.HotwordGrammar hotwordGrammar1 = de.semvox.types.asr.factory.PatternFactoryAsr.newHotwordGrammar();
		return hotwordGrammar1;
	}

	de.semvox.types.odp.spijk.Rule getRule2() {
		final de.semvox.types.odp.spijk.Rule rule2 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string1 = replaceEntities("HOTWORD_HI_SEMVOX");
		rule2.setCommonName(string1);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern3 = getPattern3();
			rule2.getSpijkPatterns().add(pattern3);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern4 = getPattern4();
			rule2.getSpijkPatterns().add(pattern4);
		}
		de.semvox.types.odp.spijk.References references5 = getReferences5();
		rule2.setSpijkReferences(references5);
		de.semvox.types.odp.multimodal.PushToActivate pushToActivate6 = getPushToActivate6();
		rule2.setMmInterpretation(pushToActivate6);
		return rule2;
	}

	de.semvox.types.odp.spijk.Pattern getPattern3() {
		final de.semvox.types.odp.spijk.Pattern pattern3 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string2 = replaceEntities("en-US");
		pattern3.setCommonLanguage(string2);
		String string3 = replaceEntities("billy billy");
		pattern3.getSpijkUtterances().add(string3);
		return pattern3;
	}

	de.semvox.types.odp.spijk.Pattern getPattern4() {
		final de.semvox.types.odp.spijk.Pattern pattern4 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string4 = replaceEntities("nl-BE");
		pattern4.setCommonLanguage(string4);
		String string5 = replaceEntities("billy billy");
		pattern4.getSpijkUtterances().add(string5);
		return pattern4;
	}

	de.semvox.types.odp.spijk.References getReferences5() {
		final de.semvox.types.odp.spijk.References references5 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string6 = replaceEntities("");
		references5.setSpijkValue(string6);
		return references5;
	}

	de.semvox.types.odp.multimodal.PushToActivate getPushToActivate6() {
		final de.semvox.types.odp.multimodal.PushToActivate pushToActivate6 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newPushToActivate();
		idRefMap.put("__HOTWORD_HI_SEMVOX", pushToActivate6);
		return pushToActivate6;
	}
}