/**
* GENERATED CODE
*/
package com.zorabots.weather.relativemoments.gen;

public final class Evening extends de.semvox.subcon.data.JavaThingLoader<com.zorabots.ontologies.weather.RelativeMoment> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "weather#RelativeMoment";

	public Evening(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public Evening(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public Evening(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public Evening() {
		super();
	}

	@Override
	protected com.zorabots.ontologies.weather.RelativeMoment createThing() {
		return getRelativeMoment();
	}

	@Override
	protected void initialize(final com.zorabots.ontologies.weather.RelativeMoment relativeMoment0, java.util.Map<String, Object> params) {

		de.semvox.types.common.Name name1 = getName1();
		relativeMoment0.setCommonNamedBy(name1);
	}

	com.zorabots.ontologies.weather.RelativeMoment getRelativeMoment() {
		final com.zorabots.ontologies.weather.RelativeMoment relativeMoment0 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newRelativeMoment();
		return relativeMoment0;
	}


	de.semvox.types.common.Name getName1() {
		final de.semvox.types.common.Name name1 = de.semvox.types.common.factory.PatternFactoryCommon.newName();
		String string0 = replaceEntities("deze avond");
		name1.setCommonName(string0);
		return name1;
	}
}