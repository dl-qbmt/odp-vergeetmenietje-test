package com.zorabots.vmn.components;

import java.util.Map;

import org.json.simple.JSONObject;

import com.zorabots.ontologies.vmn.GetHi;
import com.zorabots.ontologies.vmn.GetIntroduction;
import com.zorabots.ontologies.vmn.GetLeaveAlone;
import com.zorabots.ontologies.vmn.GetPlantDescription;
import com.zorabots.ontologies.vmn.GetTemperature;
import com.zorabots.ontologies.vmn.Temperature;
import com.zorabots.ontologies.vmn.TemperatureReport;
import com.zorabots.ontologies.vmn.User;
import com.zorabots.ontologies.vmn.YesNoTemperature;
import com.zorabots.ontologies.vmn.factory.ThingFactoryVmn;
import com.zorabots.vmn.components.gen.VmnServiceDefinition;
import com.zorabots.vmn.events.JsonParser;
import com.zorabots.vmn.mqtt.requests.GetRequests;
import com.zorabots.vmn.pojo.Person;
import com.zorabots.vmn.pojo.SharedUnits;
import com.zorabots.vmn.staticclasses.ClosedQuestionParameters;
import com.zorabots.vmn.staticclasses.UnitOntologyMap;

import de.semvox.subcon.data.ThingUtils;
import de.semvox.subcon.integration.api.IpcFacade;
import de.semvox.subcon.integration.api.Service;
import de.semvox.types.odp.multimodal.AsyncServiceResponse;
import de.semvox.types.odp.multimodal.PresentEvent;
import de.semvox.types.odp.multimodal.factory.ThingFactoryMultimodal;
import de.semvox.types.subcon.integration.AsyncServiceRequest;
import de.semvox.types.subcon.integration.ServiceDefinition;
import de.semvox.types.subcon.integration.ServiceRequest;
import de.semvox.types.subcon.integration.factory.ThingFactoryIntegration;

public class VmnService implements Service {

	private final ServiceDefinition definition;
	private IpcFacade ipcFacade;

	public VmnService(IpcFacade ipcFacade) throws Exception {
		definition = new VmnServiceDefinition().loadThing();
		this.ipcFacade = ipcFacade;
	}

	@Override
	public ServiceDefinition getServiceDefinition() {
		return definition;
	}

	@Override
	public void dispose() {

	}

	@Override
	public ServiceRequest handleRequest(ServiceRequest request,
			Map<String, Object> boundVariables) {
		
		if(request instanceof AsyncServiceRequest){
			AsyncServiceRequest asyncServiceRequest = (AsyncServiceRequest)request;
			
			if(asyncServiceRequest instanceof GetTemperature){
				asyncServiceRequest.setIntResponseStatus(ThingFactoryIntegration.newStatusAccepted());
				final GetTemperature getTemperatureRequest = ThingUtils.deepCopy((GetTemperature)request);
				GetRequests getRequests = new GetRequests("vmnengine/get/sensor/temperature", "vmnengine/response/odp/temperature", "{ \"key\":\"odp\"}", this, "handle", getTemperatureRequest);
				getRequests.start();
			}	
		}
		
		if(request instanceof GetHi){
			User user = ThingFactoryVmn.newUser();
			Person person = Person.getInstance();
			user.setVmnName(person.getName());
			
			((GetHi) request).setVmnUser(user);
			request.setIntResponseStatus(ThingFactoryIntegration.newStatusSuccess());
		}
		
		if(request instanceof GetIntroduction){
			request.setIntResponseStatus(ThingFactoryIntegration.newStatusSuccess());
		}
		
		if(request instanceof GetLeaveAlone){
			request.setIntResponseStatus(ThingFactoryIntegration.newStatusSuccess());
		}
		
		if(request instanceof GetPlantDescription){
			com.zorabots.ontologies.vmn.Plant ontologyplant = ThingFactoryVmn.newPlant();
			//String language = LanguageProvider.LANGUAGE_PROPERTY;
			String plantname = com.zorabots.vmn.pojo.Plant.getInstance().getName();
			
			PlantDescriptionEventReceiver plantDescriptionEventReceiver;
			try {
				plantDescriptionEventReceiver = new PlantDescriptionEventReceiver();
				GetRequests getRequests = new GetRequests("vmndb/get/translation/plant", "vmndb/response/odp/translation/plant", "{ \"key\":\"odp\", \"language\": \"nl\", \"plantname\": \"" +  plantname + "\"}", plantDescriptionEventReceiver, "handle");
				getRequests.start();
				//dirty way
				Thread.sleep(800);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			ontologyplant.setVmnDescription(com.zorabots.vmn.pojo.Plant.getInstance().getDescription());
			((GetPlantDescription) request).setVmnPlant(ontologyplant);
			request.setIntResponseStatus(ThingFactoryIntegration.newStatusSuccess());	
		}
		return request;
	}
	
	public void handle(String recMessage, Object request){
		
		 GetTemperature getTemperatureRequest = (GetTemperature) request;
		 TemperatureReport report = ThingFactoryVmn.newTemperatureReport();
		 
		 JSONObject jsonObj = JsonParser.parseJson(recMessage);
		 
		 Temperature temperature = ThingFactoryVmn.newTemperature();
		 
		 if(request instanceof YesNoTemperature){
			 ClosedQuestionParameters.yesNoQuestion((YesNoTemperature)request, ((Long)jsonObj.get("level")).intValue());
		 }
		 
		
		 double value = (double) jsonObj.get("value");
		 
		 int intvalue  = (int) Math.round(value);
		 
		 temperature.setCommonStringValue(Integer.toString(intvalue));
		 String tempunit = SharedUnits.getInstance().map.get("temperature");
		 temperature.setVmnUnit(UnitOntologyMap.getUnitOntology(tempunit));
		 //temperature.setCommonStringValue("22");
		 
		 report.setVmnTemperature(temperature);
		 
		 getTemperatureRequest.setVmnTemperatureReport(report);
		 getTemperatureRequest.setIntResponseStatus(ThingFactoryIntegration.newStatusSuccess());
         
		 PresentEvent presentEvent = ThingFactoryMultimodal.newPresentEvent();
         presentEvent.setMmTimeToLive((long) 2000);
         
         AsyncServiceResponse asyncServiceResponse = ThingFactoryMultimodal.newAsyncServiceResponse();
         asyncServiceResponse.setIntResponse(getTemperatureRequest);
         asyncServiceResponse.setMmCorrelationId(getTemperatureRequest.getCommonId());
         
         presentEvent.setMmEvent(asyncServiceResponse);
         ipcFacade.invokeService(presentEvent,
                 String.format("%s.%s", definition.getIntNamespace(), definition.getCommonName()));

	}
}