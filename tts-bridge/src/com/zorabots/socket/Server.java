package com.zorabots.socket;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

import de.semvox.commons.logging.Logger;
import de.semvox.commons.logging.Logger.LogLevel;

public class Server extends Thread{
	
	private ServerSocket serverSocket;
	private String text;
	private boolean needsToBeSaid = false;

	public Server() throws IOException{
		serverSocket = new ServerSocket(8888);
	}
	
	public void run(){
		
		Logger.log(LogLevel.DEV, "SocketServer", "Init Server");
		
		Socket server = null;
		try {
			server = serverSocket.accept();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		
		while(true){
			try{
				/*DataInputStream in = new DataInputStream(server.getInputStream());
				
				System.out.println(in.readUTF());*/
				if(needsToBeSaid){
					DataOutputStream out = new DataOutputStream(server.getOutputStream());
					
					out.write(text.getBytes());
					needsToBeSaid = false;
				}
				Thread.sleep(100);
				
			}catch(SocketTimeoutException e){
				System.out.println(e);
			}catch(IOException e){
				System.out.println(e);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	 public void sayText(String text){
		 this.text = text;
		 needsToBeSaid = true;
	 }
}
