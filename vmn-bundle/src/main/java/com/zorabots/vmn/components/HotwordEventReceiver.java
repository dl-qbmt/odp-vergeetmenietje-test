package com.zorabots.vmn.components;

import java.util.Map;

import org.eclipse.paho.client.mqttv3.MqttException;

import com.zorabots.vmn.components.gen.HotwordEventReceiverDefinition;
import com.zorabots.vmn.routing.MQTTClient;

import de.semvox.commons.logging.Logger;
import de.semvox.commons.logging.Logger.LogLevel;
import de.semvox.subcon.integration.api.EventReceiver;
import de.semvox.types.asr.HotwordRecognitionCancelled;
import de.semvox.types.asr.HotwordRecognitionStarted;
import de.semvox.types.asr.HotwordRecognitionSuccessful;
import de.semvox.types.common.Event;
import de.semvox.types.subcon.integration.EventReceiverDefinition;
import de.semvox.types.subcon.integration.TokenEvent;

public class HotwordEventReceiver implements EventReceiver {

	private final EventReceiverDefinition definition;

	public HotwordEventReceiver() throws Exception {
		definition = new HotwordEventReceiverDefinition().loadThing();
	}

	@Override
	public EventReceiverDefinition getEventReceiverDefinition() {
		return definition;
	}

	@Override
	public void dispose() {

	}

	@Override
	public void handleEvent(Event event, Map<String, Object> boundVariables) {
		// TODO Auto-generated method stub
		Logger.log(LogLevel.INFO, "VMN Bundle", "Hotword Event fired!!!!!!!");
		
		//not doing anything when hotword recognition is started.. bad practice? 
		if (event instanceof HotwordRecognitionStarted){
			try {
				Logger.log(LogLevel.INFO, "VMN Bundle", "Hotword Recognition Started fired!!!!!!!");
				MQTTClient.getInstance().publish("vmnengine/push/odp/hotword", "{\"hotwordlistening\": true}");
			} catch (MqttException e) {
				// TODO Auto-generated catch block
				Logger.log(LogLevel.INFO, "VMN Bundle", "Hotword Recognition Started ERROR fired!!!!!!!");
				e.printStackTrace();
			}
		}
		
		if ((event instanceof HotwordRecognitionSuccessful) || (event instanceof HotwordRecognitionCancelled)){
			try {
				Logger.log(LogLevel.INFO, "VMN Bundle", "Hotword Recognition Successful/Cancelled fired!!!!!!!");
				MQTTClient.getInstance().publish("vmnengine/push/odp/hotword", "{\"hotwordlistening\": false}");
			} catch (MqttException e) {
				// TODO Auto-generated catch block
				Logger.log(LogLevel.INFO, "VMN Bundle", "Hotword Recognition Successful/Cancelled ERROR fired!!!!!!!");
				e.printStackTrace();
			}
		}

	}

	@Override
	public void handleToken(TokenEvent token, Map<String, Object> boundVariables) {
		// TODO Auto-generated method stub

	}

}
