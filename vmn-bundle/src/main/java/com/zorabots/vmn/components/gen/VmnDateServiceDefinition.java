/**
* GENERATED CODE
*/
package com.zorabots.vmn.components.gen;

public final class VmnDateServiceDefinition extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.subcon.integration.ServiceDefinition> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "int#ServiceDefinition";

	public VmnDateServiceDefinition(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public VmnDateServiceDefinition(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public VmnDateServiceDefinition(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public VmnDateServiceDefinition() {
		super();
	}

	@Override
	protected de.semvox.types.subcon.integration.ServiceDefinition createThing() {
		return getServiceDefinition();
	}

	@Override
	protected void initialize(final de.semvox.types.subcon.integration.ServiceDefinition serviceDefinition0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("VmnDateService");
		serviceDefinition0.setCommonName(string0);
		String string1 = replaceEntities("com.zorabots.vmn.components");
		serviceDefinition0.setIntNamespace(string1);
		com.zorabots.ontologies.vmn.GetTime getTime1 = getGetTime1();
		serviceDefinition0.getIntSubscriptionsAsIntServiceRequest().put("GET_TIME", getTime1);
		com.zorabots.ontologies.vmn.GetDate getDate2 = getGetDate2();
		serviceDefinition0.getIntSubscriptionsAsIntServiceRequest().put("GET_DATE", getDate2);
	}

	de.semvox.types.subcon.integration.ServiceDefinition getServiceDefinition() {
		final de.semvox.types.subcon.integration.ServiceDefinition serviceDefinition0 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newServiceDefinition();
		return serviceDefinition0;
	}


	com.zorabots.ontologies.vmn.GetTime getGetTime1() {
		final com.zorabots.ontologies.vmn.GetTime getTime1 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetTime();
		return getTime1;
	}

	com.zorabots.ontologies.vmn.GetDate getGetDate2() {
		final com.zorabots.ontologies.vmn.GetDate getDate2 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetDate();
		return getDate2;
	}
}