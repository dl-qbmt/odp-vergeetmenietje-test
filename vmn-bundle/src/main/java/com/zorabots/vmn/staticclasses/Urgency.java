package com.zorabots.vmn.staticclasses;

import com.zorabots.ontologies.vmn.factory.ThingFactoryVmn;

public final class Urgency {
	public static com.zorabots.ontologies.vmn.Urgency getUrgency(String urgency){
		switch(urgency){
		case "ERROR":
			return (com.zorabots.ontologies.vmn.Urgency)ThingFactoryVmn.newError();
		case "WARNING":
			return (com.zorabots.ontologies.vmn.Urgency)ThingFactoryVmn.newWarning();
		case "ALERT":
			return (com.zorabots.ontologies.vmn.Urgency)ThingFactoryVmn.newAlert();
		}
			
		return null;
	}
	
	public static com.zorabots.ontologies.vmn.Urgency getHumidityUrgency(String urgency){
		switch(urgency){
			case "WARNING":
				return (com.zorabots.ontologies.vmn.Urgency)ThingFactoryVmn.newKinda();
			case "ALERT":
				return (com.zorabots.ontologies.vmn.Urgency)ThingFactoryVmn.newVery();
		}
		
		return null;
	}
}
