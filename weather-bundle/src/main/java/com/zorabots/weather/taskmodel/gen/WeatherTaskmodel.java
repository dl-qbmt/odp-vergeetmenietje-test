/**
* GENERATED CODE
*/
package com.zorabots.weather.taskmodel.gen;

public final class WeatherTaskmodel extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.squint.TaskModel> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "squint#TaskModel";

	public WeatherTaskmodel(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public WeatherTaskmodel(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public WeatherTaskmodel(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public WeatherTaskmodel() {
		super();
	}

	@Override
	protected de.semvox.types.odp.squint.TaskModel createThing() {
		return getTaskModel();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.squint.TaskModel taskModel0, java.util.Map<String, Object> params) {

		de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition1 = getTaskExecutionDefinition1();
		taskModel0.getSquintTaskDefinitions().add(taskExecutionDefinition1);
		de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition5 = getTaskExecutionDefinition5();
		taskModel0.getSquintTaskDefinitions().add(taskExecutionDefinition5);
	}

	de.semvox.types.odp.squint.TaskModel getTaskModel() {
		final de.semvox.types.odp.squint.TaskModel taskModel0 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newTaskModel();
		return taskModel0;
	}


	de.semvox.types.odp.squint.TaskExecutionDefinition getTaskExecutionDefinition1() {
		final de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition1 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newTaskExecutionDefinition();
		String string0 = replaceEntities("A task to get the news of the plant. ");
		taskExecutionDefinition1.setIntDescription(string0);
		String string1 = replaceEntities("true");
		taskExecutionDefinition1.setSquintUseHypothesisFusion(Boolean.parseBoolean(string1));
		String string2 = replaceEntities("GET_WEATHER_FORECAST");
		taskExecutionDefinition1.setCommonName(string2);
		taskExecutionDefinition1.markMmTaskAsVariable("GET_WEATHER_FORECAST", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast2 = getGetWeatherForecast2();
		taskExecutionDefinition1.setMmTask(getWeatherForecast2);
		de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution3 = getNeverConfirmExecution3();
		taskExecutionDefinition1.setSquintTaskConfirmationDirective(neverConfirmExecution3);
		String string3 = replaceEntities("true");
		taskExecutionDefinition1.setSquintAllowCorrectLastStepIntoClosedSelectionSpaces(Boolean.parseBoolean(string3));
		taskExecutionDefinition1.assignSquintCancelationDirectivePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption4 = getCancelTaskOnInterruption4();
		taskExecutionDefinition1.setSquintInterruptionDirective(cancelTaskOnInterruption4);
		String string4 = replaceEntities("com/zorabots/weather/taskmodel/WeatherTaskmodel#GET_WEATHER_FORECAST");
		taskExecutionDefinition1.setCommonId(string4);
		return taskExecutionDefinition1;
	}

	com.zorabots.ontologies.weather.GetWeatherForecast getGetWeatherForecast2() {
		final com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast2 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newGetWeatherForecast();
		return getWeatherForecast2;
	}

	de.semvox.types.odp.squint.NeverConfirmExecution getNeverConfirmExecution3() {
		final de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution3 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newNeverConfirmExecution();
		return neverConfirmExecution3;
	}

	de.semvox.types.odp.squint.CancelTaskOnInterruption getCancelTaskOnInterruption4() {
		final de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption4 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newCancelTaskOnInterruption();
		return cancelTaskOnInterruption4;
	}

	de.semvox.types.odp.squint.TaskExecutionDefinition getTaskExecutionDefinition5() {
		final de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition5 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newTaskExecutionDefinition();
		String string5 = replaceEntities("A task to get the news of the plant. ");
		taskExecutionDefinition5.setIntDescription(string5);
		String string6 = replaceEntities("true");
		taskExecutionDefinition5.setSquintUseHypothesisFusion(Boolean.parseBoolean(string6));
		String string7 = replaceEntities("GET_WEATHER");
		taskExecutionDefinition5.setCommonName(string7);
		taskExecutionDefinition5.markMmTaskAsVariable("GET_WEATHER", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.weather.GetWeather getWeather6 = getGetWeather6();
		taskExecutionDefinition5.setMmTask(getWeather6);
		de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution7 = getNeverConfirmExecution7();
		taskExecutionDefinition5.setSquintTaskConfirmationDirective(neverConfirmExecution7);
		String string8 = replaceEntities("true");
		taskExecutionDefinition5.setSquintAllowCorrectLastStepIntoClosedSelectionSpaces(Boolean.parseBoolean(string8));
		taskExecutionDefinition5.assignSquintCancelationDirectivePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption8 = getCancelTaskOnInterruption8();
		taskExecutionDefinition5.setSquintInterruptionDirective(cancelTaskOnInterruption8);
		String string9 = replaceEntities("com/zorabots/weather/taskmodel/WeatherTaskmodel#GET_WEATHER");
		taskExecutionDefinition5.setCommonId(string9);
		return taskExecutionDefinition5;
	}

	com.zorabots.ontologies.weather.GetWeather getGetWeather6() {
		final com.zorabots.ontologies.weather.GetWeather getWeather6 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newGetWeather();
		return getWeather6;
	}

	de.semvox.types.odp.squint.NeverConfirmExecution getNeverConfirmExecution7() {
		final de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution7 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newNeverConfirmExecution();
		return neverConfirmExecution7;
	}

	de.semvox.types.odp.squint.CancelTaskOnInterruption getCancelTaskOnInterruption8() {
		final de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption8 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newCancelTaskOnInterruption();
		return cancelTaskOnInterruption8;
	}
}