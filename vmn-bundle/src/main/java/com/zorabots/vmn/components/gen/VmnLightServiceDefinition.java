/**
* GENERATED CODE
*/
package com.zorabots.vmn.components.gen;

public final class VmnLightServiceDefinition extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.subcon.integration.ServiceDefinition> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "int#ServiceDefinition";

	public VmnLightServiceDefinition(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public VmnLightServiceDefinition(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public VmnLightServiceDefinition(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public VmnLightServiceDefinition() {
		super();
	}

	@Override
	protected de.semvox.types.subcon.integration.ServiceDefinition createThing() {
		return getServiceDefinition();
	}

	@Override
	protected void initialize(final de.semvox.types.subcon.integration.ServiceDefinition serviceDefinition0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("VmnLightService");
		serviceDefinition0.setCommonName(string0);
		String string1 = replaceEntities("com.zorabots.vmn.components");
		serviceDefinition0.setIntNamespace(string1);
		com.zorabots.ontologies.vmn.GetLight getLight1 = getGetLight1();
		serviceDefinition0.getIntSubscriptionsAsIntServiceRequest().put("GET_LIGHT", getLight1);
	}

	de.semvox.types.subcon.integration.ServiceDefinition getServiceDefinition() {
		final de.semvox.types.subcon.integration.ServiceDefinition serviceDefinition0 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newServiceDefinition();
		return serviceDefinition0;
	}


	com.zorabots.ontologies.vmn.GetLight getGetLight1() {
		final com.zorabots.ontologies.vmn.GetLight getLight1 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetLight();
		return getLight1;
	}
}