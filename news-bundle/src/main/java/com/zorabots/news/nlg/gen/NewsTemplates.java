/**
* GENERATED CODE
*/
package com.zorabots.news.nlg.gen;

public final class NewsTemplates extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.spit.TemplateCollection> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "spit#TemplateCollection";

	public NewsTemplates(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public NewsTemplates(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public NewsTemplates(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public NewsTemplates() {
		super();
	}

	@Override
	protected de.semvox.types.odp.spit.TemplateCollection createThing() {
		return getTemplateCollection();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.spit.TemplateCollection templateCollection0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("NewsTemplates");
		templateCollection0.setCommonName(string0);
		de.semvox.types.odp.spit.Template template1 = getTemplate1();
		templateCollection0.getSpitElements().add(template1);
		de.semvox.types.odp.spit.Template template13 = getTemplate13();
		templateCollection0.getSpitElements().add(template13);
	}

	de.semvox.types.odp.spit.TemplateCollection getTemplateCollection() {
		final de.semvox.types.odp.spit.TemplateCollection templateCollection0 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplateCollection();
		return templateCollection0;
	}


	de.semvox.types.odp.spit.Template getTemplate1() {
		final de.semvox.types.odp.spit.Template template1 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string1 = replaceEntities("ReportOnTask_StatusAccepted_GetNews");
		template1.setCommonName(string1);
		de.semvox.types.odp.spit.Switch switch2 = getSwitch2();
		template1.setSpitAction(switch2);
		de.semvox.types.odp.interaction.GenerationContext generationContext9 = getGenerationContext9();
		template1.setSpitOnContext(generationContext9);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask10 = getReportOnTask10();
		template1.setSpitOnInput(reportOnTask10);
		return template1;
	}

	de.semvox.types.odp.spit.Switch getSwitch2() {
		final de.semvox.types.odp.spit.Switch switch2 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case3 = getCase3();
			switch2.getSpitCases().add(case3);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case6 = getCase6();
			switch2.getSpitCases().add(case6);
		}
		return switch2;
	}

	de.semvox.types.odp.spit.Case getCase3() {
		final de.semvox.types.odp.spit.Case case3 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition4 = getStringCondition4();
		case3.getSpitConditions().add(stringCondition4);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs5 = getGenerateStringAndOutputs5();
		case3.setSpitAction(generateStringAndOutputs5);
		return case3;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition4() {
		final de.semvox.types.odp.spit.StringCondition stringCondition4 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string2 = replaceEntities("lang");
		stringCondition4.setAlgVariableReference(string2);
		String string3 = replaceEntities("en-US");
		stringCondition4.setCommonStringValue(string3);
		return stringCondition4;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs5() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs5 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs5.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string4 = replaceEntities("ok, just a moment, i will look up the latest news for you. ");
		generateStringAndOutputs5.getSpitAlternatives().add(string4);
		return generateStringAndOutputs5;
	}

	de.semvox.types.odp.spit.Case getCase6() {
		final de.semvox.types.odp.spit.Case case6 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition7 = getStringCondition7();
		case6.getSpitConditions().add(stringCondition7);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs8 = getGenerateStringAndOutputs8();
		case6.setSpitAction(generateStringAndOutputs8);
		return case6;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition7() {
		final de.semvox.types.odp.spit.StringCondition stringCondition7 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string5 = replaceEntities("lang");
		stringCondition7.setAlgVariableReference(string5);
		String string6 = replaceEntities("nl-BE");
		stringCondition7.setCommonStringValue(string6);
		return stringCondition7;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs8() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs8 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs8.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string7 = replaceEntities("Ok, ik zoek even het laatste nieuws voor je. ");
		generateStringAndOutputs8.getSpitAlternatives().add(string7);
		return generateStringAndOutputs8;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext9() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext9 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext9.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext9.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext9.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext9.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext9.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext9.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext9;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask10() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask10 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Accepted accepted11 = getAccepted11();
		reportOnTask10.setMmTaskStatus(accepted11);
		com.zorabots.ontologies.news.GetNews getNews12 = getGetNews12();
		reportOnTask10.setCommonContent(getNews12);
		return reportOnTask10;
	}

	de.semvox.types.odp.multimodal.Accepted getAccepted11() {
		final de.semvox.types.odp.multimodal.Accepted accepted11 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newAccepted();
		return accepted11;
	}

	com.zorabots.ontologies.news.GetNews getGetNews12() {
		final com.zorabots.ontologies.news.GetNews getNews12 = com.zorabots.ontologies.news.factory.PatternFactoryNews.newGetNews();
		getNews12.markMmTaskIdAsVariable("taskId", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		getNews12.assignMmTaskIdPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return getNews12;
	}

	de.semvox.types.odp.spit.Template getTemplate13() {
		final de.semvox.types.odp.spit.Template template13 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string8 = replaceEntities("ReportOnTask_getNews");
		template13.setCommonName(string8);
		de.semvox.types.odp.spit.Switch switch14 = getSwitch14();
		template13.setSpitAction(switch14);
		de.semvox.types.odp.interaction.GenerationContext generationContext21 = getGenerationContext21();
		template13.setSpitOnContext(generationContext21);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask22 = getReportOnTask22();
		template13.setSpitOnInput(reportOnTask22);
		return template13;
	}

	de.semvox.types.odp.spit.Switch getSwitch14() {
		final de.semvox.types.odp.spit.Switch switch14 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case15 = getCase15();
			switch14.getSpitCases().add(case15);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case18 = getCase18();
			switch14.getSpitCases().add(case18);
		}
		return switch14;
	}

	de.semvox.types.odp.spit.Case getCase15() {
		final de.semvox.types.odp.spit.Case case15 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition16 = getStringCondition16();
		case15.getSpitConditions().add(stringCondition16);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs17 = getGenerateStringAndOutputs17();
		case15.setSpitAction(generateStringAndOutputs17);
		return case15;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition16() {
		final de.semvox.types.odp.spit.StringCondition stringCondition16 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string9 = replaceEntities("lang");
		stringCondition16.setAlgVariableReference(string9);
		String string10 = replaceEntities("en-US");
		stringCondition16.setCommonStringValue(string10);
		return stringCondition16;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs17() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs17 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs17.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string11 = replaceEntities("This is the latest news, ?($MESS)");
		generateStringAndOutputs17.getSpitAlternatives().add(string11);
		return generateStringAndOutputs17;
	}

	de.semvox.types.odp.spit.Case getCase18() {
		final de.semvox.types.odp.spit.Case case18 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition19 = getStringCondition19();
		case18.getSpitConditions().add(stringCondition19);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs20 = getGenerateStringAndOutputs20();
		case18.setSpitAction(generateStringAndOutputs20);
		return case18;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition19() {
		final de.semvox.types.odp.spit.StringCondition stringCondition19 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string12 = replaceEntities("lang");
		stringCondition19.setAlgVariableReference(string12);
		String string13 = replaceEntities("nl-BE");
		stringCondition19.setCommonStringValue(string13);
		return stringCondition19;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs20() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs20 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs20.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string14 = replaceEntities("Dit is het laatste nieuws, ?($MESS)");
		generateStringAndOutputs20.getSpitAlternatives().add(string14);
		return generateStringAndOutputs20;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext21() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext21 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext21.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext21.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext21.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext21.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext21.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext21.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext21;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask22() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask22 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released23 = getReleased23();
		reportOnTask22.setMmTaskStatus(released23);
		return reportOnTask22;
	}

	de.semvox.types.odp.multimodal.Released getReleased23() {
		final de.semvox.types.odp.multimodal.Released released23 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.news.GetNews getNews24 = getGetNews24();
		released23.setMmTaskExecutionResult(getNews24);
		return released23;
	}

	com.zorabots.ontologies.news.GetNews getGetNews24() {
		final com.zorabots.ontologies.news.GetNews getNews24 = com.zorabots.ontologies.news.factory.PatternFactoryNews.newGetNews();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess25 = getStatusSuccess25();
		getNews24.setIntResponseStatus(statusSuccess25);
		getNews24.markCommonStringValueAsVariable("MESS", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		getNews24.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return getNews24;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess25() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess25 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess25;
	}
}