package com.zorabots.news.components;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.zorabots.news.components.gen.NewsServiceDefinition;
import com.zorabots.ontologies.news.Expired;
import com.zorabots.ontologies.news.GetNews;
import com.zorabots.ontologies.news.OnTime;
import com.zorabots.ontologies.news.factory.ThingFactoryNews;
import com.zorabots.socket.Client;
import com.zorabots.vmn.events.JsonParser;

import de.semvox.commons.logging.Logger.LogLevel;
import de.semvox.subcon.data.ThingUtils;
import de.semvox.subcon.integration.api.IpcFacade;
import de.semvox.subcon.integration.api.Service;
import de.semvox.types.odp.multimodal.AsyncServiceResponse;
import de.semvox.types.odp.multimodal.PresentEvent;
import de.semvox.types.odp.multimodal.factory.ThingFactoryMultimodal;
import de.semvox.types.subcon.integration.AsyncServiceRequest;
import de.semvox.types.subcon.integration.ServiceDefinition;
import de.semvox.types.subcon.integration.ServiceRequest;
import de.semvox.types.subcon.integration.factory.ThingFactoryIntegration;

public class NewsService implements Service {

	private final ServiceDefinition definition;
	private IpcFacade ipcFacade;

	public NewsService(IpcFacade ipcFacade) throws Exception {
		definition = new NewsServiceDefinition().loadThing();
		this.ipcFacade = ipcFacade;
	}

	@Override
	public ServiceDefinition getServiceDefinition() {
		return definition;
	}

	@Override
	public void dispose() {

	}

	@Override
	public ServiceRequest handleRequest(ServiceRequest request,
			Map<String, Object> boundVariables) {
		if(request instanceof AsyncServiceRequest){
			AsyncServiceRequest asyncServiceRequest = (AsyncServiceRequest)request;
			if(asyncServiceRequest instanceof GetNews){
				asyncServiceRequest.setIntResponseStatus(ThingFactoryIntegration.newStatusAccepted());
				final GetNews getNewsRequest = ThingUtils.deepCopy((GetNews)request);
    	        Client.getInstance().setNotLast(true);
				Thread thread = new Thread(){
		        	public void run(){
		        		try {
							Thread.sleep(5000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
		        		de.semvox.commons.logging.Logger.log(LogLevel.INFO, "THREAD", "Thread started!!");
		        		Boolean timeOut = false;
		        		URL url;
		    			String inputLine = "";
		    	        try {
		    	            // get URL content
		    	            String address = "http://zoraserver.cloudapp.net:3001/Nieuwsblad_Binnenlands_Nieuws.json";
		    	            url = new URL(address);
		    	            URLConnection conn = url.openConnection();

		    	            // open the stream and put it into BufferedReader
		    	            BufferedReader br = new BufferedReader(
		    	                               new InputStreamReader(conn.getInputStream()));
		    	            //de.semvox.commons.logging.Logger.log(LogLevel.INFO, "THREAD", "Stage 2");
		    	            inputLine = br.readLine();
		    	            //de.semvox.commons.logging.Logger.log(LogLevel.INFO, "THREAD", inputLine);
		    	            br.close();

		    	        } catch (MalformedURLException e) {
		    	            e.printStackTrace();
		    	        } catch (IOException e) {
		    	        	timeOut = true;
		    	            e.printStackTrace();
		    	        }
		    	        
		    	        String message = "";
		    	        String encmessage = "";
		    	        
		    	        if(!timeOut){
		    			//de.semvox.commons.logging.Logger.log(LogLevel.INFO, "json", inputLine);
		    			JSONObject jsonObject = JsonParser.parseJson(inputLine);
		    			//de.semvox.commons.logging.Logger.log(LogLevel.INFO, "MESSAGE", "Json parsed");
		    			JSONArray itemarray = (JSONArray) jsonObject.get("items");
		    			//de.semvox.commons.logging.Logger.log(LogLevel.INFO, "MESSAGE", "got item array");
		    			JSONObject itemobject0 = (JSONObject) itemarray.get(0);
		    			String title0 = ";hoofdpunt 1;" + (String)itemobject0.get("title");
		    			JSONObject itemobject1 = (JSONObject) itemarray.get(1);
		    			String title1 = ";hoofdpunt 2;" + (String)itemobject1.get("title");
		    			JSONObject itemobject2 = (JSONObject) itemarray.get(2);
		    			String title2 = ";hoofdpunt 3;" + (String)itemobject2.get("title");
		    			JSONObject itemobject3 = (JSONObject) itemarray.get(3);
		    			String title3 = ";hoofdpunt 4;" + (String)itemobject3.get("title");
		    			JSONObject itemobject4 = (JSONObject) itemarray.get(4);
		    			String title4 = ";hoofdpunt 5;" + (String)itemobject4.get("title");
		    			
		    			message = title0 + title1 + title2 + title3 + title4;
						try {
							message = message.replaceAll("”", " ");
							message = message.replaceAll("“", " ");
							message = message.replaceAll("\"", " ");
							message = message.replaceAll("-", " ");
							message = message.replaceAll(":", " ");
							encmessage = new String(message.getBytes("UTF-8"));
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		    	        }
		    			
		    			//de.semvox.commons.logging.Logger.log(LogLevel.INFO, "ENCMESSAGE", encmessage);
		    			
		    			getNewsRequest.setCommonStringValue(encmessage);
		    			getNewsRequest.setIntResponseStatus(ThingFactoryIntegration.newStatusSuccess());
		    			if (timeOut){
		    				Expired expired = ThingFactoryNews.newExpired();
		    				getNewsRequest.setNewsTimeOut(expired);
		    			}
		    			else {
		    				OnTime ontime = ThingFactoryNews.newOnTime();
		    				getNewsRequest.setNewsTimeOut(ontime);
		    			}
		    			
		    			PresentEvent presentEvent = ThingFactoryMultimodal.newPresentEvent();
		    	        presentEvent.setMmTimeToLive((long) 2000);
		    	         
		    	        AsyncServiceResponse asyncServiceResponse = ThingFactoryMultimodal.newAsyncServiceResponse();
		    	        asyncServiceResponse.setIntResponse(getNewsRequest);
		    	        asyncServiceResponse.setMmCorrelationId(getNewsRequest.getCommonId());
		    	         
		    	        presentEvent.setMmEvent(asyncServiceResponse);
		    	        ipcFacade.invokeService(presentEvent,
		    	                 String.format("%s.%s", definition.getIntNamespace(), definition.getCommonName()));
		        	}
		        };
		        thread.start();
			}
		}
		return request;
	}

}
