package com.zorabots.james;

import de.semvox.odp.s3.test.junit.PlatformTest;
import de.semvox.commons.io.locator.ResourceLocator.LocatorType;
import com.zorabots.james.specification.gen.JamesSpecification;
import de.semvox.types.odp.specification.Specification;

public class JamesPlatformTest extends PlatformTest {

	@Override
	protected LocatorType getRtDefLocatorType() {
		return LocatorType.file;
	}

	@Override
	protected String getRtDefLocation() {
		return "data/rt_test.xml";
	}

	@Override
	protected Specification getSpecification() {
		return new JamesSpecification().loadThing();
	}
}
