/**
* GENERATED CODE
*/
package com.zorabots.vmn.components.gen;

public final class VmnServiceDefinition extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.subcon.integration.ServiceDefinition> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "int#ServiceDefinition";

	public VmnServiceDefinition(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public VmnServiceDefinition(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public VmnServiceDefinition(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public VmnServiceDefinition() {
		super();
	}

	@Override
	protected de.semvox.types.subcon.integration.ServiceDefinition createThing() {
		return getServiceDefinition();
	}

	@Override
	protected void initialize(final de.semvox.types.subcon.integration.ServiceDefinition serviceDefinition0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("VmnService");
		serviceDefinition0.setCommonName(string0);
		String string1 = replaceEntities("com.zorabots.vmn.components");
		serviceDefinition0.setIntNamespace(string1);
		com.zorabots.ontologies.vmn.GetTemperature getTemperature1 = getGetTemperature1();
		serviceDefinition0.getIntSubscriptionsAsIntServiceRequest().put("GET_TEMPERATURE", getTemperature1);
		com.zorabots.ontologies.vmn.GetHi getHi2 = getGetHi2();
		serviceDefinition0.getIntSubscriptionsAsIntServiceRequest().put("GET_HI", getHi2);
		com.zorabots.ontologies.vmn.GetIntroduction getIntroduction3 = getGetIntroduction3();
		serviceDefinition0.getIntSubscriptionsAsIntServiceRequest().put("GET_INTRODUCTION", getIntroduction3);
		com.zorabots.ontologies.vmn.GetLeaveAlone getLeaveAlone4 = getGetLeaveAlone4();
		serviceDefinition0.getIntSubscriptionsAsIntServiceRequest().put("GET_LEAVE_ALONE", getLeaveAlone4);
		com.zorabots.ontologies.vmn.GetPlantDescription getPlantDescription5 = getGetPlantDescription5();
		serviceDefinition0.getIntSubscriptionsAsIntServiceRequest().put("GET_PLANT_DESCRIPTION", getPlantDescription5);
	}

	de.semvox.types.subcon.integration.ServiceDefinition getServiceDefinition() {
		final de.semvox.types.subcon.integration.ServiceDefinition serviceDefinition0 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newServiceDefinition();
		return serviceDefinition0;
	}


	com.zorabots.ontologies.vmn.GetTemperature getGetTemperature1() {
		final com.zorabots.ontologies.vmn.GetTemperature getTemperature1 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetTemperature();
		return getTemperature1;
	}

	com.zorabots.ontologies.vmn.GetHi getGetHi2() {
		final com.zorabots.ontologies.vmn.GetHi getHi2 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetHi();
		return getHi2;
	}

	com.zorabots.ontologies.vmn.GetIntroduction getGetIntroduction3() {
		final com.zorabots.ontologies.vmn.GetIntroduction getIntroduction3 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetIntroduction();
		return getIntroduction3;
	}

	com.zorabots.ontologies.vmn.GetLeaveAlone getGetLeaveAlone4() {
		final com.zorabots.ontologies.vmn.GetLeaveAlone getLeaveAlone4 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetLeaveAlone();
		return getLeaveAlone4;
	}

	com.zorabots.ontologies.vmn.GetPlantDescription getGetPlantDescription5() {
		final com.zorabots.ontologies.vmn.GetPlantDescription getPlantDescription5 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetPlantDescription();
		return getPlantDescription5;
	}
}