package com.zorabots.vmn.staticclasses;

import com.zorabots.ontologies.vmn.factory.ThingFactoryVmn;

public class YesNoAnswerType {
	public static com.zorabots.ontologies.vmn.YesNoQuestionAnswer getYesNoAnswerType(String str){
		switch(str){
		case "POSBAD":
			return (com.zorabots.ontologies.vmn.YesNoQuestionAnswer) ThingFactoryVmn.newPositiveBadAnswer();
		case "NEGBAD":
			return (com.zorabots.ontologies.vmn.YesNoQuestionAnswer) ThingFactoryVmn.newNegativeBadAnswer();
		case "GOOD":
			return (com.zorabots.ontologies.vmn.YesNoQuestionAnswer) ThingFactoryVmn.newGoodAnswer();
		}
		return null;
	}
}
