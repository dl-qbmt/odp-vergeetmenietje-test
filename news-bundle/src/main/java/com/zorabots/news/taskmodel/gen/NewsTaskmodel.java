/**
* GENERATED CODE
*/
package com.zorabots.news.taskmodel.gen;

public final class NewsTaskmodel extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.squint.TaskModel> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "squint#TaskModel";

	public NewsTaskmodel(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public NewsTaskmodel(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public NewsTaskmodel(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public NewsTaskmodel() {
		super();
	}

	@Override
	protected de.semvox.types.odp.squint.TaskModel createThing() {
		return getTaskModel();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.squint.TaskModel taskModel0, java.util.Map<String, Object> params) {

		de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition1 = getTaskExecutionDefinition1();
		taskModel0.getSquintTaskDefinitions().add(taskExecutionDefinition1);
	}

	de.semvox.types.odp.squint.TaskModel getTaskModel() {
		final de.semvox.types.odp.squint.TaskModel taskModel0 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newTaskModel();
		return taskModel0;
	}


	de.semvox.types.odp.squint.TaskExecutionDefinition getTaskExecutionDefinition1() {
		final de.semvox.types.odp.squint.TaskExecutionDefinition taskExecutionDefinition1 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newTaskExecutionDefinition();
		String string0 = replaceEntities("A task to get the news. ");
		taskExecutionDefinition1.setIntDescription(string0);
		String string1 = replaceEntities("true");
		taskExecutionDefinition1.setSquintUseHypothesisFusion(Boolean.parseBoolean(string1));
		String string2 = replaceEntities("GET_NEWS");
		taskExecutionDefinition1.setCommonName(string2);
		taskExecutionDefinition1.markMmTaskAsVariable("GET_NEWS", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		com.zorabots.ontologies.news.GetNews getNews2 = getGetNews2();
		taskExecutionDefinition1.setMmTask(getNews2);
		de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution3 = getNeverConfirmExecution3();
		taskExecutionDefinition1.setSquintTaskConfirmationDirective(neverConfirmExecution3);
		String string3 = replaceEntities("true");
		taskExecutionDefinition1.setSquintAllowCorrectLastStepIntoClosedSelectionSpaces(Boolean.parseBoolean(string3));
		taskExecutionDefinition1.assignSquintCancelationDirectivePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption4 = getCancelTaskOnInterruption4();
		taskExecutionDefinition1.setSquintInterruptionDirective(cancelTaskOnInterruption4);
		String string4 = replaceEntities("com/zorabots/news/taskmodel/NewsTaskmodel#GET_NEWS");
		taskExecutionDefinition1.setCommonId(string4);
		return taskExecutionDefinition1;
	}

	com.zorabots.ontologies.news.GetNews getGetNews2() {
		final com.zorabots.ontologies.news.GetNews getNews2 = com.zorabots.ontologies.news.factory.PatternFactoryNews.newGetNews();
		return getNews2;
	}

	de.semvox.types.odp.squint.NeverConfirmExecution getNeverConfirmExecution3() {
		final de.semvox.types.odp.squint.NeverConfirmExecution neverConfirmExecution3 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newNeverConfirmExecution();
		return neverConfirmExecution3;
	}

	de.semvox.types.odp.squint.CancelTaskOnInterruption getCancelTaskOnInterruption4() {
		final de.semvox.types.odp.squint.CancelTaskOnInterruption cancelTaskOnInterruption4 = de.semvox.types.odp.squint.factory.PatternFactorySquint.newCancelTaskOnInterruption();
		return cancelTaskOnInterruption4;
	}
}