/**
* GENERATED CODE
*/
package com.zorabots.news.nlu.gen;

public final class NewsGrammar extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.spijk.StandaloneGrammar> implements de.semvox.commons.Generated {

	private java.util.Map<java.lang.String, de.semvox.subcon.data.Thing> idRefMap = new java.util.HashMap<java.lang.String, de.semvox.subcon.data.Thing>();

	public static java.lang.String TYPE_NAME = "spijk#StandaloneGrammar";

	public NewsGrammar(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public NewsGrammar(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public NewsGrammar(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public NewsGrammar() {
		super();
	}

	@Override
	protected de.semvox.types.odp.spijk.StandaloneGrammar createThing() {
		return getStandaloneGrammar();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.spijk.StandaloneGrammar standaloneGrammar0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("NewsGrammar");
		standaloneGrammar0.setCommonName(string0);
		de.semvox.types.asr.MainGrammar mainGrammar1 = getMainGrammar1();
		standaloneGrammar0.setAsrGrammarType(mainGrammar1);
		standaloneGrammar0.assignSpijkImportsPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.spijk.Rule rule2 = getRule2();
		standaloneGrammar0.getSpijkElements().add(rule2);
	}

	de.semvox.types.odp.spijk.StandaloneGrammar getStandaloneGrammar() {
		final de.semvox.types.odp.spijk.StandaloneGrammar standaloneGrammar0 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newStandaloneGrammar();
		return standaloneGrammar0;
	}


	de.semvox.types.asr.MainGrammar getMainGrammar1() {
		final de.semvox.types.asr.MainGrammar mainGrammar1 = de.semvox.types.asr.factory.PatternFactoryAsr.newMainGrammar();
		return mainGrammar1;
	}

	de.semvox.types.odp.spijk.Rule getRule2() {
		final de.semvox.types.odp.spijk.Rule rule2 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string1 = replaceEntities("RequestTask_GetNews");
		rule2.setCommonName(string1);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern3 = getPattern3();
			rule2.getSpijkPatterns().add(pattern3);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern4 = getPattern4();
			rule2.getSpijkPatterns().add(pattern4);
		}
		de.semvox.types.odp.spijk.References references5 = getReferences5();
		rule2.setSpijkReferences(references5);
		de.semvox.types.odp.interaction.RequestTask requestTask6 = getRequestTask6();
		rule2.setMmInterpretation(requestTask6);
		return rule2;
	}

	de.semvox.types.odp.spijk.Pattern getPattern3() {
		final de.semvox.types.odp.spijk.Pattern pattern3 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string2 = replaceEntities("en-US");
		pattern3.setCommonLanguage(string2);
		String string3 = replaceEntities("whats the news for today");
		pattern3.getSpijkUtterances().add(string3);
		String string4 = replaceEntities("what are todays headlines");
		pattern3.getSpijkUtterances().add(string4);
		return pattern3;
	}

	de.semvox.types.odp.spijk.Pattern getPattern4() {
		final de.semvox.types.odp.spijk.Pattern pattern4 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string5 = replaceEntities("nl-BE");
		pattern4.setCommonLanguage(string5);
		String string6 = replaceEntities("vertel me het ?(laatste) nieuws ?(van vandaag)");
		pattern4.getSpijkUtterances().add(string6);
		String string7 = replaceEntities("wat is het ?(laatste) nieuws ?(van vandaag)");
		pattern4.getSpijkUtterances().add(string7);
		String string8 = replaceEntities("wat zijn de hoofdpunten van het ?(laatste) nieuws ?(van vandaag)");
		pattern4.getSpijkUtterances().add(string8);
		String string9 = replaceEntities("wat zijn de hoofdpunten van vandaag");
		pattern4.getSpijkUtterances().add(string9);
		return pattern4;
	}

	de.semvox.types.odp.spijk.References getReferences5() {
		final de.semvox.types.odp.spijk.References references5 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string10 = replaceEntities("");
		references5.setSpijkValue(string10);
		return references5;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask6() {
		final de.semvox.types.odp.interaction.RequestTask requestTask6 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetNews", requestTask6);
		com.zorabots.ontologies.news.GetNews getNews7 = getGetNews7();
		requestTask6.setCommonContent(getNews7);
		return requestTask6;
	}

	com.zorabots.ontologies.news.GetNews getGetNews7() {
		final com.zorabots.ontologies.news.GetNews getNews7 = com.zorabots.ontologies.news.factory.PatternFactoryNews.newGetNews();
		return getNews7;
	}
}