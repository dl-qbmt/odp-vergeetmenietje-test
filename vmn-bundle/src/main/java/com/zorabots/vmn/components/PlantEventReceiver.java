package com.zorabots.vmn.components;

import java.util.Map;

import org.json.simple.JSONObject;

import com.zorabots.ontologies.vmn.Injected;
import com.zorabots.ontologies.vmn.NewPotUpdate;
import com.zorabots.ontologies.vmn.Removed;
import com.zorabots.ontologies.vmn.factory.ThingFactoryVmn;
import com.zorabots.vmn.components.gen.PlantEventReceiverDefinition;
import com.zorabots.vmn.events.JsonParser;
import com.zorabots.vmn.pojo.Plant;

import de.semvox.commons.logging.Logger.LogLevel;
import de.semvox.subcon.integration.api.EventReceiver;
import de.semvox.subcon.integration.api.IpcFacade;
import de.semvox.types.common.Event;
import de.semvox.types.odp.multimodal.PresentEvent;
import de.semvox.types.odp.multimodal.factory.ThingFactoryMultimodal;
import de.semvox.types.subcon.integration.EventReceiverDefinition;
import de.semvox.types.subcon.integration.TokenEvent;


public class PlantEventReceiver implements EventReceiver {

	private final EventReceiverDefinition definition;
	private IpcFacade ipcFacade;

	public PlantEventReceiver() throws Exception {
		definition = new PlantEventReceiverDefinition().loadThing();
		this.ipcFacade = ipcFacade;
	}

	@Override
	public EventReceiverDefinition getEventReceiverDefinition() {
		return definition;
	}

	@Override
	public void dispose() {

	}

	@Override
	public void handleEvent(Event event, Map<String, Object> boundVariables) {
		// TODO Auto-generated method stub
		if(event instanceof NewPotUpdate){
			NewPotUpdate newPotUpdate = (NewPotUpdate) event;
			PresentEvent presentEvent = ThingFactoryMultimodal.newPresentEvent();
			presentEvent.setMmEvent(newPotUpdate);
			ipcFacade.invokeService(presentEvent, getClass().getCanonicalName());
		}

	}

	@Override
	public void handleToken(TokenEvent token, Map<String, Object> boundVariables) {
		// TODO Auto-generated method stub

	}
	
	public void handle(String json){
		
		de.semvox.commons.logging.Logger.log(LogLevel.INFO, "json", json);
		JSONObject jsonObject = JsonParser.parseJson(json);
		
		if (jsonObject.containsKey("plantname")){
			de.semvox.commons.logging.Logger.log(LogLevel.INFO, "PLANT", "Plant loaded!!!!!! " + json);
			Plant.getInstance().setPlant(json);
			de.semvox.commons.logging.Logger.log(LogLevel.INFO, "PLANT", "Plant loaded!!!!!! Name: " + Plant.getInstance().getName() + " Optimal temp: " + Plant.getInstance().getPlantParamters().get("temperature").getOptimal() + " Optimal humidity: " + Plant.getInstance().getPlantParamters().get("humidity").getOptimal());
			this.sendEventNewPlant((String) jsonObject.get("plantname"));
		}
		else{
			this.sendEventRemovedPlant();
		}
	}
	
	public void sendEventNewPlant(String plantname){
		NewPotUpdate newPotUpdate = ThingFactoryVmn.newNewPotUpdate();
		Injected injected = ThingFactoryVmn.newInjected();
		newPotUpdate.setVmnChange(injected);
		
		com.zorabots.ontologies.vmn.Plant plant = ThingFactoryVmn.newPlant();
		plant.setVmnPlantName(com.zorabots.vmn.pojo.Plant.getInstance().getName());
		
		newPotUpdate.setVmnPlant(plant);
		
		ipcFacade.sendEvent((Event) newPotUpdate, "zelve");
	}
	
	public void sendEventRemovedPlant(){
		NewPotUpdate newPotUpdate = ThingFactoryVmn.newNewPotUpdate();
		Removed removed = ThingFactoryVmn.newRemoved();
		newPotUpdate.setVmnChange(removed);
		com.zorabots.ontologies.vmn.Plant plant = ThingFactoryVmn.newPlant();
		newPotUpdate.setVmnPlant(plant);
		ipcFacade.sendEvent((Event) newPotUpdate, "zelve");
	}

}
