/**
* GENERATED CODE
*/
package com.zorabots.vmn.nlu.gen;

public final class VmnGrammar extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.spijk.StandaloneGrammar> implements de.semvox.commons.Generated {

	private java.util.Map<java.lang.String, de.semvox.subcon.data.Thing> idRefMap = new java.util.HashMap<java.lang.String, de.semvox.subcon.data.Thing>();

	public static java.lang.String TYPE_NAME = "spijk#StandaloneGrammar";

	public VmnGrammar(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public VmnGrammar(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public VmnGrammar(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public VmnGrammar() {
		super();
	}

	@Override
	protected de.semvox.types.odp.spijk.StandaloneGrammar createThing() {
		return getStandaloneGrammar();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.spijk.StandaloneGrammar standaloneGrammar0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("VmnGrammar");
		standaloneGrammar0.setCommonName(string0);
		de.semvox.types.asr.MainGrammar mainGrammar1 = getMainGrammar1();
		standaloneGrammar0.setAsrGrammarType(mainGrammar1);
		de.semvox.subcon.data.Thing include2 = getThing2();
		standaloneGrammar0.getSpijkImports().add((de.semvox.types.odp.spijk.Grammar)include2);
		de.semvox.subcon.data.Thing include3 = getThing3();
		standaloneGrammar0.getSpijkImports().add((de.semvox.types.odp.spijk.Grammar)include3);
		de.semvox.subcon.data.Thing include4 = getThing4();
		standaloneGrammar0.getSpijkImports().add((de.semvox.types.odp.spijk.Grammar)include4);
		de.semvox.types.odp.spijk.Rule rule5 = getRule5();
		standaloneGrammar0.getSpijkElements().add(rule5);
		de.semvox.types.odp.spijk.Rule rule10 = getRule10();
		standaloneGrammar0.getSpijkElements().add(rule10);
		de.semvox.types.odp.spijk.Rule rule16 = getRule16();
		standaloneGrammar0.getSpijkElements().add(rule16);
		de.semvox.types.odp.spijk.Rule rule22 = getRule22();
		standaloneGrammar0.getSpijkElements().add(rule22);
		de.semvox.types.odp.spijk.Rule rule28 = getRule28();
		standaloneGrammar0.getSpijkElements().add(rule28);
		de.semvox.types.odp.spijk.Rule rule34 = getRule34();
		standaloneGrammar0.getSpijkElements().add(rule34);
		de.semvox.types.odp.spijk.Rule rule40 = getRule40();
		standaloneGrammar0.getSpijkElements().add(rule40);
		de.semvox.types.odp.spijk.Rule rule46 = getRule46();
		standaloneGrammar0.getSpijkElements().add(rule46);
		de.semvox.types.odp.spijk.Rule rule52 = getRule52();
		standaloneGrammar0.getSpijkElements().add(rule52);
		de.semvox.types.odp.spijk.Rule rule58 = getRule58();
		standaloneGrammar0.getSpijkElements().add(rule58);
		de.semvox.types.odp.spijk.Rule rule64 = getRule64();
		standaloneGrammar0.getSpijkElements().add(rule64);
		de.semvox.types.odp.spijk.Rule rule70 = getRule70();
		standaloneGrammar0.getSpijkElements().add(rule70);
		de.semvox.types.odp.spijk.Rule rule76 = getRule76();
		standaloneGrammar0.getSpijkElements().add(rule76);
	}

	de.semvox.types.odp.spijk.StandaloneGrammar getStandaloneGrammar() {
		final de.semvox.types.odp.spijk.StandaloneGrammar standaloneGrammar0 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newStandaloneGrammar();
		return standaloneGrammar0;
	}


	de.semvox.types.asr.MainGrammar getMainGrammar1() {
		final de.semvox.types.asr.MainGrammar mainGrammar1 = de.semvox.types.asr.factory.PatternFactoryAsr.newMainGrammar();
		return mainGrammar1;
	}

	de.semvox.subcon.data.Thing getThing2() {
		java.util.Map<java.lang.String, java.lang.String> newEntities = new java.util.HashMap<java.lang.String, java.lang.String>(entities);
		de.semvox.subcon.data.Thing include2 = new com.zorabots.vmn.nlu.gen.VmnHumidityGrammar(getParameterProvider(), newEntities).loadThing(false);
		return include2;
	}

	de.semvox.subcon.data.Thing getThing3() {
		java.util.Map<java.lang.String, java.lang.String> newEntities = new java.util.HashMap<java.lang.String, java.lang.String>(entities);
		de.semvox.subcon.data.Thing include3 = new com.zorabots.vmn.nlu.gen.VmnLightGrammar(getParameterProvider(), newEntities).loadThing(false);
		return include3;
	}

	de.semvox.subcon.data.Thing getThing4() {
		java.util.Map<java.lang.String, java.lang.String> newEntities = new java.util.HashMap<java.lang.String, java.lang.String>(entities);
		de.semvox.subcon.data.Thing include4 = new com.zorabots.vmn.nlu.gen.VmnTemperatureGrammar(getParameterProvider(), newEntities).loadThing(false);
		return include4;
	}

	de.semvox.types.odp.spijk.Rule getRule5() {
		final de.semvox.types.odp.spijk.Rule rule5 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string1 = replaceEntities("RequestRepeat");
		rule5.setCommonName(string1);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern6 = getPattern6();
			rule5.getSpijkPatterns().add(pattern6);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern7 = getPattern7();
			rule5.getSpijkPatterns().add(pattern7);
		}
		de.semvox.types.odp.spijk.References references8 = getReferences8();
		rule5.setSpijkReferences(references8);
		de.semvox.types.odp.interaction.RequestRepeat requestRepeat9 = getRequestRepeat9();
		rule5.setMmInterpretation(requestRepeat9);
		return rule5;
	}

	de.semvox.types.odp.spijk.Pattern getPattern6() {
		final de.semvox.types.odp.spijk.Pattern pattern6 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string2 = replaceEntities("en-US");
		pattern6.setCommonLanguage(string2);
		String string3 = replaceEntities("could you repeat ?(please)");
		pattern6.getSpijkUtterances().add(string3);
		return pattern6;
	}

	de.semvox.types.odp.spijk.Pattern getPattern7() {
		final de.semvox.types.odp.spijk.Pattern pattern7 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string4 = replaceEntities("nl-BE");
		pattern7.setCommonLanguage(string4);
		String string5 = replaceEntities("kun je ?(even) herhalen ?(alstublieft|alsjeblief)");
		pattern7.getSpijkUtterances().add(string5);
		return pattern7;
	}

	de.semvox.types.odp.spijk.References getReferences8() {
		final de.semvox.types.odp.spijk.References references8 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string6 = replaceEntities("");
		references8.setSpijkValue(string6);
		return references8;
	}

	de.semvox.types.odp.interaction.RequestRepeat getRequestRepeat9() {
		final de.semvox.types.odp.interaction.RequestRepeat requestRepeat9 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestRepeat();
		idRefMap.put("__RequestRepeat", requestRepeat9);
		return requestRepeat9;
	}

	de.semvox.types.odp.spijk.Rule getRule10() {
		final de.semvox.types.odp.spijk.Rule rule10 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string7 = replaceEntities("Hello");
		rule10.setCommonName(string7);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern11 = getPattern11();
			rule10.getSpijkPatterns().add(pattern11);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern12 = getPattern12();
			rule10.getSpijkPatterns().add(pattern12);
		}
		de.semvox.types.odp.spijk.References references13 = getReferences13();
		rule10.setSpijkReferences(references13);
		de.semvox.types.odp.interaction.RequestTask requestTask14 = getRequestTask14();
		rule10.setMmInterpretation(requestTask14);
		return rule10;
	}

	de.semvox.types.odp.spijk.Pattern getPattern11() {
		final de.semvox.types.odp.spijk.Pattern pattern11 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string8 = replaceEntities("en-US");
		pattern11.setCommonLanguage(string8);
		String string9 = replaceEntities("hi billy");
		pattern11.getSpijkUtterances().add(string9);
		return pattern11;
	}

	de.semvox.types.odp.spijk.Pattern getPattern12() {
		final de.semvox.types.odp.spijk.Pattern pattern12 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string10 = replaceEntities("nl-BE");
		pattern12.setCommonLanguage(string10);
		String string11 = replaceEntities("dag billy");
		pattern12.getSpijkUtterances().add(string11);
		return pattern12;
	}

	de.semvox.types.odp.spijk.References getReferences13() {
		final de.semvox.types.odp.spijk.References references13 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string12 = replaceEntities("");
		references13.setSpijkValue(string12);
		return references13;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask14() {
		final de.semvox.types.odp.interaction.RequestTask requestTask14 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__Hello", requestTask14);
		com.zorabots.ontologies.vmn.GetHi getHi15 = getGetHi15();
		requestTask14.setCommonContent(getHi15);
		return requestTask14;
	}

	com.zorabots.ontologies.vmn.GetHi getGetHi15() {
		final com.zorabots.ontologies.vmn.GetHi getHi15 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetHi();
		return getHi15;
	}

	de.semvox.types.odp.spijk.Rule getRule16() {
		final de.semvox.types.odp.spijk.Rule rule16 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string13 = replaceEntities("RequestTask_GetIntroduction");
		rule16.setCommonName(string13);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern17 = getPattern17();
			rule16.getSpijkPatterns().add(pattern17);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern18 = getPattern18();
			rule16.getSpijkPatterns().add(pattern18);
		}
		de.semvox.types.odp.spijk.References references19 = getReferences19();
		rule16.setSpijkReferences(references19);
		de.semvox.types.odp.interaction.RequestTask requestTask20 = getRequestTask20();
		rule16.setMmInterpretation(requestTask20);
		return rule16;
	}

	de.semvox.types.odp.spijk.Pattern getPattern17() {
		final de.semvox.types.odp.spijk.Pattern pattern17 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string14 = replaceEntities("en-US");
		pattern17.setCommonLanguage(string14);
		String string15 = replaceEntities("introduce yourself");
		pattern17.getSpijkUtterances().add(string15);
		return pattern17;
	}

	de.semvox.types.odp.spijk.Pattern getPattern18() {
		final de.semvox.types.odp.spijk.Pattern pattern18 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string16 = replaceEntities("nl-BE");
		pattern18.setCommonLanguage(string16);
		String string17 = replaceEntities("stel jezelf ?(eens) voor");
		pattern18.getSpijkUtterances().add(string17);
		return pattern18;
	}

	de.semvox.types.odp.spijk.References getReferences19() {
		final de.semvox.types.odp.spijk.References references19 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string18 = replaceEntities("");
		references19.setSpijkValue(string18);
		return references19;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask20() {
		final de.semvox.types.odp.interaction.RequestTask requestTask20 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetIntroduction", requestTask20);
		com.zorabots.ontologies.vmn.GetIntroduction getIntroduction21 = getGetIntroduction21();
		requestTask20.setCommonContent(getIntroduction21);
		return requestTask20;
	}

	com.zorabots.ontologies.vmn.GetIntroduction getGetIntroduction21() {
		final com.zorabots.ontologies.vmn.GetIntroduction getIntroduction21 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetIntroduction();
		return getIntroduction21;
	}

	de.semvox.types.odp.spijk.Rule getRule22() {
		final de.semvox.types.odp.spijk.Rule rule22 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string19 = replaceEntities("RequestTask_GetLeaveAlone");
		rule22.setCommonName(string19);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern23 = getPattern23();
			rule22.getSpijkPatterns().add(pattern23);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern24 = getPattern24();
			rule22.getSpijkPatterns().add(pattern24);
		}
		de.semvox.types.odp.spijk.References references25 = getReferences25();
		rule22.setSpijkReferences(references25);
		de.semvox.types.odp.interaction.RequestTask requestTask26 = getRequestTask26();
		rule22.setMmInterpretation(requestTask26);
		return rule22;
	}

	de.semvox.types.odp.spijk.Pattern getPattern23() {
		final de.semvox.types.odp.spijk.Pattern pattern23 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string20 = replaceEntities("en-US");
		pattern23.setCommonLanguage(string20);
		String string21 = replaceEntities("i\\'m going to leave you for a while");
		pattern23.getSpijkUtterances().add(string21);
		return pattern23;
	}

	de.semvox.types.odp.spijk.Pattern getPattern24() {
		final de.semvox.types.odp.spijk.Pattern pattern24 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string22 = replaceEntities("nl-BE");
		pattern24.setCommonLanguage(string22);
		String string23 = replaceEntities("ik ga je even alleen laten");
		pattern24.getSpijkUtterances().add(string23);
		return pattern24;
	}

	de.semvox.types.odp.spijk.References getReferences25() {
		final de.semvox.types.odp.spijk.References references25 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string24 = replaceEntities("");
		references25.setSpijkValue(string24);
		return references25;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask26() {
		final de.semvox.types.odp.interaction.RequestTask requestTask26 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetLeaveAlone", requestTask26);
		com.zorabots.ontologies.vmn.GetLeaveAlone getLeaveAlone27 = getGetLeaveAlone27();
		requestTask26.setCommonContent(getLeaveAlone27);
		return requestTask26;
	}

	com.zorabots.ontologies.vmn.GetLeaveAlone getGetLeaveAlone27() {
		final com.zorabots.ontologies.vmn.GetLeaveAlone getLeaveAlone27 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetLeaveAlone();
		return getLeaveAlone27;
	}

	de.semvox.types.odp.spijk.Rule getRule28() {
		final de.semvox.types.odp.spijk.Rule rule28 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string25 = replaceEntities("RequestTask_GetTime");
		rule28.setCommonName(string25);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern29 = getPattern29();
			rule28.getSpijkPatterns().add(pattern29);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern30 = getPattern30();
			rule28.getSpijkPatterns().add(pattern30);
		}
		de.semvox.types.odp.spijk.References references31 = getReferences31();
		rule28.setSpijkReferences(references31);
		de.semvox.types.odp.interaction.RequestTask requestTask32 = getRequestTask32();
		rule28.setMmInterpretation(requestTask32);
		return rule28;
	}

	de.semvox.types.odp.spijk.Pattern getPattern29() {
		final de.semvox.types.odp.spijk.Pattern pattern29 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string26 = replaceEntities("en-US");
		pattern29.setCommonLanguage(string26);
		String string27 = replaceEntities("what(s| is) the time");
		pattern29.getSpijkUtterances().add(string27);
		String string28 = replaceEntities("how late is it");
		pattern29.getSpijkUtterances().add(string28);
		return pattern29;
	}

	de.semvox.types.odp.spijk.Pattern getPattern30() {
		final de.semvox.types.odp.spijk.Pattern pattern30 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string29 = replaceEntities("nl-BE");
		pattern30.setCommonLanguage(string29);
		String string30 = replaceEntities("hoe laat is het");
		pattern30.getSpijkUtterances().add(string30);
		return pattern30;
	}

	de.semvox.types.odp.spijk.References getReferences31() {
		final de.semvox.types.odp.spijk.References references31 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string31 = replaceEntities("");
		references31.setSpijkValue(string31);
		return references31;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask32() {
		final de.semvox.types.odp.interaction.RequestTask requestTask32 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetTime", requestTask32);
		com.zorabots.ontologies.vmn.GetTime getTime33 = getGetTime33();
		requestTask32.setCommonContent(getTime33);
		return requestTask32;
	}

	com.zorabots.ontologies.vmn.GetTime getGetTime33() {
		final com.zorabots.ontologies.vmn.GetTime getTime33 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetTime();
		return getTime33;
	}

	de.semvox.types.odp.spijk.Rule getRule34() {
		final de.semvox.types.odp.spijk.Rule rule34 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string32 = replaceEntities("RequestTask_GetDate");
		rule34.setCommonName(string32);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern35 = getPattern35();
			rule34.getSpijkPatterns().add(pattern35);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern36 = getPattern36();
			rule34.getSpijkPatterns().add(pattern36);
		}
		de.semvox.types.odp.spijk.References references37 = getReferences37();
		rule34.setSpijkReferences(references37);
		de.semvox.types.odp.interaction.RequestTask requestTask38 = getRequestTask38();
		rule34.setMmInterpretation(requestTask38);
		return rule34;
	}

	de.semvox.types.odp.spijk.Pattern getPattern35() {
		final de.semvox.types.odp.spijk.Pattern pattern35 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string33 = replaceEntities("en-US");
		pattern35.setCommonLanguage(string33);
		String string34 = replaceEntities("which day are we ?(today)");
		pattern35.getSpijkUtterances().add(string34);
		String string35 = replaceEntities("what is the date ?(today)");
		pattern35.getSpijkUtterances().add(string35);
		return pattern35;
	}

	de.semvox.types.odp.spijk.Pattern getPattern36() {
		final de.semvox.types.odp.spijk.Pattern pattern36 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string36 = replaceEntities("nl-BE");
		pattern36.setCommonLanguage(string36);
		String string37 = replaceEntities("welke dag (zijn we|is het) ?(vandaag)");
		pattern36.getSpijkUtterances().add(string37);
		String string38 = replaceEntities("wat is de datum van vandaag");
		pattern36.getSpijkUtterances().add(string38);
		return pattern36;
	}

	de.semvox.types.odp.spijk.References getReferences37() {
		final de.semvox.types.odp.spijk.References references37 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string39 = replaceEntities("");
		references37.setSpijkValue(string39);
		return references37;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask38() {
		final de.semvox.types.odp.interaction.RequestTask requestTask38 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetDate", requestTask38);
		com.zorabots.ontologies.vmn.GetDate getDate39 = getGetDate39();
		requestTask38.setCommonContent(getDate39);
		return requestTask38;
	}

	com.zorabots.ontologies.vmn.GetDate getGetDate39() {
		final com.zorabots.ontologies.vmn.GetDate getDate39 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetDate();
		return getDate39;
	}

	de.semvox.types.odp.spijk.Rule getRule40() {
		final de.semvox.types.odp.spijk.Rule rule40 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string40 = replaceEntities("RequestTask_GetIpAddress");
		rule40.setCommonName(string40);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern41 = getPattern41();
			rule40.getSpijkPatterns().add(pattern41);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern42 = getPattern42();
			rule40.getSpijkPatterns().add(pattern42);
		}
		de.semvox.types.odp.spijk.References references43 = getReferences43();
		rule40.setSpijkReferences(references43);
		de.semvox.types.odp.interaction.RequestTask requestTask44 = getRequestTask44();
		rule40.setMmInterpretation(requestTask44);
		return rule40;
	}

	de.semvox.types.odp.spijk.Pattern getPattern41() {
		final de.semvox.types.odp.spijk.Pattern pattern41 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string41 = replaceEntities("en-US");
		pattern41.setCommonLanguage(string41);
		String string42 = replaceEntities("what is your i pee ?(address|dress)");
		pattern41.getSpijkUtterances().add(string42);
		return pattern41;
	}

	de.semvox.types.odp.spijk.Pattern getPattern42() {
		final de.semvox.types.odp.spijk.Pattern pattern42 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string43 = replaceEntities("nl-BE");
		pattern42.setCommonLanguage(string43);
		String string44 = replaceEntities("wat is (jouw|je) i pee ?(adres)");
		pattern42.getSpijkUtterances().add(string44);
		return pattern42;
	}

	de.semvox.types.odp.spijk.References getReferences43() {
		final de.semvox.types.odp.spijk.References references43 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string45 = replaceEntities("");
		references43.setSpijkValue(string45);
		return references43;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask44() {
		final de.semvox.types.odp.interaction.RequestTask requestTask44 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetIpAddress", requestTask44);
		com.zorabots.ontologies.vmn.GetIpAddress getIpAddress45 = getGetIpAddress45();
		requestTask44.setCommonContent(getIpAddress45);
		return requestTask44;
	}

	com.zorabots.ontologies.vmn.GetIpAddress getGetIpAddress45() {
		final com.zorabots.ontologies.vmn.GetIpAddress getIpAddress45 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetIpAddress();
		return getIpAddress45;
	}

	de.semvox.types.odp.spijk.Rule getRule46() {
		final de.semvox.types.odp.spijk.Rule rule46 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string46 = replaceEntities("RequestTask_GetPlantName");
		rule46.setCommonName(string46);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern47 = getPattern47();
			rule46.getSpijkPatterns().add(pattern47);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern48 = getPattern48();
			rule46.getSpijkPatterns().add(pattern48);
		}
		de.semvox.types.odp.spijk.References references49 = getReferences49();
		rule46.setSpijkReferences(references49);
		de.semvox.types.odp.interaction.RequestTask requestTask50 = getRequestTask50();
		rule46.setMmInterpretation(requestTask50);
		return rule46;
	}

	de.semvox.types.odp.spijk.Pattern getPattern47() {
		final de.semvox.types.odp.spijk.Pattern pattern47 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string47 = replaceEntities("en-US");
		pattern47.setCommonLanguage(string47);
		String string48 = replaceEntities("what is your plant name");
		pattern47.getSpijkUtterances().add(string48);
		String string49 = replaceEntities("what kind of plant are you");
		pattern47.getSpijkUtterances().add(string49);
		String string50 = replaceEntities("for what ?(kind of) plant are you taking care ?(today)");
		pattern47.getSpijkUtterances().add(string50);
		String string51 = replaceEntities("what ?(kind of) plant are you taking care of ?(today)");
		pattern47.getSpijkUtterances().add(string51);
		return pattern47;
	}

	de.semvox.types.odp.spijk.Pattern getPattern48() {
		final de.semvox.types.odp.spijk.Pattern pattern48 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string52 = replaceEntities("nl-BE");
		pattern48.setCommonLanguage(string52);
		String string53 = replaceEntities("wat is je plant naam");
		pattern48.getSpijkUtterances().add(string53);
		String string54 = replaceEntities("welke plant heb je nu");
		pattern48.getSpijkUtterances().add(string54);
		String string55 = replaceEntities("voor welke plant zorg je ?(nu)");
		pattern48.getSpijkUtterances().add(string55);
		return pattern48;
	}

	de.semvox.types.odp.spijk.References getReferences49() {
		final de.semvox.types.odp.spijk.References references49 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string56 = replaceEntities("");
		references49.setSpijkValue(string56);
		return references49;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask50() {
		final de.semvox.types.odp.interaction.RequestTask requestTask50 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetPlantName", requestTask50);
		com.zorabots.ontologies.vmn.GetPlantName getPlantName51 = getGetPlantName51();
		requestTask50.setCommonContent(getPlantName51);
		return requestTask50;
	}

	com.zorabots.ontologies.vmn.GetPlantName getGetPlantName51() {
		final com.zorabots.ontologies.vmn.GetPlantName getPlantName51 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetPlantName();
		return getPlantName51;
	}

	de.semvox.types.odp.spijk.Rule getRule52() {
		final de.semvox.types.odp.spijk.Rule rule52 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string57 = replaceEntities("RequestTask_GetParameters");
		rule52.setCommonName(string57);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern53 = getPattern53();
			rule52.getSpijkPatterns().add(pattern53);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern54 = getPattern54();
			rule52.getSpijkPatterns().add(pattern54);
		}
		de.semvox.types.odp.spijk.References references55 = getReferences55();
		rule52.setSpijkReferences(references55);
		de.semvox.types.odp.interaction.RequestTask requestTask56 = getRequestTask56();
		rule52.setMmInterpretation(requestTask56);
		return rule52;
	}

	de.semvox.types.odp.spijk.Pattern getPattern53() {
		final de.semvox.types.odp.spijk.Pattern pattern53 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string58 = replaceEntities("en-US");
		pattern53.setCommonLanguage(string58);
		String string59 = replaceEntities("what are your ?(optimal) parameters");
		pattern53.getSpijkUtterances().add(string59);
		String string60 = replaceEntities("how should i take care of (you|(the|your|this) plant)");
		pattern53.getSpijkUtterances().add(string60);
		String string61 = replaceEntities("what is the best way to take care of (you|(the|your|this) plant)");
		pattern53.getSpijkUtterances().add(string61);
		return pattern53;
	}

	de.semvox.types.odp.spijk.Pattern getPattern54() {
		final de.semvox.types.odp.spijk.Pattern pattern54 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string62 = replaceEntities("nl-BE");
		pattern54.setCommonLanguage(string62);
		String string63 = replaceEntities("wat zijn je exacte parameters");
		pattern54.getSpijkUtterances().add(string63);
		String string64 = replaceEntities("hoe moet ik je ?(plant) verzorgen");
		pattern54.getSpijkUtterances().add(string64);
		String string65 = replaceEntities("hoe verzorg ik de plant het best");
		pattern54.getSpijkUtterances().add(string65);
		String string66 = replaceEntities("waar moet ik op letten");
		pattern54.getSpijkUtterances().add(string66);
		return pattern54;
	}

	de.semvox.types.odp.spijk.References getReferences55() {
		final de.semvox.types.odp.spijk.References references55 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string67 = replaceEntities("");
		references55.setSpijkValue(string67);
		return references55;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask56() {
		final de.semvox.types.odp.interaction.RequestTask requestTask56 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetParameters", requestTask56);
		com.zorabots.ontologies.vmn.GetParameters getParameters57 = getGetParameters57();
		requestTask56.setCommonContent(getParameters57);
		return requestTask56;
	}

	com.zorabots.ontologies.vmn.GetParameters getGetParameters57() {
		final com.zorabots.ontologies.vmn.GetParameters getParameters57 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetParameters();
		return getParameters57;
	}

	de.semvox.types.odp.spijk.Rule getRule58() {
		final de.semvox.types.odp.spijk.Rule rule58 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string68 = replaceEntities("RequestTask_GetPlantDescription");
		rule58.setCommonName(string68);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern59 = getPattern59();
			rule58.getSpijkPatterns().add(pattern59);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern60 = getPattern60();
			rule58.getSpijkPatterns().add(pattern60);
		}
		de.semvox.types.odp.spijk.References references61 = getReferences61();
		rule58.setSpijkReferences(references61);
		de.semvox.types.odp.interaction.RequestTask requestTask62 = getRequestTask62();
		rule58.setMmInterpretation(requestTask62);
		return rule58;
	}

	de.semvox.types.odp.spijk.Pattern getPattern59() {
		final de.semvox.types.odp.spijk.Pattern pattern59 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string69 = replaceEntities("en-US");
		pattern59.setCommonLanguage(string69);
		String string70 = replaceEntities("what is the description of (the|your|this) plant");
		pattern59.getSpijkUtterances().add(string70);
		String string71 = replaceEntities("what is ?(so) special about (you|(the|your|this) plant)");
		pattern59.getSpijkUtterances().add(string71);
		return pattern59;
	}

	de.semvox.types.odp.spijk.Pattern getPattern60() {
		final de.semvox.types.odp.spijk.Pattern pattern60 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string72 = replaceEntities("nl-BE");
		pattern60.setCommonLanguage(string72);
		String string73 = replaceEntities("wat is de beschrijving van de plant");
		pattern60.getSpijkUtterances().add(string73);
		String string74 = replaceEntities("wat is er speciaal aan deze plant");
		pattern60.getSpijkUtterances().add(string74);
		String string75 = replaceEntities("wat weet je ?(allemaal) over deze plant");
		pattern60.getSpijkUtterances().add(string75);
		return pattern60;
	}

	de.semvox.types.odp.spijk.References getReferences61() {
		final de.semvox.types.odp.spijk.References references61 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string76 = replaceEntities("");
		references61.setSpijkValue(string76);
		return references61;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask62() {
		final de.semvox.types.odp.interaction.RequestTask requestTask62 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetPlantDescription", requestTask62);
		com.zorabots.ontologies.vmn.GetPlantDescription getPlantDescription63 = getGetPlantDescription63();
		requestTask62.setCommonContent(getPlantDescription63);
		return requestTask62;
	}

	com.zorabots.ontologies.vmn.GetPlantDescription getGetPlantDescription63() {
		final com.zorabots.ontologies.vmn.GetPlantDescription getPlantDescription63 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetPlantDescription();
		return getPlantDescription63;
	}

	de.semvox.types.odp.spijk.Rule getRule64() {
		final de.semvox.types.odp.spijk.Rule rule64 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string77 = replaceEntities("RequestTask_GetOptimalPlantHumidity");
		rule64.setCommonName(string77);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern65 = getPattern65();
			rule64.getSpijkPatterns().add(pattern65);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern66 = getPattern66();
			rule64.getSpijkPatterns().add(pattern66);
		}
		de.semvox.types.odp.spijk.References references67 = getReferences67();
		rule64.setSpijkReferences(references67);
		de.semvox.types.odp.interaction.RequestTask requestTask68 = getRequestTask68();
		rule64.setMmInterpretation(requestTask68);
		return rule64;
	}

	de.semvox.types.odp.spijk.Pattern getPattern65() {
		final de.semvox.types.odp.spijk.Pattern pattern65 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string78 = replaceEntities("en-US");
		pattern65.setCommonLanguage(string78);
		String string79 = replaceEntities("what is (your|the) optimal humidity ?(for your soil|for your ground)");
		pattern65.getSpijkUtterances().add(string79);
		return pattern65;
	}

	de.semvox.types.odp.spijk.Pattern getPattern66() {
		final de.semvox.types.odp.spijk.Pattern pattern66 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string80 = replaceEntities("nl-BE");
		pattern66.setCommonLanguage(string80);
		String string81 = replaceEntities("((wat|hoe) is je|hoeveel bedraagt je) optimale vochtigheid?(sgraad)");
		pattern66.getSpijkUtterances().add(string81);
		return pattern66;
	}

	de.semvox.types.odp.spijk.References getReferences67() {
		final de.semvox.types.odp.spijk.References references67 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string82 = replaceEntities("");
		references67.setSpijkValue(string82);
		return references67;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask68() {
		final de.semvox.types.odp.interaction.RequestTask requestTask68 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetOptimalPlantHumidity", requestTask68);
		com.zorabots.ontologies.vmn.GetPlantHumidityParamater getPlantHumidityParamater69 = getGetPlantHumidityParamater69();
		requestTask68.setCommonContent(getPlantHumidityParamater69);
		return requestTask68;
	}

	com.zorabots.ontologies.vmn.GetPlantHumidityParamater getGetPlantHumidityParamater69() {
		final com.zorabots.ontologies.vmn.GetPlantHumidityParamater getPlantHumidityParamater69 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetPlantHumidityParamater();
		return getPlantHumidityParamater69;
	}

	de.semvox.types.odp.spijk.Rule getRule70() {
		final de.semvox.types.odp.spijk.Rule rule70 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string83 = replaceEntities("RequestTask_GetOptimalPlantLight");
		rule70.setCommonName(string83);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern71 = getPattern71();
			rule70.getSpijkPatterns().add(pattern71);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern72 = getPattern72();
			rule70.getSpijkPatterns().add(pattern72);
		}
		de.semvox.types.odp.spijk.References references73 = getReferences73();
		rule70.setSpijkReferences(references73);
		de.semvox.types.odp.interaction.RequestTask requestTask74 = getRequestTask74();
		rule70.setMmInterpretation(requestTask74);
		return rule70;
	}

	de.semvox.types.odp.spijk.Pattern getPattern71() {
		final de.semvox.types.odp.spijk.Pattern pattern71 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string84 = replaceEntities("en-US");
		pattern71.setCommonLanguage(string84);
		String string85 = replaceEntities("what is (your|the) optimal light intensity");
		pattern71.getSpijkUtterances().add(string85);
		return pattern71;
	}

	de.semvox.types.odp.spijk.Pattern getPattern72() {
		final de.semvox.types.odp.spijk.Pattern pattern72 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string86 = replaceEntities("nl-BE");
		pattern72.setCommonLanguage(string86);
		String string87 = replaceEntities("((wat|hoe) is je|hoeveel bedraagt je) optimale licht intensiteit");
		pattern72.getSpijkUtterances().add(string87);
		return pattern72;
	}

	de.semvox.types.odp.spijk.References getReferences73() {
		final de.semvox.types.odp.spijk.References references73 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string88 = replaceEntities("");
		references73.setSpijkValue(string88);
		return references73;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask74() {
		final de.semvox.types.odp.interaction.RequestTask requestTask74 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetOptimalPlantLight", requestTask74);
		com.zorabots.ontologies.vmn.GetPlantLightParamater getPlantLightParamater75 = getGetPlantLightParamater75();
		requestTask74.setCommonContent(getPlantLightParamater75);
		return requestTask74;
	}

	com.zorabots.ontologies.vmn.GetPlantLightParamater getGetPlantLightParamater75() {
		final com.zorabots.ontologies.vmn.GetPlantLightParamater getPlantLightParamater75 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetPlantLightParamater();
		return getPlantLightParamater75;
	}

	de.semvox.types.odp.spijk.Rule getRule76() {
		final de.semvox.types.odp.spijk.Rule rule76 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string89 = replaceEntities("RequestTask_GetOptimalPlantTemperature");
		rule76.setCommonName(string89);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern77 = getPattern77();
			rule76.getSpijkPatterns().add(pattern77);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern78 = getPattern78();
			rule76.getSpijkPatterns().add(pattern78);
		}
		de.semvox.types.odp.spijk.References references79 = getReferences79();
		rule76.setSpijkReferences(references79);
		de.semvox.types.odp.interaction.RequestTask requestTask80 = getRequestTask80();
		rule76.setMmInterpretation(requestTask80);
		return rule76;
	}

	de.semvox.types.odp.spijk.Pattern getPattern77() {
		final de.semvox.types.odp.spijk.Pattern pattern77 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string90 = replaceEntities("en-US");
		pattern77.setCommonLanguage(string90);
		String string91 = replaceEntities("what is (your|the) optimal temperature");
		pattern77.getSpijkUtterances().add(string91);
		return pattern77;
	}

	de.semvox.types.odp.spijk.Pattern getPattern78() {
		final de.semvox.types.odp.spijk.Pattern pattern78 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string92 = replaceEntities("nl-BE");
		pattern78.setCommonLanguage(string92);
		String string93 = replaceEntities("((wat|hoe) is je|hoeveel bedraagt je) optimale temperatuur");
		pattern78.getSpijkUtterances().add(string93);
		return pattern78;
	}

	de.semvox.types.odp.spijk.References getReferences79() {
		final de.semvox.types.odp.spijk.References references79 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string94 = replaceEntities("");
		references79.setSpijkValue(string94);
		return references79;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask80() {
		final de.semvox.types.odp.interaction.RequestTask requestTask80 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetOptimalPlantTemperature", requestTask80);
		com.zorabots.ontologies.vmn.GetPlantTemperatureParamter getPlantTemperatureParamter81 = getGetPlantTemperatureParamter81();
		requestTask80.setCommonContent(getPlantTemperatureParamter81);
		return requestTask80;
	}

	com.zorabots.ontologies.vmn.GetPlantTemperatureParamter getGetPlantTemperatureParamter81() {
		final com.zorabots.ontologies.vmn.GetPlantTemperatureParamter getPlantTemperatureParamter81 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetPlantTemperatureParamter();
		return getPlantTemperatureParamter81;
	}
}