package com.zorabots.vmn.routing;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import com.zorabots.vmn.pojo.MethodInvoker;

import de.semvox.commons.logging.Logger;
import de.semvox.commons.logging.Logger.LogLevel;

public class MQTTRouter {
	
	private HashMap<String, ArrayList<MethodInvoker>> routingMap;
	private static MQTTRouter mqttRouter = null;
	private MQTTClient mqttClient;
	
	private MQTTRouter(){
		routingMap = new HashMap<String, ArrayList<MethodInvoker>>();
		mqttClient = MQTTClient.getInstance();
	}
	
	public static MQTTRouter getInstance(){
		if(mqttRouter == null){
			mqttRouter = new MQTTRouter();
		}
		return mqttRouter;
	}
	
	private Method getMethod(String methodName, Object obj){
		Method method;
		try {
			method = obj.getClass().getMethod(methodName, String.class);
			return method;

		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Logger.log(LogLevel.INFO, "MQTT Router GetMethod ERROR", "MQTT Router GetMethod ERROR calling " + methodName);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Logger.log(LogLevel.INFO, "MQTT Router GetMethod ERROR", "MQTT Router GetMethod ERROR calling " + methodName);
		}
		return null;
	}
	
	
	public void route(String mqtt, String methodName, Object obj){
		//subscribe
		mqttClient.subscribe(mqtt);
		ArrayList<MethodInvoker> m;
		
		String regexmqtt = mqtt.replaceAll("\\+", "[a-z]+");
		
		if(routingMap.containsKey(regexmqtt)){
			m = routingMap.get(regexmqtt);
		}else{
			m = new ArrayList<MethodInvoker>();
		}
		
		m.add(new MethodInvoker(obj, getMethod(methodName, obj)));
		routingMap.put(regexmqtt, m);
	
	}
	
	public void unSubscribe(String mqtt, String  methodname, Object obj){
		
	}
	
	public HashMap<String, ArrayList<MethodInvoker>> getRouting(){
		Logger.log(LogLevel.INFO, "MQTT router", "Getting routing map !!!! " + routingMap.toString());
		return routingMap;
	}
	
}
