package com.zorabots.vmn.staticclasses;

import com.zorabots.ontologies.vmn.factory.ThingFactoryVmn;

public class Extreme {
	
	public static com.zorabots.ontologies.vmn.Extreme getExtremeFromServer(int i){
		if(i == 1){
			return getExtreme("HIGH");
		}else{
			return getExtreme("LOW");
		}
	}
	
	public static com.zorabots.ontologies.vmn.Extreme getExtreme(String extreme){
		switch(extreme){
		case "HIGH":
			return (com.zorabots.ontologies.vmn.Extreme)ThingFactoryVmn.newToHight();
		case "LOW":
			return (com.zorabots.ontologies.vmn.Extreme)ThingFactoryVmn.newToLow();
		
		}
		return null;
	}

}
