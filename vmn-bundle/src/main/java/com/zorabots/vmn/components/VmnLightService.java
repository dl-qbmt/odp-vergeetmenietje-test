package com.zorabots.vmn.components;

import java.util.Map;

import org.json.simple.JSONObject;

import com.zorabots.ontologies.vmn.GetLight;
import com.zorabots.ontologies.vmn.Light;
import com.zorabots.ontologies.vmn.LightReport;
import com.zorabots.ontologies.vmn.YesNoLight;
import com.zorabots.ontologies.vmn.factory.ThingFactoryVmn;
import com.zorabots.vmn.components.gen.VmnLightServiceDefinition;
import com.zorabots.vmn.events.JsonParser;
import com.zorabots.vmn.mqtt.requests.GetRequests;
import com.zorabots.vmn.pojo.SharedUnits;
import com.zorabots.vmn.staticclasses.ClosedQuestionParameters;
import com.zorabots.vmn.staticclasses.UnitOntologyMap;

import de.semvox.subcon.data.ThingUtils;
import de.semvox.subcon.integration.api.IpcFacade;
import de.semvox.subcon.integration.api.Service;
import de.semvox.types.odp.multimodal.AsyncServiceResponse;
import de.semvox.types.odp.multimodal.PresentEvent;
import de.semvox.types.odp.multimodal.factory.ThingFactoryMultimodal;
import de.semvox.types.subcon.integration.AsyncServiceRequest;
import de.semvox.types.subcon.integration.ServiceDefinition;
import de.semvox.types.subcon.integration.ServiceRequest;
import de.semvox.types.subcon.integration.factory.ThingFactoryIntegration;

public class VmnLightService implements Service {

	private final ServiceDefinition definition;
	private IpcFacade ipcFacade;

	public VmnLightService(IpcFacade ipcFacade) throws Exception {
		definition = new VmnLightServiceDefinition().loadThing();
		this.ipcFacade = ipcFacade;
	}

	@Override
	public ServiceDefinition getServiceDefinition() {
		return definition;
	}

	@Override
	public void dispose() {

	}

	@Override
	public ServiceRequest handleRequest(ServiceRequest request,
			Map<String, Object> boundVariables) {
		if(request instanceof AsyncServiceRequest){
			AsyncServiceRequest asyncServiceRequest = (AsyncServiceRequest)request;
			
			if(asyncServiceRequest instanceof GetLight){
				asyncServiceRequest.setIntResponseStatus(ThingFactoryIntegration.newStatusAccepted());
				final GetLight getTemperatureRequest = ThingUtils.deepCopy((GetLight)request);
				GetRequests getRequests = new GetRequests("vmnengine/get/sensor/light", "vmnengine/response/odp/light", "{ \"key\":\"odp\"}", this, "handle", getTemperatureRequest);
				getRequests.start();
			}
			
		}
		return request;
	}
	
	public void handle(String recMessage, Object request){
		GetLight getLightRequest = (GetLight) request;
		JSONObject jsonObject = JsonParser.parseJson(recMessage);
		 LightReport report = ThingFactoryVmn.newLightReport();
         
		 if(request instanceof YesNoLight){
			 ClosedQuestionParameters.yesNoQuestion((YesNoLight)request, ((Long)jsonObject.get("level")).intValue());
		 }
		 
		 Light light = ThingFactoryVmn.newLight();
		 
		 double value = (double)jsonObject.get("value");
		 
		 //int scale = (int) Math.pow(10, 0);
		 int intvalue  = (int) Math.round(value);
		    		 
		 light.setCommonStringValue(Integer.toString(intvalue));
		 String lightunit = SharedUnits.getInstance().map.get("light");
		 light.setVmnUnit(UnitOntologyMap.getUnitOntology(lightunit));
		 
		 report.setVmnLight(light);
		 
		 getLightRequest.setVmnLightReport(report);
		 getLightRequest.setIntResponseStatus(ThingFactoryIntegration.newStatusSuccess());
         
		 PresentEvent presentEvent = ThingFactoryMultimodal.newPresentEvent();
         presentEvent.setMmTimeToLive((long) 2000);
         
         AsyncServiceResponse asyncServiceResponse = ThingFactoryMultimodal.newAsyncServiceResponse();
         asyncServiceResponse.setIntResponse(getLightRequest);
         asyncServiceResponse.setMmCorrelationId(getLightRequest.getCommonId());
         
         presentEvent.setMmEvent(asyncServiceResponse);
         ipcFacade.invokeService(presentEvent,
                 String.format("%s.%s", definition.getIntNamespace(), definition.getCommonName()));
	}

}
