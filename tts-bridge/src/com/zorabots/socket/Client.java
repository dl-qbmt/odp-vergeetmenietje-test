package com.zorabots.socket;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.paho.client.mqttv3.MqttException;

import com.zorabots.vmn.routing.MQTTClient;

import de.semvox.commons.logging.Logger;
import de.semvox.commons.logging.Logger.LogLevel;

public class Client extends Thread{
	
	private Socket client;
	private final static String serverName = "127.0.0.1";
	private final static String devServerName = "192.168.0.154";
	private final static int port = 8888;
	public ArrayList<String> text = new ArrayList<String>();
	private Boolean notlast = false;
	public static Client ttsclient;
	private HashMap<String, String> languagemap = new HashMap<String, String>();
	private String currentLanguage;
	private String originalLanguage;
	private int pitch;
	private int speechrate;
	
	public Client(){
		currentLanguage = "nl-BE";
		originalLanguage = "nl-BE";
		
		pitch = 100;
		speechrate = 86;
		
		languagemap.put("en-US", "englishu");
		languagemap.put("nl-BE", "nederlandsm");
		languagemap.put("de-DE", "german");
		languagemap.put("fr-FR", "french");
	}
	
	public static Client getInstance(){
		if (ttsclient == null){
	          ttsclient=new Client();
		}
		return ttsclient;
	}
	
	public void run(){
		while(true){
		      try{
		    	  		if (this.text.size() > 0){
		    		  /*JSONObject obj = new JSONObject();
		    			obj.put("language", "english");
		    			obj.put("text", text);*/
		    		  	try{
		    		  		client  = new Socket(serverName, port);
		    		  		Logger.log(LogLevel.INFO, "TTS CONNECT", "Connected to tts server on " + serverName + ":" + Integer.toString(port));
		    		  	}
		    		  	catch(ConnectException ce){
		    		  		Logger.log(LogLevel.INFO, "TTS CONNECT ERROR", "Not able to connect to tts server on " + serverName + ":" + Integer.toString(port));
		    		  		try{
		    		  			Logger.log(LogLevel.INFO, "TTS CONNECT", "Attempting to connect to development TTS server. ");
		    		  			client  = new Socket(devServerName, port);
		    		  			Logger.log(LogLevel.INFO, "TTS CONNECT", "Connected to tts server on " + devServerName + ":" + Integer.toString(port));
		    		  		}
		    		  		catch(ConnectException ce2){
		    		  			Logger.log(LogLevel.INFO, "TTS CONNECT ERROR", "Not able to connect to tts server on " + devServerName + ":" + Integer.toString(port));
		    		  			System.exit(-1);
		    		  		}
		    		  	}
		    		  	
		  				client.setTcpNoDelay(true);
		  				client.setKeepAlive(false);

		    		  
		    		  	 OutputStream outToServer = client.getOutputStream();
				         DataOutputStream out = new DataOutputStream(outToServer);
				         
				         out.write(("{\"language\":\"" + this.languagemap.get(currentLanguage) + "\", \"pitch\":\"" + this.pitch 
				        		 + "\",\"speechrate\":\"" + this.speechrate + "\", \"text\": \"" + this.text.get(0) + "\"}").getBytes());
				         //client.close();
				         out.flush();
				         InputStream inFromServer = client.getInputStream();
				         BufferedReader in = new BufferedReader(new InputStreamReader(inFromServer));
				         String line = null;

				         while((line = in.readLine()) != null) {
				             if (line.equals("Pass1")){
				            	try {
				     				MQTTClient.getInstance().publish("vmnengine/push/odp/processing", "{\"processing\": false}");
				     				Logger.log(LogLevel.INFO, "TTS Bridge", "TTS conversion to text, MQTT ODP processing false sent to engine!!!!!!!");
				     			} catch (MqttException e) {
				     				// TODO Auto-generated catch block
				     				Logger.log(LogLevel.INFO, "TTS Bridge", "MQTT ERROR fired!!!!!!!");
				     				e.printStackTrace();
				     			}
				            	try {
					     				MQTTClient.getInstance().publish("vmnengine/push/odp/tts", "{\"tts_processing\": true}");
					     				Logger.log(LogLevel.INFO, "TTS Bridge", "TTS conversion to text, MQTT TTS processing true sent to engine!!!!!!!");
					     		} catch (MqttException e) {
				     				// TODO Auto-generated catch block
				     				Logger.log(LogLevel.INFO, "TTS Bridge", "MQTT ERROR fired!!!!!!!");
				     				e.printStackTrace();
					     			}
				            	break;
				             }
				         }
				         
				         while((line = in.readLine()) != null) {
				             if (line.equals("Pass2")){
				            	try {
				     				MQTTClient.getInstance().publish("vmnengine/push/odp/tts", "{\"tts_processing\": false}");
				     				Logger.log(LogLevel.INFO, "TTS Bridge", "TTS done, MQTT TTS processing false sent to engine!!!!!!!");
				     			} catch (MqttException e) {
				     				// TODO Auto-generated catch block
				     				Logger.log(LogLevel.INFO, "TTS Bridge", "MQTT ERROR fired!!!!!!!");
				     				e.printStackTrace();
				     			}
				            	break;
				             }
				         }
				         
				         if(notlast){
				        	 try {
				     				MQTTClient.getInstance().publish("vmnengine/push/odp/processing", "{\"priority\": 1, \"processing\": true}");
				     				Logger.log(LogLevel.INFO, "TTS Bridge", "TTS conversion to text, MQTT ODP processing false sent to engine!!!!!!!");
				     			} catch (MqttException e) {
				     				// TODO Auto-generated catch block
				     				Logger.log(LogLevel.INFO, "TTS Bridge", "MQTT ERROR fired!!!!!!!");
				     				e.printStackTrace();
				     			}
				        	 this.notlast = false;
				         }
				         
				         out.close();
				         this.text.remove(0);
				         if (this.text.size() == 0){
				        	 this.setLanguage(this.originalLanguage);
				         }
				         client.close();
				         Thread.sleep(1000);
		    	  		}
		    	  		Thread.sleep(500);
		    	  		
		      }catch(Exception e){
		    	  e.printStackTrace();
		      }
		      
		}
	
	}
	
	public void sayText(String text){
		String[] temptext = text.split(";");
		for(String piece : temptext){
			if(!(piece.equals(""))){
				this.text.add(piece);
				de.semvox.commons.logging.Logger.log(LogLevel.INFO, "NEWS", "Piece " + piece);
			}
		}
	}
	
	public void setLanguage(String language){
		if(languagemap.containsKey(language)){
			this.currentLanguage = language;
		}
	}
	
	public void setOriginalLanguage(String language){
		if(languagemap.containsKey(language)){
			this.originalLanguage = language;
		}
	}
	
	public String getOriginalLanguage(){
		return this.originalLanguage;
	}
	
	public void setPitch(int pitch){
		if (pitch > 300){
			this.pitch = 300;
		}
		else{
			if (pitch <= 0){
				this.pitch = 1;
			}
			else{
				this.pitch = pitch;
			}
		}
	}
	
	public int getPitch(){
		return this.pitch;
	}
	
	public void setSpeechRate(int speechrate){
		if(speechrate > 200 ){
			this.speechrate = 200;
		}
		else{
			if (speechrate <= 0){
				this.speechrate = 1;
			}
			else{
				this.speechrate = speechrate;
			}
		}
	}
	
	public int getSpeechRate(){
		return this.speechrate;
	}
	
	public void setNotLast(Boolean notlast){
		this.notlast = notlast;
	}
	
}
