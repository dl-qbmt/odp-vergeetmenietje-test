/**
* GENERATED CODE
*/
package com.zorabots.vmn.nlu.gen;

public final class VmnLightGrammar extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.spijk.StandaloneGrammar> implements de.semvox.commons.Generated {

	private java.util.Map<java.lang.String, de.semvox.subcon.data.Thing> idRefMap = new java.util.HashMap<java.lang.String, de.semvox.subcon.data.Thing>();

	public static java.lang.String TYPE_NAME = "spijk#StandaloneGrammar";

	public VmnLightGrammar(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public VmnLightGrammar(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public VmnLightGrammar(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public VmnLightGrammar() {
		super();
	}

	@Override
	protected de.semvox.types.odp.spijk.StandaloneGrammar createThing() {
		return getStandaloneGrammar();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.spijk.StandaloneGrammar standaloneGrammar0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("VmnLightGrammar");
		standaloneGrammar0.setCommonName(string0);
		de.semvox.types.asr.MainGrammar mainGrammar1 = getMainGrammar1();
		standaloneGrammar0.setAsrGrammarType(mainGrammar1);
		standaloneGrammar0.assignSpijkImportsPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.spijk.Rule rule2 = getRule2();
		standaloneGrammar0.getSpijkElements().add(rule2);
		de.semvox.types.odp.spijk.Rule rule8 = getRule8();
		standaloneGrammar0.getSpijkElements().add(rule8);
		de.semvox.types.odp.spijk.Rule rule15 = getRule15();
		standaloneGrammar0.getSpijkElements().add(rule15);
		de.semvox.types.odp.spijk.Rule rule22 = getRule22();
		standaloneGrammar0.getSpijkElements().add(rule22);
	}

	de.semvox.types.odp.spijk.StandaloneGrammar getStandaloneGrammar() {
		final de.semvox.types.odp.spijk.StandaloneGrammar standaloneGrammar0 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newStandaloneGrammar();
		return standaloneGrammar0;
	}


	de.semvox.types.asr.MainGrammar getMainGrammar1() {
		final de.semvox.types.asr.MainGrammar mainGrammar1 = de.semvox.types.asr.factory.PatternFactoryAsr.newMainGrammar();
		return mainGrammar1;
	}

	de.semvox.types.odp.spijk.Rule getRule2() {
		final de.semvox.types.odp.spijk.Rule rule2 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string1 = replaceEntities("RequestTask_GetLight");
		rule2.setCommonName(string1);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern3 = getPattern3();
			rule2.getSpijkPatterns().add(pattern3);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern4 = getPattern4();
			rule2.getSpijkPatterns().add(pattern4);
		}
		de.semvox.types.odp.spijk.References references5 = getReferences5();
		rule2.setSpijkReferences(references5);
		de.semvox.types.odp.interaction.RequestTask requestTask6 = getRequestTask6();
		rule2.setMmInterpretation(requestTask6);
		return rule2;
	}

	de.semvox.types.odp.spijk.Pattern getPattern3() {
		final de.semvox.types.odp.spijk.Pattern pattern3 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string2 = replaceEntities("en-US");
		pattern3.setCommonLanguage(string2);
		String string3 = replaceEntities("what is your light intensity");
		pattern3.getSpijkUtterances().add(string3);
		String string4 = replaceEntities("how much light are you getting");
		pattern3.getSpijkUtterances().add(string4);
		return pattern3;
	}

	de.semvox.types.odp.spijk.Pattern getPattern4() {
		final de.semvox.types.odp.spijk.Pattern pattern4 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string5 = replaceEntities("nl-BE");
		pattern4.setCommonLanguage(string5);
		String string6 = replaceEntities("(wat|hoe) is je licht intensiteit");
		pattern4.getSpijkUtterances().add(string6);
		String string7 = replaceEntities("hoeveel bedraagt je licht intensiteit");
		pattern4.getSpijkUtterances().add(string7);
		String string8 = replaceEntities("hoeveel licht (vang je|heb je)");
		pattern4.getSpijkUtterances().add(string8);
		String string9 = replaceEntities("hoeveel licht vangt de plant");
		pattern4.getSpijkUtterances().add(string9);
		return pattern4;
	}

	de.semvox.types.odp.spijk.References getReferences5() {
		final de.semvox.types.odp.spijk.References references5 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string10 = replaceEntities("");
		references5.setSpijkValue(string10);
		return references5;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask6() {
		final de.semvox.types.odp.interaction.RequestTask requestTask6 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetLight", requestTask6);
		com.zorabots.ontologies.vmn.GetLight getLight7 = getGetLight7();
		requestTask6.setCommonContent(getLight7);
		return requestTask6;
	}

	com.zorabots.ontologies.vmn.GetLight getGetLight7() {
		final com.zorabots.ontologies.vmn.GetLight getLight7 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetLight();
		return getLight7;
	}

	de.semvox.types.odp.spijk.Rule getRule8() {
		final de.semvox.types.odp.spijk.Rule rule8 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string11 = replaceEntities("RequestTask_GetLight_YesNo_Pos");
		rule8.setCommonName(string11);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern9 = getPattern9();
			rule8.getSpijkPatterns().add(pattern9);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern10 = getPattern10();
			rule8.getSpijkPatterns().add(pattern10);
		}
		de.semvox.types.odp.spijk.References references11 = getReferences11();
		rule8.setSpijkReferences(references11);
		de.semvox.types.odp.interaction.RequestTask requestTask12 = getRequestTask12();
		rule8.setMmInterpretation(requestTask12);
		return rule8;
	}

	de.semvox.types.odp.spijk.Pattern getPattern9() {
		final de.semvox.types.odp.spijk.Pattern pattern9 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string12 = replaceEntities("en-US");
		pattern9.setCommonLanguage(string12);
		String string13 = replaceEntities("is the light too bright");
		pattern9.getSpijkUtterances().add(string13);
		String string14 = replaceEntities("is the amount of light too high");
		pattern9.getSpijkUtterances().add(string14);
		String string15 = replaceEntities("is there too much light");
		pattern9.getSpijkUtterances().add(string15);
		String string16 = replaceEntities("do you (want|need) less light");
		pattern9.getSpijkUtterances().add(string16);
		return pattern9;
	}

	de.semvox.types.odp.spijk.Pattern getPattern10() {
		final de.semvox.types.odp.spijk.Pattern pattern10 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string17 = replaceEntities("nl-BE");
		pattern10.setCommonLanguage(string17);
		String string18 = replaceEntities("is de hoeveelheid licht te hoog");
		pattern10.getSpijkUtterances().add(string18);
		String string19 = replaceEntities("(is er|heb je|vang je) ?(niet) te veel licht");
		pattern10.getSpijkUtterances().add(string19);
		String string20 = replaceEntities("wil je minder licht");
		pattern10.getSpijkUtterances().add(string20);
		String string21 = replaceEntities("heb je minder licht nodig");
		pattern10.getSpijkUtterances().add(string21);
		String string22 = replaceEntities("heeft (je|de) plant minder licht nodig");
		pattern10.getSpijkUtterances().add(string22);
		return pattern10;
	}

	de.semvox.types.odp.spijk.References getReferences11() {
		final de.semvox.types.odp.spijk.References references11 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string23 = replaceEntities("");
		references11.setSpijkValue(string23);
		return references11;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask12() {
		final de.semvox.types.odp.interaction.RequestTask requestTask12 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetLight_YesNo_Pos", requestTask12);
		com.zorabots.ontologies.vmn.YesNoLight yesNoLight13 = getYesNoLight13();
		requestTask12.setCommonContent(yesNoLight13);
		return requestTask12;
	}

	com.zorabots.ontologies.vmn.YesNoLight getYesNoLight13() {
		final com.zorabots.ontologies.vmn.YesNoLight yesNoLight13 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newYesNoLight();
		com.zorabots.ontologies.vmn.PositiveQuestion positiveQuestion14 = getPositiveQuestion14();
		yesNoLight13.setVmnYesNoQuestionType(positiveQuestion14);
		return yesNoLight13;
	}

	com.zorabots.ontologies.vmn.PositiveQuestion getPositiveQuestion14() {
		final com.zorabots.ontologies.vmn.PositiveQuestion positiveQuestion14 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newPositiveQuestion();
		return positiveQuestion14;
	}

	de.semvox.types.odp.spijk.Rule getRule15() {
		final de.semvox.types.odp.spijk.Rule rule15 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string24 = replaceEntities("RequestTask_GetLight_YesNo_Neutral");
		rule15.setCommonName(string24);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern16 = getPattern16();
			rule15.getSpijkPatterns().add(pattern16);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern17 = getPattern17();
			rule15.getSpijkPatterns().add(pattern17);
		}
		de.semvox.types.odp.spijk.References references18 = getReferences18();
		rule15.setSpijkReferences(references18);
		de.semvox.types.odp.interaction.RequestTask requestTask19 = getRequestTask19();
		rule15.setMmInterpretation(requestTask19);
		return rule15;
	}

	de.semvox.types.odp.spijk.Pattern getPattern16() {
		final de.semvox.types.odp.spijk.Pattern pattern16 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string25 = replaceEntities("en-US");
		pattern16.setCommonLanguage(string25);
		String string26 = replaceEntities("is the amount of light ok");
		pattern16.getSpijkUtterances().add(string26);
		String string27 = replaceEntities("(do you have|are you getting) enough light");
		pattern16.getSpijkUtterances().add(string27);
		return pattern16;
	}

	de.semvox.types.odp.spijk.Pattern getPattern17() {
		final de.semvox.types.odp.spijk.Pattern pattern17 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string28 = replaceEntities("nl-BE");
		pattern17.setCommonLanguage(string28);
		String string29 = replaceEntities("is de hoeveelheid licht ?(nog) ok");
		pattern17.getSpijkUtterances().add(string29);
		String string30 = replaceEntities("?((is er|heb je|vang je) ?(nog)) genoeg licht");
		pattern17.getSpijkUtterances().add(string30);
		String string31 = replaceEntities("vangt (je|de) plant genoeg licht");
		pattern17.getSpijkUtterances().add(string31);
		return pattern17;
	}

	de.semvox.types.odp.spijk.References getReferences18() {
		final de.semvox.types.odp.spijk.References references18 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string32 = replaceEntities("");
		references18.setSpijkValue(string32);
		return references18;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask19() {
		final de.semvox.types.odp.interaction.RequestTask requestTask19 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetLight_YesNo_Neutral", requestTask19);
		com.zorabots.ontologies.vmn.YesNoLight yesNoLight20 = getYesNoLight20();
		requestTask19.setCommonContent(yesNoLight20);
		return requestTask19;
	}

	com.zorabots.ontologies.vmn.YesNoLight getYesNoLight20() {
		final com.zorabots.ontologies.vmn.YesNoLight yesNoLight20 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newYesNoLight();
		com.zorabots.ontologies.vmn.NeutralQuestion neutralQuestion21 = getNeutralQuestion21();
		yesNoLight20.setVmnYesNoQuestionType(neutralQuestion21);
		return yesNoLight20;
	}

	com.zorabots.ontologies.vmn.NeutralQuestion getNeutralQuestion21() {
		final com.zorabots.ontologies.vmn.NeutralQuestion neutralQuestion21 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNeutralQuestion();
		return neutralQuestion21;
	}

	de.semvox.types.odp.spijk.Rule getRule22() {
		final de.semvox.types.odp.spijk.Rule rule22 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string33 = replaceEntities("RequestTask_GetLight_YesNo_Negative");
		rule22.setCommonName(string33);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern23 = getPattern23();
			rule22.getSpijkPatterns().add(pattern23);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern24 = getPattern24();
			rule22.getSpijkPatterns().add(pattern24);
		}
		de.semvox.types.odp.spijk.References references25 = getReferences25();
		rule22.setSpijkReferences(references25);
		de.semvox.types.odp.interaction.RequestTask requestTask26 = getRequestTask26();
		rule22.setMmInterpretation(requestTask26);
		return rule22;
	}

	de.semvox.types.odp.spijk.Pattern getPattern23() {
		final de.semvox.types.odp.spijk.Pattern pattern23 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string34 = replaceEntities("en-US");
		pattern23.setCommonLanguage(string34);
		String string35 = replaceEntities("is the light too dim");
		pattern23.getSpijkUtterances().add(string35);
		String string36 = replaceEntities("is the amount of light too low");
		pattern23.getSpijkUtterances().add(string36);
		String string37 = replaceEntities("is there too little light");
		pattern23.getSpijkUtterances().add(string37);
		String string38 = replaceEntities("do you (want|need) more light");
		pattern23.getSpijkUtterances().add(string38);
		return pattern23;
	}

	de.semvox.types.odp.spijk.Pattern getPattern24() {
		final de.semvox.types.odp.spijk.Pattern pattern24 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string39 = replaceEntities("nl-BE");
		pattern24.setCommonLanguage(string39);
		String string40 = replaceEntities("(is er|heb je|vang je) ?(niet) te weinig licht");
		pattern24.getSpijkUtterances().add(string40);
		String string41 = replaceEntities("wil je ?(niet) meer licht");
		pattern24.getSpijkUtterances().add(string41);
		String string42 = replaceEntities("heb je ?(niet) meer licht nodig");
		pattern24.getSpijkUtterances().add(string42);
		String string43 = replaceEntities("heeft (je|de) plant ?(niet) meer licht nodig");
		pattern24.getSpijkUtterances().add(string43);
		return pattern24;
	}

	de.semvox.types.odp.spijk.References getReferences25() {
		final de.semvox.types.odp.spijk.References references25 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string44 = replaceEntities("");
		references25.setSpijkValue(string44);
		return references25;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask26() {
		final de.semvox.types.odp.interaction.RequestTask requestTask26 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetLight_YesNo_Negative", requestTask26);
		com.zorabots.ontologies.vmn.YesNoLight yesNoLight27 = getYesNoLight27();
		requestTask26.setCommonContent(yesNoLight27);
		return requestTask26;
	}

	com.zorabots.ontologies.vmn.YesNoLight getYesNoLight27() {
		final com.zorabots.ontologies.vmn.YesNoLight yesNoLight27 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newYesNoLight();
		com.zorabots.ontologies.vmn.NegativeQuestion negativeQuestion28 = getNegativeQuestion28();
		yesNoLight27.setVmnYesNoQuestionType(negativeQuestion28);
		return yesNoLight27;
	}

	com.zorabots.ontologies.vmn.NegativeQuestion getNegativeQuestion28() {
		final com.zorabots.ontologies.vmn.NegativeQuestion negativeQuestion28 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNegativeQuestion();
		return negativeQuestion28;
	}
}