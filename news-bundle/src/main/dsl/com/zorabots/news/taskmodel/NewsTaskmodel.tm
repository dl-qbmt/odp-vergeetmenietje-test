taskmodel {
	tasks{
	executable-task GET_NEWS {
		      description "A task to get the news. "
		      task-confirmation-directive never-confirm-execution
		      cancelation-directive none
		      interruption-directive cancel-on-interruption
		      execution direct-execution
		      use-hypothesis-fusion yes
		      pattern {
		        object news#GetNews ;
		      }
	  }
	}
}