package com.zorabots.bridge;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.zorabots.socket.Client;

import de.semvox.commons.concurrent.IdentifiableThreadFactory;
import de.semvox.types.tts.Voice;

public class TtsBridge implements de.semvox.tts.bridge.TtsBridge {
	
	private static Pattern MARK_PATTERN = Pattern.compile("<mark name=\"([^\\s]+)\"\\s*/>");

    private ExecutorService executor = Executors.newSingleThreadExecutor(new IdentifiableThreadFactory("QbmtTts"));

    private TtsCallback callback;
    private PrepareVoiceOutputCallback prepareVoiceOutputCallback;
    private TtsConfiguration configuration;
    private Client socketClient;

    public TtsBridge(TtsCallback callback, PrepareVoiceOutputCallback prepareVoiceOutputCallback,
    		TtsConfiguration configuration) {
        this.callback = callback;
        this.prepareVoiceOutputCallback = prepareVoiceOutputCallback;
        this.configuration = configuration;
        
        //start socketserver
         startClient();
    }

    @Override
    public void initialize(Voice voice) {

        // TODO 
        // Initialize your TTS engine. You can pre-load the voice defined by the parameter to prevent
        // waiting times during the first startSpeaking

    }

    @Override
    public void startSpeaking(final String synthesisId, final String markup, final Voice voice, boolean queue) {
        executor.execute(new Runnable() {

            @Override
            public void run() {

                // Before the voice output starts
                prepareVoiceOutputCallback.onPrepareVoiceOutput(synthesisId);
                
                System.out.println("--------------------------------------------------------------------");
                System.out.println(synthesisId);
                System.out.println(markup);
                
                //socketServer.sayText(xmlParseSpeak(markup));
                socketClient.sayText(xmlParseSpeak(markup));

                // The audio starts to play
                callback.onStarted(synthesisId, markup, voice);
                

                // If your engine doesn't support markers please fire the callbacks manually. 
                if (configuration.enableMarkers()) {
                    List<String> marks = getMarks(markup);
                    for (String m : marks) {
                        callback.onMarkReached(synthesisId, m);
                    }
                }

                // Sleep 
                try {
                    Thread.sleep(configuration.mockUtteranceLength());
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }

                // The audio output is completed
                callback.onFinished(synthesisId, false);
            }
        });
    }

    private static List<String> getMarks(String markup) {
        List<String> marks = new ArrayList<String>();

        Matcher m = MARK_PATTERN.matcher(markup);

        while (m.find()) {
            String mark = m.group(1);
            marks.add(mark);
        }

        return marks;
    }

    @Override
    public void cancel() {

        // TODO
        // If you want to support the cancellation of a running voice output implement this method.

    }

    @Override
    public List<Voice> listAvailableVoices() {
        return new ArrayList<Voice>();
    }

    @Override
    public void dispose() {
        executor.shutdown();
        try {
            while (!executor.awaitTermination(1, TimeUnit.SECONDS)) {
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
    
    public String xmlParseSpeak(String speak){
    	try {	
    		
    		DocumentBuilderFactory factory =
    				DocumentBuilderFactory.newInstance();
    				DocumentBuilder builder = factory.newDocumentBuilder();
    				
    				
    		StringBuilder xmlStringBuilder = new StringBuilder();
    		xmlStringBuilder.append(speak);
    		ByteArrayInputStream input =  new ByteArrayInputStream(xmlStringBuilder.toString().getBytes("UTF-8"));
    		Document doc = builder.parse(input);
            doc.getDocumentElement().normalize();
            System.out.println("Root element :" 
               + doc.getDocumentElement().getNodeName());
            Element root = doc.getDocumentElement();
            Node n = root.getFirstChild();
            String str = root.getFirstChild().getNodeValue().toString();
            return str;
         } catch (Exception e) {
            e.printStackTrace();
         }
    	return null;
      
    }
    
    private void startClient(){
    	socketClient = Client.getInstance();
    	socketClient.start();
    }
}
