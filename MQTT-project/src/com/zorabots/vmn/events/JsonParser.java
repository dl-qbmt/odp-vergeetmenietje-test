package com.zorabots.vmn.events;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;



public class JsonParser {
	
	
	
	public static JSONObject parseJson(String json){
		try{			
			JSONParser parser = new JSONParser();
			//Object obj = parser
			Object obj	=parser.parse(json);
			
			JSONObject jsonObject = (JSONObject) obj;

			
			return jsonObject;
			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return null;
		 
	}

}
