/**
* GENERATED CODE
*/
package com.zorabots.vmn.nlu.gen;

public final class VmnHumidityGrammar extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.spijk.StandaloneGrammar> implements de.semvox.commons.Generated {

	private java.util.Map<java.lang.String, de.semvox.subcon.data.Thing> idRefMap = new java.util.HashMap<java.lang.String, de.semvox.subcon.data.Thing>();

	public static java.lang.String TYPE_NAME = "spijk#StandaloneGrammar";

	public VmnHumidityGrammar(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public VmnHumidityGrammar(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public VmnHumidityGrammar(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public VmnHumidityGrammar() {
		super();
	}

	@Override
	protected de.semvox.types.odp.spijk.StandaloneGrammar createThing() {
		return getStandaloneGrammar();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.spijk.StandaloneGrammar standaloneGrammar0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("VmnHumidityGrammar");
		standaloneGrammar0.setCommonName(string0);
		de.semvox.types.asr.MainGrammar mainGrammar1 = getMainGrammar1();
		standaloneGrammar0.setAsrGrammarType(mainGrammar1);
		standaloneGrammar0.assignSpijkImportsPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.spijk.Rule rule2 = getRule2();
		standaloneGrammar0.getSpijkElements().add(rule2);
		de.semvox.types.odp.spijk.Rule rule8 = getRule8();
		standaloneGrammar0.getSpijkElements().add(rule8);
		de.semvox.types.odp.spijk.Rule rule15 = getRule15();
		standaloneGrammar0.getSpijkElements().add(rule15);
		de.semvox.types.odp.spijk.Rule rule22 = getRule22();
		standaloneGrammar0.getSpijkElements().add(rule22);
	}

	de.semvox.types.odp.spijk.StandaloneGrammar getStandaloneGrammar() {
		final de.semvox.types.odp.spijk.StandaloneGrammar standaloneGrammar0 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newStandaloneGrammar();
		return standaloneGrammar0;
	}


	de.semvox.types.asr.MainGrammar getMainGrammar1() {
		final de.semvox.types.asr.MainGrammar mainGrammar1 = de.semvox.types.asr.factory.PatternFactoryAsr.newMainGrammar();
		return mainGrammar1;
	}

	de.semvox.types.odp.spijk.Rule getRule2() {
		final de.semvox.types.odp.spijk.Rule rule2 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string1 = replaceEntities("RequestTask_GetHumidity");
		rule2.setCommonName(string1);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern3 = getPattern3();
			rule2.getSpijkPatterns().add(pattern3);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern4 = getPattern4();
			rule2.getSpijkPatterns().add(pattern4);
		}
		de.semvox.types.odp.spijk.References references5 = getReferences5();
		rule2.setSpijkReferences(references5);
		de.semvox.types.odp.interaction.RequestTask requestTask6 = getRequestTask6();
		rule2.setMmInterpretation(requestTask6);
		return rule2;
	}

	de.semvox.types.odp.spijk.Pattern getPattern3() {
		final de.semvox.types.odp.spijk.Pattern pattern3 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string2 = replaceEntities("en-US");
		pattern3.setCommonLanguage(string2);
		String string3 = replaceEntities("what is your humidity (in the ground|level|in your soil)");
		pattern3.getSpijkUtterances().add(string3);
		String string4 = replaceEntities("how is your humidity");
		pattern3.getSpijkUtterances().add(string4);
		String string5 = replaceEntities("how much water did you get");
		pattern3.getSpijkUtterances().add(string5);
		String string6 = replaceEntities("how much water do you have");
		pattern3.getSpijkUtterances().add(string6);
		return pattern3;
	}

	de.semvox.types.odp.spijk.Pattern getPattern4() {
		final de.semvox.types.odp.spijk.Pattern pattern4 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string7 = replaceEntities("nl-BE");
		pattern4.setCommonLanguage(string7);
		String string8 = replaceEntities("(wat|hoe) is je vochtigheid?(sgraad)");
		pattern4.getSpijkUtterances().add(string8);
		String string9 = replaceEntities("hoeveel bedraagt je vochtigheid?(sgraad)");
		pattern4.getSpijkUtterances().add(string9);
		String string10 = replaceEntities("hoeveel water heb je");
		pattern4.getSpijkUtterances().add(string10);
		String string11 = replaceEntities("hoeveel water heeft (de|je) plant");
		pattern4.getSpijkUtterances().add(string11);
		return pattern4;
	}

	de.semvox.types.odp.spijk.References getReferences5() {
		final de.semvox.types.odp.spijk.References references5 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string12 = replaceEntities("");
		references5.setSpijkValue(string12);
		return references5;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask6() {
		final de.semvox.types.odp.interaction.RequestTask requestTask6 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetHumidity", requestTask6);
		com.zorabots.ontologies.vmn.GetHumidity getHumidity7 = getGetHumidity7();
		requestTask6.setCommonContent(getHumidity7);
		return requestTask6;
	}

	com.zorabots.ontologies.vmn.GetHumidity getGetHumidity7() {
		final com.zorabots.ontologies.vmn.GetHumidity getHumidity7 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetHumidity();
		return getHumidity7;
	}

	de.semvox.types.odp.spijk.Rule getRule8() {
		final de.semvox.types.odp.spijk.Rule rule8 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string13 = replaceEntities("RequestTask_GetHumidity_YesNo_Pos");
		rule8.setCommonName(string13);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern9 = getPattern9();
			rule8.getSpijkPatterns().add(pattern9);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern10 = getPattern10();
			rule8.getSpijkPatterns().add(pattern10);
		}
		de.semvox.types.odp.spijk.References references11 = getReferences11();
		rule8.setSpijkReferences(references11);
		de.semvox.types.odp.interaction.RequestTask requestTask12 = getRequestTask12();
		rule8.setMmInterpretation(requestTask12);
		return rule8;
	}

	de.semvox.types.odp.spijk.Pattern getPattern9() {
		final de.semvox.types.odp.spijk.Pattern pattern9 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string14 = replaceEntities("en-US");
		pattern9.setCommonLanguage(string14);
		String string15 = replaceEntities("is your humidity too high");
		pattern9.getSpijkUtterances().add(string15);
		String string16 = replaceEntities("do you have too much water");
		pattern9.getSpijkUtterances().add(string16);
		String string17 = replaceEntities("did you get too much water");
		pattern9.getSpijkUtterances().add(string17);
		return pattern9;
	}

	de.semvox.types.odp.spijk.Pattern getPattern10() {
		final de.semvox.types.odp.spijk.Pattern pattern10 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string18 = replaceEntities("nl-BE");
		pattern10.setCommonLanguage(string18);
		String string19 = replaceEntities("is je vochtigheid?(sgraad) te hoog");
		pattern10.getSpijkUtterances().add(string19);
		String string20 = replaceEntities("heb je te veel water ?(gekregen)");
		pattern10.getSpijkUtterances().add(string20);
		String string21 = replaceEntities("heeft (de|je) plant ?(niet) te veel water");
		pattern10.getSpijkUtterances().add(string21);
		return pattern10;
	}

	de.semvox.types.odp.spijk.References getReferences11() {
		final de.semvox.types.odp.spijk.References references11 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string22 = replaceEntities("");
		references11.setSpijkValue(string22);
		return references11;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask12() {
		final de.semvox.types.odp.interaction.RequestTask requestTask12 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetHumidity_YesNo_Pos", requestTask12);
		com.zorabots.ontologies.vmn.YesNoHumidity yesNoHumidity13 = getYesNoHumidity13();
		requestTask12.setCommonContent(yesNoHumidity13);
		return requestTask12;
	}

	com.zorabots.ontologies.vmn.YesNoHumidity getYesNoHumidity13() {
		final com.zorabots.ontologies.vmn.YesNoHumidity yesNoHumidity13 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newYesNoHumidity();
		com.zorabots.ontologies.vmn.PositiveQuestion positiveQuestion14 = getPositiveQuestion14();
		yesNoHumidity13.setVmnYesNoQuestionType(positiveQuestion14);
		return yesNoHumidity13;
	}

	com.zorabots.ontologies.vmn.PositiveQuestion getPositiveQuestion14() {
		final com.zorabots.ontologies.vmn.PositiveQuestion positiveQuestion14 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newPositiveQuestion();
		return positiveQuestion14;
	}

	de.semvox.types.odp.spijk.Rule getRule15() {
		final de.semvox.types.odp.spijk.Rule rule15 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string23 = replaceEntities("RequestTask_GetHumidity_YesNo_Neutral");
		rule15.setCommonName(string23);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern16 = getPattern16();
			rule15.getSpijkPatterns().add(pattern16);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern17 = getPattern17();
			rule15.getSpijkPatterns().add(pattern17);
		}
		de.semvox.types.odp.spijk.References references18 = getReferences18();
		rule15.setSpijkReferences(references18);
		de.semvox.types.odp.interaction.RequestTask requestTask19 = getRequestTask19();
		rule15.setMmInterpretation(requestTask19);
		return rule15;
	}

	de.semvox.types.odp.spijk.Pattern getPattern16() {
		final de.semvox.types.odp.spijk.Pattern pattern16 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string24 = replaceEntities("en-US");
		pattern16.setCommonLanguage(string24);
		String string25 = replaceEntities("is your humidity ok");
		pattern16.getSpijkUtterances().add(string25);
		String string26 = replaceEntities("do you have enough water");
		pattern16.getSpijkUtterances().add(string26);
		String string27 = replaceEntities("did you get enough water");
		pattern16.getSpijkUtterances().add(string27);
		String string28 = replaceEntities("?(is there) enough water ?(in the soil)");
		pattern16.getSpijkUtterances().add(string28);
		return pattern16;
	}

	de.semvox.types.odp.spijk.Pattern getPattern17() {
		final de.semvox.types.odp.spijk.Pattern pattern17 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string29 = replaceEntities("nl-BE");
		pattern17.setCommonLanguage(string29);
		String string30 = replaceEntities("is je vochtigheid?(sgraad) ?(nog) ok");
		pattern17.getSpijkUtterances().add(string30);
		String string31 = replaceEntities("heb je ?(nog) (genoeg|voldoende) water ?(gekregen)");
		pattern17.getSpijkUtterances().add(string31);
		String string32 = replaceEntities("is er genoeg water in de potgrond");
		pattern17.getSpijkUtterances().add(string32);
		String string33 = replaceEntities("heeft (de|je) plant voldoende water");
		pattern17.getSpijkUtterances().add(string33);
		String string34 = replaceEntities("genoeg water");
		pattern17.getSpijkUtterances().add(string34);
		return pattern17;
	}

	de.semvox.types.odp.spijk.References getReferences18() {
		final de.semvox.types.odp.spijk.References references18 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string35 = replaceEntities("");
		references18.setSpijkValue(string35);
		return references18;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask19() {
		final de.semvox.types.odp.interaction.RequestTask requestTask19 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetHumidity_YesNo_Neutral", requestTask19);
		com.zorabots.ontologies.vmn.YesNoHumidity yesNoHumidity20 = getYesNoHumidity20();
		requestTask19.setCommonContent(yesNoHumidity20);
		return requestTask19;
	}

	com.zorabots.ontologies.vmn.YesNoHumidity getYesNoHumidity20() {
		final com.zorabots.ontologies.vmn.YesNoHumidity yesNoHumidity20 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newYesNoHumidity();
		com.zorabots.ontologies.vmn.NeutralQuestion neutralQuestion21 = getNeutralQuestion21();
		yesNoHumidity20.setVmnYesNoQuestionType(neutralQuestion21);
		return yesNoHumidity20;
	}

	com.zorabots.ontologies.vmn.NeutralQuestion getNeutralQuestion21() {
		final com.zorabots.ontologies.vmn.NeutralQuestion neutralQuestion21 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNeutralQuestion();
		return neutralQuestion21;
	}

	de.semvox.types.odp.spijk.Rule getRule22() {
		final de.semvox.types.odp.spijk.Rule rule22 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string36 = replaceEntities("RequestTask_GetHumidity_YesNo_Negative");
		rule22.setCommonName(string36);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern23 = getPattern23();
			rule22.getSpijkPatterns().add(pattern23);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern24 = getPattern24();
			rule22.getSpijkPatterns().add(pattern24);
		}
		de.semvox.types.odp.spijk.References references25 = getReferences25();
		rule22.setSpijkReferences(references25);
		de.semvox.types.odp.interaction.RequestTask requestTask26 = getRequestTask26();
		rule22.setMmInterpretation(requestTask26);
		return rule22;
	}

	de.semvox.types.odp.spijk.Pattern getPattern23() {
		final de.semvox.types.odp.spijk.Pattern pattern23 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string37 = replaceEntities("en-US");
		pattern23.setCommonLanguage(string37);
		String string38 = replaceEntities("is your humidity too low");
		pattern23.getSpijkUtterances().add(string38);
		String string39 = replaceEntities("do you have too little water");
		pattern23.getSpijkUtterances().add(string39);
		String string40 = replaceEntities("did you get too little water");
		pattern23.getSpijkUtterances().add(string40);
		String string41 = replaceEntities("are you thirsty");
		pattern23.getSpijkUtterances().add(string41);
		return pattern23;
	}

	de.semvox.types.odp.spijk.Pattern getPattern24() {
		final de.semvox.types.odp.spijk.Pattern pattern24 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string42 = replaceEntities("nl-BE");
		pattern24.setCommonLanguage(string42);
		String string43 = replaceEntities("is je vochtigheids?(sgraad) te laag");
		pattern24.getSpijkUtterances().add(string43);
		String string44 = replaceEntities("heb je te weinig water ?(gekregen)");
		pattern24.getSpijkUtterances().add(string44);
		String string45 = replaceEntities("heb je dorst");
		pattern24.getSpijkUtterances().add(string45);
		String string46 = replaceEntities("wil je meer water");
		pattern24.getSpijkUtterances().add(string46);
		String string47 = replaceEntities("heb je meer water nodig");
		pattern24.getSpijkUtterances().add(string47);
		String string48 = replaceEntities("heeft (de|je) plant ?(niet) te weinig water");
		pattern24.getSpijkUtterances().add(string48);
		return pattern24;
	}

	de.semvox.types.odp.spijk.References getReferences25() {
		final de.semvox.types.odp.spijk.References references25 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string49 = replaceEntities("");
		references25.setSpijkValue(string49);
		return references25;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask26() {
		final de.semvox.types.odp.interaction.RequestTask requestTask26 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetHumidity_YesNo_Negative", requestTask26);
		com.zorabots.ontologies.vmn.YesNoHumidity yesNoHumidity27 = getYesNoHumidity27();
		requestTask26.setCommonContent(yesNoHumidity27);
		return requestTask26;
	}

	com.zorabots.ontologies.vmn.YesNoHumidity getYesNoHumidity27() {
		final com.zorabots.ontologies.vmn.YesNoHumidity yesNoHumidity27 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newYesNoHumidity();
		com.zorabots.ontologies.vmn.NegativeQuestion negativeQuestion28 = getNegativeQuestion28();
		yesNoHumidity27.setVmnYesNoQuestionType(negativeQuestion28);
		return yesNoHumidity27;
	}

	com.zorabots.ontologies.vmn.NegativeQuestion getNegativeQuestion28() {
		final com.zorabots.ontologies.vmn.NegativeQuestion negativeQuestion28 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNegativeQuestion();
		return negativeQuestion28;
	}
}