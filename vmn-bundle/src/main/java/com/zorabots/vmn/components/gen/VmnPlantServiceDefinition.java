/**
* GENERATED CODE
*/
package com.zorabots.vmn.components.gen;

public final class VmnPlantServiceDefinition extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.subcon.integration.ServiceDefinition> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "int#ServiceDefinition";

	public VmnPlantServiceDefinition(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public VmnPlantServiceDefinition(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public VmnPlantServiceDefinition(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public VmnPlantServiceDefinition() {
		super();
	}

	@Override
	protected de.semvox.types.subcon.integration.ServiceDefinition createThing() {
		return getServiceDefinition();
	}

	@Override
	protected void initialize(final de.semvox.types.subcon.integration.ServiceDefinition serviceDefinition0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("VmnPlantService");
		serviceDefinition0.setCommonName(string0);
		String string1 = replaceEntities("com.zorabots.vmn.components");
		serviceDefinition0.setIntNamespace(string1);
		com.zorabots.ontologies.vmn.GetPlantName getPlantName1 = getGetPlantName1();
		serviceDefinition0.getIntSubscriptionsAsIntServiceRequest().put("GET_PLANTNAME", getPlantName1);
		com.zorabots.ontologies.vmn.GetParameters getParameters2 = getGetParameters2();
		serviceDefinition0.getIntSubscriptionsAsIntServiceRequest().put("GET_PARAMETERS", getParameters2);
	}

	de.semvox.types.subcon.integration.ServiceDefinition getServiceDefinition() {
		final de.semvox.types.subcon.integration.ServiceDefinition serviceDefinition0 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newServiceDefinition();
		return serviceDefinition0;
	}


	com.zorabots.ontologies.vmn.GetPlantName getGetPlantName1() {
		final com.zorabots.ontologies.vmn.GetPlantName getPlantName1 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetPlantName();
		return getPlantName1;
	}

	com.zorabots.ontologies.vmn.GetParameters getGetParameters2() {
		final com.zorabots.ontologies.vmn.GetParameters getParameters2 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetParameters();
		return getParameters2;
	}
}