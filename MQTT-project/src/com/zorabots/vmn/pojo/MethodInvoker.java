package com.zorabots.vmn.pojo;

import java.lang.reflect.Method;

public class MethodInvoker {

	private Object o;
	private Method m;
	
	public MethodInvoker(Object o, Method m){
		this.m = m;
		this.o = o;
	}

	/**
	 * @return the o
	 */
	public Object getO() {
		return o;
	}

	/**
	 * @return the m
	 */
	public Method getM() {
		return m;
	}

	
	
}
