package com.zorabots.vmn.pojo;

import java.util.HashMap;

import org.json.simple.JSONObject;

import com.zorabots.vmn.events.JsonParser;

public class Plant {
	private String name;
	private String latinName;
	private String uniqueID;
	private String description = ""; 
	private HashMap<String, PlantParameter>  plantParamters;
	private static Plant plant = null;
	
	//Singleton
	private Plant(){
		
	}
	
	public static Plant getInstance(){
		if(plant == null){
			plant = new Plant();
		}
		return plant;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param latinName the latinName to set
	 */
	private void setLatinName(String latinName) {
		this.latinName = latinName;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * @return the plantParamters
	 */
	public HashMap<String, PlantParameter> getPlantParamters() {
		return plantParamters;
	}
	/**
	 * @param plantParamters the plantParamters to set
	 */
	private void setPlantParamters(HashMap<String, PlantParameter> plantParamters) {
		this.plantParamters = plantParamters;
	}
	
	public void setPlant(String json){
		
		JSONObject jsonObject = JsonParser.parseJson(json);
		
		JSONObject parameters = (JSONObject)jsonObject.get("parameters");
		
		this.latinName = (String) parameters.get("latin_name");
		
		JSONObject pot = (JSONObject) jsonObject.get("pot");
		this.name = (String) pot.get("name");
		this.uniqueID = (String) pot.get("unique_id");
		
		this.plantParamters = new HashMap<String, PlantParameter>();
		
		this.plantParamters.put("temperature", new PlantParameter());
		this.plantParamters.put("humidity", new PlantParameter());
		this.plantParamters.put("light", new PlantParameter());
		
		if(parameters.containsKey("temperature")){
			this.plantParamters.get("temperature").setUnit("graden");
			JSONObject temperature = (JSONObject) parameters.get("temperature");
			this.plantParamters.put("temperature",makeParameter(temperature, this.plantParamters.get("temperature")));
			 
		}
		
		if(parameters.containsKey("humidity")){
			this.plantParamters.get("humidity").setUnit("%");
			JSONObject humidity = (JSONObject) parameters.get("humidity");
			this.plantParamters.put("humidity",makeParameter(humidity, this.plantParamters.get("humidity")));
		}
		
		if(parameters.containsKey("light")){
			this.plantParamters.get("light").setUnit("lux");
			JSONObject light = (JSONObject) parameters.get("light");
			this.plantParamters.put("light",makeParameter(light, this.plantParamters.get("light")));
		}
		
		/*
		Plant.getInstance().getPlantParamters().get("humidity").setOptimal(optHum);
		
		
		JSONObject plant = (JSONObject) jsonObject.get("plant");
		getInstance().setLatinName((String)plant.get("latin_name"));
		getInstance().setName((String)plant.get("name"));
		
		JSONObject parameter = (JSONObject) jsonObject.get("parameters");
		*/
		
	}
	
	private PlantParameter makeParameter(JSONObject object, PlantParameter plantparameter){
		try{
			//PlantParameter p = plantparameter;
			
			if(object.containsKey("optimal")){
				double optimalvalue = (Double)object.get("optimal");
				int intoptvalue = (int) Math.round(optimalvalue);
				plantparameter.setOptimal(Integer.toString(intoptvalue));
			}
			
			
			if(object.containsKey("minimum")){
				double minimumvalue = (Double)object.get("minimum");
				int intminvalue = (int) Math.round(minimumvalue);
				plantparameter.setMinimum(Integer.toString(intminvalue));
			}
			
			
			if(object.containsKey("maximum")){
				double maximumvalue = (Double)object.get("maximum");
				int intmaxvalue = (int) Math.round(maximumvalue);
				plantparameter.setMaximum(Integer.toString(intmaxvalue));
			}
			
			return plantparameter;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;

	}
}
