/**
* GENERATED CODE
*/
package com.zorabots.weather.nlu.gen;

public final class WeatherGrammar extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.spijk.StandaloneGrammar> implements de.semvox.commons.Generated {

	private java.util.Map<java.lang.String, de.semvox.subcon.data.Thing> idRefMap = new java.util.HashMap<java.lang.String, de.semvox.subcon.data.Thing>();

	public static java.lang.String TYPE_NAME = "spijk#StandaloneGrammar";

	public WeatherGrammar(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public WeatherGrammar(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public WeatherGrammar(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public WeatherGrammar() {
		super();
	}

	@Override
	protected de.semvox.types.odp.spijk.StandaloneGrammar createThing() {
		return getStandaloneGrammar();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.spijk.StandaloneGrammar standaloneGrammar0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("WeatherGrammar");
		standaloneGrammar0.setCommonName(string0);
		de.semvox.types.asr.MainGrammar mainGrammar1 = getMainGrammar1();
		standaloneGrammar0.setAsrGrammarType(mainGrammar1);
		de.semvox.subcon.data.Thing include2 = getThing2();
		standaloneGrammar0.getSpijkImports().add((de.semvox.types.odp.spijk.Grammar)include2);
		de.semvox.types.odp.spijk.Rule rule3 = getRule3();
		standaloneGrammar0.getSpijkElements().add(rule3);
		de.semvox.types.odp.spijk.Rule rule10 = getRule10();
		standaloneGrammar0.getSpijkElements().add(rule10);
		de.semvox.types.odp.spijk.Rule rule17 = getRule17();
		standaloneGrammar0.getSpijkElements().add(rule17);
		de.semvox.types.odp.spijk.Rule rule24 = getRule24();
		standaloneGrammar0.getSpijkElements().add(rule24);
		de.semvox.types.odp.spijk.Rule rule31 = getRule31();
		standaloneGrammar0.getSpijkElements().add(rule31);
		de.semvox.types.odp.spijk.Rule rule38 = getRule38();
		standaloneGrammar0.getSpijkElements().add(rule38);
	}

	de.semvox.types.odp.spijk.StandaloneGrammar getStandaloneGrammar() {
		final de.semvox.types.odp.spijk.StandaloneGrammar standaloneGrammar0 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newStandaloneGrammar();
		return standaloneGrammar0;
	}


	de.semvox.types.asr.MainGrammar getMainGrammar1() {
		final de.semvox.types.asr.MainGrammar mainGrammar1 = de.semvox.types.asr.factory.PatternFactoryAsr.newMainGrammar();
		return mainGrammar1;
	}

	de.semvox.subcon.data.Thing getThing2() {
		java.util.Map<java.lang.String, java.lang.String> newEntities = new java.util.HashMap<java.lang.String, java.lang.String>(entities);
		de.semvox.subcon.data.Thing include2 = new com.zorabots.weather.nlu.gen.WeatherResources(getParameterProvider(), newEntities).loadThing(false);
		return include2;
	}

	de.semvox.types.odp.spijk.Rule getRule3() {
		final de.semvox.types.odp.spijk.Rule rule3 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string1 = replaceEntities("RequestTask_GetWeatherForecast_Morning");
		rule3.setCommonName(string1);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern4 = getPattern4();
			rule3.getSpijkPatterns().add(pattern4);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern5 = getPattern5();
			rule3.getSpijkPatterns().add(pattern5);
		}
		de.semvox.types.odp.spijk.References references6 = getReferences6();
		rule3.setSpijkReferences(references6);
		de.semvox.types.odp.interaction.RequestTask requestTask7 = getRequestTask7();
		rule3.setMmInterpretation(requestTask7);
		return rule3;
	}

	de.semvox.types.odp.spijk.Pattern getPattern4() {
		final de.semvox.types.odp.spijk.Pattern pattern4 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string2 = replaceEntities("en-US");
		pattern4.setCommonLanguage(string2);
		String string3 = replaceEntities("what will the weather be ?(like) this morning");
		pattern4.getSpijkUtterances().add(string3);
		String string4 = replaceEntities("what is the weather forecast for this morning");
		pattern4.getSpijkUtterances().add(string4);
		return pattern4;
	}

	de.semvox.types.odp.spijk.Pattern getPattern5() {
		final de.semvox.types.odp.spijk.Pattern pattern5 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string5 = replaceEntities("nl-BE");
		pattern5.setCommonLanguage(string5);
		String string6 = replaceEntities("wat is het weerbericht voor deze ochtend");
		pattern5.getSpijkUtterances().add(string6);
		return pattern5;
	}

	de.semvox.types.odp.spijk.References getReferences6() {
		final de.semvox.types.odp.spijk.References references6 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string7 = replaceEntities("");
		references6.setSpijkValue(string7);
		return references6;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask7() {
		final de.semvox.types.odp.interaction.RequestTask requestTask7 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetWeatherForecast_Morning", requestTask7);
		com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast8 = getGetWeatherForecast8();
		requestTask7.setCommonContent(getWeatherForecast8);
		return requestTask7;
	}

	com.zorabots.ontologies.weather.GetWeatherForecast getGetWeatherForecast8() {
		final com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast8 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newGetWeatherForecast();
		com.zorabots.ontologies.weather.Morning morning9 = getMorning9();
		getWeatherForecast8.setWeatherRelativeMoment(morning9);
		return getWeatherForecast8;
	}

	com.zorabots.ontologies.weather.Morning getMorning9() {
		final com.zorabots.ontologies.weather.Morning morning9 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newMorning();
		return morning9;
	}

	de.semvox.types.odp.spijk.Rule getRule10() {
		final de.semvox.types.odp.spijk.Rule rule10 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string8 = replaceEntities("RequestTask_GetWeatherForecast_Afternoon");
		rule10.setCommonName(string8);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern11 = getPattern11();
			rule10.getSpijkPatterns().add(pattern11);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern12 = getPattern12();
			rule10.getSpijkPatterns().add(pattern12);
		}
		de.semvox.types.odp.spijk.References references13 = getReferences13();
		rule10.setSpijkReferences(references13);
		de.semvox.types.odp.interaction.RequestTask requestTask14 = getRequestTask14();
		rule10.setMmInterpretation(requestTask14);
		return rule10;
	}

	de.semvox.types.odp.spijk.Pattern getPattern11() {
		final de.semvox.types.odp.spijk.Pattern pattern11 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string9 = replaceEntities("en-US");
		pattern11.setCommonLanguage(string9);
		String string10 = replaceEntities("what will the weather be ?(like) this afternoon");
		pattern11.getSpijkUtterances().add(string10);
		String string11 = replaceEntities("what is the weather forecast for this afternoon");
		pattern11.getSpijkUtterances().add(string11);
		return pattern11;
	}

	de.semvox.types.odp.spijk.Pattern getPattern12() {
		final de.semvox.types.odp.spijk.Pattern pattern12 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string12 = replaceEntities("nl-BE");
		pattern12.setCommonLanguage(string12);
		String string13 = replaceEntities("wat is het weerbericht voor deze namiddag");
		pattern12.getSpijkUtterances().add(string13);
		return pattern12;
	}

	de.semvox.types.odp.spijk.References getReferences13() {
		final de.semvox.types.odp.spijk.References references13 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string14 = replaceEntities("");
		references13.setSpijkValue(string14);
		return references13;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask14() {
		final de.semvox.types.odp.interaction.RequestTask requestTask14 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetWeatherForecast_Afternoon", requestTask14);
		com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast15 = getGetWeatherForecast15();
		requestTask14.setCommonContent(getWeatherForecast15);
		return requestTask14;
	}

	com.zorabots.ontologies.weather.GetWeatherForecast getGetWeatherForecast15() {
		final com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast15 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newGetWeatherForecast();
		com.zorabots.ontologies.weather.Afternoon afternoon16 = getAfternoon16();
		getWeatherForecast15.setWeatherRelativeMoment(afternoon16);
		return getWeatherForecast15;
	}

	com.zorabots.ontologies.weather.Afternoon getAfternoon16() {
		final com.zorabots.ontologies.weather.Afternoon afternoon16 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newAfternoon();
		return afternoon16;
	}

	de.semvox.types.odp.spijk.Rule getRule17() {
		final de.semvox.types.odp.spijk.Rule rule17 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string15 = replaceEntities("RequestTask_GetWeatherForecast_Evening");
		rule17.setCommonName(string15);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern18 = getPattern18();
			rule17.getSpijkPatterns().add(pattern18);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern19 = getPattern19();
			rule17.getSpijkPatterns().add(pattern19);
		}
		de.semvox.types.odp.spijk.References references20 = getReferences20();
		rule17.setSpijkReferences(references20);
		de.semvox.types.odp.interaction.RequestTask requestTask21 = getRequestTask21();
		rule17.setMmInterpretation(requestTask21);
		return rule17;
	}

	de.semvox.types.odp.spijk.Pattern getPattern18() {
		final de.semvox.types.odp.spijk.Pattern pattern18 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string16 = replaceEntities("en-US");
		pattern18.setCommonLanguage(string16);
		String string17 = replaceEntities("what will the weather be ?(like) this evening");
		pattern18.getSpijkUtterances().add(string17);
		String string18 = replaceEntities("what is the weather forecast for this evening");
		pattern18.getSpijkUtterances().add(string18);
		return pattern18;
	}

	de.semvox.types.odp.spijk.Pattern getPattern19() {
		final de.semvox.types.odp.spijk.Pattern pattern19 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string19 = replaceEntities("nl-BE");
		pattern19.setCommonLanguage(string19);
		String string20 = replaceEntities("wat is het weerbericht voor deze avond");
		pattern19.getSpijkUtterances().add(string20);
		return pattern19;
	}

	de.semvox.types.odp.spijk.References getReferences20() {
		final de.semvox.types.odp.spijk.References references20 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string21 = replaceEntities("");
		references20.setSpijkValue(string21);
		return references20;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask21() {
		final de.semvox.types.odp.interaction.RequestTask requestTask21 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetWeatherForecast_Evening", requestTask21);
		com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast22 = getGetWeatherForecast22();
		requestTask21.setCommonContent(getWeatherForecast22);
		return requestTask21;
	}

	com.zorabots.ontologies.weather.GetWeatherForecast getGetWeatherForecast22() {
		final com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast22 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newGetWeatherForecast();
		com.zorabots.ontologies.weather.Evening evening23 = getEvening23();
		getWeatherForecast22.setWeatherRelativeMoment(evening23);
		return getWeatherForecast22;
	}

	com.zorabots.ontologies.weather.Evening getEvening23() {
		final com.zorabots.ontologies.weather.Evening evening23 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newEvening();
		return evening23;
	}

	de.semvox.types.odp.spijk.Rule getRule24() {
		final de.semvox.types.odp.spijk.Rule rule24 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string22 = replaceEntities("RequestTask_GetWeatherForecast_Tomorrow");
		rule24.setCommonName(string22);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern25 = getPattern25();
			rule24.getSpijkPatterns().add(pattern25);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern26 = getPattern26();
			rule24.getSpijkPatterns().add(pattern26);
		}
		de.semvox.types.odp.spijk.References references27 = getReferences27();
		rule24.setSpijkReferences(references27);
		de.semvox.types.odp.interaction.RequestTask requestTask28 = getRequestTask28();
		rule24.setMmInterpretation(requestTask28);
		return rule24;
	}

	de.semvox.types.odp.spijk.Pattern getPattern25() {
		final de.semvox.types.odp.spijk.Pattern pattern25 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string23 = replaceEntities("en-US");
		pattern25.setCommonLanguage(string23);
		String string24 = replaceEntities("what will the weather be ?(like) tommorow");
		pattern25.getSpijkUtterances().add(string24);
		String string25 = replaceEntities("what is the weather forecast for tomorrow");
		pattern25.getSpijkUtterances().add(string25);
		return pattern25;
	}

	de.semvox.types.odp.spijk.Pattern getPattern26() {
		final de.semvox.types.odp.spijk.Pattern pattern26 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string26 = replaceEntities("nl-BE");
		pattern26.setCommonLanguage(string26);
		String string27 = replaceEntities("wat is het weerbericht voor morgen");
		pattern26.getSpijkUtterances().add(string27);
		return pattern26;
	}

	de.semvox.types.odp.spijk.References getReferences27() {
		final de.semvox.types.odp.spijk.References references27 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string28 = replaceEntities("");
		references27.setSpijkValue(string28);
		return references27;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask28() {
		final de.semvox.types.odp.interaction.RequestTask requestTask28 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetWeatherForecast_Tomorrow", requestTask28);
		com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast29 = getGetWeatherForecast29();
		requestTask28.setCommonContent(getWeatherForecast29);
		return requestTask28;
	}

	com.zorabots.ontologies.weather.GetWeatherForecast getGetWeatherForecast29() {
		final com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast29 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newGetWeatherForecast();
		com.zorabots.ontologies.weather.Tomorrow tomorrow30 = getTomorrow30();
		getWeatherForecast29.setWeatherRelativeMoment(tomorrow30);
		return getWeatherForecast29;
	}

	com.zorabots.ontologies.weather.Tomorrow getTomorrow30() {
		final com.zorabots.ontologies.weather.Tomorrow tomorrow30 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newTomorrow();
		return tomorrow30;
	}

	de.semvox.types.odp.spijk.Rule getRule31() {
		final de.semvox.types.odp.spijk.Rule rule31 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string29 = replaceEntities("RequestTask_GetWeatherForecast_DayAfterTomorrow");
		rule31.setCommonName(string29);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern32 = getPattern32();
			rule31.getSpijkPatterns().add(pattern32);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern33 = getPattern33();
			rule31.getSpijkPatterns().add(pattern33);
		}
		de.semvox.types.odp.spijk.References references34 = getReferences34();
		rule31.setSpijkReferences(references34);
		de.semvox.types.odp.interaction.RequestTask requestTask35 = getRequestTask35();
		rule31.setMmInterpretation(requestTask35);
		return rule31;
	}

	de.semvox.types.odp.spijk.Pattern getPattern32() {
		final de.semvox.types.odp.spijk.Pattern pattern32 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string30 = replaceEntities("en-US");
		pattern32.setCommonLanguage(string30);
		String string31 = replaceEntities("what will the weather be ?(like) the day after tommorow");
		pattern32.getSpijkUtterances().add(string31);
		String string32 = replaceEntities("what is the weather forecast for the day after tomorrow");
		pattern32.getSpijkUtterances().add(string32);
		return pattern32;
	}

	de.semvox.types.odp.spijk.Pattern getPattern33() {
		final de.semvox.types.odp.spijk.Pattern pattern33 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string33 = replaceEntities("nl-BE");
		pattern33.setCommonLanguage(string33);
		String string34 = replaceEntities("wat is het weerbericht voor overmorgen");
		pattern33.getSpijkUtterances().add(string34);
		return pattern33;
	}

	de.semvox.types.odp.spijk.References getReferences34() {
		final de.semvox.types.odp.spijk.References references34 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string35 = replaceEntities("");
		references34.setSpijkValue(string35);
		return references34;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask35() {
		final de.semvox.types.odp.interaction.RequestTask requestTask35 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetWeatherForecast_DayAfterTomorrow", requestTask35);
		com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast36 = getGetWeatherForecast36();
		requestTask35.setCommonContent(getWeatherForecast36);
		return requestTask35;
	}

	com.zorabots.ontologies.weather.GetWeatherForecast getGetWeatherForecast36() {
		final com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast36 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newGetWeatherForecast();
		com.zorabots.ontologies.weather.DayAfterTomorrow dayAfterTomorrow37 = getDayAfterTomorrow37();
		getWeatherForecast36.setWeatherRelativeMoment(dayAfterTomorrow37);
		return getWeatherForecast36;
	}

	com.zorabots.ontologies.weather.DayAfterTomorrow getDayAfterTomorrow37() {
		final com.zorabots.ontologies.weather.DayAfterTomorrow dayAfterTomorrow37 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newDayAfterTomorrow();
		return dayAfterTomorrow37;
	}

	de.semvox.types.odp.spijk.Rule getRule38() {
		final de.semvox.types.odp.spijk.Rule rule38 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newRule();
		String string36 = replaceEntities("RequestTask_GetWeather");
		rule38.setCommonName(string36);
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spijk.Pattern pattern39 = getPattern39();
			rule38.getSpijkPatterns().add(pattern39);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spijk.Pattern pattern40 = getPattern40();
			rule38.getSpijkPatterns().add(pattern40);
		}
		de.semvox.types.odp.spijk.References references41 = getReferences41();
		rule38.setSpijkReferences(references41);
		de.semvox.types.odp.interaction.RequestTask requestTask42 = getRequestTask42();
		rule38.setMmInterpretation(requestTask42);
		return rule38;
	}

	de.semvox.types.odp.spijk.Pattern getPattern39() {
		final de.semvox.types.odp.spijk.Pattern pattern39 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string37 = replaceEntities("en-US");
		pattern39.setCommonLanguage(string37);
		String string38 = replaceEntities("what is the weather ?(forecast) ?(for today)");
		pattern39.getSpijkUtterances().add(string38);
		return pattern39;
	}

	de.semvox.types.odp.spijk.Pattern getPattern40() {
		final de.semvox.types.odp.spijk.Pattern pattern40 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newPattern();
		String string39 = replaceEntities("nl-BE");
		pattern40.setCommonLanguage(string39);
		String string40 = replaceEntities("wat is het weerbericht ?(voor vandaag)");
		pattern40.getSpijkUtterances().add(string40);
		return pattern40;
	}

	de.semvox.types.odp.spijk.References getReferences41() {
		final de.semvox.types.odp.spijk.References references41 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newReferences();
		String string41 = replaceEntities("");
		references41.setSpijkValue(string41);
		return references41;
	}

	de.semvox.types.odp.interaction.RequestTask getRequestTask42() {
		final de.semvox.types.odp.interaction.RequestTask requestTask42 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newRequestTask();
		idRefMap.put("__RequestTask_GetWeather", requestTask42);
		com.zorabots.ontologies.weather.GetWeather getWeather43 = getGetWeather43();
		requestTask42.setCommonContent(getWeather43);
		return requestTask42;
	}

	com.zorabots.ontologies.weather.GetWeather getGetWeather43() {
		final com.zorabots.ontologies.weather.GetWeather getWeather43 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newGetWeather();
		return getWeather43;
	}
}