package com.zorabots.vmn.components;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import com.zorabots.ontologies.vmn.GetDate;
import com.zorabots.ontologies.vmn.GetTime;
import com.zorabots.vmn.components.gen.VmnDateServiceDefinition;

import de.semvox.commons.logging.Logger;
import de.semvox.commons.logging.Logger.LogLevel;
import de.semvox.subcon.integration.api.Service;
import de.semvox.types.subcon.integration.ServiceDefinition;
import de.semvox.types.subcon.integration.ServiceRequest;
import de.semvox.types.subcon.integration.factory.ThingFactoryIntegration;
import de.semvox.types.time.Time;
import de.semvox.types.time.factory.ThingFactoryTime;

public class VmnDateService implements Service {

	private final ServiceDefinition definition;

	public VmnDateService() throws Exception {
		definition = new VmnDateServiceDefinition().loadThing();
	}

	@Override
	public ServiceDefinition getServiceDefinition() {
		return definition;
	}

	@Override
	public void dispose() {

	}

	@Override
	public ServiceRequest handleRequest(ServiceRequest request,
			Map<String, Object> boundVariables) {
		TimeZone zone = TimeZone.getTimeZone("Europe/Brussels");
		Date c = Calendar.getInstance(zone).getTime();
		
		if(request instanceof GetDate){	
			de.semvox.types.time.Date d = ThingFactoryTime.newDate();
			
			SimpleDateFormat dayOfMonth = new SimpleDateFormat("dd");
			dayOfMonth.setTimeZone(zone);
			SimpleDateFormat month = new SimpleDateFormat("MM");
			SimpleDateFormat year = new SimpleDateFormat("yyyy");
			
			String strDayOfMonth = dayOfMonth.format(c);
			String strMonth = month.format(c);
			String strYear = year.format(c);
			
			d.setTimeDayOfMonth(Integer.parseInt(strDayOfMonth));
			d.setTimeMonth(Integer.parseInt(strMonth));
			d.setTimeYear(Integer.parseInt(strYear));
			
			((GetDate) request).setTimeDate(d);

		}
		
		if(request instanceof GetTime){
			Time t = ThingFactoryTime.newTime();
						
			SimpleDateFormat hours = new SimpleDateFormat("hh");
			hours.setTimeZone(zone);
			SimpleDateFormat minutes = new SimpleDateFormat("mm");
			SimpleDateFormat seconds = new SimpleDateFormat("ss");
			
			String strHours = hours.format(c);
			String strMinutes = minutes.format(c);
			String strSeconds = seconds.format(c);
			 
			if(strHours.length() == 1){
				strHours = "0"+strHours;
			}
			if(strMinutes.length() == 1){
				strMinutes = "0" +strMinutes;
			}
						
			t.setTimeHour(Integer.parseInt(strHours));
			t.setTimeMinute(Integer.parseInt(strMinutes));
			t.setTimeSecond(Integer.parseInt(strSeconds));
			
			Logger.log(LogLevel.INFO, "VMN TIME", "Time found!!!!!! " + strMinutes);
			
			((GetTime) request).setTimeTime(t);
		
		}
		request.setIntResponseStatus(ThingFactoryIntegration.newStatusSuccess());
		
		return request;
	}

}
