package com.zorabots.weather.components;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.zorabots.ontologies.weather.Afternoon;
import com.zorabots.ontologies.weather.Evening;
import com.zorabots.ontologies.weather.Expired;
import com.zorabots.ontologies.weather.GetWeather;
import com.zorabots.ontologies.weather.GetWeatherForecast;
import com.zorabots.ontologies.weather.Morning;
import com.zorabots.ontologies.weather.OnTime;
import com.zorabots.ontologies.weather.RelativeMoment;
import com.zorabots.ontologies.weather.TimePassed;
import com.zorabots.ontologies.weather.Today;
import com.zorabots.ontologies.weather.Tomorrow;
import com.zorabots.ontologies.weather.factory.ThingFactoryWeather;
import com.zorabots.socket.Client;
import com.zorabots.vmn.events.JsonParser;
import com.zorabots.weather.components.gen.WeatherServiceDefinition;

import de.semvox.commons.logging.Logger.LogLevel;
import de.semvox.odp.multimodal.LanguageProvider;
import de.semvox.subcon.data.ThingUtils;
import de.semvox.subcon.integration.api.IpcFacade;
import de.semvox.subcon.integration.api.Service;
import de.semvox.types.odp.multimodal.AsyncServiceResponse;
import de.semvox.types.odp.multimodal.PresentEvent;
import de.semvox.types.odp.multimodal.factory.ThingFactoryMultimodal;
import de.semvox.types.subcon.integration.AsyncServiceRequest;
import de.semvox.types.subcon.integration.ServiceDefinition;
import de.semvox.types.subcon.integration.ServiceRequest;
import de.semvox.types.subcon.integration.factory.ThingFactoryIntegration;

public class WeatherService implements Service {
	private static final int ANYDAYSHOUR = 14;
	
	private final ServiceDefinition definition;
	private IpcFacade ipcFacade;
	private LanguageProvider languageprovider;
	
	private static Map<String, Integer> todaysMomentHours = new HashMap<String, Integer>();
	static {
		todaysMomentHours.put("morning", 9); 
		todaysMomentHours.put("afternoon", 15);
		todaysMomentHours.put("evening", 21);
	}
	
	private static Map<String, Integer> todaysBorderHours = new HashMap<String, Integer>();
	static {
		todaysBorderHours.put("morning", 12); 
		todaysBorderHours.put("afternoon", 18);
		todaysBorderHours.put("evening", 0);
	}

	public WeatherService(IpcFacade ipcFacade, LanguageProvider languageprovider) throws Exception {
		definition = new WeatherServiceDefinition().loadThing();
		this.ipcFacade = ipcFacade;
		this.languageprovider = languageprovider;
	}

	@Override
	public ServiceDefinition getServiceDefinition() {
		return definition;
	}

	@Override
	public void dispose() {

	}

	@Override
	public ServiceRequest handleRequest(ServiceRequest request,
			Map<String, Object> boundVariables) {
		if(request instanceof AsyncServiceRequest){
			AsyncServiceRequest asyncServiceRequest = (AsyncServiceRequest)request;
				
			if(asyncServiceRequest instanceof GetWeather){
				asyncServiceRequest.setIntResponseStatus(ThingFactoryIntegration.newStatusAccepted());
				final GetWeather getWeatherRequest = ThingUtils.deepCopy((GetWeather)request);
    	        Client.getInstance().setNotLast(true);
				Thread thread = new Thread(){
		        	public void run(){
		        		try {
							Thread.sleep(4000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
		        		de.semvox.commons.logging.Logger.log(LogLevel.INFO, "CURRENTWEATHERTHREAD", "CurrentWeatherThread started!!");
		        		URL url;
		    			String inputLine = "";
		    			Boolean timeOut = false;
		    	        try {
		    	            // get URL content
		    	            String address="http://api.openweathermap.org/data/2.5/weather?q=ostend,belgium&lang=nl&units=metric&APPID=531ba891d5a754b3b3c317bd8b4efe2f";
		    	            url = new URL(address);
		    	            URLConnection conn = url.openConnection();
		    	            conn.setConnectTimeout(20000);
		    	            conn.setReadTimeout(20000);

		    	            // open the stream and put it into BufferedReader
		    	            BufferedReader br = new BufferedReader(
		    	                               new InputStreamReader(conn.getInputStream()));
		    	            //de.semvox.commons.logging.Logger.log(LogLevel.INFO, "THREAD", "Stage 2");
		    	            inputLine = br.readLine();
		    	            //de.semvox.commons.logging.Logger.log(LogLevel.INFO, "THREAD", inputLine);
		    	            br.close();

		    	        } catch (MalformedURLException e) {
		    	            e.printStackTrace();
		    	        } catch (IOException e) {
		    	        	timeOut = true;
		    	            e.printStackTrace();
		    	        } 
		    	        
		    	        String message = "";
		    	        String encmessage = "";
		    	        
		    	        if(!timeOut){
			    			//de.semvox.commons.logging.Logger.log(LogLevel.INFO, "json", inputLine);
			    			JSONObject jsonObject = JsonParser.parseJson(inputLine);
			    			JSONObject mainobject = (JSONObject)jsonObject.get("main");
			    			Double tempDouble = 0.0;
			    			Long tempLong = (long) 0;
			    			String strtemp = "";
			    			try{
			    				tempDouble = (Double) mainobject.get("temp");
			    				tempLong = (Long) Math.round(tempDouble);
			    				strtemp = Double.toString(tempLong);
			    				String language = languageprovider.getCurrentLanguage();
			    				//only for dutch
			    				if (language.equals("nl-BE")){
			    					strtemp = strtemp.replaceAll("\\.", ",");
			    				}
			    			}
			    			catch (ClassCastException exc){
			    				tempLong = (long) mainobject.get("temp");
			    				strtemp = Long.toString(tempLong);
			    			}
			    			JSONArray weatherarray = (JSONArray)jsonObject.get("weather");
			    			JSONObject weatherobject = (JSONObject) weatherarray.get(0);
			    			//de.semvox.commons.logging.Logger.log(LogLevel.INFO, "MESSAGE", "data extracted");
			    			
			    			message = strtemp + " graden en " + (String)weatherobject.get("description");
							try {
								//message = message.replaceAll("”", "");
								//message = message.replaceAll("“", "");
								encmessage = new String(message.getBytes("UTF-8"));
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
		    	        }
		    			//de.semvox.commons.logging.Logger.log(LogLevel.INFO, "ENCMESSAGE", encmessage);
		    			
		    			getWeatherRequest.setCommonStringValue(encmessage);
		    			if (timeOut){
		    				Expired expired = ThingFactoryWeather.newExpired();
		    				getWeatherRequest.setWeatherTimeOut(expired);
		    			}
		    			else{
		    				OnTime ontime = ThingFactoryWeather.newOnTime();
		    				getWeatherRequest.setWeatherTimeOut(ontime);
		    			}
		    			getWeatherRequest.setIntResponseStatus(ThingFactoryIntegration.newStatusSuccess());
		    			
		    			PresentEvent presentEvent = ThingFactoryMultimodal.newPresentEvent();
		    	        presentEvent.setMmTimeToLive((long) 2000);
		    	         
		    	        AsyncServiceResponse asyncServiceResponse = ThingFactoryMultimodal.newAsyncServiceResponse();
		    	        asyncServiceResponse.setIntResponse(getWeatherRequest);
		    	        asyncServiceResponse.setMmCorrelationId(getWeatherRequest.getCommonId());
		    	         
		    	        presentEvent.setMmEvent(asyncServiceResponse);
		    	        ipcFacade.invokeService(presentEvent,
		    	                 String.format("%s.%s", definition.getIntNamespace(), definition.getCommonName()));
		        	}
		        };
		        thread.start();
			}
			
			if(asyncServiceRequest instanceof GetWeatherForecast){
				final GetWeatherForecast getWeatherForecastRequest = ThingUtils.deepCopy((GetWeatherForecast)request);
				
				Boolean todaySelected = this.checkTodaySelected(getWeatherForecastRequest);
				String relativeMomentString = this.getRelativeMomentString(getWeatherForecastRequest.getWeatherRelativeMoment());
				
				if(todaySelected){
					Boolean relativeMomentPassed = this.checkRelativeMomentPassed(WeatherService.todaysBorderHours.get(relativeMomentString));
					if(relativeMomentPassed){
						TimePassed timePassed = ThingFactoryWeather.newTimePassed();
						getWeatherForecastRequest.setWeatherTimeState(timePassed);
						getWeatherForecastRequest.setIntResponseStatus(ThingFactoryIntegration.newStatusNoService());
		    			
		    			PresentEvent presentEvent = ThingFactoryMultimodal.newPresentEvent();
		    	        presentEvent.setMmTimeToLive((long) 2000);
		    	         
		    	        AsyncServiceResponse asyncServiceResponse = ThingFactoryMultimodal.newAsyncServiceResponse();
		    	        asyncServiceResponse.setIntResponse(getWeatherForecastRequest);
		    	        asyncServiceResponse.setMmCorrelationId(getWeatherForecastRequest.getCommonId());
		    	         
		    	        presentEvent.setMmEvent(asyncServiceResponse);
		    	        ipcFacade.invokeService(presentEvent,
		    	                 String.format("%s.%s", definition.getIntNamespace(), definition.getCommonName()));
		    	        return request;
					}
				}
				
				asyncServiceRequest.setIntResponseStatus(ThingFactoryIntegration.newStatusAccepted());
				Client.getInstance().setNotLast(true);
				
				Thread thread = new Thread(){
		        	public void run(){
		        		try {
							Thread.sleep(4000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
		        		de.semvox.commons.logging.Logger.log(LogLevel.INFO, "WEATHERFORECASTTHREAD", "WeatherForecastThread started!!");
		        		URL url;
		    			String inputLine = "";
		    			Boolean timeOut = false;
		    	        try {
		    	            // get URL content
		    	            String address="http://api.openweathermap.org/data/2.5/forecast?q=ostend,belgium&lang=nl&units=metric&APPID=531ba891d5a754b3b3c317bd8b4efe2f";
		    	            url = new URL(address);
		    	            URLConnection conn = url.openConnection();
		    	            conn.setConnectTimeout(20000);
		    	            conn.setReadTimeout(20000);

		    	            // open the stream and put it into BufferedReader
		    	            BufferedReader br = new BufferedReader(
		    	                               new InputStreamReader(conn.getInputStream()));
		    	            //de.semvox.commons.logging.Logger.log(LogLevel.INFO, "THREAD", "Stage 2");
		    	            inputLine = br.readLine();
		    	            //de.semvox.commons.logging.Logger.log(LogLevel.INFO, "THREAD", inputLine);
		    	            br.close();

		    	        } catch (MalformedURLException e) {
		    	            e.printStackTrace();
		    	        } catch (IOException e) {
		    	        	timeOut = true;
		    	            e.printStackTrace();
		    	        } 
		    	        
		    	        String message = "";
		    	        String encmessage = "";
		    	        
		    	        if(!timeOut){
		    	        	String relativeMoment = getWeatherForecastRequest.getWeatherRelativeMoment().getCommonNamedBy().toString();
			    			//de.semvox.commons.logging.Logger.log(LogLevel.INFO, "json", inputLine);
			    			JSONObject jsonObject = JsonParser.parseJson(inputLine);
			    			//de.semvox.commons.logging.Logger.log(LogLevel.INFO, "MESSAGE", "Json parsed");
			    			JSONArray listarray = (JSONArray) jsonObject.get("list");
			    			//de.semvox.commons.logging.Logger.log(LogLevel.INFO, "MESSAGE", "got item array");
			    			
			    			LocalDate dateOfToday = LocalDate.now();
			    			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			    			String dateOfTodayText = dateOfToday.format(formatter);
			    			
			    			JSONObject selectedListObject = null;
			    			if(todaySelected){
			    				int selectedHour = WeatherService.todaysMomentHours.get(relativeMomentString);
			    				String selectedTimestampText = dateOfTodayText + " " + selectedHour + ":00:00";
			    				for(int i = 0; i < listarray.size(); i++){
				    				JSONObject listobject = (JSONObject)listarray.get(i);
				    				
				    				if(listobject.get("dt_text").equals(selectedTimestampText)){
				    					selectedListObject = listobject;
				    					break;
				    				}
			    				
			    				}
			    			}
			    			else{
			    				switch(relativeMomentString){
			    				case "tomorrow" :
			    					LocalDate tomorrowsDate = dateOfToday.plusDays(1); 
			    					DateTimeFormatter tomorrowFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
					    			String tomorrowsDateText = tomorrowsDate.format(tomorrowFormatter);
					    			String selectedTimestampText = tomorrowsDateText + " " + WeatherService.ANYDAYSHOUR + ":00:00";
					    			
					    			for(int i = 0; i < listarray.size(); i++){
					    				JSONObject listobject = (JSONObject)listarray.get(i);
					    				
					    				if(listobject.get("dt_text").equals(selectedTimestampText)){
					    					selectedListObject = listobject;
					    					break;
					    				}
				    				
				    				}
			    				}
			    			}
			    			
			    			JSONObject mainobject = (JSONObject)selectedListObject.get("main");
			    			Double tempDouble = 0.0;
			    			Long tempLong = (long) 0;
			    			String strtemp = "";
			    			try{
			    				tempDouble = (Double) mainobject.get("temp");
			    				tempLong = (Long) Math.round(tempDouble);
			    				strtemp = Double.toString(tempLong);
			    				String language = languageprovider.getCurrentLanguage();
			    				//only for dutch
			    				if (language.equals("nl-BE")){
			    					strtemp = strtemp.replaceAll("\\.", ",");
			    				}
			    			}
			    			catch (ClassCastException exc){
			    				tempLong = (long) mainobject.get("temp");
			    				strtemp = Long.toString(tempLong);
			    			}
			    			JSONArray weatherarray = (JSONArray)selectedListObject.get("weather");
			    			JSONObject weatherobject = (JSONObject) weatherarray.get(0);
			    			//de.semvox.commons.logging.Logger.log(LogLevel.INFO, "MESSAGE", "data extracted");
			    			
			    			message = strtemp + " graden en " + (String)weatherobject.get("description");
							try {
								//message = message.replaceAll("”", "");
								//message = message.replaceAll("“", "");
								encmessage = new String(message.getBytes("UTF-8"));
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
		    	        }
		    			//de.semvox.commons.logging.Logger.log(LogLevel.INFO, "ENCMESSAGE", encmessage);
		    			
		    			getWeatherForecastRequest.setCommonStringValue(encmessage);
		    			if (timeOut){
		    				Expired expired = ThingFactoryWeather.newExpired();
		    				getWeatherForecastRequest.setWeatherTimeOut(expired);
		    			}
		    			else{
		    				OnTime ontime = ThingFactoryWeather.newOnTime();
		    				getWeatherForecastRequest.setWeatherTimeOut(ontime);
		    			}
		    			getWeatherForecastRequest.setIntResponseStatus(ThingFactoryIntegration.newStatusSuccess());
		    			
		    			PresentEvent presentEvent = ThingFactoryMultimodal.newPresentEvent();
		    	        presentEvent.setMmTimeToLive((long) 2000);
		    	         
		    	        AsyncServiceResponse asyncServiceResponse = ThingFactoryMultimodal.newAsyncServiceResponse();
		    	        asyncServiceResponse.setIntResponse(getWeatherForecastRequest);
		    	        asyncServiceResponse.setMmCorrelationId(getWeatherForecastRequest.getCommonId());
		    	         
		    	        presentEvent.setMmEvent(asyncServiceResponse);
		    	        ipcFacade.invokeService(presentEvent,
		    	                 String.format("%s.%s", definition.getIntNamespace(), definition.getCommonName()));
		        	}
		        };
		        thread.start();
				
			}
		}
		return request;
	}
	
	public Boolean checkTodaySelected(GetWeatherForecast getWeatherForecastRequest){
		RelativeMoment relativeMoment = getWeatherForecastRequest.getWeatherRelativeMoment();
		
		if(relativeMoment instanceof Today){
			return false;
		}
		else{
			return true;
		}
	}
	
	public String getRelativeMomentString(RelativeMoment relativeMoment){
		if(relativeMoment instanceof Morning){
			return "morning";
		}
		else if(relativeMoment instanceof Afternoon){
			return "afternoon";
		}
		else if(relativeMoment instanceof Evening){
			return "evening";
		}
		else if (relativeMoment instanceof Tomorrow){
			return "tomorrow";
		}
		else {
			return "dayaftertomorrow";
		}
	}
	
	
	public Boolean checkRelativeMomentPassed(int relativeBorderHour){
		if (Calendar.HOUR_OF_DAY >= relativeBorderHour){
			return true;
		}
		else{
			return false;
		}
	}
}
