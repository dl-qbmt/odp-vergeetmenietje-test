/**
* GENERATED CODE
*/
package com.zorabots.vmn.nlg.gen;

public final class VmnEventTemplates extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.spit.TemplateCollection> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "spit#TemplateCollection";

	public VmnEventTemplates(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public VmnEventTemplates(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public VmnEventTemplates(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public VmnEventTemplates() {
		super();
	}

	@Override
	protected de.semvox.types.odp.spit.TemplateCollection createThing() {
		return getTemplateCollection();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.spit.TemplateCollection templateCollection0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("VmnEventTemplates");
		templateCollection0.setCommonName(string0);
		de.semvox.subcon.data.Thing include1 = getThing1();
		templateCollection0.getSpitImports().add((de.semvox.types.odp.spit.TemplateCollection)include1);
		de.semvox.subcon.data.Thing include2 = getThing2();
		templateCollection0.getSpitImports().add((de.semvox.types.odp.spit.TemplateCollection)include2);
		de.semvox.subcon.data.Thing include3 = getThing3();
		templateCollection0.getSpitImports().add((de.semvox.types.odp.spit.TemplateCollection)include3);
		de.semvox.types.odp.spit.Template template4 = getTemplate4();
		templateCollection0.getSpitElements().add(template4);
		de.semvox.types.odp.spit.Template template19 = getTemplate19();
		templateCollection0.getSpitElements().add(template19);
		de.semvox.types.odp.spit.Template template34 = getTemplate34();
		templateCollection0.getSpitElements().add(template34);
		de.semvox.types.odp.spit.Template template49 = getTemplate49();
		templateCollection0.getSpitElements().add(template49);
		de.semvox.types.odp.spit.Template template64 = getTemplate64();
		templateCollection0.getSpitElements().add(template64);
		de.semvox.types.odp.spit.Template template79 = getTemplate79();
		templateCollection0.getSpitElements().add(template79);
		de.semvox.types.odp.spit.Template template94 = getTemplate94();
		templateCollection0.getSpitElements().add(template94);
		de.semvox.types.odp.spit.Template template109 = getTemplate109();
		templateCollection0.getSpitElements().add(template109);
		de.semvox.types.odp.spit.Template template124 = getTemplate124();
		templateCollection0.getSpitElements().add(template124);
		de.semvox.types.odp.spit.Template template139 = getTemplate139();
		templateCollection0.getSpitElements().add(template139);
		de.semvox.types.odp.spit.Template template154 = getTemplate154();
		templateCollection0.getSpitElements().add(template154);
		de.semvox.types.odp.spit.Template template169 = getTemplate169();
		templateCollection0.getSpitElements().add(template169);
		de.semvox.types.odp.spit.Template template184 = getTemplate184();
		templateCollection0.getSpitElements().add(template184);
		de.semvox.types.odp.spit.Template template197 = getTemplate197();
		templateCollection0.getSpitElements().add(template197);
		de.semvox.types.odp.spit.Template template210 = getTemplate210();
		templateCollection0.getSpitElements().add(template210);
		de.semvox.types.odp.spit.Template template222 = getTemplate222();
		templateCollection0.getSpitElements().add(template222);
		de.semvox.types.odp.spit.Template template234 = getTemplate234();
		templateCollection0.getSpitElements().add(template234);
		de.semvox.types.odp.spit.Template template246 = getTemplate246();
		templateCollection0.getSpitElements().add(template246);
		de.semvox.types.odp.spit.Template template258 = getTemplate258();
		templateCollection0.getSpitElements().add(template258);
	}

	de.semvox.types.odp.spit.TemplateCollection getTemplateCollection() {
		final de.semvox.types.odp.spit.TemplateCollection templateCollection0 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplateCollection();
		return templateCollection0;
	}


	de.semvox.subcon.data.Thing getThing1() {
		java.util.Map<java.lang.String, java.lang.String> newEntities = new java.util.HashMap<java.lang.String, java.lang.String>(entities);
		de.semvox.subcon.data.Thing include1 = new com.zorabots.vmn.nlg.gen.VmnUrgencyTemplates(getParameterProvider(), newEntities).loadThing(false);
		return include1;
	}

	de.semvox.subcon.data.Thing getThing2() {
		java.util.Map<java.lang.String, java.lang.String> newEntities = new java.util.HashMap<java.lang.String, java.lang.String>(entities);
		de.semvox.subcon.data.Thing include2 = new com.zorabots.vmn.nlg.gen.VmnExtremeTemplates(getParameterProvider(), newEntities).loadThing(false);
		return include2;
	}

	de.semvox.subcon.data.Thing getThing3() {
		java.util.Map<java.lang.String, java.lang.String> newEntities = new java.util.HashMap<java.lang.String, java.lang.String>(entities);
		de.semvox.subcon.data.Thing include3 = new com.zorabots.vmn.nlg.gen.VmnTempExtremeTemplates(getParameterProvider(), newEntities).loadThing(false);
		return include3;
	}

	de.semvox.types.odp.spit.Template getTemplate4() {
		final de.semvox.types.odp.spit.Template template4 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string1 = replaceEntities("Inform_Humidity");
		template4.setCommonName(string1);
		de.semvox.types.odp.spit.Switch switch5 = getSwitch5();
		template4.setSpitAction(switch5);
		de.semvox.types.odp.interaction.GenerationContext generationContext12 = getGenerationContext12();
		template4.setSpitOnContext(generationContext12);
		de.semvox.types.odp.interaction.Inform inform13 = getInform13();
		template4.setSpitOnInput(inform13);
		return template4;
	}

	de.semvox.types.odp.spit.Switch getSwitch5() {
		final de.semvox.types.odp.spit.Switch switch5 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case6 = getCase6();
			switch5.getSpitCases().add(case6);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case9 = getCase9();
			switch5.getSpitCases().add(case9);
		}
		return switch5;
	}

	de.semvox.types.odp.spit.Case getCase6() {
		final de.semvox.types.odp.spit.Case case6 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition7 = getStringCondition7();
		case6.getSpitConditions().add(stringCondition7);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs8 = getGenerateStringAndOutputs8();
		case6.setSpitAction(generateStringAndOutputs8);
		return case6;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition7() {
		final de.semvox.types.odp.spit.StringCondition stringCondition7 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string2 = replaceEntities("lang");
		stringCondition7.setAlgVariableReference(string2);
		String string3 = replaceEntities("en-US");
		stringCondition7.setCommonStringValue(string3);
		return stringCondition7;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs8() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs8 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs8.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string4 = replaceEntities("You have given me too much water. At this moment my humidity is ?($HUMIDITY) %. Can you give me less water next time?");
		generateStringAndOutputs8.getSpitAlternatives().add(string4);
		String string5 = replaceEntities("?(Excuse me. )There is too much water in my soil. Now my humidity is ?($HUMIDITY) %. Could you give me less water next time?");
		generateStringAndOutputs8.getSpitAlternatives().add(string5);
		return generateStringAndOutputs8;
	}

	de.semvox.types.odp.spit.Case getCase9() {
		final de.semvox.types.odp.spit.Case case9 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition10 = getStringCondition10();
		case9.getSpitConditions().add(stringCondition10);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs11 = getGenerateStringAndOutputs11();
		case9.setSpitAction(generateStringAndOutputs11);
		return case9;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition10() {
		final de.semvox.types.odp.spit.StringCondition stringCondition10 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string6 = replaceEntities("lang");
		stringCondition10.setAlgVariableReference(string6);
		String string7 = replaceEntities("nl-BE");
		stringCondition10.setCommonStringValue(string7);
		return stringCondition10;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs11() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs11 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs11.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string8 = replaceEntities("Je hebt me te veel water gegeven. Op dit moment is mijn vochtigheidsgraad ?($HUMIDITY) %. Kun je me de volgende keer minder water geven?");
		generateStringAndOutputs11.getSpitAlternatives().add(string8);
		String string9 = replaceEntities("?(Excuseer me. )Ik zie dat je me te veel water hebt gegeven. Mijn huidige vochtigheidsgraad (is|bedraagt) ?($HUMIDITY) %. Misschien is het beter dat je me de volgende keer minder water geeft. ");
		generateStringAndOutputs11.getSpitAlternatives().add(string9);
		return generateStringAndOutputs11;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext12() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext12 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext12.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext12.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext12.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext12.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext12.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext12.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext12;
	}

	de.semvox.types.odp.interaction.Inform getInform13() {
		final de.semvox.types.odp.interaction.Inform inform13 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.HumidityUpdate humidityUpdate14 = getHumidityUpdate14();
		inform13.setCommonContent(humidityUpdate14);
		return inform13;
	}

	com.zorabots.ontologies.vmn.HumidityUpdate getHumidityUpdate14() {
		final com.zorabots.ontologies.vmn.HumidityUpdate humidityUpdate14 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidityUpdate();
		com.zorabots.ontologies.vmn.First first15 = getFirst15();
		humidityUpdate14.setVmnUpdatePosition(first15);
		com.zorabots.ontologies.vmn.ToHight toHight16 = getToHight16();
		humidityUpdate14.setVmnExtreme(toHight16);
		humidityUpdate14.markVmnUrgencyAsVariable("URGENCY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		humidityUpdate14.assignVmnUrgencyPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.HumidityReport humidityReport17 = getHumidityReport17();
		humidityUpdate14.setVmnHumidityReport(humidityReport17);
		return humidityUpdate14;
	}

	com.zorabots.ontologies.vmn.First getFirst15() {
		final com.zorabots.ontologies.vmn.First first15 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newFirst();
		return first15;
	}

	com.zorabots.ontologies.vmn.ToHight getToHight16() {
		final com.zorabots.ontologies.vmn.ToHight toHight16 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newToHight();
		return toHight16;
	}

	com.zorabots.ontologies.vmn.HumidityReport getHumidityReport17() {
		final com.zorabots.ontologies.vmn.HumidityReport humidityReport17 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidityReport();
		com.zorabots.ontologies.vmn.Humidity humidity18 = getHumidity18();
		humidityReport17.setVmnHumidity(humidity18);
		return humidityReport17;
	}

	com.zorabots.ontologies.vmn.Humidity getHumidity18() {
		final com.zorabots.ontologies.vmn.Humidity humidity18 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidity();
		humidity18.markCommonStringValueAsVariable("HUMIDITY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		humidity18.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return humidity18;
	}

	de.semvox.types.odp.spit.Template getTemplate19() {
		final de.semvox.types.odp.spit.Template template19 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string10 = replaceEntities("Inform_Humidity");
		template19.setCommonName(string10);
		de.semvox.types.odp.spit.Switch switch20 = getSwitch20();
		template19.setSpitAction(switch20);
		de.semvox.types.odp.interaction.GenerationContext generationContext27 = getGenerationContext27();
		template19.setSpitOnContext(generationContext27);
		de.semvox.types.odp.interaction.Inform inform28 = getInform28();
		template19.setSpitOnInput(inform28);
		return template19;
	}

	de.semvox.types.odp.spit.Switch getSwitch20() {
		final de.semvox.types.odp.spit.Switch switch20 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case21 = getCase21();
			switch20.getSpitCases().add(case21);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case24 = getCase24();
			switch20.getSpitCases().add(case24);
		}
		return switch20;
	}

	de.semvox.types.odp.spit.Case getCase21() {
		final de.semvox.types.odp.spit.Case case21 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition22 = getStringCondition22();
		case21.getSpitConditions().add(stringCondition22);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs23 = getGenerateStringAndOutputs23();
		case21.setSpitAction(generateStringAndOutputs23);
		return case21;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition22() {
		final de.semvox.types.odp.spit.StringCondition stringCondition22 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string11 = replaceEntities("lang");
		stringCondition22.setAlgVariableReference(string11);
		String string12 = replaceEntities("en-US");
		stringCondition22.setCommonStringValue(string12);
		return stringCondition22;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs23() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs23 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs23.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string13 = replaceEntities("You have also given me too much water. At this moment my humidity is ?($HUMIDITY) %. Can you give me less water next time?");
		generateStringAndOutputs23.getSpitAlternatives().add(string13);
		String string14 = replaceEntities("There is also too much water in my soil. Now my humidity is ?($HUMIDITY) %. Could you give me less water next time?");
		generateStringAndOutputs23.getSpitAlternatives().add(string14);
		return generateStringAndOutputs23;
	}

	de.semvox.types.odp.spit.Case getCase24() {
		final de.semvox.types.odp.spit.Case case24 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition25 = getStringCondition25();
		case24.getSpitConditions().add(stringCondition25);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs26 = getGenerateStringAndOutputs26();
		case24.setSpitAction(generateStringAndOutputs26);
		return case24;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition25() {
		final de.semvox.types.odp.spit.StringCondition stringCondition25 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string15 = replaceEntities("lang");
		stringCondition25.setAlgVariableReference(string15);
		String string16 = replaceEntities("nl-BE");
		stringCondition25.setCommonStringValue(string16);
		return stringCondition25;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs26() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs26 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs26.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string17 = replaceEntities("Je hebt me ook te veel water gegeven. Op dit moment is mijn vochtigheidsgraad ?($HUMIDITY) %. Kun je me de volgende keer minder water geven? ");
		generateStringAndOutputs26.getSpitAlternatives().add(string17);
		String string18 = replaceEntities("Ik zie ook dat je me te veel water hebt gegeven. Mijn huidige vochtigheidsgraad (is|bedraagt) ?($HUMIDITY) %. Misschien is het beter dat je me de volgende keer minder water geeft. ");
		generateStringAndOutputs26.getSpitAlternatives().add(string18);
		return generateStringAndOutputs26;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext27() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext27 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext27.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext27.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext27.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext27.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext27.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext27.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext27;
	}

	de.semvox.types.odp.interaction.Inform getInform28() {
		final de.semvox.types.odp.interaction.Inform inform28 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.HumidityUpdate humidityUpdate29 = getHumidityUpdate29();
		inform28.setCommonContent(humidityUpdate29);
		return inform28;
	}

	com.zorabots.ontologies.vmn.HumidityUpdate getHumidityUpdate29() {
		final com.zorabots.ontologies.vmn.HumidityUpdate humidityUpdate29 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidityUpdate();
		com.zorabots.ontologies.vmn.NotFirst notFirst30 = getNotFirst30();
		humidityUpdate29.setVmnUpdatePosition(notFirst30);
		com.zorabots.ontologies.vmn.ToHight toHight31 = getToHight31();
		humidityUpdate29.setVmnExtreme(toHight31);
		humidityUpdate29.markVmnUrgencyAsVariable("URGENCY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		humidityUpdate29.assignVmnUrgencyPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.HumidityReport humidityReport32 = getHumidityReport32();
		humidityUpdate29.setVmnHumidityReport(humidityReport32);
		return humidityUpdate29;
	}

	com.zorabots.ontologies.vmn.NotFirst getNotFirst30() {
		final com.zorabots.ontologies.vmn.NotFirst notFirst30 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNotFirst();
		return notFirst30;
	}

	com.zorabots.ontologies.vmn.ToHight getToHight31() {
		final com.zorabots.ontologies.vmn.ToHight toHight31 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newToHight();
		return toHight31;
	}

	com.zorabots.ontologies.vmn.HumidityReport getHumidityReport32() {
		final com.zorabots.ontologies.vmn.HumidityReport humidityReport32 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidityReport();
		com.zorabots.ontologies.vmn.Humidity humidity33 = getHumidity33();
		humidityReport32.setVmnHumidity(humidity33);
		return humidityReport32;
	}

	com.zorabots.ontologies.vmn.Humidity getHumidity33() {
		final com.zorabots.ontologies.vmn.Humidity humidity33 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidity();
		humidity33.markCommonStringValueAsVariable("HUMIDITY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		humidity33.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return humidity33;
	}

	de.semvox.types.odp.spit.Template getTemplate34() {
		final de.semvox.types.odp.spit.Template template34 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string19 = replaceEntities("Inform_Humidity");
		template34.setCommonName(string19);
		de.semvox.types.odp.spit.Switch switch35 = getSwitch35();
		template34.setSpitAction(switch35);
		de.semvox.types.odp.interaction.GenerationContext generationContext42 = getGenerationContext42();
		template34.setSpitOnContext(generationContext42);
		de.semvox.types.odp.interaction.Inform inform43 = getInform43();
		template34.setSpitOnInput(inform43);
		return template34;
	}

	de.semvox.types.odp.spit.Switch getSwitch35() {
		final de.semvox.types.odp.spit.Switch switch35 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case36 = getCase36();
			switch35.getSpitCases().add(case36);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case39 = getCase39();
			switch35.getSpitCases().add(case39);
		}
		return switch35;
	}

	de.semvox.types.odp.spit.Case getCase36() {
		final de.semvox.types.odp.spit.Case case36 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition37 = getStringCondition37();
		case36.getSpitConditions().add(stringCondition37);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs38 = getGenerateStringAndOutputs38();
		case36.setSpitAction(generateStringAndOutputs38);
		return case36;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition37() {
		final de.semvox.types.odp.spit.StringCondition stringCondition37 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string20 = replaceEntities("lang");
		stringCondition37.setAlgVariableReference(string20);
		String string21 = replaceEntities("en-US");
		stringCondition37.setCommonStringValue(string21);
		return stringCondition37;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs38() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs38 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs38.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string22 = replaceEntities("I'm !($URGENCY) thirsty. At this moment my humidity is ?($HUMIDITY) %. Can you give me some water?");
		generateStringAndOutputs38.getSpitAlternatives().add(string22);
		String string23 = replaceEntities("?(Excuse me.) I'm !($URGENCY) thirsty and I could use some water right now. My humidity is now ?($HUMIDITY) %. Can you help me out?");
		generateStringAndOutputs38.getSpitAlternatives().add(string23);
		return generateStringAndOutputs38;
	}

	de.semvox.types.odp.spit.Case getCase39() {
		final de.semvox.types.odp.spit.Case case39 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition40 = getStringCondition40();
		case39.getSpitConditions().add(stringCondition40);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs41 = getGenerateStringAndOutputs41();
		case39.setSpitAction(generateStringAndOutputs41);
		return case39;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition40() {
		final de.semvox.types.odp.spit.StringCondition stringCondition40 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string24 = replaceEntities("lang");
		stringCondition40.setAlgVariableReference(string24);
		String string25 = replaceEntities("nl-BE");
		stringCondition40.setCommonStringValue(string25);
		return stringCondition40;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs41() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs41 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs41.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string26 = replaceEntities("Ik heb !($URGENCY) water nodig. Op dit moment is mijn vochtigheidsgraad ?($HUMIDITY) %. Kan je me zo snel mogelijk water geven? ");
		generateStringAndOutputs41.getSpitAlternatives().add(string26);
		String string27 = replaceEntities("?(Excuseer me. ) Ik heb dorst en ik zou wel wat water kunnen gebruiken. Mijn huidige vochtigheidsgraad is ?($HUMIDITY) %. Kun je me even water geven?");
		generateStringAndOutputs41.getSpitAlternatives().add(string27);
		return generateStringAndOutputs41;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext42() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext42 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext42.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext42.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext42.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext42.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext42.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext42.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext42;
	}

	de.semvox.types.odp.interaction.Inform getInform43() {
		final de.semvox.types.odp.interaction.Inform inform43 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.HumidityUpdate humidityUpdate44 = getHumidityUpdate44();
		inform43.setCommonContent(humidityUpdate44);
		return inform43;
	}

	com.zorabots.ontologies.vmn.HumidityUpdate getHumidityUpdate44() {
		final com.zorabots.ontologies.vmn.HumidityUpdate humidityUpdate44 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidityUpdate();
		com.zorabots.ontologies.vmn.First first45 = getFirst45();
		humidityUpdate44.setVmnUpdatePosition(first45);
		com.zorabots.ontologies.vmn.ToLow toLow46 = getToLow46();
		humidityUpdate44.setVmnExtreme(toLow46);
		humidityUpdate44.markVmnUrgencyAsVariable("URGENCY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		humidityUpdate44.assignVmnUrgencyPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.HumidityReport humidityReport47 = getHumidityReport47();
		humidityUpdate44.setVmnHumidityReport(humidityReport47);
		return humidityUpdate44;
	}

	com.zorabots.ontologies.vmn.First getFirst45() {
		final com.zorabots.ontologies.vmn.First first45 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newFirst();
		return first45;
	}

	com.zorabots.ontologies.vmn.ToLow getToLow46() {
		final com.zorabots.ontologies.vmn.ToLow toLow46 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newToLow();
		return toLow46;
	}

	com.zorabots.ontologies.vmn.HumidityReport getHumidityReport47() {
		final com.zorabots.ontologies.vmn.HumidityReport humidityReport47 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidityReport();
		com.zorabots.ontologies.vmn.Humidity humidity48 = getHumidity48();
		humidityReport47.setVmnHumidity(humidity48);
		return humidityReport47;
	}

	com.zorabots.ontologies.vmn.Humidity getHumidity48() {
		final com.zorabots.ontologies.vmn.Humidity humidity48 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidity();
		humidity48.markCommonStringValueAsVariable("HUMIDITY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		humidity48.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return humidity48;
	}

	de.semvox.types.odp.spit.Template getTemplate49() {
		final de.semvox.types.odp.spit.Template template49 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string28 = replaceEntities("Inform_Humidity");
		template49.setCommonName(string28);
		de.semvox.types.odp.spit.Switch switch50 = getSwitch50();
		template49.setSpitAction(switch50);
		de.semvox.types.odp.interaction.GenerationContext generationContext57 = getGenerationContext57();
		template49.setSpitOnContext(generationContext57);
		de.semvox.types.odp.interaction.Inform inform58 = getInform58();
		template49.setSpitOnInput(inform58);
		return template49;
	}

	de.semvox.types.odp.spit.Switch getSwitch50() {
		final de.semvox.types.odp.spit.Switch switch50 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case51 = getCase51();
			switch50.getSpitCases().add(case51);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case54 = getCase54();
			switch50.getSpitCases().add(case54);
		}
		return switch50;
	}

	de.semvox.types.odp.spit.Case getCase51() {
		final de.semvox.types.odp.spit.Case case51 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition52 = getStringCondition52();
		case51.getSpitConditions().add(stringCondition52);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs53 = getGenerateStringAndOutputs53();
		case51.setSpitAction(generateStringAndOutputs53);
		return case51;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition52() {
		final de.semvox.types.odp.spit.StringCondition stringCondition52 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string29 = replaceEntities("lang");
		stringCondition52.setAlgVariableReference(string29);
		String string30 = replaceEntities("en-US");
		stringCondition52.setCommonStringValue(string30);
		return stringCondition52;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs53() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs53 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs53.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string31 = replaceEntities("I'm also !($URGENCY) thirsty. At this moment my humidity is ?($HUMIDITY) %. Can you give me some water?");
		generateStringAndOutputs53.getSpitAlternatives().add(string31);
		String string32 = replaceEntities("I'm also !($URGENCY) thirsty. I could use some water right now. My humidity is now ?($HUMIDITY) %. Can you help me out?");
		generateStringAndOutputs53.getSpitAlternatives().add(string32);
		return generateStringAndOutputs53;
	}

	de.semvox.types.odp.spit.Case getCase54() {
		final de.semvox.types.odp.spit.Case case54 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition55 = getStringCondition55();
		case54.getSpitConditions().add(stringCondition55);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs56 = getGenerateStringAndOutputs56();
		case54.setSpitAction(generateStringAndOutputs56);
		return case54;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition55() {
		final de.semvox.types.odp.spit.StringCondition stringCondition55 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string33 = replaceEntities("lang");
		stringCondition55.setAlgVariableReference(string33);
		String string34 = replaceEntities("nl-BE");
		stringCondition55.setCommonStringValue(string34);
		return stringCondition55;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs56() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs56 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs56.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string35 = replaceEntities("Ik heb ook !($URGENCY) water nodig. Op dit moment is mijn vochtigheidsgraad ?($HUMIDITY) %. Kan je me zo snel mogelijk water geven?");
		generateStringAndOutputs56.getSpitAlternatives().add(string35);
		String string36 = replaceEntities("Ik heb ook dorst. Ik zou wel wat water kunnen gebruiken. Mijn huidige vochtigheidsgraad is ?($HUMIDITY) %. Kun je me even water geven?");
		generateStringAndOutputs56.getSpitAlternatives().add(string36);
		return generateStringAndOutputs56;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext57() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext57 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext57.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext57.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext57.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext57.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext57.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext57.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext57;
	}

	de.semvox.types.odp.interaction.Inform getInform58() {
		final de.semvox.types.odp.interaction.Inform inform58 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.HumidityUpdate humidityUpdate59 = getHumidityUpdate59();
		inform58.setCommonContent(humidityUpdate59);
		return inform58;
	}

	com.zorabots.ontologies.vmn.HumidityUpdate getHumidityUpdate59() {
		final com.zorabots.ontologies.vmn.HumidityUpdate humidityUpdate59 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidityUpdate();
		com.zorabots.ontologies.vmn.NotFirst notFirst60 = getNotFirst60();
		humidityUpdate59.setVmnUpdatePosition(notFirst60);
		com.zorabots.ontologies.vmn.ToLow toLow61 = getToLow61();
		humidityUpdate59.setVmnExtreme(toLow61);
		humidityUpdate59.markVmnUrgencyAsVariable("URGENCY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		humidityUpdate59.assignVmnUrgencyPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.HumidityReport humidityReport62 = getHumidityReport62();
		humidityUpdate59.setVmnHumidityReport(humidityReport62);
		return humidityUpdate59;
	}

	com.zorabots.ontologies.vmn.NotFirst getNotFirst60() {
		final com.zorabots.ontologies.vmn.NotFirst notFirst60 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNotFirst();
		return notFirst60;
	}

	com.zorabots.ontologies.vmn.ToLow getToLow61() {
		final com.zorabots.ontologies.vmn.ToLow toLow61 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newToLow();
		return toLow61;
	}

	com.zorabots.ontologies.vmn.HumidityReport getHumidityReport62() {
		final com.zorabots.ontologies.vmn.HumidityReport humidityReport62 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidityReport();
		com.zorabots.ontologies.vmn.Humidity humidity63 = getHumidity63();
		humidityReport62.setVmnHumidity(humidity63);
		return humidityReport62;
	}

	com.zorabots.ontologies.vmn.Humidity getHumidity63() {
		final com.zorabots.ontologies.vmn.Humidity humidity63 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidity();
		humidity63.markCommonStringValueAsVariable("HUMIDITY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		humidity63.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return humidity63;
	}

	de.semvox.types.odp.spit.Template getTemplate64() {
		final de.semvox.types.odp.spit.Template template64 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string37 = replaceEntities("Inform_Temperature");
		template64.setCommonName(string37);
		de.semvox.types.odp.spit.Switch switch65 = getSwitch65();
		template64.setSpitAction(switch65);
		de.semvox.types.odp.interaction.GenerationContext generationContext72 = getGenerationContext72();
		template64.setSpitOnContext(generationContext72);
		de.semvox.types.odp.interaction.Inform inform73 = getInform73();
		template64.setSpitOnInput(inform73);
		return template64;
	}

	de.semvox.types.odp.spit.Switch getSwitch65() {
		final de.semvox.types.odp.spit.Switch switch65 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case66 = getCase66();
			switch65.getSpitCases().add(case66);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case69 = getCase69();
			switch65.getSpitCases().add(case69);
		}
		return switch65;
	}

	de.semvox.types.odp.spit.Case getCase66() {
		final de.semvox.types.odp.spit.Case case66 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition67 = getStringCondition67();
		case66.getSpitConditions().add(stringCondition67);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs68 = getGenerateStringAndOutputs68();
		case66.setSpitAction(generateStringAndOutputs68);
		return case66;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition67() {
		final de.semvox.types.odp.spit.StringCondition stringCondition67 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string38 = replaceEntities("lang");
		stringCondition67.setAlgVariableReference(string38);
		String string39 = replaceEntities("en-US");
		stringCondition67.setCommonStringValue(string39);
		return stringCondition67;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs68() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs68 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs68.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string40 = replaceEntities("I'm !($URGENCY) warm!. At this moment my temperature is ?($TEMP) degrees. Can you move me to a cooler place?");
		generateStringAndOutputs68.getSpitAlternatives().add(string40);
		String string41 = replaceEntities("?(Excuse me. )I'm !($URGENCY) warm!. Now my temperature is ?($TEMP) degrees. Can you pick out a cooler spot for me?");
		generateStringAndOutputs68.getSpitAlternatives().add(string41);
		return generateStringAndOutputs68;
	}

	de.semvox.types.odp.spit.Case getCase69() {
		final de.semvox.types.odp.spit.Case case69 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition70 = getStringCondition70();
		case69.getSpitConditions().add(stringCondition70);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs71 = getGenerateStringAndOutputs71();
		case69.setSpitAction(generateStringAndOutputs71);
		return case69;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition70() {
		final de.semvox.types.odp.spit.StringCondition stringCondition70 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string42 = replaceEntities("lang");
		stringCondition70.setAlgVariableReference(string42);
		String string43 = replaceEntities("nl-BE");
		stringCondition70.setCommonStringValue(string43);
		return stringCondition70;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs71() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs71 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs71.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string44 = replaceEntities("Ik heb het !($URGENCY) warm. (Op dit moment|nu) is het ?($TEMP) graden. (Kun je me naar een koelere plaats brengen?|Kun je me op een koelere plaats zetten?)");
		generateStringAndOutputs71.getSpitAlternatives().add(string44);
		String string45 = replaceEntities("?(Excuseer me. )Ik heb het !($URGENCY) warm. (Op dit moment|nu) is het ?($TEMP) graden. (Kun je me naar een koelere plaats brengen?|Kun je me op een koelere plaats zetten?)");
		generateStringAndOutputs71.getSpitAlternatives().add(string45);
		return generateStringAndOutputs71;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext72() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext72 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext72.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext72.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext72.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext72.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext72.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext72.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext72;
	}

	de.semvox.types.odp.interaction.Inform getInform73() {
		final de.semvox.types.odp.interaction.Inform inform73 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.TemperatureUpdate temperatureUpdate74 = getTemperatureUpdate74();
		inform73.setCommonContent(temperatureUpdate74);
		return inform73;
	}

	com.zorabots.ontologies.vmn.TemperatureUpdate getTemperatureUpdate74() {
		final com.zorabots.ontologies.vmn.TemperatureUpdate temperatureUpdate74 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperatureUpdate();
		com.zorabots.ontologies.vmn.First first75 = getFirst75();
		temperatureUpdate74.setVmnUpdatePosition(first75);
		com.zorabots.ontologies.vmn.Hot hot76 = getHot76();
		temperatureUpdate74.setVmnTempExtreme(hot76);
		temperatureUpdate74.markVmnUrgencyAsVariable("URGENCY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		temperatureUpdate74.assignVmnUrgencyPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.TemperatureReport temperatureReport77 = getTemperatureReport77();
		temperatureUpdate74.setVmnTemperatureReport(temperatureReport77);
		return temperatureUpdate74;
	}

	com.zorabots.ontologies.vmn.First getFirst75() {
		final com.zorabots.ontologies.vmn.First first75 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newFirst();
		return first75;
	}

	com.zorabots.ontologies.vmn.Hot getHot76() {
		final com.zorabots.ontologies.vmn.Hot hot76 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHot();
		return hot76;
	}

	com.zorabots.ontologies.vmn.TemperatureReport getTemperatureReport77() {
		final com.zorabots.ontologies.vmn.TemperatureReport temperatureReport77 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperatureReport();
		com.zorabots.ontologies.vmn.Temperature temperature78 = getTemperature78();
		temperatureReport77.setVmnTemperature(temperature78);
		return temperatureReport77;
	}

	com.zorabots.ontologies.vmn.Temperature getTemperature78() {
		final com.zorabots.ontologies.vmn.Temperature temperature78 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperature();
		temperature78.markCommonStringValueAsVariable("TEMP", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		temperature78.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return temperature78;
	}

	de.semvox.types.odp.spit.Template getTemplate79() {
		final de.semvox.types.odp.spit.Template template79 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string46 = replaceEntities("Inform_Temperature");
		template79.setCommonName(string46);
		de.semvox.types.odp.spit.Switch switch80 = getSwitch80();
		template79.setSpitAction(switch80);
		de.semvox.types.odp.interaction.GenerationContext generationContext87 = getGenerationContext87();
		template79.setSpitOnContext(generationContext87);
		de.semvox.types.odp.interaction.Inform inform88 = getInform88();
		template79.setSpitOnInput(inform88);
		return template79;
	}

	de.semvox.types.odp.spit.Switch getSwitch80() {
		final de.semvox.types.odp.spit.Switch switch80 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case81 = getCase81();
			switch80.getSpitCases().add(case81);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case84 = getCase84();
			switch80.getSpitCases().add(case84);
		}
		return switch80;
	}

	de.semvox.types.odp.spit.Case getCase81() {
		final de.semvox.types.odp.spit.Case case81 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition82 = getStringCondition82();
		case81.getSpitConditions().add(stringCondition82);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs83 = getGenerateStringAndOutputs83();
		case81.setSpitAction(generateStringAndOutputs83);
		return case81;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition82() {
		final de.semvox.types.odp.spit.StringCondition stringCondition82 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string47 = replaceEntities("lang");
		stringCondition82.setAlgVariableReference(string47);
		String string48 = replaceEntities("en-US");
		stringCondition82.setCommonStringValue(string48);
		return stringCondition82;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs83() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs83 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs83.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string49 = replaceEntities("I'm also !($URGENCY) warm!. At this moment my temperature is ?($TEMP) degrees. Can you move me to a cooler place?");
		generateStringAndOutputs83.getSpitAlternatives().add(string49);
		String string50 = replaceEntities("I'm also !($URGENCY) warm!. Now my temperature is ?($TEMP) degrees. Can you pick out a cooler spot for me?");
		generateStringAndOutputs83.getSpitAlternatives().add(string50);
		return generateStringAndOutputs83;
	}

	de.semvox.types.odp.spit.Case getCase84() {
		final de.semvox.types.odp.spit.Case case84 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition85 = getStringCondition85();
		case84.getSpitConditions().add(stringCondition85);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs86 = getGenerateStringAndOutputs86();
		case84.setSpitAction(generateStringAndOutputs86);
		return case84;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition85() {
		final de.semvox.types.odp.spit.StringCondition stringCondition85 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string51 = replaceEntities("lang");
		stringCondition85.setAlgVariableReference(string51);
		String string52 = replaceEntities("nl-BE");
		stringCondition85.setCommonStringValue(string52);
		return stringCondition85;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs86() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs86 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs86.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string53 = replaceEntities("Ik heb het ook !($URGENCY) warm. (Op dit moment|nu) is het ?($TEMP) graden. (Kan je me naar een koelere plaats brengen?|Kan je me op een koelere plaats zetten?)");
		generateStringAndOutputs86.getSpitAlternatives().add(string53);
		return generateStringAndOutputs86;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext87() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext87 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext87.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext87.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext87.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext87.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext87.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext87.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext87;
	}

	de.semvox.types.odp.interaction.Inform getInform88() {
		final de.semvox.types.odp.interaction.Inform inform88 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.TemperatureUpdate temperatureUpdate89 = getTemperatureUpdate89();
		inform88.setCommonContent(temperatureUpdate89);
		return inform88;
	}

	com.zorabots.ontologies.vmn.TemperatureUpdate getTemperatureUpdate89() {
		final com.zorabots.ontologies.vmn.TemperatureUpdate temperatureUpdate89 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperatureUpdate();
		com.zorabots.ontologies.vmn.NotFirst notFirst90 = getNotFirst90();
		temperatureUpdate89.setVmnUpdatePosition(notFirst90);
		com.zorabots.ontologies.vmn.Hot hot91 = getHot91();
		temperatureUpdate89.setVmnTempExtreme(hot91);
		temperatureUpdate89.markVmnUrgencyAsVariable("URGENCY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		temperatureUpdate89.assignVmnUrgencyPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.TemperatureReport temperatureReport92 = getTemperatureReport92();
		temperatureUpdate89.setVmnTemperatureReport(temperatureReport92);
		return temperatureUpdate89;
	}

	com.zorabots.ontologies.vmn.NotFirst getNotFirst90() {
		final com.zorabots.ontologies.vmn.NotFirst notFirst90 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNotFirst();
		return notFirst90;
	}

	com.zorabots.ontologies.vmn.Hot getHot91() {
		final com.zorabots.ontologies.vmn.Hot hot91 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHot();
		return hot91;
	}

	com.zorabots.ontologies.vmn.TemperatureReport getTemperatureReport92() {
		final com.zorabots.ontologies.vmn.TemperatureReport temperatureReport92 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperatureReport();
		com.zorabots.ontologies.vmn.Temperature temperature93 = getTemperature93();
		temperatureReport92.setVmnTemperature(temperature93);
		return temperatureReport92;
	}

	com.zorabots.ontologies.vmn.Temperature getTemperature93() {
		final com.zorabots.ontologies.vmn.Temperature temperature93 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperature();
		temperature93.markCommonStringValueAsVariable("TEMP", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		temperature93.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return temperature93;
	}

	de.semvox.types.odp.spit.Template getTemplate94() {
		final de.semvox.types.odp.spit.Template template94 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string54 = replaceEntities("Inform_Temperature");
		template94.setCommonName(string54);
		de.semvox.types.odp.spit.Switch switch95 = getSwitch95();
		template94.setSpitAction(switch95);
		de.semvox.types.odp.interaction.GenerationContext generationContext102 = getGenerationContext102();
		template94.setSpitOnContext(generationContext102);
		de.semvox.types.odp.interaction.Inform inform103 = getInform103();
		template94.setSpitOnInput(inform103);
		return template94;
	}

	de.semvox.types.odp.spit.Switch getSwitch95() {
		final de.semvox.types.odp.spit.Switch switch95 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case96 = getCase96();
			switch95.getSpitCases().add(case96);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case99 = getCase99();
			switch95.getSpitCases().add(case99);
		}
		return switch95;
	}

	de.semvox.types.odp.spit.Case getCase96() {
		final de.semvox.types.odp.spit.Case case96 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition97 = getStringCondition97();
		case96.getSpitConditions().add(stringCondition97);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs98 = getGenerateStringAndOutputs98();
		case96.setSpitAction(generateStringAndOutputs98);
		return case96;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition97() {
		final de.semvox.types.odp.spit.StringCondition stringCondition97 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string55 = replaceEntities("lang");
		stringCondition97.setAlgVariableReference(string55);
		String string56 = replaceEntities("en-US");
		stringCondition97.setCommonStringValue(string56);
		return stringCondition97;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs98() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs98 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs98.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string57 = replaceEntities("I'm !($URGENCY) cold!. At this moment my temperature is ?($TEMP) degrees. Can you move me to a warmer place?");
		generateStringAndOutputs98.getSpitAlternatives().add(string57);
		String string58 = replaceEntities("?(Excuse me. )I'm !($URGENCY) cold!. Now my temperature is ?($TEMP) degrees. Can you move me to a warmer spot?");
		generateStringAndOutputs98.getSpitAlternatives().add(string58);
		return generateStringAndOutputs98;
	}

	de.semvox.types.odp.spit.Case getCase99() {
		final de.semvox.types.odp.spit.Case case99 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition100 = getStringCondition100();
		case99.getSpitConditions().add(stringCondition100);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs101 = getGenerateStringAndOutputs101();
		case99.setSpitAction(generateStringAndOutputs101);
		return case99;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition100() {
		final de.semvox.types.odp.spit.StringCondition stringCondition100 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string59 = replaceEntities("lang");
		stringCondition100.setAlgVariableReference(string59);
		String string60 = replaceEntities("nl-BE");
		stringCondition100.setCommonStringValue(string60);
		return stringCondition100;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs101() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs101 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs101.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string61 = replaceEntities("Ik heb het !($URGENCY) koud. Op dit moment (is|bedraagt) mijn temperatuur ?($TEMP) graden. Kun je me op een warmere plaats zetten? ");
		generateStringAndOutputs101.getSpitAlternatives().add(string61);
		String string62 = replaceEntities("?(Excuseer me. )Ik heb het !($URGENCY) koud. Nu bedraagt mijn temperatuur ?($TEMP) graden. Kun je me verplaatsen naar een warmere plek? ");
		generateStringAndOutputs101.getSpitAlternatives().add(string62);
		return generateStringAndOutputs101;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext102() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext102 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext102.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext102.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext102.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext102.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext102.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext102.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext102;
	}

	de.semvox.types.odp.interaction.Inform getInform103() {
		final de.semvox.types.odp.interaction.Inform inform103 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.TemperatureUpdate temperatureUpdate104 = getTemperatureUpdate104();
		inform103.setCommonContent(temperatureUpdate104);
		return inform103;
	}

	com.zorabots.ontologies.vmn.TemperatureUpdate getTemperatureUpdate104() {
		final com.zorabots.ontologies.vmn.TemperatureUpdate temperatureUpdate104 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperatureUpdate();
		com.zorabots.ontologies.vmn.First first105 = getFirst105();
		temperatureUpdate104.setVmnUpdatePosition(first105);
		com.zorabots.ontologies.vmn.Hot hot106 = getHot106();
		temperatureUpdate104.setVmnTempExtreme(hot106);
		temperatureUpdate104.markVmnUrgencyAsVariable("URGENCY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		temperatureUpdate104.assignVmnUrgencyPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.TemperatureReport temperatureReport107 = getTemperatureReport107();
		temperatureUpdate104.setVmnTemperatureReport(temperatureReport107);
		return temperatureUpdate104;
	}

	com.zorabots.ontologies.vmn.First getFirst105() {
		final com.zorabots.ontologies.vmn.First first105 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newFirst();
		return first105;
	}

	com.zorabots.ontologies.vmn.Hot getHot106() {
		final com.zorabots.ontologies.vmn.Hot hot106 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHot();
		return hot106;
	}

	com.zorabots.ontologies.vmn.TemperatureReport getTemperatureReport107() {
		final com.zorabots.ontologies.vmn.TemperatureReport temperatureReport107 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperatureReport();
		com.zorabots.ontologies.vmn.Temperature temperature108 = getTemperature108();
		temperatureReport107.setVmnTemperature(temperature108);
		return temperatureReport107;
	}

	com.zorabots.ontologies.vmn.Temperature getTemperature108() {
		final com.zorabots.ontologies.vmn.Temperature temperature108 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperature();
		temperature108.markCommonStringValueAsVariable("TEMP", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		temperature108.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return temperature108;
	}

	de.semvox.types.odp.spit.Template getTemplate109() {
		final de.semvox.types.odp.spit.Template template109 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string63 = replaceEntities("Inform_Temperature");
		template109.setCommonName(string63);
		de.semvox.types.odp.spit.Switch switch110 = getSwitch110();
		template109.setSpitAction(switch110);
		de.semvox.types.odp.interaction.GenerationContext generationContext117 = getGenerationContext117();
		template109.setSpitOnContext(generationContext117);
		de.semvox.types.odp.interaction.Inform inform118 = getInform118();
		template109.setSpitOnInput(inform118);
		return template109;
	}

	de.semvox.types.odp.spit.Switch getSwitch110() {
		final de.semvox.types.odp.spit.Switch switch110 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case111 = getCase111();
			switch110.getSpitCases().add(case111);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case114 = getCase114();
			switch110.getSpitCases().add(case114);
		}
		return switch110;
	}

	de.semvox.types.odp.spit.Case getCase111() {
		final de.semvox.types.odp.spit.Case case111 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition112 = getStringCondition112();
		case111.getSpitConditions().add(stringCondition112);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs113 = getGenerateStringAndOutputs113();
		case111.setSpitAction(generateStringAndOutputs113);
		return case111;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition112() {
		final de.semvox.types.odp.spit.StringCondition stringCondition112 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string64 = replaceEntities("lang");
		stringCondition112.setAlgVariableReference(string64);
		String string65 = replaceEntities("en-US");
		stringCondition112.setCommonStringValue(string65);
		return stringCondition112;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs113() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs113 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs113.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string66 = replaceEntities("I'm also !($URGENCY) cold!. At this moment my temperature is ?($TEMP) degrees. Can you move me to a warmer place?");
		generateStringAndOutputs113.getSpitAlternatives().add(string66);
		String string67 = replaceEntities("I'm also !($URGENCY) cold!. Now my temperature is ?($TEMP) degrees. Can you move me to a warmer spot?");
		generateStringAndOutputs113.getSpitAlternatives().add(string67);
		return generateStringAndOutputs113;
	}

	de.semvox.types.odp.spit.Case getCase114() {
		final de.semvox.types.odp.spit.Case case114 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition115 = getStringCondition115();
		case114.getSpitConditions().add(stringCondition115);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs116 = getGenerateStringAndOutputs116();
		case114.setSpitAction(generateStringAndOutputs116);
		return case114;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition115() {
		final de.semvox.types.odp.spit.StringCondition stringCondition115 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string68 = replaceEntities("lang");
		stringCondition115.setAlgVariableReference(string68);
		String string69 = replaceEntities("nl-BE");
		stringCondition115.setCommonStringValue(string69);
		return stringCondition115;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs116() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs116 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs116.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string70 = replaceEntities("Ik heb het ook !($URGENCY) koud. Op dit moment is mijn temperatuur ?($TEMP) graden. Kan je me op een warmere plaats zetten?");
		generateStringAndOutputs116.getSpitAlternatives().add(string70);
		String string71 = replaceEntities("Ik heb het ook !($URGENCY) koud. Nu bedraagt mijn temperatuur ?($TEMP) graden. Kun je me verplaatsen naar een warmere plek?");
		generateStringAndOutputs116.getSpitAlternatives().add(string71);
		return generateStringAndOutputs116;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext117() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext117 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext117.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext117.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext117.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext117.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext117.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext117.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext117;
	}

	de.semvox.types.odp.interaction.Inform getInform118() {
		final de.semvox.types.odp.interaction.Inform inform118 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.TemperatureUpdate temperatureUpdate119 = getTemperatureUpdate119();
		inform118.setCommonContent(temperatureUpdate119);
		return inform118;
	}

	com.zorabots.ontologies.vmn.TemperatureUpdate getTemperatureUpdate119() {
		final com.zorabots.ontologies.vmn.TemperatureUpdate temperatureUpdate119 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperatureUpdate();
		com.zorabots.ontologies.vmn.NotFirst notFirst120 = getNotFirst120();
		temperatureUpdate119.setVmnUpdatePosition(notFirst120);
		com.zorabots.ontologies.vmn.Hot hot121 = getHot121();
		temperatureUpdate119.setVmnTempExtreme(hot121);
		temperatureUpdate119.markVmnUrgencyAsVariable("URGENCY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		temperatureUpdate119.assignVmnUrgencyPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.TemperatureReport temperatureReport122 = getTemperatureReport122();
		temperatureUpdate119.setVmnTemperatureReport(temperatureReport122);
		return temperatureUpdate119;
	}

	com.zorabots.ontologies.vmn.NotFirst getNotFirst120() {
		final com.zorabots.ontologies.vmn.NotFirst notFirst120 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNotFirst();
		return notFirst120;
	}

	com.zorabots.ontologies.vmn.Hot getHot121() {
		final com.zorabots.ontologies.vmn.Hot hot121 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHot();
		return hot121;
	}

	com.zorabots.ontologies.vmn.TemperatureReport getTemperatureReport122() {
		final com.zorabots.ontologies.vmn.TemperatureReport temperatureReport122 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperatureReport();
		com.zorabots.ontologies.vmn.Temperature temperature123 = getTemperature123();
		temperatureReport122.setVmnTemperature(temperature123);
		return temperatureReport122;
	}

	com.zorabots.ontologies.vmn.Temperature getTemperature123() {
		final com.zorabots.ontologies.vmn.Temperature temperature123 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperature();
		temperature123.markCommonStringValueAsVariable("TEMP", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		temperature123.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return temperature123;
	}

	de.semvox.types.odp.spit.Template getTemplate124() {
		final de.semvox.types.odp.spit.Template template124 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string72 = replaceEntities("Inform_Light");
		template124.setCommonName(string72);
		de.semvox.types.odp.spit.Switch switch125 = getSwitch125();
		template124.setSpitAction(switch125);
		de.semvox.types.odp.interaction.GenerationContext generationContext132 = getGenerationContext132();
		template124.setSpitOnContext(generationContext132);
		de.semvox.types.odp.interaction.Inform inform133 = getInform133();
		template124.setSpitOnInput(inform133);
		return template124;
	}

	de.semvox.types.odp.spit.Switch getSwitch125() {
		final de.semvox.types.odp.spit.Switch switch125 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case126 = getCase126();
			switch125.getSpitCases().add(case126);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case129 = getCase129();
			switch125.getSpitCases().add(case129);
		}
		return switch125;
	}

	de.semvox.types.odp.spit.Case getCase126() {
		final de.semvox.types.odp.spit.Case case126 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition127 = getStringCondition127();
		case126.getSpitConditions().add(stringCondition127);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs128 = getGenerateStringAndOutputs128();
		case126.setSpitAction(generateStringAndOutputs128);
		return case126;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition127() {
		final de.semvox.types.odp.spit.StringCondition stringCondition127 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string73 = replaceEntities("lang");
		stringCondition127.setAlgVariableReference(string73);
		String string74 = replaceEntities("en-US");
		stringCondition127.setCommonStringValue(string74);
		return stringCondition127;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs128() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs128 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs128.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string75 = replaceEntities("The amount of light is !($URGENCY) high here!. At this moment the amount of light is ?($LIGHT) lux. Can you move me to a darker place?");
		generateStringAndOutputs128.getSpitAlternatives().add(string75);
		String string76 = replaceEntities("?(Excuse me. )The light intensity here is !($URGENCY) high!. The light intensity now is ?($LIGHT) lux. Can you find a darker place for me?");
		generateStringAndOutputs128.getSpitAlternatives().add(string76);
		return generateStringAndOutputs128;
	}

	de.semvox.types.odp.spit.Case getCase129() {
		final de.semvox.types.odp.spit.Case case129 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition130 = getStringCondition130();
		case129.getSpitConditions().add(stringCondition130);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs131 = getGenerateStringAndOutputs131();
		case129.setSpitAction(generateStringAndOutputs131);
		return case129;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition130() {
		final de.semvox.types.odp.spit.StringCondition stringCondition130 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string77 = replaceEntities("lang");
		stringCondition130.setAlgVariableReference(string77);
		String string78 = replaceEntities("nl-BE");
		stringCondition130.setCommonStringValue(string78);
		return stringCondition130;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs131() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs131 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs131.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string79 = replaceEntities("Er is !($URGENCY) veel licht. Op dit moment (is|bedraagt) de licht intensiteit ?($LIGHT) lux. Kun je me op een meer donkere plaats zetten?");
		generateStringAndOutputs131.getSpitAlternatives().add(string79);
		String string80 = replaceEntities("?(Excuseer me. )Er is !($URGENCY) veel licht. Nu bedraagt de licht intensiteit ?($LIGHT) lux. Kun je een minder heldere plek voor me vinden?");
		generateStringAndOutputs131.getSpitAlternatives().add(string80);
		return generateStringAndOutputs131;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext132() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext132 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext132.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext132.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext132.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext132.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext132.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext132.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext132;
	}

	de.semvox.types.odp.interaction.Inform getInform133() {
		final de.semvox.types.odp.interaction.Inform inform133 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.LightUpdate lightUpdate134 = getLightUpdate134();
		inform133.setCommonContent(lightUpdate134);
		return inform133;
	}

	com.zorabots.ontologies.vmn.LightUpdate getLightUpdate134() {
		final com.zorabots.ontologies.vmn.LightUpdate lightUpdate134 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLightUpdate();
		com.zorabots.ontologies.vmn.First first135 = getFirst135();
		lightUpdate134.setVmnUpdatePosition(first135);
		com.zorabots.ontologies.vmn.ToHight toHight136 = getToHight136();
		lightUpdate134.setVmnExtreme(toHight136);
		lightUpdate134.markVmnUrgencyAsVariable("URGENCY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		lightUpdate134.assignVmnUrgencyPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.LightReport lightReport137 = getLightReport137();
		lightUpdate134.setVmnLightReport(lightReport137);
		return lightUpdate134;
	}

	com.zorabots.ontologies.vmn.First getFirst135() {
		final com.zorabots.ontologies.vmn.First first135 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newFirst();
		return first135;
	}

	com.zorabots.ontologies.vmn.ToHight getToHight136() {
		final com.zorabots.ontologies.vmn.ToHight toHight136 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newToHight();
		return toHight136;
	}

	com.zorabots.ontologies.vmn.LightReport getLightReport137() {
		final com.zorabots.ontologies.vmn.LightReport lightReport137 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLightReport();
		com.zorabots.ontologies.vmn.Light light138 = getLight138();
		lightReport137.setVmnLight(light138);
		return lightReport137;
	}

	com.zorabots.ontologies.vmn.Light getLight138() {
		final com.zorabots.ontologies.vmn.Light light138 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLight();
		light138.markCommonStringValueAsVariable("LIGHT", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		light138.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return light138;
	}

	de.semvox.types.odp.spit.Template getTemplate139() {
		final de.semvox.types.odp.spit.Template template139 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string81 = replaceEntities("Inform_Light");
		template139.setCommonName(string81);
		de.semvox.types.odp.spit.Switch switch140 = getSwitch140();
		template139.setSpitAction(switch140);
		de.semvox.types.odp.interaction.GenerationContext generationContext147 = getGenerationContext147();
		template139.setSpitOnContext(generationContext147);
		de.semvox.types.odp.interaction.Inform inform148 = getInform148();
		template139.setSpitOnInput(inform148);
		return template139;
	}

	de.semvox.types.odp.spit.Switch getSwitch140() {
		final de.semvox.types.odp.spit.Switch switch140 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case141 = getCase141();
			switch140.getSpitCases().add(case141);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case144 = getCase144();
			switch140.getSpitCases().add(case144);
		}
		return switch140;
	}

	de.semvox.types.odp.spit.Case getCase141() {
		final de.semvox.types.odp.spit.Case case141 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition142 = getStringCondition142();
		case141.getSpitConditions().add(stringCondition142);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs143 = getGenerateStringAndOutputs143();
		case141.setSpitAction(generateStringAndOutputs143);
		return case141;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition142() {
		final de.semvox.types.odp.spit.StringCondition stringCondition142 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string82 = replaceEntities("lang");
		stringCondition142.setAlgVariableReference(string82);
		String string83 = replaceEntities("en-US");
		stringCondition142.setCommonStringValue(string83);
		return stringCondition142;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs143() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs143 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs143.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string84 = replaceEntities("It also seems like the amount of light i'm getting is !($URGENCY) high!. At this moment the light is ?($LIGHT) lux. Can you move me to a darker place?");
		generateStringAndOutputs143.getSpitAlternatives().add(string84);
		String string85 = replaceEntities("Also, the light intensity here is !($URGENCY) high!. The light intensity now is ?($LIGHT) lux. Can you find a darker place for me?");
		generateStringAndOutputs143.getSpitAlternatives().add(string85);
		return generateStringAndOutputs143;
	}

	de.semvox.types.odp.spit.Case getCase144() {
		final de.semvox.types.odp.spit.Case case144 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition145 = getStringCondition145();
		case144.getSpitConditions().add(stringCondition145);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs146 = getGenerateStringAndOutputs146();
		case144.setSpitAction(generateStringAndOutputs146);
		return case144;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition145() {
		final de.semvox.types.odp.spit.StringCondition stringCondition145 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string86 = replaceEntities("lang");
		stringCondition145.setAlgVariableReference(string86);
		String string87 = replaceEntities("nl-BE");
		stringCondition145.setCommonStringValue(string87);
		return stringCondition145;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs146() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs146 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs146.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string88 = replaceEntities("Er is ook !($URGENCY) veel licht. Op dit moment is de licht intensiteit ?($LIGHT) lux. Kun je me op een meer donkere plaats zetten?");
		generateStringAndOutputs146.getSpitAlternatives().add(string88);
		String string89 = replaceEntities("Er is ook !($URGENCY) veel licht. Nu bedraagt de licht intensiteit ?($LIGHT) lux. Kun je een minder heldere plek voor me vinden?");
		generateStringAndOutputs146.getSpitAlternatives().add(string89);
		return generateStringAndOutputs146;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext147() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext147 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext147.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext147.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext147.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext147.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext147.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext147.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext147;
	}

	de.semvox.types.odp.interaction.Inform getInform148() {
		final de.semvox.types.odp.interaction.Inform inform148 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.LightUpdate lightUpdate149 = getLightUpdate149();
		inform148.setCommonContent(lightUpdate149);
		return inform148;
	}

	com.zorabots.ontologies.vmn.LightUpdate getLightUpdate149() {
		final com.zorabots.ontologies.vmn.LightUpdate lightUpdate149 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLightUpdate();
		com.zorabots.ontologies.vmn.NotFirst notFirst150 = getNotFirst150();
		lightUpdate149.setVmnUpdatePosition(notFirst150);
		com.zorabots.ontologies.vmn.ToHight toHight151 = getToHight151();
		lightUpdate149.setVmnExtreme(toHight151);
		lightUpdate149.markVmnUrgencyAsVariable("URGENCY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		lightUpdate149.assignVmnUrgencyPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.LightReport lightReport152 = getLightReport152();
		lightUpdate149.setVmnLightReport(lightReport152);
		return lightUpdate149;
	}

	com.zorabots.ontologies.vmn.NotFirst getNotFirst150() {
		final com.zorabots.ontologies.vmn.NotFirst notFirst150 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNotFirst();
		return notFirst150;
	}

	com.zorabots.ontologies.vmn.ToHight getToHight151() {
		final com.zorabots.ontologies.vmn.ToHight toHight151 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newToHight();
		return toHight151;
	}

	com.zorabots.ontologies.vmn.LightReport getLightReport152() {
		final com.zorabots.ontologies.vmn.LightReport lightReport152 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLightReport();
		com.zorabots.ontologies.vmn.Light light153 = getLight153();
		lightReport152.setVmnLight(light153);
		return lightReport152;
	}

	com.zorabots.ontologies.vmn.Light getLight153() {
		final com.zorabots.ontologies.vmn.Light light153 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLight();
		light153.markCommonStringValueAsVariable("LIGHT", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		light153.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return light153;
	}

	de.semvox.types.odp.spit.Template getTemplate154() {
		final de.semvox.types.odp.spit.Template template154 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string90 = replaceEntities("Inform_Light");
		template154.setCommonName(string90);
		de.semvox.types.odp.spit.Switch switch155 = getSwitch155();
		template154.setSpitAction(switch155);
		de.semvox.types.odp.interaction.GenerationContext generationContext162 = getGenerationContext162();
		template154.setSpitOnContext(generationContext162);
		de.semvox.types.odp.interaction.Inform inform163 = getInform163();
		template154.setSpitOnInput(inform163);
		return template154;
	}

	de.semvox.types.odp.spit.Switch getSwitch155() {
		final de.semvox.types.odp.spit.Switch switch155 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case156 = getCase156();
			switch155.getSpitCases().add(case156);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case159 = getCase159();
			switch155.getSpitCases().add(case159);
		}
		return switch155;
	}

	de.semvox.types.odp.spit.Case getCase156() {
		final de.semvox.types.odp.spit.Case case156 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition157 = getStringCondition157();
		case156.getSpitConditions().add(stringCondition157);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs158 = getGenerateStringAndOutputs158();
		case156.setSpitAction(generateStringAndOutputs158);
		return case156;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition157() {
		final de.semvox.types.odp.spit.StringCondition stringCondition157 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string91 = replaceEntities("lang");
		stringCondition157.setAlgVariableReference(string91);
		String string92 = replaceEntities("en-US");
		stringCondition157.setCommonStringValue(string92);
		return stringCondition157;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs158() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs158 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs158.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string93 = replaceEntities("The amount of light i'm getting is !($URGENCY) low!. At this moment the amount of light is ?($LIGHT) lux. Can you move me to a brighter place?");
		generateStringAndOutputs158.getSpitAlternatives().add(string93);
		String string94 = replaceEntities("?(Excuse me. )The light intensity here is !($URGENCY) low!. Now the light intensity is ?($LIGHT) lux. Can you find a brighter spot for me?");
		generateStringAndOutputs158.getSpitAlternatives().add(string94);
		return generateStringAndOutputs158;
	}

	de.semvox.types.odp.spit.Case getCase159() {
		final de.semvox.types.odp.spit.Case case159 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition160 = getStringCondition160();
		case159.getSpitConditions().add(stringCondition160);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs161 = getGenerateStringAndOutputs161();
		case159.setSpitAction(generateStringAndOutputs161);
		return case159;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition160() {
		final de.semvox.types.odp.spit.StringCondition stringCondition160 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string95 = replaceEntities("lang");
		stringCondition160.setAlgVariableReference(string95);
		String string96 = replaceEntities("nl-BE");
		stringCondition160.setCommonStringValue(string96);
		return stringCondition160;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs161() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs161 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs161.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string97 = replaceEntities("Er is !($URGENCY) weinig licht. Op dit moment is de licht intensiteit ?($LIGHT) lux. Kun je me op een meer heldere plaats zetten? ");
		generateStringAndOutputs161.getSpitAlternatives().add(string97);
		String string98 = replaceEntities("?(Excuseer me. )Ik vang !($URGENCY) weinig licht op. Nu is de licht intensiteit ?($LIGHT) lux. Kun je me verplaatsen naar een meer heldere plek?");
		generateStringAndOutputs161.getSpitAlternatives().add(string98);
		return generateStringAndOutputs161;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext162() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext162 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext162.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext162.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext162.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext162.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext162.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext162.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext162;
	}

	de.semvox.types.odp.interaction.Inform getInform163() {
		final de.semvox.types.odp.interaction.Inform inform163 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.LightUpdate lightUpdate164 = getLightUpdate164();
		inform163.setCommonContent(lightUpdate164);
		return inform163;
	}

	com.zorabots.ontologies.vmn.LightUpdate getLightUpdate164() {
		final com.zorabots.ontologies.vmn.LightUpdate lightUpdate164 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLightUpdate();
		com.zorabots.ontologies.vmn.First first165 = getFirst165();
		lightUpdate164.setVmnUpdatePosition(first165);
		com.zorabots.ontologies.vmn.ToLow toLow166 = getToLow166();
		lightUpdate164.setVmnExtreme(toLow166);
		lightUpdate164.markVmnUrgencyAsVariable("URGENCY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		lightUpdate164.assignVmnUrgencyPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.LightReport lightReport167 = getLightReport167();
		lightUpdate164.setVmnLightReport(lightReport167);
		return lightUpdate164;
	}

	com.zorabots.ontologies.vmn.First getFirst165() {
		final com.zorabots.ontologies.vmn.First first165 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newFirst();
		return first165;
	}

	com.zorabots.ontologies.vmn.ToLow getToLow166() {
		final com.zorabots.ontologies.vmn.ToLow toLow166 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newToLow();
		return toLow166;
	}

	com.zorabots.ontologies.vmn.LightReport getLightReport167() {
		final com.zorabots.ontologies.vmn.LightReport lightReport167 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLightReport();
		com.zorabots.ontologies.vmn.Light light168 = getLight168();
		lightReport167.setVmnLight(light168);
		return lightReport167;
	}

	com.zorabots.ontologies.vmn.Light getLight168() {
		final com.zorabots.ontologies.vmn.Light light168 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLight();
		light168.markCommonStringValueAsVariable("LIGHT", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		light168.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return light168;
	}

	de.semvox.types.odp.spit.Template getTemplate169() {
		final de.semvox.types.odp.spit.Template template169 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string99 = replaceEntities("Inform_Light");
		template169.setCommonName(string99);
		de.semvox.types.odp.spit.Switch switch170 = getSwitch170();
		template169.setSpitAction(switch170);
		de.semvox.types.odp.interaction.GenerationContext generationContext177 = getGenerationContext177();
		template169.setSpitOnContext(generationContext177);
		de.semvox.types.odp.interaction.Inform inform178 = getInform178();
		template169.setSpitOnInput(inform178);
		return template169;
	}

	de.semvox.types.odp.spit.Switch getSwitch170() {
		final de.semvox.types.odp.spit.Switch switch170 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case171 = getCase171();
			switch170.getSpitCases().add(case171);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case174 = getCase174();
			switch170.getSpitCases().add(case174);
		}
		return switch170;
	}

	de.semvox.types.odp.spit.Case getCase171() {
		final de.semvox.types.odp.spit.Case case171 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition172 = getStringCondition172();
		case171.getSpitConditions().add(stringCondition172);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs173 = getGenerateStringAndOutputs173();
		case171.setSpitAction(generateStringAndOutputs173);
		return case171;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition172() {
		final de.semvox.types.odp.spit.StringCondition stringCondition172 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string100 = replaceEntities("lang");
		stringCondition172.setAlgVariableReference(string100);
		String string101 = replaceEntities("en-US");
		stringCondition172.setCommonStringValue(string101);
		return stringCondition172;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs173() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs173 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs173.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string102 = replaceEntities("It also seems like the amount of light is !($URGENCY) low!. At this moment the light is ?($LIGHT) lux. Can you move me to a brighter place?");
		generateStringAndOutputs173.getSpitAlternatives().add(string102);
		String string103 = replaceEntities("Also, the light intensity here is !($URGENCY) low!. Now the light intensity is ?($LIGHT) lux. Can you find a brighter spot for me?");
		generateStringAndOutputs173.getSpitAlternatives().add(string103);
		return generateStringAndOutputs173;
	}

	de.semvox.types.odp.spit.Case getCase174() {
		final de.semvox.types.odp.spit.Case case174 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition175 = getStringCondition175();
		case174.getSpitConditions().add(stringCondition175);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs176 = getGenerateStringAndOutputs176();
		case174.setSpitAction(generateStringAndOutputs176);
		return case174;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition175() {
		final de.semvox.types.odp.spit.StringCondition stringCondition175 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string104 = replaceEntities("lang");
		stringCondition175.setAlgVariableReference(string104);
		String string105 = replaceEntities("nl-BE");
		stringCondition175.setCommonStringValue(string105);
		return stringCondition175;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs176() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs176 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs176.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string106 = replaceEntities("Er is ook !($URGENCY) weinig licht. Op dit moment is de licht intensiteit ?($LIGHT) lux. Kun je me op een meer heldere plaats zetten? ");
		generateStringAndOutputs176.getSpitAlternatives().add(string106);
		String string107 = replaceEntities("Ik vang ook !($URGENCY) weinig licht op. Nu is de licht intensiteit ?($LIGHT) lux. Kun je me verplaatsen naar een meer heldere plek?");
		generateStringAndOutputs176.getSpitAlternatives().add(string107);
		return generateStringAndOutputs176;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext177() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext177 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext177.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext177.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext177.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext177.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext177.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext177.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext177;
	}

	de.semvox.types.odp.interaction.Inform getInform178() {
		final de.semvox.types.odp.interaction.Inform inform178 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.LightUpdate lightUpdate179 = getLightUpdate179();
		inform178.setCommonContent(lightUpdate179);
		return inform178;
	}

	com.zorabots.ontologies.vmn.LightUpdate getLightUpdate179() {
		final com.zorabots.ontologies.vmn.LightUpdate lightUpdate179 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLightUpdate();
		com.zorabots.ontologies.vmn.NotFirst notFirst180 = getNotFirst180();
		lightUpdate179.setVmnUpdatePosition(notFirst180);
		com.zorabots.ontologies.vmn.ToLow toLow181 = getToLow181();
		lightUpdate179.setVmnExtreme(toLow181);
		lightUpdate179.markVmnUrgencyAsVariable("URGENCY", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		lightUpdate179.assignVmnUrgencyPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		com.zorabots.ontologies.vmn.LightReport lightReport182 = getLightReport182();
		lightUpdate179.setVmnLightReport(lightReport182);
		return lightUpdate179;
	}

	com.zorabots.ontologies.vmn.NotFirst getNotFirst180() {
		final com.zorabots.ontologies.vmn.NotFirst notFirst180 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNotFirst();
		return notFirst180;
	}

	com.zorabots.ontologies.vmn.ToLow getToLow181() {
		final com.zorabots.ontologies.vmn.ToLow toLow181 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newToLow();
		return toLow181;
	}

	com.zorabots.ontologies.vmn.LightReport getLightReport182() {
		final com.zorabots.ontologies.vmn.LightReport lightReport182 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLightReport();
		com.zorabots.ontologies.vmn.Light light183 = getLight183();
		lightReport182.setVmnLight(light183);
		return lightReport182;
	}

	com.zorabots.ontologies.vmn.Light getLight183() {
		final com.zorabots.ontologies.vmn.Light light183 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLight();
		light183.markCommonStringValueAsVariable("LIGHT", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		light183.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return light183;
	}

	de.semvox.types.odp.spit.Template getTemplate184() {
		final de.semvox.types.odp.spit.Template template184 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string108 = replaceEntities("Inform_NewPlant");
		template184.setCommonName(string108);
		de.semvox.types.odp.spit.Switch switch185 = getSwitch185();
		template184.setSpitAction(switch185);
		de.semvox.types.odp.interaction.GenerationContext generationContext192 = getGenerationContext192();
		template184.setSpitOnContext(generationContext192);
		de.semvox.types.odp.interaction.Inform inform193 = getInform193();
		template184.setSpitOnInput(inform193);
		return template184;
	}

	de.semvox.types.odp.spit.Switch getSwitch185() {
		final de.semvox.types.odp.spit.Switch switch185 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case186 = getCase186();
			switch185.getSpitCases().add(case186);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case189 = getCase189();
			switch185.getSpitCases().add(case189);
		}
		return switch185;
	}

	de.semvox.types.odp.spit.Case getCase186() {
		final de.semvox.types.odp.spit.Case case186 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition187 = getStringCondition187();
		case186.getSpitConditions().add(stringCondition187);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs188 = getGenerateStringAndOutputs188();
		case186.setSpitAction(generateStringAndOutputs188);
		return case186;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition187() {
		final de.semvox.types.odp.spit.StringCondition stringCondition187 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string109 = replaceEntities("lang");
		stringCondition187.setAlgVariableReference(string109);
		String string110 = replaceEntities("en-US");
		stringCondition187.setCommonStringValue(string110);
		return stringCondition187;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs188() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs188 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs188.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string111 = replaceEntities("I see that you inserted a new plant. My new plant is (called|a) ?($NAME)");
		generateStringAndOutputs188.getSpitAlternatives().add(string111);
		return generateStringAndOutputs188;
	}

	de.semvox.types.odp.spit.Case getCase189() {
		final de.semvox.types.odp.spit.Case case189 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition190 = getStringCondition190();
		case189.getSpitConditions().add(stringCondition190);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs191 = getGenerateStringAndOutputs191();
		case189.setSpitAction(generateStringAndOutputs191);
		return case189;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition190() {
		final de.semvox.types.odp.spit.StringCondition stringCondition190 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string112 = replaceEntities("lang");
		stringCondition190.setAlgVariableReference(string112);
		String string113 = replaceEntities("nl-BE");
		stringCondition190.setCommonStringValue(string113);
		return stringCondition190;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs191() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs191 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs191.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string114 = replaceEntities("Ik zie dat je een nieuwe plant hebt ingevoegd. (De naam van de plant is ?($NAME)|De naam van mijn plant is nu ?($NAME))");
		generateStringAndOutputs191.getSpitAlternatives().add(string114);
		return generateStringAndOutputs191;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext192() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext192 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext192.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext192.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext192.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext192.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext192.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext192.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext192;
	}

	de.semvox.types.odp.interaction.Inform getInform193() {
		final de.semvox.types.odp.interaction.Inform inform193 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.NewPotUpdate newPotUpdate194 = getNewPotUpdate194();
		inform193.setCommonContent(newPotUpdate194);
		return inform193;
	}

	com.zorabots.ontologies.vmn.NewPotUpdate getNewPotUpdate194() {
		final com.zorabots.ontologies.vmn.NewPotUpdate newPotUpdate194 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNewPotUpdate();
		com.zorabots.ontologies.vmn.Plant plant195 = getPlant195();
		newPotUpdate194.setVmnPlant(plant195);
		com.zorabots.ontologies.vmn.Injected injected196 = getInjected196();
		newPotUpdate194.setVmnChange(injected196);
		return newPotUpdate194;
	}

	com.zorabots.ontologies.vmn.Plant getPlant195() {
		final com.zorabots.ontologies.vmn.Plant plant195 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newPlant();
		plant195.markVmnPlantNameAsVariable("NAME", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant195.assignVmnPlantNamePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return plant195;
	}

	com.zorabots.ontologies.vmn.Injected getInjected196() {
		final com.zorabots.ontologies.vmn.Injected injected196 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newInjected();
		return injected196;
	}

	de.semvox.types.odp.spit.Template getTemplate197() {
		final de.semvox.types.odp.spit.Template template197 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string115 = replaceEntities("Inform_PlantRemoved");
		template197.setCommonName(string115);
		de.semvox.types.odp.spit.Switch switch198 = getSwitch198();
		template197.setSpitAction(switch198);
		de.semvox.types.odp.interaction.GenerationContext generationContext205 = getGenerationContext205();
		template197.setSpitOnContext(generationContext205);
		de.semvox.types.odp.interaction.Inform inform206 = getInform206();
		template197.setSpitOnInput(inform206);
		return template197;
	}

	de.semvox.types.odp.spit.Switch getSwitch198() {
		final de.semvox.types.odp.spit.Switch switch198 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case199 = getCase199();
			switch198.getSpitCases().add(case199);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case202 = getCase202();
			switch198.getSpitCases().add(case202);
		}
		return switch198;
	}

	de.semvox.types.odp.spit.Case getCase199() {
		final de.semvox.types.odp.spit.Case case199 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition200 = getStringCondition200();
		case199.getSpitConditions().add(stringCondition200);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs201 = getGenerateStringAndOutputs201();
		case199.setSpitAction(generateStringAndOutputs201);
		return case199;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition200() {
		final de.semvox.types.odp.spit.StringCondition stringCondition200 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string116 = replaceEntities("lang");
		stringCondition200.setAlgVariableReference(string116);
		String string117 = replaceEntities("en-US");
		stringCondition200.setCommonStringValue(string117);
		return stringCondition200;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs201() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs201 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs201.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string118 = replaceEntities("I see that you removed the ?($NAME), I will not be able to know the state of the plant anymore. Take good care of it.  ");
		generateStringAndOutputs201.getSpitAlternatives().add(string118);
		return generateStringAndOutputs201;
	}

	de.semvox.types.odp.spit.Case getCase202() {
		final de.semvox.types.odp.spit.Case case202 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition203 = getStringCondition203();
		case202.getSpitConditions().add(stringCondition203);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs204 = getGenerateStringAndOutputs204();
		case202.setSpitAction(generateStringAndOutputs204);
		return case202;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition203() {
		final de.semvox.types.odp.spit.StringCondition stringCondition203 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string119 = replaceEntities("lang");
		stringCondition203.setAlgVariableReference(string119);
		String string120 = replaceEntities("nl-BE");
		stringCondition203.setCommonStringValue(string120);
		return stringCondition203;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs204() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs204 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs204.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string121 = replaceEntities("Ik zie dat je de ?($NAME) hebt verwijderd. Ik kan er nu dus niet meer voor zorgen. Dat zal je nu helemaal zelf moeten doen. Succes.");
		generateStringAndOutputs204.getSpitAlternatives().add(string121);
		return generateStringAndOutputs204;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext205() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext205 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext205.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext205.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext205.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext205.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext205.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext205.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext205;
	}

	de.semvox.types.odp.interaction.Inform getInform206() {
		final de.semvox.types.odp.interaction.Inform inform206 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.NewPotUpdate newPotUpdate207 = getNewPotUpdate207();
		inform206.setCommonContent(newPotUpdate207);
		return inform206;
	}

	com.zorabots.ontologies.vmn.NewPotUpdate getNewPotUpdate207() {
		final com.zorabots.ontologies.vmn.NewPotUpdate newPotUpdate207 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNewPotUpdate();
		com.zorabots.ontologies.vmn.Plant plant208 = getPlant208();
		newPotUpdate207.setVmnPlant(plant208);
		com.zorabots.ontologies.vmn.Removed removed209 = getRemoved209();
		newPotUpdate207.setVmnChange(removed209);
		return newPotUpdate207;
	}

	com.zorabots.ontologies.vmn.Plant getPlant208() {
		final com.zorabots.ontologies.vmn.Plant plant208 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newPlant();
		plant208.markVmnPlantNameAsVariable("NAME", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		plant208.assignVmnPlantNamePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return plant208;
	}

	com.zorabots.ontologies.vmn.Removed getRemoved209() {
		final com.zorabots.ontologies.vmn.Removed removed209 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newRemoved();
		return removed209;
	}

	de.semvox.types.odp.spit.Template getTemplate210() {
		final de.semvox.types.odp.spit.Template template210 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string122 = replaceEntities("Start");
		template210.setCommonName(string122);
		de.semvox.types.odp.spit.Switch switch211 = getSwitch211();
		template210.setSpitAction(switch211);
		de.semvox.types.odp.interaction.GenerationContext generationContext218 = getGenerationContext218();
		template210.setSpitOnContext(generationContext218);
		de.semvox.types.odp.interaction.Inform inform219 = getInform219();
		template210.setSpitOnInput(inform219);
		return template210;
	}

	de.semvox.types.odp.spit.Switch getSwitch211() {
		final de.semvox.types.odp.spit.Switch switch211 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case212 = getCase212();
			switch211.getSpitCases().add(case212);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case215 = getCase215();
			switch211.getSpitCases().add(case215);
		}
		return switch211;
	}

	de.semvox.types.odp.spit.Case getCase212() {
		final de.semvox.types.odp.spit.Case case212 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition213 = getStringCondition213();
		case212.getSpitConditions().add(stringCondition213);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs214 = getGenerateStringAndOutputs214();
		case212.setSpitAction(generateStringAndOutputs214);
		return case212;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition213() {
		final de.semvox.types.odp.spit.StringCondition stringCondition213 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string123 = replaceEntities("lang");
		stringCondition213.setAlgVariableReference(string123);
		String string124 = replaceEntities("en-US");
		stringCondition213.setCommonStringValue(string124);
		return stringCondition213;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs214() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs214 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs214.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string125 = replaceEntities("Hi. My name is Billy. I am ready for use. My IP address is ?($IPADDR)");
		generateStringAndOutputs214.getSpitAlternatives().add(string125);
		return generateStringAndOutputs214;
	}

	de.semvox.types.odp.spit.Case getCase215() {
		final de.semvox.types.odp.spit.Case case215 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition216 = getStringCondition216();
		case215.getSpitConditions().add(stringCondition216);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs217 = getGenerateStringAndOutputs217();
		case215.setSpitAction(generateStringAndOutputs217);
		return case215;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition216() {
		final de.semvox.types.odp.spit.StringCondition stringCondition216 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string126 = replaceEntities("lang");
		stringCondition216.setAlgVariableReference(string126);
		String string127 = replaceEntities("nl-BE");
		stringCondition216.setCommonStringValue(string127);
		return stringCondition216;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs217() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs217 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs217.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string128 = replaceEntities("Hallo. Mijn naam is Billy. Ik ben klaar voor gebruik. Mijn IP adres is ?($IPADDR)");
		generateStringAndOutputs217.getSpitAlternatives().add(string128);
		return generateStringAndOutputs217;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext218() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext218 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext218.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext218.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext218.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext218.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext218.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext218.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext218;
	}

	de.semvox.types.odp.interaction.Inform getInform219() {
		final de.semvox.types.odp.interaction.Inform inform219 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.StartUpdate startUpdate220 = getStartUpdate220();
		inform219.setCommonContent(startUpdate220);
		return inform219;
	}

	com.zorabots.ontologies.vmn.StartUpdate getStartUpdate220() {
		final com.zorabots.ontologies.vmn.StartUpdate startUpdate220 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newStartUpdate();
		com.zorabots.ontologies.vmn.IpAddress ipAddress221 = getIpAddress221();
		startUpdate220.setVmnIpAddress(ipAddress221);
		return startUpdate220;
	}

	com.zorabots.ontologies.vmn.IpAddress getIpAddress221() {
		final com.zorabots.ontologies.vmn.IpAddress ipAddress221 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newIpAddress();
		ipAddress221.markCommonStringValueAsVariable("IPADDR", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		ipAddress221.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return ipAddress221;
	}

	de.semvox.types.odp.spit.Template getTemplate222() {
		final de.semvox.types.odp.spit.Template template222 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string129 = replaceEntities("Wifi_connecting");
		template222.setCommonName(string129);
		de.semvox.types.odp.spit.Switch switch223 = getSwitch223();
		template222.setSpitAction(switch223);
		de.semvox.types.odp.interaction.GenerationContext generationContext230 = getGenerationContext230();
		template222.setSpitOnContext(generationContext230);
		de.semvox.types.odp.interaction.Inform inform231 = getInform231();
		template222.setSpitOnInput(inform231);
		return template222;
	}

	de.semvox.types.odp.spit.Switch getSwitch223() {
		final de.semvox.types.odp.spit.Switch switch223 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case224 = getCase224();
			switch223.getSpitCases().add(case224);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case227 = getCase227();
			switch223.getSpitCases().add(case227);
		}
		return switch223;
	}

	de.semvox.types.odp.spit.Case getCase224() {
		final de.semvox.types.odp.spit.Case case224 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition225 = getStringCondition225();
		case224.getSpitConditions().add(stringCondition225);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs226 = getGenerateStringAndOutputs226();
		case224.setSpitAction(generateStringAndOutputs226);
		return case224;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition225() {
		final de.semvox.types.odp.spit.StringCondition stringCondition225 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string130 = replaceEntities("lang");
		stringCondition225.setAlgVariableReference(string130);
		String string131 = replaceEntities("en-US");
		stringCondition225.setCommonStringValue(string131);
		return stringCondition225;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs226() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs226 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs226.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string132 = replaceEntities("Just a moment please, i'm trying to connect to the local wireless network. ");
		generateStringAndOutputs226.getSpitAlternatives().add(string132);
		return generateStringAndOutputs226;
	}

	de.semvox.types.odp.spit.Case getCase227() {
		final de.semvox.types.odp.spit.Case case227 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition228 = getStringCondition228();
		case227.getSpitConditions().add(stringCondition228);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs229 = getGenerateStringAndOutputs229();
		case227.setSpitAction(generateStringAndOutputs229);
		return case227;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition228() {
		final de.semvox.types.odp.spit.StringCondition stringCondition228 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string133 = replaceEntities("lang");
		stringCondition228.setAlgVariableReference(string133);
		String string134 = replaceEntities("nl-BE");
		stringCondition228.setCommonStringValue(string134);
		return stringCondition228;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs229() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs229 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs229.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string135 = replaceEntities("Een moment geduld alstublieft, ik probeer te connecteren met het lokaal draadloos netwerk. ");
		generateStringAndOutputs229.getSpitAlternatives().add(string135);
		return generateStringAndOutputs229;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext230() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext230 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext230.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext230.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext230.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext230.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext230.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext230.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext230;
	}

	de.semvox.types.odp.interaction.Inform getInform231() {
		final de.semvox.types.odp.interaction.Inform inform231 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.WifiUpdate wifiUpdate232 = getWifiUpdate232();
		inform231.setCommonContent(wifiUpdate232);
		return inform231;
	}

	com.zorabots.ontologies.vmn.WifiUpdate getWifiUpdate232() {
		final com.zorabots.ontologies.vmn.WifiUpdate wifiUpdate232 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newWifiUpdate();
		com.zorabots.ontologies.vmn.Connecting connecting233 = getConnecting233();
		wifiUpdate232.setVmnWifiStatus(connecting233);
		return wifiUpdate232;
	}

	com.zorabots.ontologies.vmn.Connecting getConnecting233() {
		final com.zorabots.ontologies.vmn.Connecting connecting233 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newConnecting();
		return connecting233;
	}

	de.semvox.types.odp.spit.Template getTemplate234() {
		final de.semvox.types.odp.spit.Template template234 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string136 = replaceEntities("Wifi_connected");
		template234.setCommonName(string136);
		de.semvox.types.odp.spit.Switch switch235 = getSwitch235();
		template234.setSpitAction(switch235);
		de.semvox.types.odp.interaction.GenerationContext generationContext242 = getGenerationContext242();
		template234.setSpitOnContext(generationContext242);
		de.semvox.types.odp.interaction.Inform inform243 = getInform243();
		template234.setSpitOnInput(inform243);
		return template234;
	}

	de.semvox.types.odp.spit.Switch getSwitch235() {
		final de.semvox.types.odp.spit.Switch switch235 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case236 = getCase236();
			switch235.getSpitCases().add(case236);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case239 = getCase239();
			switch235.getSpitCases().add(case239);
		}
		return switch235;
	}

	de.semvox.types.odp.spit.Case getCase236() {
		final de.semvox.types.odp.spit.Case case236 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition237 = getStringCondition237();
		case236.getSpitConditions().add(stringCondition237);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs238 = getGenerateStringAndOutputs238();
		case236.setSpitAction(generateStringAndOutputs238);
		return case236;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition237() {
		final de.semvox.types.odp.spit.StringCondition stringCondition237 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string137 = replaceEntities("lang");
		stringCondition237.setAlgVariableReference(string137);
		String string138 = replaceEntities("en-US");
		stringCondition237.setCommonStringValue(string138);
		return stringCondition237;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs238() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs238 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs238.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string139 = replaceEntities("I succesfully connected to the local network. ");
		generateStringAndOutputs238.getSpitAlternatives().add(string139);
		return generateStringAndOutputs238;
	}

	de.semvox.types.odp.spit.Case getCase239() {
		final de.semvox.types.odp.spit.Case case239 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition240 = getStringCondition240();
		case239.getSpitConditions().add(stringCondition240);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs241 = getGenerateStringAndOutputs241();
		case239.setSpitAction(generateStringAndOutputs241);
		return case239;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition240() {
		final de.semvox.types.odp.spit.StringCondition stringCondition240 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string140 = replaceEntities("lang");
		stringCondition240.setAlgVariableReference(string140);
		String string141 = replaceEntities("nl-BE");
		stringCondition240.setCommonStringValue(string141);
		return stringCondition240;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs241() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs241 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs241.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string142 = replaceEntities("ik kreeg zonet toegang tot het lokaal netwerk. ");
		generateStringAndOutputs241.getSpitAlternatives().add(string142);
		return generateStringAndOutputs241;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext242() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext242 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext242.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext242.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext242.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext242.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext242.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext242.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext242;
	}

	de.semvox.types.odp.interaction.Inform getInform243() {
		final de.semvox.types.odp.interaction.Inform inform243 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.WifiUpdate wifiUpdate244 = getWifiUpdate244();
		inform243.setCommonContent(wifiUpdate244);
		return inform243;
	}

	com.zorabots.ontologies.vmn.WifiUpdate getWifiUpdate244() {
		final com.zorabots.ontologies.vmn.WifiUpdate wifiUpdate244 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newWifiUpdate();
		com.zorabots.ontologies.vmn.Connected connected245 = getConnected245();
		wifiUpdate244.setVmnWifiStatus(connected245);
		return wifiUpdate244;
	}

	com.zorabots.ontologies.vmn.Connected getConnected245() {
		final com.zorabots.ontologies.vmn.Connected connected245 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newConnected();
		return connected245;
	}

	de.semvox.types.odp.spit.Template getTemplate246() {
		final de.semvox.types.odp.spit.Template template246 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string143 = replaceEntities("Wifi_connected");
		template246.setCommonName(string143);
		de.semvox.types.odp.spit.Switch switch247 = getSwitch247();
		template246.setSpitAction(switch247);
		de.semvox.types.odp.interaction.GenerationContext generationContext254 = getGenerationContext254();
		template246.setSpitOnContext(generationContext254);
		de.semvox.types.odp.interaction.Inform inform255 = getInform255();
		template246.setSpitOnInput(inform255);
		return template246;
	}

	de.semvox.types.odp.spit.Switch getSwitch247() {
		final de.semvox.types.odp.spit.Switch switch247 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case248 = getCase248();
			switch247.getSpitCases().add(case248);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case251 = getCase251();
			switch247.getSpitCases().add(case251);
		}
		return switch247;
	}

	de.semvox.types.odp.spit.Case getCase248() {
		final de.semvox.types.odp.spit.Case case248 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition249 = getStringCondition249();
		case248.getSpitConditions().add(stringCondition249);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs250 = getGenerateStringAndOutputs250();
		case248.setSpitAction(generateStringAndOutputs250);
		return case248;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition249() {
		final de.semvox.types.odp.spit.StringCondition stringCondition249 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string144 = replaceEntities("lang");
		stringCondition249.setAlgVariableReference(string144);
		String string145 = replaceEntities("en-US");
		stringCondition249.setCommonStringValue(string145);
		return stringCondition249;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs250() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs250 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs250.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string146 = replaceEntities("I just got disconnected from the local network. ");
		generateStringAndOutputs250.getSpitAlternatives().add(string146);
		return generateStringAndOutputs250;
	}

	de.semvox.types.odp.spit.Case getCase251() {
		final de.semvox.types.odp.spit.Case case251 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition252 = getStringCondition252();
		case251.getSpitConditions().add(stringCondition252);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs253 = getGenerateStringAndOutputs253();
		case251.setSpitAction(generateStringAndOutputs253);
		return case251;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition252() {
		final de.semvox.types.odp.spit.StringCondition stringCondition252 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string147 = replaceEntities("lang");
		stringCondition252.setAlgVariableReference(string147);
		String string148 = replaceEntities("nl-BE");
		stringCondition252.setCommonStringValue(string148);
		return stringCondition252;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs253() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs253 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs253.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string149 = replaceEntities("De verbinding met het lokaal netwerk is zojuist afgebroken. ");
		generateStringAndOutputs253.getSpitAlternatives().add(string149);
		return generateStringAndOutputs253;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext254() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext254 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext254.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext254.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext254.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext254.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext254.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext254.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext254;
	}

	de.semvox.types.odp.interaction.Inform getInform255() {
		final de.semvox.types.odp.interaction.Inform inform255 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.WifiUpdate wifiUpdate256 = getWifiUpdate256();
		inform255.setCommonContent(wifiUpdate256);
		return inform255;
	}

	com.zorabots.ontologies.vmn.WifiUpdate getWifiUpdate256() {
		final com.zorabots.ontologies.vmn.WifiUpdate wifiUpdate256 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newWifiUpdate();
		com.zorabots.ontologies.vmn.Disconnected disconnected257 = getDisconnected257();
		wifiUpdate256.setVmnWifiStatus(disconnected257);
		return wifiUpdate256;
	}

	com.zorabots.ontologies.vmn.Disconnected getDisconnected257() {
		final com.zorabots.ontologies.vmn.Disconnected disconnected257 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newDisconnected();
		return disconnected257;
	}

	de.semvox.types.odp.spit.Template getTemplate258() {
		final de.semvox.types.odp.spit.Template template258 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string150 = replaceEntities("tts_event");
		template258.setCommonName(string150);
		de.semvox.types.odp.spit.Switch switch259 = getSwitch259();
		template258.setSpitAction(switch259);
		de.semvox.types.odp.interaction.GenerationContext generationContext266 = getGenerationContext266();
		template258.setSpitOnContext(generationContext266);
		de.semvox.types.odp.interaction.Inform inform267 = getInform267();
		template258.setSpitOnInput(inform267);
		return template258;
	}

	de.semvox.types.odp.spit.Switch getSwitch259() {
		final de.semvox.types.odp.spit.Switch switch259 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case260 = getCase260();
			switch259.getSpitCases().add(case260);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case263 = getCase263();
			switch259.getSpitCases().add(case263);
		}
		return switch259;
	}

	de.semvox.types.odp.spit.Case getCase260() {
		final de.semvox.types.odp.spit.Case case260 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition261 = getStringCondition261();
		case260.getSpitConditions().add(stringCondition261);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs262 = getGenerateStringAndOutputs262();
		case260.setSpitAction(generateStringAndOutputs262);
		return case260;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition261() {
		final de.semvox.types.odp.spit.StringCondition stringCondition261 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string151 = replaceEntities("lang");
		stringCondition261.setAlgVariableReference(string151);
		String string152 = replaceEntities("en-US");
		stringCondition261.setCommonStringValue(string152);
		return stringCondition261;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs262() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs262 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs262.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string153 = replaceEntities("?($MESSAGE)");
		generateStringAndOutputs262.getSpitAlternatives().add(string153);
		return generateStringAndOutputs262;
	}

	de.semvox.types.odp.spit.Case getCase263() {
		final de.semvox.types.odp.spit.Case case263 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition264 = getStringCondition264();
		case263.getSpitConditions().add(stringCondition264);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs265 = getGenerateStringAndOutputs265();
		case263.setSpitAction(generateStringAndOutputs265);
		return case263;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition264() {
		final de.semvox.types.odp.spit.StringCondition stringCondition264 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string154 = replaceEntities("lang");
		stringCondition264.setAlgVariableReference(string154);
		String string155 = replaceEntities("nl-BE");
		stringCondition264.setCommonStringValue(string155);
		return stringCondition264;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs265() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs265 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs265.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string156 = replaceEntities("?($MESSAGE)");
		generateStringAndOutputs265.getSpitAlternatives().add(string156);
		return generateStringAndOutputs265;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext266() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext266 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext266.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext266.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext266.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext266.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext266.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext266.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext266;
	}

	de.semvox.types.odp.interaction.Inform getInform267() {
		final de.semvox.types.odp.interaction.Inform inform267 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newInform();
		com.zorabots.ontologies.vmn.TtsUpdate ttsUpdate268 = getTtsUpdate268();
		inform267.setCommonContent(ttsUpdate268);
		return inform267;
	}

	com.zorabots.ontologies.vmn.TtsUpdate getTtsUpdate268() {
		final com.zorabots.ontologies.vmn.TtsUpdate ttsUpdate268 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTtsUpdate();
		ttsUpdate268.markCommonStringValueAsVariable("MESSAGE", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		ttsUpdate268.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return ttsUpdate268;
	}
}