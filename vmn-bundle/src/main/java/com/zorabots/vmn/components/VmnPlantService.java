package com.zorabots.vmn.components;

import java.util.Map;

import com.zorabots.ontologies.vmn.GetParameters;
import com.zorabots.ontologies.vmn.GetPlantName;
import com.zorabots.ontologies.vmn.Plant;
import com.zorabots.ontologies.vmn.factory.ThingFactoryVmn;
import com.zorabots.vmn.components.gen.VmnPlantServiceDefinition;

import de.semvox.subcon.integration.api.Service;
import de.semvox.types.subcon.integration.ServiceDefinition;
import de.semvox.types.subcon.integration.ServiceRequest;
import de.semvox.types.subcon.integration.factory.ThingFactoryIntegration;

public class VmnPlantService implements Service {

	private final ServiceDefinition definition;

	public VmnPlantService() throws Exception {
		definition = new VmnPlantServiceDefinition().loadThing();
	}

	@Override
	public ServiceDefinition getServiceDefinition() {
		return definition;
	}

	@Override
	public void dispose() {

	}

	@Override
	public ServiceRequest handleRequest(ServiceRequest request,
			Map<String, Object> boundVariables) {
			if(request instanceof GetPlantName){
				request = getPlantName(request);
			}
			if(request instanceof GetParameters){
				request = getParamters(request);
			}
			
			request.setIntResponseStatus(ThingFactoryIntegration.newStatusSuccess());
		return request;
	}
	
	private ServiceRequest getParamters(ServiceRequest request) {
		Plant p =  ThingFactoryVmn.newPlant();
		
		p.setVmnPlantName(com.zorabots.vmn.pojo.Plant.getInstance().getName());
		String optimalHum = com.zorabots.vmn.pojo.Plant.getInstance().getPlantParamters().get("humidity").getOptimal();
		p.setVmnOptimalHumidity(optimalHum);
		String optimalLight = com.zorabots.vmn.pojo.Plant.getInstance().getPlantParamters().get("light").getOptimal();
		p.setVmnOptimalLight(optimalLight);
		String optimalTemperature = com.zorabots.vmn.pojo.Plant.getInstance().getPlantParamters().get("temperature").getOptimal();
		p.setVmnOptimalTemperature(optimalTemperature);

		((GetParameters)request).setVmnPlant(p);
		request.setIntResponseStatus(ThingFactoryIntegration.newStatusSuccess());
		
		return request;
	}

	private ServiceRequest getPlantName(ServiceRequest request){
		Plant p =  ThingFactoryVmn.newPlant();
		p.setVmnPlantName(com.zorabots.vmn.pojo.Plant.getInstance().getName());
		
		((GetPlantName)request).setVmnPlant(p);
		request.setIntResponseStatus(ThingFactoryIntegration.newStatusSuccess());
		return request;
	}
	
	

}
