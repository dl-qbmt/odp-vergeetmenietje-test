/**
* GENERATED CODE
*/
package com.zorabots.vmn.components.gen;

public final class HumidityEventReceiverDefinition extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.subcon.integration.EventReceiverDefinition> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "int#EventReceiverDefinition";

	public HumidityEventReceiverDefinition(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public HumidityEventReceiverDefinition(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public HumidityEventReceiverDefinition(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public HumidityEventReceiverDefinition() {
		super();
	}

	@Override
	protected de.semvox.types.subcon.integration.EventReceiverDefinition createThing() {
		return getEventReceiverDefinition();
	}

	@Override
	protected void initialize(final de.semvox.types.subcon.integration.EventReceiverDefinition eventReceiverDefinition0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("HumidityEventReceiver");
		eventReceiverDefinition0.setCommonName(string0);
		String string1 = replaceEntities("com.zorabots.vmn.components");
		eventReceiverDefinition0.setIntNamespace(string1);
		com.zorabots.ontologies.vmn.HumidityUpdate humidityUpdate1 = getHumidityUpdate1();
		eventReceiverDefinition0.getIntSubscriptionsAsCommonEvent().put("HUMIDITY_UPDATE", humidityUpdate1);
		com.zorabots.ontologies.vmn.LightUpdate lightUpdate2 = getLightUpdate2();
		eventReceiverDefinition0.getIntSubscriptionsAsCommonEvent().put("LIGHT_UPDATE", lightUpdate2);
		com.zorabots.ontologies.vmn.TemperatureUpdate temperatureUpdate3 = getTemperatureUpdate3();
		eventReceiverDefinition0.getIntSubscriptionsAsCommonEvent().put("TEMPERATURE_UPDATE", temperatureUpdate3);
		com.zorabots.ontologies.vmn.StartUpdate startUpdate4 = getStartUpdate4();
		eventReceiverDefinition0.getIntSubscriptionsAsCommonEvent().put("START_UPDATE", startUpdate4);
	}

	de.semvox.types.subcon.integration.EventReceiverDefinition getEventReceiverDefinition() {
		final de.semvox.types.subcon.integration.EventReceiverDefinition eventReceiverDefinition0 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newEventReceiverDefinition();
		return eventReceiverDefinition0;
	}


	com.zorabots.ontologies.vmn.HumidityUpdate getHumidityUpdate1() {
		final com.zorabots.ontologies.vmn.HumidityUpdate humidityUpdate1 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newHumidityUpdate();
		return humidityUpdate1;
	}

	com.zorabots.ontologies.vmn.LightUpdate getLightUpdate2() {
		final com.zorabots.ontologies.vmn.LightUpdate lightUpdate2 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLightUpdate();
		return lightUpdate2;
	}

	com.zorabots.ontologies.vmn.TemperatureUpdate getTemperatureUpdate3() {
		final com.zorabots.ontologies.vmn.TemperatureUpdate temperatureUpdate3 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newTemperatureUpdate();
		return temperatureUpdate3;
	}

	com.zorabots.ontologies.vmn.StartUpdate getStartUpdate4() {
		final com.zorabots.ontologies.vmn.StartUpdate startUpdate4 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newStartUpdate();
		return startUpdate4;
	}
}