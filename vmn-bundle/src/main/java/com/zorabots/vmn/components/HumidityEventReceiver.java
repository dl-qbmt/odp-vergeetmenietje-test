 package com.zorabots.vmn.components;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Map;

import org.json.simple.JSONObject;

import com.zorabots.ontologies.vmn.Humidity;
import com.zorabots.ontologies.vmn.HumidityReport;
import com.zorabots.ontologies.vmn.HumidityUpdate;
import com.zorabots.ontologies.vmn.IpAddress;
import com.zorabots.ontologies.vmn.Light;
import com.zorabots.ontologies.vmn.LightReport;
import com.zorabots.ontologies.vmn.LightUpdate;
import com.zorabots.ontologies.vmn.NewPotUpdate;
import com.zorabots.ontologies.vmn.StartUpdate;
import com.zorabots.ontologies.vmn.Temperature;
import com.zorabots.ontologies.vmn.TemperatureReport;
import com.zorabots.ontologies.vmn.TemperatureUpdate;
import com.zorabots.ontologies.vmn.factory.ThingFactoryVmn;
import com.zorabots.vmn.components.gen.HumidityEventReceiverDefinition;
import com.zorabots.vmn.events.JsonParser;
import com.zorabots.vmn.staticclasses.Urgency;

import de.semvox.commons.logging.Logger.LogLevel;
import de.semvox.subcon.integration.api.EventReceiver;
import de.semvox.subcon.integration.api.IpcFacade;
import de.semvox.types.common.Event;
import de.semvox.types.odp.multimodal.PresentEvent;
import de.semvox.types.odp.multimodal.factory.ThingFactoryMultimodal;
import de.semvox.types.subcon.integration.EventReceiverDefinition;
import de.semvox.types.subcon.integration.TokenEvent;

public class HumidityEventReceiver implements EventReceiver {

	private final EventReceiverDefinition definition;
	private IpcFacade ipcFacade;

	public HumidityEventReceiver(IpcFacade ipcFacade) throws Exception {
		definition = new HumidityEventReceiverDefinition().loadThing();
		this.ipcFacade = ipcFacade;
	}

	@Override
	public EventReceiverDefinition getEventReceiverDefinition() {
		return definition;
	}

	@Override
	public void dispose() {

	}

	@Override
	public void handleEvent(Event event, Map<String, Object> boundVariables) {
		if(event instanceof HumidityUpdate){
			HumidityUpdate humidityUpdate = (HumidityUpdate) event;
			PresentEvent presentEvent = ThingFactoryMultimodal.newPresentEvent();
			presentEvent.setMmEvent(humidityUpdate);
			ipcFacade.invokeService(presentEvent, getClass().getCanonicalName());
		}
		if(event instanceof LightUpdate){
			LightUpdate lightUpdate = (LightUpdate) event;
			PresentEvent presentEvent = ThingFactoryMultimodal.newPresentEvent();
			presentEvent.setMmEvent(lightUpdate);
			ipcFacade.invokeService(presentEvent, getClass().getCanonicalName());
		}
		if(event instanceof TemperatureUpdate){
			TemperatureUpdate tempUpdate = (TemperatureUpdate) event;
			PresentEvent presentEvent = ThingFactoryMultimodal.newPresentEvent();
			presentEvent.setMmEvent(tempUpdate);
			ipcFacade.invokeService(presentEvent, getClass().getCanonicalName());
		}
		if(event instanceof NewPotUpdate){
			NewPotUpdate potUpdate = (NewPotUpdate) event;
			PresentEvent presentEvent = ThingFactoryMultimodal.newPresentEvent();
			presentEvent.setMmEvent(potUpdate);
			ipcFacade.invokeService(presentEvent, getClass().getCanonicalName());
		}
		if(event instanceof StartUpdate){
			StartUpdate startUpdate = (StartUpdate) event;
			PresentEvent presentEvent = ThingFactoryMultimodal.newPresentEvent();
			presentEvent.setMmEvent(startUpdate);
			ipcFacade.invokeService(presentEvent, getClass().getCanonicalName());
		}
	}

	@Override
	public void handleToken(TokenEvent token, Map<String, Object> boundVariables) {
		// TODO Auto-generated method stub

	}
	
	
	
	public void handle(String json){
		
		try{
			de.semvox.commons.logging.Logger.log(LogLevel.INFO, "json", json);
			JSONObject jsonObject = JsonParser.parseJson(json);
			
			String sensor = (String)jsonObject.get("sensor");
			double value = (double)jsonObject.get("value");
			String typeUrgency = (String)jsonObject.get("type");
			long levelJsonObject = (long) jsonObject.get("level");
			int level = (int) levelJsonObject;
			long orderJsonObject = (long) jsonObject.get("ordercoefficient");
			int ordercoefficient = (int) orderJsonObject;
			
			
			if(sensor.equals("light")){
				int lightvalue = (int) value;
				sendEventLight(Integer.toString(lightvalue), typeUrgency, level, ordercoefficient);
			}else if(sensor.equals("humidity")){
				int humidityvalue = (int) value;
				sendEventHumidity(Integer.toString(humidityvalue), typeUrgency, level, ordercoefficient);
			}else if(sensor.equals("temperature")){
				int temperaturevalue = (int) value;
				sendEventTemperature(Double.toString(temperaturevalue), typeUrgency, level, ordercoefficient);
			}
		}
		catch(Exception e){
			de.semvox.commons.logging.Logger.log(LogLevel.INFO, "HANDLE HUMDITYEVENTRECEIVER", "Something went wrong while parsing the JSON string; " + e.toString());
			e.printStackTrace();
		}
	}
	
	
	public void sendEventTemperature(String strTemperature, String strUrgency, int level, int ordercoefficient){
		TemperatureUpdate temperatureUpdate = ThingFactoryVmn.newTemperatureUpdate();
				
		Temperature temperature = ThingFactoryVmn.newTemperature();
		temperature.setCommonStringValue(strTemperature);
		
		TemperatureReport report = ThingFactoryVmn.newTemperatureReport();
		report.setVmnTemperature(temperature);
	
		temperatureUpdate.setVmnTemperatureReport(report);
		temperatureUpdate.setVmnUrgency(Urgency.getUrgency(strUrgency.toUpperCase()));
		temperatureUpdate.setVmnTempExtreme(com.zorabots.vmn.staticclasses.TempExtreme.getTempExtremeFromServer(level));
		
		if (ordercoefficient == 1){
			temperatureUpdate.setVmnUpdatePosition(ThingFactoryVmn.newFirst());
		}
		else {
			temperatureUpdate.setVmnUpdatePosition(ThingFactoryVmn.newNotFirst());
		}

		ipcFacade.sendEvent((Event) temperatureUpdate, "zelve");
	}
	
	public void sendEventLight(String strLight, String strUrgency, int level, int ordercoefficient){
		LightUpdate lightUpdate = ThingFactoryVmn.newLightUpdate();
				
		Light light = ThingFactoryVmn.newLight();
		light.setCommonStringValue(strLight);
		
		LightReport report = ThingFactoryVmn.newLightReport();
		report.setVmnLight(light);
	
		lightUpdate.setVmnLightReport(report);
		lightUpdate.setVmnUrgency(Urgency.getUrgency(strUrgency.toUpperCase()));
		lightUpdate.setVmnExtreme(com.zorabots.vmn.staticclasses.Extreme.getExtremeFromServer(level));
		
		if (ordercoefficient == 1){
			lightUpdate.setVmnUpdatePosition(ThingFactoryVmn.newFirst());
		}
		else{
			lightUpdate.setVmnUpdatePosition(ThingFactoryVmn.newNotFirst());
		}

		ipcFacade.sendEvent((Event) lightUpdate, "zelve");
	}
	
	public void sendEventHumidity(String strHumidity, String strUrgency, int level, int ordercoefficient){
		HumidityUpdate humidityUpdate = ThingFactoryVmn.newHumidityUpdate();
				
		Humidity humidity = ThingFactoryVmn.newHumidity();
		humidity.setCommonStringValue(strHumidity);
		
		HumidityReport report = ThingFactoryVmn.newHumidityReport();
		report.setVmnHumidity(humidity);
	
		humidityUpdate.setVmnHumidityReport(report);
		humidityUpdate.setVmnUrgency(Urgency.getHumidityUrgency(strUrgency.toUpperCase()));
		humidityUpdate.setVmnExtreme(com.zorabots.vmn.staticclasses.Extreme.getExtremeFromServer(level));
		
		if (ordercoefficient == 1){
			humidityUpdate.setVmnUpdatePosition(ThingFactoryVmn.newFirst());
		}
		else{
			humidityUpdate.setVmnUpdatePosition(ThingFactoryVmn.newNotFirst());
		}

		ipcFacade.sendEvent((Event) humidityUpdate, "zelve");
	}
	
	public void sendEventStart(){
		StartUpdate startUpdate = ThingFactoryVmn.newStartUpdate();
		IpAddress vmnipaddress = ThingFactoryVmn.newIpAddress();
		
		try {
			InetAddress ipaddress = InetAddress.getLocalHost();
			String ipstring = "";
			String ipstringdotted = "";
			//Boolean foundip = false;
			
			
			for (Enumeration en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = (NetworkInterface) en.nextElement();
                for (Enumeration enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = (InetAddress) enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()&&inetAddress instanceof Inet4Address) {
                        ipstring=inetAddress.getHostAddress().toString();
                        ipstringdotted = ipstring.replace(".", ", punt, ");
                        vmnipaddress.setCommonStringValue(ipstringdotted);
            			startUpdate.setVmnIpAddress(vmnipaddress);
            			//foundip = true;
                        //break;
                    }
                }
                /*
                if (foundip){
                	break;
                }
                */
            }	
		} 
		catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			System.out.println("Unknown Host Exception was thrown while trying to retrieve local IP address in event; ");
			e.printStackTrace();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			System.out.println("Unknown Host Exception was thrown while trying to retrieve local IP address in event; ");
			e.printStackTrace();
		}
		
		ipcFacade.sendEvent((Event) startUpdate, "zelve");
	}

}
