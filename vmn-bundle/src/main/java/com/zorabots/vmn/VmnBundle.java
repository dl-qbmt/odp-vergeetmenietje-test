package com.zorabots.vmn;

import java.util.ArrayList;
import java.util.List;

import com.zorabots.vmn.components.HotwordEventReceiver;
import com.zorabots.vmn.components.HumidityEventReceiver;
import com.zorabots.vmn.components.PlantEventReceiver;
import com.zorabots.vmn.components.PotEventReceiver;
import com.zorabots.vmn.components.RecognitionEventReceiver;
import com.zorabots.vmn.components.TtsEventReceiver;
import com.zorabots.vmn.components.VmnDateService;
import com.zorabots.vmn.components.VmnHumidityService;
import com.zorabots.vmn.components.VmnIpService;
import com.zorabots.vmn.components.VmnLightService;
import com.zorabots.vmn.components.VmnPlantService;
import com.zorabots.vmn.components.VmnService;
import com.zorabots.vmn.components.WifiEventReceiver;
import com.zorabots.vmn.mqtt.requests.GetRequests;
import com.zorabots.vmn.nlg.gen.VmnTemplates;
import com.zorabots.vmn.nlu.gen.HotwordGrammer;
import com.zorabots.vmn.nlu.gen.VmnGrammar;
import com.zorabots.vmn.routing.MQTTClient;
import com.zorabots.vmn.routing.MQTTRouter;
import com.zorabots.vmn.taskmodel.gen.VmnTaskmodel;

import de.semvox.commons.logging.Logger;
import de.semvox.commons.logging.Logger.LogLevel;
import de.semvox.odp.multimodal.LanguageProvider;
import de.semvox.odp.s3.Platform;
import de.semvox.odp.s3.bundle.AbstractDialogBundle;
import de.semvox.types.odp.s3.AutomaticAcceptanceTestMode;
import de.semvox.types.odp.spijk.StandaloneGrammar;
import de.semvox.types.odp.spit.TemplateCollection;
import de.semvox.types.odp.squint.TaskModel;

public class VmnBundle extends AbstractDialogBundle {

	@Override
	protected void onLoad(Platform platform) throws Exception {
	    // initialize bundle resources
        
        // initialize grammar
        List<StandaloneGrammar> grammars = getGrammars(platform);
        
        // initialize template collection
        List<TemplateCollection> templates = getTemplates(platform);
        
        // initialize taskmodel
        TaskModel taskModel = new VmnTaskmodel().loadThing();
        
        loadBundleResources(grammars, taskModel, templates, platform);
	 
        //register services
        platform.registerComponent(new VmnService(platform.getIpcFacade()));
        platform.registerComponent(new VmnHumidityService(platform.getIpcFacade()));
        platform.registerComponent(new VmnLightService(platform.getIpcFacade()));
        platform.registerComponent(new VmnDateService());
        platform.registerComponent(new VmnPlantService());
        platform.registerComponent(new VmnIpService());
        
        //init eventreceivers
        TtsEventReceiver ttsEventReceiver = new TtsEventReceiver(platform.getIpcFacade());
        HumidityEventReceiver humidityEventReceiver = new HumidityEventReceiver(platform.getIpcFacade());
        PlantEventReceiver plantEventReceiver = new PlantEventReceiver();
        HotwordEventReceiver hotwordEventReceiver = new HotwordEventReceiver();
        RecognitionEventReceiver recognitionEventReceiver = new RecognitionEventReceiver();
        PotEventReceiver potEventReceiver = new PotEventReceiver(platform.getIpcFacade());
        WifiEventReceiver wifiEventReceiver = new WifiEventReceiver(platform.getIpcFacade());
        
        //register eventreceivers
        platform.registerComponent(ttsEventReceiver);
        platform.registerComponent(humidityEventReceiver);
        platform.registerComponent(plantEventReceiver);
        platform.registerComponent(hotwordEventReceiver);
        platform.registerComponent(recognitionEventReceiver);
        platform.registerComponent(potEventReceiver);
        platform.registerComponent(wifiEventReceiver);
        
        
        MQTTClient.getInstance().runClient();
        
        MQTTRouter mqttRouter = MQTTRouter.getInstance();
        
        
        Thread thread = new Thread(){
        	public void run(){
        		try {
					Thread.sleep(10000); 
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		//this is a start up event of ODP in general!
        		humidityEventReceiver.sendEventStart();
        		mqttRouter.route("vmnodp/push/tts/say", "messageHandle", ttsEventReceiver);
        		mqttRouter.route("vmnodp/push/tts/config", "setConfigHandle", ttsEventReceiver);
        		mqttRouter.route("vmnodp/get/tts/config", "getConfigHandle", ttsEventReceiver);
        		mqttRouter.route("vmnodp/get/tts/language", "getLanguageHandle", ttsEventReceiver);
        		mqttRouter.route("vmnengine/event/odp/alert", "handle", humidityEventReceiver);
        		mqttRouter.route("vmnengine/event/odp/warning", "handle", humidityEventReceiver);
        		mqttRouter.route("vmnengine/event/wifi/connecting", "handle", wifiEventReceiver);
        		mqttRouter.route("vmnengine/event/wifi/connect", "handle", wifiEventReceiver);
        		mqttRouter.route("vmnengine/event/wifi/disconnect", "handle", wifiEventReceiver);
        		mqttRouter.route("+/event/plant/change", "handle", plantEventReceiver);
        		mqttRouter.route("vmnengine/event/pot/change", "handle", potEventReceiver);
        		mqttRouter.route("vmnengine/event/pot/removed", "handle", potEventReceiver);
        		GetRequests requests = new GetRequests("vmnengine/get/plantparameters", "vmnengine/response/odp/plantparameters", "{\"key\": \"odp\"}", com.zorabots.vmn.pojo.Plant.getInstance(), "setPlant");
                requests.start();
        	}
        };
        thread.start();
        //GetRequests requests = new GetRequests("vmndb/get/vase/detail", "vmndb/response/johndoe/vase/detail", "{\"user\":{\"username\":\"johndoe\",\"token\":\"bullshit\"},\"vase\":1}", com.zorabots.vmn.pojo.Plant.getInstance(),"setPlant");
        //requests.start();
        
        Logger.log(LogLevel.INFO, "VMN Bundle", "VMN Bundle loaded!!!!");
	}
	
	
	protected List<StandaloneGrammar> getGrammars(Platform platform) {
	 	LanguageProvider langProvider = platform.getLanguageProvider();
	 
        List<StandaloneGrammar> grammars = new ArrayList<StandaloneGrammar>();
        grammars.add(new VmnGrammar(langProvider).loadThing());
        
        if (!platform.getRuntimeEnvironment().isMode(AutomaticAcceptanceTestMode.class)) {
        	  grammars.add(new HotwordGrammer(langProvider).loadThing());
        	}

        return grammars;
    }

    protected List<TemplateCollection> getTemplates(Platform platform) {
	    LanguageProvider langProvider = platform.getLanguageProvider();
    
        List<TemplateCollection> templates = new ArrayList<TemplateCollection>();
        templates.add(new VmnTemplates(langProvider).loadThing());

        return templates;
    }
	
	@Override
    public void changeLanguage(String language, Platform platform) throws Exception {
        reloadGrammars(getGrammars(platform), platform);
        reloadTemplates(getTemplates(platform), platform);
    }

	@Override
	protected void onUnload(Platform arg0) {
		// TODO implement onUnLoad

	}
	
	
	
	
}