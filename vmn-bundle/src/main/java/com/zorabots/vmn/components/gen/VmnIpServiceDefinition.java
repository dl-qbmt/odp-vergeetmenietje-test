/**
* GENERATED CODE
*/
package com.zorabots.vmn.components.gen;

public final class VmnIpServiceDefinition extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.subcon.integration.ServiceDefinition> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "int#ServiceDefinition";

	public VmnIpServiceDefinition(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public VmnIpServiceDefinition(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public VmnIpServiceDefinition(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public VmnIpServiceDefinition() {
		super();
	}

	@Override
	protected de.semvox.types.subcon.integration.ServiceDefinition createThing() {
		return getServiceDefinition();
	}

	@Override
	protected void initialize(final de.semvox.types.subcon.integration.ServiceDefinition serviceDefinition0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("VmnIpService");
		serviceDefinition0.setCommonName(string0);
		String string1 = replaceEntities("com.zorabots.vmn.components");
		serviceDefinition0.setIntNamespace(string1);
		com.zorabots.ontologies.vmn.GetIpAddress getIpAddress1 = getGetIpAddress1();
		serviceDefinition0.getIntSubscriptionsAsIntServiceRequest().put("GET_IP_ADDRESS", getIpAddress1);
	}

	de.semvox.types.subcon.integration.ServiceDefinition getServiceDefinition() {
		final de.semvox.types.subcon.integration.ServiceDefinition serviceDefinition0 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newServiceDefinition();
		return serviceDefinition0;
	}


	com.zorabots.ontologies.vmn.GetIpAddress getGetIpAddress1() {
		final com.zorabots.ontologies.vmn.GetIpAddress getIpAddress1 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetIpAddress();
		return getIpAddress1;
	}
}