/**
* GENERATED CODE
*/
package com.zorabots.vmn.components.gen;

public final class VmnHumidityServiceDefinition extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.subcon.integration.ServiceDefinition> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "int#ServiceDefinition";

	public VmnHumidityServiceDefinition(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public VmnHumidityServiceDefinition(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public VmnHumidityServiceDefinition(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public VmnHumidityServiceDefinition() {
		super();
	}

	@Override
	protected de.semvox.types.subcon.integration.ServiceDefinition createThing() {
		return getServiceDefinition();
	}

	@Override
	protected void initialize(final de.semvox.types.subcon.integration.ServiceDefinition serviceDefinition0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("VmnHumidityService");
		serviceDefinition0.setCommonName(string0);
		String string1 = replaceEntities("com.zorabots.vmn.components");
		serviceDefinition0.setIntNamespace(string1);
		com.zorabots.ontologies.vmn.GetHumidity getHumidity1 = getGetHumidity1();
		serviceDefinition0.getIntSubscriptionsAsIntServiceRequest().put("GET_HUMIDITY", getHumidity1);
	}

	de.semvox.types.subcon.integration.ServiceDefinition getServiceDefinition() {
		final de.semvox.types.subcon.integration.ServiceDefinition serviceDefinition0 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newServiceDefinition();
		return serviceDefinition0;
	}


	com.zorabots.ontologies.vmn.GetHumidity getGetHumidity1() {
		final com.zorabots.ontologies.vmn.GetHumidity getHumidity1 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newGetHumidity();
		return getHumidity1;
	}
}