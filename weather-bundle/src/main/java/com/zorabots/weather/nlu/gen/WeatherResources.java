/**
* GENERATED CODE
*/
package com.zorabots.weather.nlu.gen;

public final class WeatherResources extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.spijk.Grammar> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "spijk#Grammar";

	public WeatherResources(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public WeatherResources(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public WeatherResources(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public WeatherResources() {
		super();
	}

	@Override
	protected de.semvox.types.odp.spijk.Grammar createThing() {
		return getGrammar();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.spijk.Grammar grammar0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("WeatherResources");
		grammar0.setCommonName(string0);
		grammar0.assignSpijkImportsPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		de.semvox.types.odp.spijk.Slot slot1 = getSlot1();
		grammar0.getSpijkElements().add(slot1);
	}

	de.semvox.types.odp.spijk.Grammar getGrammar() {
		final de.semvox.types.odp.spijk.Grammar grammar0 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newGrammar();
		return grammar0;
	}


	de.semvox.types.odp.spijk.Slot getSlot1() {
		final de.semvox.types.odp.spijk.Slot slot1 = de.semvox.types.odp.spijk.factory.PatternFactorySpijk.newSlot();
		String string1 = replaceEntities("RELATIVEMOMENT");
		slot1.setCommonName(string1);
		com.zorabots.ontologies.weather.RelativeMoment relativeMoment2 = getRelativeMoment2();
		slot1.setMmInterpretation(relativeMoment2);
		return slot1;
	}

	com.zorabots.ontologies.weather.RelativeMoment getRelativeMoment2() {
		final com.zorabots.ontologies.weather.RelativeMoment relativeMoment2 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newRelativeMoment();
		de.semvox.types.common.Name name3 = getName3();
		relativeMoment2.setCommonNamedBy(name3);
		return relativeMoment2;
	}

	de.semvox.types.common.Name getName3() {
		final de.semvox.types.common.Name name3 = de.semvox.types.common.factory.PatternFactoryCommon.newName();
		name3.markCommonNameAsVariable("RELATIVEMOMENT", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		name3.assignCommonNamePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return name3;
	}
}