/**
* GENERATED CODE
*/
package com.zorabots.vmn.nlg.gen;

public final class VmnUnitTemplates extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.spit.TemplateCollection> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "spit#TemplateCollection";

	public VmnUnitTemplates(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public VmnUnitTemplates(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public VmnUnitTemplates(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public VmnUnitTemplates() {
		super();
	}

	@Override
	protected de.semvox.types.odp.spit.TemplateCollection createThing() {
		return getTemplateCollection();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.spit.TemplateCollection templateCollection0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("VmnUnitTemplates");
		templateCollection0.setCommonName(string0);
		de.semvox.types.odp.spit.Template template1 = getTemplate1();
		templateCollection0.getSpitElements().add(template1);
		de.semvox.types.odp.spit.Template template11 = getTemplate11();
		templateCollection0.getSpitElements().add(template11);
		de.semvox.types.odp.spit.Template template21 = getTemplate21();
		templateCollection0.getSpitElements().add(template21);
	}

	de.semvox.types.odp.spit.TemplateCollection getTemplateCollection() {
		final de.semvox.types.odp.spit.TemplateCollection templateCollection0 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplateCollection();
		return templateCollection0;
	}


	de.semvox.types.odp.spit.Template getTemplate1() {
		final de.semvox.types.odp.spit.Template template1 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string1 = replaceEntities("Temperature_Degrees");
		template1.setCommonName(string1);
		de.semvox.types.odp.spit.Switch switch2 = getSwitch2();
		template1.setSpitAction(switch2);
		de.semvox.types.odp.interaction.GenerationContext generationContext9 = getGenerationContext9();
		template1.setSpitOnContext(generationContext9);
		com.zorabots.ontologies.vmn.Degrees degrees10 = getDegrees10();
		template1.setSpitOnInput(degrees10);
		return template1;
	}

	de.semvox.types.odp.spit.Switch getSwitch2() {
		final de.semvox.types.odp.spit.Switch switch2 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case3 = getCase3();
			switch2.getSpitCases().add(case3);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case6 = getCase6();
			switch2.getSpitCases().add(case6);
		}
		return switch2;
	}

	de.semvox.types.odp.spit.Case getCase3() {
		final de.semvox.types.odp.spit.Case case3 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition4 = getStringCondition4();
		case3.getSpitConditions().add(stringCondition4);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs5 = getGenerateStringAndOutputs5();
		case3.setSpitAction(generateStringAndOutputs5);
		return case3;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition4() {
		final de.semvox.types.odp.spit.StringCondition stringCondition4 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string2 = replaceEntities("lang");
		stringCondition4.setAlgVariableReference(string2);
		String string3 = replaceEntities("en-US");
		stringCondition4.setCommonStringValue(string3);
		return stringCondition4;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs5() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs5 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs5.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string4 = replaceEntities("degrees");
		generateStringAndOutputs5.getSpitAlternatives().add(string4);
		return generateStringAndOutputs5;
	}

	de.semvox.types.odp.spit.Case getCase6() {
		final de.semvox.types.odp.spit.Case case6 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition7 = getStringCondition7();
		case6.getSpitConditions().add(stringCondition7);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs8 = getGenerateStringAndOutputs8();
		case6.setSpitAction(generateStringAndOutputs8);
		return case6;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition7() {
		final de.semvox.types.odp.spit.StringCondition stringCondition7 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string5 = replaceEntities("lang");
		stringCondition7.setAlgVariableReference(string5);
		String string6 = replaceEntities("nl-BE");
		stringCondition7.setCommonStringValue(string6);
		return stringCondition7;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs8() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs8 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs8.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string7 = replaceEntities("graden");
		generateStringAndOutputs8.getSpitAlternatives().add(string7);
		return generateStringAndOutputs8;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext9() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext9 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext9.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext9.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext9.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext9.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext9.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext9.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext9;
	}

	com.zorabots.ontologies.vmn.Degrees getDegrees10() {
		final com.zorabots.ontologies.vmn.Degrees degrees10 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newDegrees();
		return degrees10;
	}

	de.semvox.types.odp.spit.Template getTemplate11() {
		final de.semvox.types.odp.spit.Template template11 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string8 = replaceEntities("Light_Lux");
		template11.setCommonName(string8);
		de.semvox.types.odp.spit.Switch switch12 = getSwitch12();
		template11.setSpitAction(switch12);
		de.semvox.types.odp.interaction.GenerationContext generationContext19 = getGenerationContext19();
		template11.setSpitOnContext(generationContext19);
		com.zorabots.ontologies.vmn.Lux lux20 = getLux20();
		template11.setSpitOnInput(lux20);
		return template11;
	}

	de.semvox.types.odp.spit.Switch getSwitch12() {
		final de.semvox.types.odp.spit.Switch switch12 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case13 = getCase13();
			switch12.getSpitCases().add(case13);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case16 = getCase16();
			switch12.getSpitCases().add(case16);
		}
		return switch12;
	}

	de.semvox.types.odp.spit.Case getCase13() {
		final de.semvox.types.odp.spit.Case case13 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition14 = getStringCondition14();
		case13.getSpitConditions().add(stringCondition14);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs15 = getGenerateStringAndOutputs15();
		case13.setSpitAction(generateStringAndOutputs15);
		return case13;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition14() {
		final de.semvox.types.odp.spit.StringCondition stringCondition14 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string9 = replaceEntities("lang");
		stringCondition14.setAlgVariableReference(string9);
		String string10 = replaceEntities("en-US");
		stringCondition14.setCommonStringValue(string10);
		return stringCondition14;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs15() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs15 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs15.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string11 = replaceEntities("lux");
		generateStringAndOutputs15.getSpitAlternatives().add(string11);
		return generateStringAndOutputs15;
	}

	de.semvox.types.odp.spit.Case getCase16() {
		final de.semvox.types.odp.spit.Case case16 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition17 = getStringCondition17();
		case16.getSpitConditions().add(stringCondition17);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs18 = getGenerateStringAndOutputs18();
		case16.setSpitAction(generateStringAndOutputs18);
		return case16;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition17() {
		final de.semvox.types.odp.spit.StringCondition stringCondition17 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string12 = replaceEntities("lang");
		stringCondition17.setAlgVariableReference(string12);
		String string13 = replaceEntities("nl-BE");
		stringCondition17.setCommonStringValue(string13);
		return stringCondition17;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs18() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs18 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs18.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string14 = replaceEntities("lux");
		generateStringAndOutputs18.getSpitAlternatives().add(string14);
		return generateStringAndOutputs18;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext19() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext19 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext19.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext19.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext19.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext19.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext19.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext19.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext19;
	}

	com.zorabots.ontologies.vmn.Lux getLux20() {
		final com.zorabots.ontologies.vmn.Lux lux20 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newLux();
		return lux20;
	}

	de.semvox.types.odp.spit.Template getTemplate21() {
		final de.semvox.types.odp.spit.Template template21 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string15 = replaceEntities("Humidity_Percent");
		template21.setCommonName(string15);
		de.semvox.types.odp.spit.Switch switch22 = getSwitch22();
		template21.setSpitAction(switch22);
		de.semvox.types.odp.interaction.GenerationContext generationContext29 = getGenerationContext29();
		template21.setSpitOnContext(generationContext29);
		com.zorabots.ontologies.vmn.Percent percent30 = getPercent30();
		template21.setSpitOnInput(percent30);
		return template21;
	}

	de.semvox.types.odp.spit.Switch getSwitch22() {
		final de.semvox.types.odp.spit.Switch switch22 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case23 = getCase23();
			switch22.getSpitCases().add(case23);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case26 = getCase26();
			switch22.getSpitCases().add(case26);
		}
		return switch22;
	}

	de.semvox.types.odp.spit.Case getCase23() {
		final de.semvox.types.odp.spit.Case case23 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition24 = getStringCondition24();
		case23.getSpitConditions().add(stringCondition24);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs25 = getGenerateStringAndOutputs25();
		case23.setSpitAction(generateStringAndOutputs25);
		return case23;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition24() {
		final de.semvox.types.odp.spit.StringCondition stringCondition24 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string16 = replaceEntities("lang");
		stringCondition24.setAlgVariableReference(string16);
		String string17 = replaceEntities("en-US");
		stringCondition24.setCommonStringValue(string17);
		return stringCondition24;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs25() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs25 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs25.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string18 = replaceEntities("percent");
		generateStringAndOutputs25.getSpitAlternatives().add(string18);
		return generateStringAndOutputs25;
	}

	de.semvox.types.odp.spit.Case getCase26() {
		final de.semvox.types.odp.spit.Case case26 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition27 = getStringCondition27();
		case26.getSpitConditions().add(stringCondition27);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs28 = getGenerateStringAndOutputs28();
		case26.setSpitAction(generateStringAndOutputs28);
		return case26;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition27() {
		final de.semvox.types.odp.spit.StringCondition stringCondition27 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string19 = replaceEntities("lang");
		stringCondition27.setAlgVariableReference(string19);
		String string20 = replaceEntities("nl-BE");
		stringCondition27.setCommonStringValue(string20);
		return stringCondition27;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs28() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs28 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs28.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string21 = replaceEntities("procent");
		generateStringAndOutputs28.getSpitAlternatives().add(string21);
		return generateStringAndOutputs28;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext29() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext29 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext29.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext29.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext29.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext29.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext29.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext29.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext29;
	}

	com.zorabots.ontologies.vmn.Percent getPercent30() {
		final com.zorabots.ontologies.vmn.Percent percent30 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newPercent();
		return percent30;
	}
}