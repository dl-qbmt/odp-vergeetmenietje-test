/**
* GENERATED CODE
*/
package com.zorabots.weather.nlg.gen;

public final class WeatherTemplates extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.spit.TemplateCollection> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "spit#TemplateCollection";

	public WeatherTemplates(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public WeatherTemplates(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public WeatherTemplates(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public WeatherTemplates() {
		super();
	}

	@Override
	protected de.semvox.types.odp.spit.TemplateCollection createThing() {
		return getTemplateCollection();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.spit.TemplateCollection templateCollection0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("WeatherTemplates");
		templateCollection0.setCommonName(string0);
		de.semvox.types.odp.spit.Template template1 = getTemplate1();
		templateCollection0.getSpitElements().add(template1);
		de.semvox.types.odp.spit.Template template13 = getTemplate13();
		templateCollection0.getSpitElements().add(template13);
		de.semvox.types.odp.spit.Template template25 = getTemplate25();
		templateCollection0.getSpitElements().add(template25);
		de.semvox.types.odp.spit.Template template39 = getTemplate39();
		templateCollection0.getSpitElements().add(template39);
		de.semvox.types.odp.spit.Template template53 = getTemplate53();
		templateCollection0.getSpitElements().add(template53);
		de.semvox.types.odp.spit.Template template67 = getTemplate67();
		templateCollection0.getSpitElements().add(template67);
	}

	de.semvox.types.odp.spit.TemplateCollection getTemplateCollection() {
		final de.semvox.types.odp.spit.TemplateCollection templateCollection0 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplateCollection();
		return templateCollection0;
	}


	de.semvox.types.odp.spit.Template getTemplate1() {
		final de.semvox.types.odp.spit.Template template1 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string1 = replaceEntities("ReportOnTask_StatusAccepted_GetWeatherForecast");
		template1.setCommonName(string1);
		de.semvox.types.odp.spit.Switch switch2 = getSwitch2();
		template1.setSpitAction(switch2);
		de.semvox.types.odp.interaction.GenerationContext generationContext9 = getGenerationContext9();
		template1.setSpitOnContext(generationContext9);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask10 = getReportOnTask10();
		template1.setSpitOnInput(reportOnTask10);
		return template1;
	}

	de.semvox.types.odp.spit.Switch getSwitch2() {
		final de.semvox.types.odp.spit.Switch switch2 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case3 = getCase3();
			switch2.getSpitCases().add(case3);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case6 = getCase6();
			switch2.getSpitCases().add(case6);
		}
		return switch2;
	}

	de.semvox.types.odp.spit.Case getCase3() {
		final de.semvox.types.odp.spit.Case case3 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition4 = getStringCondition4();
		case3.getSpitConditions().add(stringCondition4);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs5 = getGenerateStringAndOutputs5();
		case3.setSpitAction(generateStringAndOutputs5);
		return case3;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition4() {
		final de.semvox.types.odp.spit.StringCondition stringCondition4 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string2 = replaceEntities("lang");
		stringCondition4.setAlgVariableReference(string2);
		String string3 = replaceEntities("en-US");
		stringCondition4.setCommonStringValue(string3);
		return stringCondition4;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs5() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs5 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs5.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string4 = replaceEntities("ok, i'm looking up the weather forecast for you. ");
		generateStringAndOutputs5.getSpitAlternatives().add(string4);
		return generateStringAndOutputs5;
	}

	de.semvox.types.odp.spit.Case getCase6() {
		final de.semvox.types.odp.spit.Case case6 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition7 = getStringCondition7();
		case6.getSpitConditions().add(stringCondition7);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs8 = getGenerateStringAndOutputs8();
		case6.setSpitAction(generateStringAndOutputs8);
		return case6;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition7() {
		final de.semvox.types.odp.spit.StringCondition stringCondition7 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string5 = replaceEntities("lang");
		stringCondition7.setAlgVariableReference(string5);
		String string6 = replaceEntities("nl-BE");
		stringCondition7.setCommonStringValue(string6);
		return stringCondition7;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs8() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs8 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs8.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string7 = replaceEntities("Ok, ik zoek het weerbericht. ");
		generateStringAndOutputs8.getSpitAlternatives().add(string7);
		return generateStringAndOutputs8;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext9() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext9 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext9.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext9.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext9.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext9.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext9.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext9.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext9;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask10() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask10 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Accepted accepted11 = getAccepted11();
		reportOnTask10.setMmTaskStatus(accepted11);
		com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast12 = getGetWeatherForecast12();
		reportOnTask10.setCommonContent(getWeatherForecast12);
		return reportOnTask10;
	}

	de.semvox.types.odp.multimodal.Accepted getAccepted11() {
		final de.semvox.types.odp.multimodal.Accepted accepted11 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newAccepted();
		return accepted11;
	}

	com.zorabots.ontologies.weather.GetWeatherForecast getGetWeatherForecast12() {
		final com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast12 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newGetWeatherForecast();
		getWeatherForecast12.markMmTaskIdAsVariable("taskId", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		getWeatherForecast12.assignMmTaskIdPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return getWeatherForecast12;
	}

	de.semvox.types.odp.spit.Template getTemplate13() {
		final de.semvox.types.odp.spit.Template template13 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string8 = replaceEntities("ReportOnTask_StatusAccepted_GetWeather");
		template13.setCommonName(string8);
		de.semvox.types.odp.spit.Switch switch14 = getSwitch14();
		template13.setSpitAction(switch14);
		de.semvox.types.odp.interaction.GenerationContext generationContext21 = getGenerationContext21();
		template13.setSpitOnContext(generationContext21);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask22 = getReportOnTask22();
		template13.setSpitOnInput(reportOnTask22);
		return template13;
	}

	de.semvox.types.odp.spit.Switch getSwitch14() {
		final de.semvox.types.odp.spit.Switch switch14 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case15 = getCase15();
			switch14.getSpitCases().add(case15);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case18 = getCase18();
			switch14.getSpitCases().add(case18);
		}
		return switch14;
	}

	de.semvox.types.odp.spit.Case getCase15() {
		final de.semvox.types.odp.spit.Case case15 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition16 = getStringCondition16();
		case15.getSpitConditions().add(stringCondition16);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs17 = getGenerateStringAndOutputs17();
		case15.setSpitAction(generateStringAndOutputs17);
		return case15;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition16() {
		final de.semvox.types.odp.spit.StringCondition stringCondition16 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string9 = replaceEntities("lang");
		stringCondition16.setAlgVariableReference(string9);
		String string10 = replaceEntities("en-US");
		stringCondition16.setCommonStringValue(string10);
		return stringCondition16;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs17() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs17 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs17.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string11 = replaceEntities("ok, i'm looking up the weather for you. ");
		generateStringAndOutputs17.getSpitAlternatives().add(string11);
		return generateStringAndOutputs17;
	}

	de.semvox.types.odp.spit.Case getCase18() {
		final de.semvox.types.odp.spit.Case case18 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition19 = getStringCondition19();
		case18.getSpitConditions().add(stringCondition19);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs20 = getGenerateStringAndOutputs20();
		case18.setSpitAction(generateStringAndOutputs20);
		return case18;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition19() {
		final de.semvox.types.odp.spit.StringCondition stringCondition19 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string12 = replaceEntities("lang");
		stringCondition19.setAlgVariableReference(string12);
		String string13 = replaceEntities("nl-BE");
		stringCondition19.setCommonStringValue(string13);
		return stringCondition19;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs20() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs20 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs20.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string14 = replaceEntities("Ok, ik zoek het weerbericht. ");
		generateStringAndOutputs20.getSpitAlternatives().add(string14);
		return generateStringAndOutputs20;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext21() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext21 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext21.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext21.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext21.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext21.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext21.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext21.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext21;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask22() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask22 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Accepted accepted23 = getAccepted23();
		reportOnTask22.setMmTaskStatus(accepted23);
		com.zorabots.ontologies.weather.GetWeather getWeather24 = getGetWeather24();
		reportOnTask22.setCommonContent(getWeather24);
		return reportOnTask22;
	}

	de.semvox.types.odp.multimodal.Accepted getAccepted23() {
		final de.semvox.types.odp.multimodal.Accepted accepted23 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newAccepted();
		return accepted23;
	}

	com.zorabots.ontologies.weather.GetWeather getGetWeather24() {
		final com.zorabots.ontologies.weather.GetWeather getWeather24 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newGetWeather();
		getWeather24.markMmTaskIdAsVariable("taskId", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		getWeather24.assignMmTaskIdPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return getWeather24;
	}

	de.semvox.types.odp.spit.Template getTemplate25() {
		final de.semvox.types.odp.spit.Template template25 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string15 = replaceEntities("ReportOnTask_getWeather");
		template25.setCommonName(string15);
		de.semvox.types.odp.spit.Switch switch26 = getSwitch26();
		template25.setSpitAction(switch26);
		de.semvox.types.odp.interaction.GenerationContext generationContext33 = getGenerationContext33();
		template25.setSpitOnContext(generationContext33);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask34 = getReportOnTask34();
		template25.setSpitOnInput(reportOnTask34);
		return template25;
	}

	de.semvox.types.odp.spit.Switch getSwitch26() {
		final de.semvox.types.odp.spit.Switch switch26 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case27 = getCase27();
			switch26.getSpitCases().add(case27);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case30 = getCase30();
			switch26.getSpitCases().add(case30);
		}
		return switch26;
	}

	de.semvox.types.odp.spit.Case getCase27() {
		final de.semvox.types.odp.spit.Case case27 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition28 = getStringCondition28();
		case27.getSpitConditions().add(stringCondition28);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs29 = getGenerateStringAndOutputs29();
		case27.setSpitAction(generateStringAndOutputs29);
		return case27;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition28() {
		final de.semvox.types.odp.spit.StringCondition stringCondition28 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string16 = replaceEntities("lang");
		stringCondition28.setAlgVariableReference(string16);
		String string17 = replaceEntities("en-US");
		stringCondition28.setCommonStringValue(string17);
		return stringCondition28;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs29() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs29 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs29.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string18 = replaceEntities("Today it is ?($MESS)");
		generateStringAndOutputs29.getSpitAlternatives().add(string18);
		return generateStringAndOutputs29;
	}

	de.semvox.types.odp.spit.Case getCase30() {
		final de.semvox.types.odp.spit.Case case30 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition31 = getStringCondition31();
		case30.getSpitConditions().add(stringCondition31);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs32 = getGenerateStringAndOutputs32();
		case30.setSpitAction(generateStringAndOutputs32);
		return case30;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition31() {
		final de.semvox.types.odp.spit.StringCondition stringCondition31 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string19 = replaceEntities("lang");
		stringCondition31.setAlgVariableReference(string19);
		String string20 = replaceEntities("nl-BE");
		stringCondition31.setCommonStringValue(string20);
		return stringCondition31;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs32() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs32 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs32.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string21 = replaceEntities("Vandaag is het ?($MESS)");
		generateStringAndOutputs32.getSpitAlternatives().add(string21);
		return generateStringAndOutputs32;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext33() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext33 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext33.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext33.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext33.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext33.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext33.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext33.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext33;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask34() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask34 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released35 = getReleased35();
		reportOnTask34.setMmTaskStatus(released35);
		return reportOnTask34;
	}

	de.semvox.types.odp.multimodal.Released getReleased35() {
		final de.semvox.types.odp.multimodal.Released released35 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.weather.GetWeather getWeather36 = getGetWeather36();
		released35.setMmTaskExecutionResult(getWeather36);
		return released35;
	}

	com.zorabots.ontologies.weather.GetWeather getGetWeather36() {
		final com.zorabots.ontologies.weather.GetWeather getWeather36 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newGetWeather();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess37 = getStatusSuccess37();
		getWeather36.setIntResponseStatus(statusSuccess37);
		com.zorabots.ontologies.weather.OnTime onTime38 = getOnTime38();
		getWeather36.setWeatherTimeOut(onTime38);
		getWeather36.markCommonStringValueAsVariable("MESS", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		getWeather36.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return getWeather36;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess37() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess37 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess37;
	}

	com.zorabots.ontologies.weather.OnTime getOnTime38() {
		final com.zorabots.ontologies.weather.OnTime onTime38 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newOnTime();
		return onTime38;
	}

	de.semvox.types.odp.spit.Template getTemplate39() {
		final de.semvox.types.odp.spit.Template template39 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string22 = replaceEntities("ReportOnTask_getWeatherForecast");
		template39.setCommonName(string22);
		de.semvox.types.odp.spit.Switch switch40 = getSwitch40();
		template39.setSpitAction(switch40);
		de.semvox.types.odp.interaction.GenerationContext generationContext47 = getGenerationContext47();
		template39.setSpitOnContext(generationContext47);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask48 = getReportOnTask48();
		template39.setSpitOnInput(reportOnTask48);
		return template39;
	}

	de.semvox.types.odp.spit.Switch getSwitch40() {
		final de.semvox.types.odp.spit.Switch switch40 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case41 = getCase41();
			switch40.getSpitCases().add(case41);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case44 = getCase44();
			switch40.getSpitCases().add(case44);
		}
		return switch40;
	}

	de.semvox.types.odp.spit.Case getCase41() {
		final de.semvox.types.odp.spit.Case case41 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition42 = getStringCondition42();
		case41.getSpitConditions().add(stringCondition42);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs43 = getGenerateStringAndOutputs43();
		case41.setSpitAction(generateStringAndOutputs43);
		return case41;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition42() {
		final de.semvox.types.odp.spit.StringCondition stringCondition42 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string23 = replaceEntities("lang");
		stringCondition42.setAlgVariableReference(string23);
		String string24 = replaceEntities("en-US");
		stringCondition42.setCommonStringValue(string24);
		return stringCondition42;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs43() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs43 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs43.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string25 = replaceEntities("Tomorrow it will be ?($MESS)");
		generateStringAndOutputs43.getSpitAlternatives().add(string25);
		return generateStringAndOutputs43;
	}

	de.semvox.types.odp.spit.Case getCase44() {
		final de.semvox.types.odp.spit.Case case44 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition45 = getStringCondition45();
		case44.getSpitConditions().add(stringCondition45);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs46 = getGenerateStringAndOutputs46();
		case44.setSpitAction(generateStringAndOutputs46);
		return case44;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition45() {
		final de.semvox.types.odp.spit.StringCondition stringCondition45 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string26 = replaceEntities("lang");
		stringCondition45.setAlgVariableReference(string26);
		String string27 = replaceEntities("nl-BE");
		stringCondition45.setCommonStringValue(string27);
		return stringCondition45;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs46() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs46 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs46.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string28 = replaceEntities("Morgen is het ?($MESS)");
		generateStringAndOutputs46.getSpitAlternatives().add(string28);
		return generateStringAndOutputs46;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext47() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext47 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext47.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext47.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext47.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext47.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext47.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext47.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext47;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask48() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask48 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released49 = getReleased49();
		reportOnTask48.setMmTaskStatus(released49);
		return reportOnTask48;
	}

	de.semvox.types.odp.multimodal.Released getReleased49() {
		final de.semvox.types.odp.multimodal.Released released49 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast50 = getGetWeatherForecast50();
		released49.setMmTaskExecutionResult(getWeatherForecast50);
		return released49;
	}

	com.zorabots.ontologies.weather.GetWeatherForecast getGetWeatherForecast50() {
		final com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast50 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newGetWeatherForecast();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess51 = getStatusSuccess51();
		getWeatherForecast50.setIntResponseStatus(statusSuccess51);
		com.zorabots.ontologies.weather.OnTime onTime52 = getOnTime52();
		getWeatherForecast50.setWeatherTimeOut(onTime52);
		getWeatherForecast50.markCommonStringValueAsVariable("MESS", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		getWeatherForecast50.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return getWeatherForecast50;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess51() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess51 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess51;
	}

	com.zorabots.ontologies.weather.OnTime getOnTime52() {
		final com.zorabots.ontologies.weather.OnTime onTime52 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newOnTime();
		return onTime52;
	}

	de.semvox.types.odp.spit.Template getTemplate53() {
		final de.semvox.types.odp.spit.Template template53 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string29 = replaceEntities("ReportOnTask_getWeatherForecast");
		template53.setCommonName(string29);
		de.semvox.types.odp.spit.Switch switch54 = getSwitch54();
		template53.setSpitAction(switch54);
		de.semvox.types.odp.interaction.GenerationContext generationContext61 = getGenerationContext61();
		template53.setSpitOnContext(generationContext61);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask62 = getReportOnTask62();
		template53.setSpitOnInput(reportOnTask62);
		return template53;
	}

	de.semvox.types.odp.spit.Switch getSwitch54() {
		final de.semvox.types.odp.spit.Switch switch54 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case55 = getCase55();
			switch54.getSpitCases().add(case55);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case58 = getCase58();
			switch54.getSpitCases().add(case58);
		}
		return switch54;
	}

	de.semvox.types.odp.spit.Case getCase55() {
		final de.semvox.types.odp.spit.Case case55 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition56 = getStringCondition56();
		case55.getSpitConditions().add(stringCondition56);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs57 = getGenerateStringAndOutputs57();
		case55.setSpitAction(generateStringAndOutputs57);
		return case55;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition56() {
		final de.semvox.types.odp.spit.StringCondition stringCondition56 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string30 = replaceEntities("lang");
		stringCondition56.setAlgVariableReference(string30);
		String string31 = replaceEntities("en-US");
		stringCondition56.setCommonStringValue(string31);
		return stringCondition56;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs57() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs57 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs57.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string32 = replaceEntities("Sorry, I can not connect to the weather forecast server. ");
		generateStringAndOutputs57.getSpitAlternatives().add(string32);
		return generateStringAndOutputs57;
	}

	de.semvox.types.odp.spit.Case getCase58() {
		final de.semvox.types.odp.spit.Case case58 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition59 = getStringCondition59();
		case58.getSpitConditions().add(stringCondition59);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs60 = getGenerateStringAndOutputs60();
		case58.setSpitAction(generateStringAndOutputs60);
		return case58;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition59() {
		final de.semvox.types.odp.spit.StringCondition stringCondition59 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string33 = replaceEntities("lang");
		stringCondition59.setAlgVariableReference(string33);
		String string34 = replaceEntities("nl-BE");
		stringCondition59.setCommonStringValue(string34);
		return stringCondition59;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs60() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs60 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs60.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string35 = replaceEntities("Ik kan niet connecteren met de weer voorspelling server. ");
		generateStringAndOutputs60.getSpitAlternatives().add(string35);
		return generateStringAndOutputs60;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext61() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext61 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext61.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext61.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext61.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext61.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext61.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext61.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext61;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask62() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask62 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released63 = getReleased63();
		reportOnTask62.setMmTaskStatus(released63);
		return reportOnTask62;
	}

	de.semvox.types.odp.multimodal.Released getReleased63() {
		final de.semvox.types.odp.multimodal.Released released63 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast64 = getGetWeatherForecast64();
		released63.setMmTaskExecutionResult(getWeatherForecast64);
		return released63;
	}

	com.zorabots.ontologies.weather.GetWeatherForecast getGetWeatherForecast64() {
		final com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast64 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newGetWeatherForecast();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess65 = getStatusSuccess65();
		getWeatherForecast64.setIntResponseStatus(statusSuccess65);
		com.zorabots.ontologies.weather.Expired expired66 = getExpired66();
		getWeatherForecast64.setWeatherTimeOut(expired66);
		getWeatherForecast64.markCommonStringValueAsVariable("MESS", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		getWeatherForecast64.assignCommonStringValuePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return getWeatherForecast64;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess65() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess65 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess65;
	}

	com.zorabots.ontologies.weather.Expired getExpired66() {
		final com.zorabots.ontologies.weather.Expired expired66 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newExpired();
		return expired66;
	}

	de.semvox.types.odp.spit.Template getTemplate67() {
		final de.semvox.types.odp.spit.Template template67 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newTemplate();
		String string36 = replaceEntities("ReportOnTask_getWeather");
		template67.setCommonName(string36);
		de.semvox.types.odp.spit.Switch switch68 = getSwitch68();
		template67.setSpitAction(switch68);
		de.semvox.types.odp.interaction.GenerationContext generationContext75 = getGenerationContext75();
		template67.setSpitOnContext(generationContext75);
		de.semvox.types.odp.interaction.ReportOnTask reportOnTask76 = getReportOnTask76();
		template67.setSpitOnInput(reportOnTask76);
		return template67;
	}

	de.semvox.types.odp.spit.Switch getSwitch68() {
		final de.semvox.types.odp.spit.Switch switch68 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newSwitch();
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "en-US")) {
			de.semvox.types.odp.spit.Case case69 = getCase69();
			switch68.getSpitCases().add(case69);
		}
		if (isSatisfied(de.semvox.odp.multimodal.LanguageProvider.LANGUAGE_PROPERTY, "nl-BE")) {
			de.semvox.types.odp.spit.Case case72 = getCase72();
			switch68.getSpitCases().add(case72);
		}
		return switch68;
	}

	de.semvox.types.odp.spit.Case getCase69() {
		final de.semvox.types.odp.spit.Case case69 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition70 = getStringCondition70();
		case69.getSpitConditions().add(stringCondition70);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs71 = getGenerateStringAndOutputs71();
		case69.setSpitAction(generateStringAndOutputs71);
		return case69;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition70() {
		final de.semvox.types.odp.spit.StringCondition stringCondition70 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string37 = replaceEntities("lang");
		stringCondition70.setAlgVariableReference(string37);
		String string38 = replaceEntities("en-US");
		stringCondition70.setCommonStringValue(string38);
		return stringCondition70;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs71() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs71 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs71.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string39 = replaceEntities("Sorry, I can not connect to the weather forecast server. ");
		generateStringAndOutputs71.getSpitAlternatives().add(string39);
		return generateStringAndOutputs71;
	}

	de.semvox.types.odp.spit.Case getCase72() {
		final de.semvox.types.odp.spit.Case case72 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newCase();
		de.semvox.types.odp.spit.StringCondition stringCondition73 = getStringCondition73();
		case72.getSpitConditions().add(stringCondition73);
		de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs74 = getGenerateStringAndOutputs74();
		case72.setSpitAction(generateStringAndOutputs74);
		return case72;
	}

	de.semvox.types.odp.spit.StringCondition getStringCondition73() {
		final de.semvox.types.odp.spit.StringCondition stringCondition73 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newStringCondition();
		String string40 = replaceEntities("lang");
		stringCondition73.setAlgVariableReference(string40);
		String string41 = replaceEntities("nl-BE");
		stringCondition73.setCommonStringValue(string41);
		return stringCondition73;
	}

	de.semvox.types.odp.spit.GenerateStringAndOutputs getGenerateStringAndOutputs74() {
		final de.semvox.types.odp.spit.GenerateStringAndOutputs generateStringAndOutputs74 = de.semvox.types.odp.spit.factory.PatternFactorySpit.newGenerateStringAndOutputs();
		generateStringAndOutputs74.assignSpitOutputPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		String string42 = replaceEntities("Ik kan niet connecteren met de weer voorspelling server. ");
		generateStringAndOutputs74.getSpitAlternatives().add(string42);
		return generateStringAndOutputs74;
	}

	de.semvox.types.odp.interaction.GenerationContext getGenerationContext75() {
		final de.semvox.types.odp.interaction.GenerationContext generationContext75 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newGenerationContext();
		generationContext75.markCommonLanguageAsVariable("lang", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext75.assignCommonLanguagePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext75.markMmSpeechStyleAsVariable("style", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext75.assignMmSpeechStylePropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		generationContext75.markMmVerbosityAsVariable("verbosity", de.semvox.subcon.data.matcher.FunctionalAssignMethod.getByTag(null));
		generationContext75.assignMmVerbosityPropertyMatcher(de.semvox.subcon.data.matcher.PropertyMatcher.getByToken(null));
		return generationContext75;
	}

	de.semvox.types.odp.interaction.ReportOnTask getReportOnTask76() {
		final de.semvox.types.odp.interaction.ReportOnTask reportOnTask76 = de.semvox.types.odp.interaction.factory.PatternFactoryInteraction.newReportOnTask();
		de.semvox.types.odp.multimodal.Released released77 = getReleased77();
		reportOnTask76.setMmTaskStatus(released77);
		return reportOnTask76;
	}

	de.semvox.types.odp.multimodal.Released getReleased77() {
		final de.semvox.types.odp.multimodal.Released released77 = de.semvox.types.odp.multimodal.factory.PatternFactoryMultimodal.newReleased();
		com.zorabots.ontologies.weather.GetWeather getWeather78 = getGetWeather78();
		released77.setMmTaskExecutionResult(getWeather78);
		return released77;
	}

	com.zorabots.ontologies.weather.GetWeather getGetWeather78() {
		final com.zorabots.ontologies.weather.GetWeather getWeather78 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newGetWeather();
		de.semvox.types.subcon.integration.StatusSuccess statusSuccess79 = getStatusSuccess79();
		getWeather78.setIntResponseStatus(statusSuccess79);
		com.zorabots.ontologies.weather.Expired expired80 = getExpired80();
		getWeather78.setWeatherTimeOut(expired80);
		return getWeather78;
	}

	de.semvox.types.subcon.integration.StatusSuccess getStatusSuccess79() {
		final de.semvox.types.subcon.integration.StatusSuccess statusSuccess79 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newStatusSuccess();
		return statusSuccess79;
	}

	com.zorabots.ontologies.weather.Expired getExpired80() {
		final com.zorabots.ontologies.weather.Expired expired80 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newExpired();
		return expired80;
	}
}