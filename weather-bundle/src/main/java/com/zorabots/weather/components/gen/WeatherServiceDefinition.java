/**
* GENERATED CODE
*/
package com.zorabots.weather.components.gen;

public final class WeatherServiceDefinition extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.subcon.integration.ServiceDefinition> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "int#ServiceDefinition";

	public WeatherServiceDefinition(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public WeatherServiceDefinition(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public WeatherServiceDefinition(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public WeatherServiceDefinition() {
		super();
	}

	@Override
	protected de.semvox.types.subcon.integration.ServiceDefinition createThing() {
		return getServiceDefinition();
	}

	@Override
	protected void initialize(final de.semvox.types.subcon.integration.ServiceDefinition serviceDefinition0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("WeatherService");
		serviceDefinition0.setCommonName(string0);
		String string1 = replaceEntities("com.zorabots.weather.components");
		serviceDefinition0.setIntNamespace(string1);
		com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast1 = getGetWeatherForecast1();
		serviceDefinition0.getIntSubscriptionsAsIntServiceRequest().put("GET_WEATHER_FORECAST", getWeatherForecast1);
		com.zorabots.ontologies.weather.GetWeather getWeather2 = getGetWeather2();
		serviceDefinition0.getIntSubscriptionsAsIntServiceRequest().put("GET_WEATHER", getWeather2);
	}

	de.semvox.types.subcon.integration.ServiceDefinition getServiceDefinition() {
		final de.semvox.types.subcon.integration.ServiceDefinition serviceDefinition0 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newServiceDefinition();
		return serviceDefinition0;
	}


	com.zorabots.ontologies.weather.GetWeatherForecast getGetWeatherForecast1() {
		final com.zorabots.ontologies.weather.GetWeatherForecast getWeatherForecast1 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newGetWeatherForecast();
		return getWeatherForecast1;
	}

	com.zorabots.ontologies.weather.GetWeather getGetWeather2() {
		final com.zorabots.ontologies.weather.GetWeather getWeather2 = com.zorabots.ontologies.weather.factory.PatternFactoryWeather.newGetWeather();
		return getWeather2;
	}
}