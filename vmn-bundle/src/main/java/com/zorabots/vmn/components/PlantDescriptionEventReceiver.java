package com.zorabots.vmn.components;

import java.util.Map;

import org.json.simple.JSONObject;

import com.zorabots.vmn.components.gen.PlantDescriptionEventReceiverDefinition;
import com.zorabots.vmn.events.JsonParser;
import com.zorabots.vmn.pojo.Plant;

import de.semvox.commons.logging.Logger.LogLevel;
import de.semvox.subcon.integration.api.EventReceiver;
import de.semvox.types.common.Event;
import de.semvox.types.subcon.integration.EventReceiverDefinition;
import de.semvox.types.subcon.integration.TokenEvent;

public class PlantDescriptionEventReceiver implements EventReceiver {

	private final EventReceiverDefinition definition;

	public PlantDescriptionEventReceiver() throws Exception {
		definition = new PlantDescriptionEventReceiverDefinition().loadThing();
	}

	@Override
	public EventReceiverDefinition getEventReceiverDefinition() {
		return definition;
	}

	@Override
	public void dispose() {

	}

	@Override
	public void handleEvent(Event event, Map<String, Object> boundVariables) {
		// TODO Auto-generated method stub

	}
	
	public void handle(String json){
		de.semvox.commons.logging.Logger.log(LogLevel.INFO, "HEEEEEYYYY", "AM I BEING CALLED HERE?");
		de.semvox.commons.logging.Logger.log(LogLevel.INFO, "json", json);
		JSONObject jsonObject = JsonParser.parseJson(json);
		
		if (jsonObject.containsKey("facts")){
			String facts = (String) jsonObject.get("facts");
			Plant.getInstance().setDescription(facts);
		}
		
	}

	@Override
	public void handleToken(TokenEvent token, Map<String, Object> boundVariables) {
		// TODO Auto-generated method stub

	}

}
