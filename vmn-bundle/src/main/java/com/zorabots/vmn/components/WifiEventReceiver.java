package com.zorabots.vmn.components;

import java.util.Map;

import org.json.simple.JSONObject;

import com.zorabots.ontologies.vmn.Connected;
import com.zorabots.ontologies.vmn.Connecting;
import com.zorabots.ontologies.vmn.Disconnected;
import com.zorabots.ontologies.vmn.WifiUpdate;
import com.zorabots.ontologies.vmn.factory.ThingFactoryVmn;
import com.zorabots.vmn.components.gen.WifiEventReceiverDefinition;
import com.zorabots.vmn.events.JsonParser;

import de.semvox.commons.logging.Logger.LogLevel;
import de.semvox.subcon.integration.api.EventReceiver;
import de.semvox.subcon.integration.api.IpcFacade;
import de.semvox.types.common.Event;
import de.semvox.types.odp.multimodal.PresentEvent;
import de.semvox.types.odp.multimodal.factory.ThingFactoryMultimodal;
import de.semvox.types.subcon.integration.EventReceiverDefinition;
import de.semvox.types.subcon.integration.TokenEvent;

public class WifiEventReceiver implements EventReceiver {

	private final EventReceiverDefinition definition;
	private IpcFacade ipcFacade;

	public WifiEventReceiver(IpcFacade ipcFacade) throws Exception {
		definition = new WifiEventReceiverDefinition().loadThing();
		this.ipcFacade = ipcFacade;
	}

	@Override
	public EventReceiverDefinition getEventReceiverDefinition() {
		return definition;
	}

	@Override
	public void dispose() {

	}

	@Override
	public void handleEvent(Event event, Map<String, Object> boundVariables) {
		if (event instanceof WifiUpdate) {
			WifiUpdate wifiUpdate = (WifiUpdate) event;
			PresentEvent presentEvent = ThingFactoryMultimodal.newPresentEvent();
			presentEvent.setMmEvent(wifiUpdate);
			ipcFacade.invokeService(presentEvent, getClass().getCanonicalName());
		}
	}

	@Override
	public void handleToken(TokenEvent token, Map<String, Object> boundVariables) {
		// TODO Auto-generated method stub

	}

	public void handle(String json) {
		de.semvox.commons.logging.Logger.log(LogLevel.INFO, "json", json);
		JSONObject jsonObject = JsonParser.parseJson(json);
		WifiUpdate wifiUpdate = ThingFactoryVmn.newWifiUpdate();
		if (jsonObject.containsKey("status")) {
			String status = (String) jsonObject.get("status");
			switch(status){
				case "connecting":
					Connecting connecting = ThingFactoryVmn.newConnecting();
					wifiUpdate.setVmnWifiStatus(connecting);
					ipcFacade.sendEvent((Event) wifiUpdate, "zelve");
					break;
				case "connected":
					Connected connected = ThingFactoryVmn.newConnected();
					wifiUpdate.setVmnWifiStatus(connected);
					ipcFacade.sendEvent((Event) wifiUpdate, "zelve");
					break;
				case "disconnected":
					Disconnected disconnected = ThingFactoryVmn.newDisconnected();
					wifiUpdate.setVmnWifiStatus(disconnected);
					ipcFacade.sendEvent((Event) wifiUpdate, "zelve");
					break;
			}
		}
	}

}
