package com.zorabots.vmn.pojo;

public class PlantParameter {
	private String optimal = "niet vermeld";
	private String minimum = "niet vermeld";
	private String maximum = "niet vermeld";
	private String unit = "";

	/**
	 * @return the unit
	 */
	public String getUnit() {
		return unit;
	}
	/**
	 * @param unit the unit to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}
	/**
	 * @return the optimal
	 */
	public String getOptimal() {
		return optimal;
	}
	/**
	 * @param optimal the optimal to set
	 */
	public void setOptimal(String optimal) {
		this.optimal = optimal + " " + this.getUnit();
	}
	/**
	 * @return the minimum
	 */
	public String getMinimum() {
		return minimum;
	}
	/**
	 * @param minimum the minimum to set
	 */
	public void setMinimum(String minimum) {
		this.minimum = minimum  + " " + this.getUnit();
	}
	/**
	 * @return the maximum
	 */
	public String getMaximum() {
		return maximum;
	}
	/**
	 * @param maximum the maximum to set
	 */
	public void setMaximum(String maximum) {
		this.maximum = maximum  + " " + this.getUnit();
	}
	
	
}
