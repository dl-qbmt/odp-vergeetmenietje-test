/**
* GENERATED CODE
*/
package com.zorabots.deployment.gen;

public final class weather extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.odp.s3.DialogBundleDefinition> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "s3#DialogBundleDefinition";

	public weather(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public weather(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public weather(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public weather() {
		super();
	}

	@Override
	protected de.semvox.types.odp.s3.DialogBundleDefinition createThing() {
		return getDialogBundleDefinition();
	}

	@Override
	protected void initialize(final de.semvox.types.odp.s3.DialogBundleDefinition dialogBundleDefinition0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("com.zorabots.weather.0.1");
		dialogBundleDefinition0.setCommonId(string0);
		String string1 = replaceEntities("com.zorabots.weather.WeatherBundle");
		dialogBundleDefinition0.setS3BundleClass(string1);
		String string2 = replaceEntities("weather");
		dialogBundleDefinition0.setS3BundleName(string2);
		String string3 = replaceEntities("com.zorabots");
		dialogBundleDefinition0.setS3Realm(string3);
		String string4 = replaceEntities("0.1");
		dialogBundleDefinition0.setS3Version(string4);
	}

	de.semvox.types.odp.s3.DialogBundleDefinition getDialogBundleDefinition() {
		final de.semvox.types.odp.s3.DialogBundleDefinition dialogBundleDefinition0 = de.semvox.types.odp.s3.factory.PatternFactoryS3.newDialogBundleDefinition();
		return dialogBundleDefinition0;
	}

}