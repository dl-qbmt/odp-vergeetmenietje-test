/**
* GENERATED CODE
*/
package com.zorabots.vmn.components.gen;

public final class RecognitionEventReceiverDefinition extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.subcon.integration.EventReceiverDefinition> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "int#EventReceiverDefinition";

	public RecognitionEventReceiverDefinition(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public RecognitionEventReceiverDefinition(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public RecognitionEventReceiverDefinition(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public RecognitionEventReceiverDefinition() {
		super();
	}

	@Override
	protected de.semvox.types.subcon.integration.EventReceiverDefinition createThing() {
		return getEventReceiverDefinition();
	}

	@Override
	protected void initialize(final de.semvox.types.subcon.integration.EventReceiverDefinition eventReceiverDefinition0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("RecognitionEventReceiver");
		eventReceiverDefinition0.setCommonName(string0);
		String string1 = replaceEntities("com.zorabots.vmn.components");
		eventReceiverDefinition0.setIntNamespace(string1);
		de.semvox.types.asr.RecognitionEvent recognitionEvent1 = getRecognitionEvent1();
		eventReceiverDefinition0.getIntSubscriptionsAsCommonEvent().put("RECOGNITIONEVENT", recognitionEvent1);
	}

	de.semvox.types.subcon.integration.EventReceiverDefinition getEventReceiverDefinition() {
		final de.semvox.types.subcon.integration.EventReceiverDefinition eventReceiverDefinition0 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newEventReceiverDefinition();
		return eventReceiverDefinition0;
	}


	de.semvox.types.asr.RecognitionEvent getRecognitionEvent1() {
		final de.semvox.types.asr.RecognitionEvent recognitionEvent1 = de.semvox.types.asr.factory.PatternFactoryAsr.newRecognitionEvent();
		return recognitionEvent1;
	}
}