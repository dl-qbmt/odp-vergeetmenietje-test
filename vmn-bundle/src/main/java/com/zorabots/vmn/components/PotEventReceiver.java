package com.zorabots.vmn.components;

import java.util.Map;

import org.json.simple.JSONObject;

import com.zorabots.ontologies.vmn.Injected;
import com.zorabots.ontologies.vmn.NewPotUpdate;
import com.zorabots.ontologies.vmn.Removed;
import com.zorabots.ontologies.vmn.factory.ThingFactoryVmn;
import com.zorabots.vmn.components.gen.PotEventReceiverDefinition;
import com.zorabots.vmn.events.JsonParser;

import de.semvox.commons.logging.Logger;
import de.semvox.commons.logging.Logger.LogLevel;
import de.semvox.subcon.integration.api.EventReceiver;
import de.semvox.subcon.integration.api.IpcFacade;
import de.semvox.types.common.Event;
import de.semvox.types.odp.multimodal.PresentEvent;
import de.semvox.types.odp.multimodal.factory.ThingFactoryMultimodal;
import de.semvox.types.subcon.integration.EventReceiverDefinition;
import de.semvox.types.subcon.integration.TokenEvent;

public class PotEventReceiver implements EventReceiver {

	private final EventReceiverDefinition definition;
	private IpcFacade ipcFacade;

	public PotEventReceiver(IpcFacade ipcFacade) throws Exception {
		definition = new PotEventReceiverDefinition().loadThing();
		this.ipcFacade = ipcFacade;
	}

	@Override
	public EventReceiverDefinition getEventReceiverDefinition() {
		return definition;
	}

	@Override
	public void dispose() {

	}

	@Override
	public void handleEvent(Event event, Map<String, Object> boundVariables) {
		// TODO Auto-generated method stub
		if(event instanceof NewPotUpdate){
			NewPotUpdate newPotUpdate = (NewPotUpdate) event;
			PresentEvent presentEvent = ThingFactoryMultimodal.newPresentEvent();
			presentEvent.setMmEvent(newPotUpdate);
			ipcFacade.invokeService(presentEvent, getClass().getCanonicalName());
		}

	}
	
	public void handle(String json){
		
		de.semvox.commons.logging.Logger.log(LogLevel.INFO, "json", json);
		JSONObject jsonObject = JsonParser.parseJson(json);
		
		if (jsonObject.containsKey("plantname")){
			this.sendEventNewPlant((String) jsonObject.get("plantname"));
		}
		else{
			this.sendEventRemovedPlant();
		}
	}
	
	public void sendEventNewPlant(String plantname){
		String oldplant = com.zorabots.vmn.pojo.Plant.getInstance().getName();
		Logger.log(LogLevel.INFO, "VMN Bundle", "Plant new pot update: name plant: " + plantname);
		if(!(oldplant.equals(plantname)) && !(plantname.equals("")) ){
			NewPotUpdate newPotUpdate = ThingFactoryVmn.newNewPotUpdate();
			Injected injected = ThingFactoryVmn.newInjected();
			newPotUpdate.setVmnChange(injected);
			
			com.zorabots.ontologies.vmn.Plant plant = ThingFactoryVmn.newPlant();
			plant.setVmnPlantName(plantname);
			
			com.zorabots.vmn.pojo.Plant.getInstance().setName(plantname);
			
			newPotUpdate.setVmnPlant(plant);
			
			ipcFacade.sendEvent((Event) newPotUpdate, "zelve");
		}
	}
	
	public void sendEventRemovedPlant(){
		NewPotUpdate newPotUpdate = ThingFactoryVmn.newNewPotUpdate();
		Removed removed = ThingFactoryVmn.newRemoved();
		newPotUpdate.setVmnChange(removed);
		com.zorabots.ontologies.vmn.Plant plant = ThingFactoryVmn.newPlant();
		plant.setVmnPlantName(com.zorabots.vmn.pojo.Plant.getInstance().getName());
		newPotUpdate.setVmnPlant(plant);
		
		//TODO: clear plant instance. 
		
		ipcFacade.sendEvent((Event) newPotUpdate, "zelve");
	}

	@Override
	public void handleToken(TokenEvent token, Map<String, Object> boundVariables) {
		// TODO Auto-generated method stub

	}

}
