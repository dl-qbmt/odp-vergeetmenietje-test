package com.zorabots.vmn.components;

import java.util.Map;

import org.json.simple.JSONObject;

import com.zorabots.ontologies.vmn.GetHumidity;
import com.zorabots.ontologies.vmn.Humidity;
import com.zorabots.ontologies.vmn.HumidityReport;
import com.zorabots.ontologies.vmn.YesNoHumidity;
import com.zorabots.ontologies.vmn.factory.ThingFactoryVmn;
import com.zorabots.vmn.components.gen.VmnHumidityServiceDefinition;
import com.zorabots.vmn.events.JsonParser;
import com.zorabots.vmn.mqtt.requests.GetRequests;
import com.zorabots.vmn.pojo.SharedUnits;
import com.zorabots.vmn.staticclasses.ClosedQuestionParameters;
import com.zorabots.vmn.staticclasses.UnitOntologyMap;

import de.semvox.subcon.data.ThingUtils;
import de.semvox.subcon.integration.api.IpcFacade;
import de.semvox.subcon.integration.api.Service;
import de.semvox.types.odp.multimodal.AsyncServiceResponse;
import de.semvox.types.odp.multimodal.PresentEvent;
import de.semvox.types.odp.multimodal.factory.ThingFactoryMultimodal;
import de.semvox.types.subcon.integration.AsyncServiceRequest;
import de.semvox.types.subcon.integration.ServiceDefinition;
import de.semvox.types.subcon.integration.ServiceRequest;
import de.semvox.types.subcon.integration.factory.ThingFactoryIntegration;

public class VmnHumidityService implements Service {

	private final ServiceDefinition definition;
	private IpcFacade ipcFacade;

	public VmnHumidityService(IpcFacade ipcFacade) throws Exception {
		definition = new VmnHumidityServiceDefinition().loadThing();
		this.ipcFacade = ipcFacade;
	}

	@Override
	public ServiceDefinition getServiceDefinition() {
		return definition;
	}

	@Override
	public void dispose() {

	}

	@Override
	public ServiceRequest handleRequest(ServiceRequest request,
			Map<String, Object> boundVariables) {
		if(request instanceof AsyncServiceRequest){
			AsyncServiceRequest asyncServiceRequest = (AsyncServiceRequest)request;
			
			if(asyncServiceRequest instanceof GetHumidity){
				asyncServiceRequest.setIntResponseStatus(ThingFactoryIntegration.newStatusAccepted());
				final GetHumidity getTemperatureRequest = ThingUtils.deepCopy((GetHumidity)request);
				GetRequests getRequests = new GetRequests("vmnengine/get/sensor/humidity", "vmnengine/response/odp/humidity", "{ \"key\":\"odp\"}", this, "handle", getTemperatureRequest);
				getRequests.start();
			}
			
		}
		return request;
	}
	
	public void handle(String recMessage, Object request){
		GetHumidity getHumidityRequest = (GetHumidity) request;
		 HumidityReport report = ThingFactoryVmn.newHumidityReport();
		 
		 JSONObject jsonObject = JsonParser.parseJson(recMessage);
         
		 if(request instanceof YesNoHumidity){
			 ClosedQuestionParameters.yesNoQuestion((YesNoHumidity)request, ((Long)jsonObject.get("level")).intValue());

		 }
		 
		 Humidity humidity = ThingFactoryVmn.newHumidity();
		 
		 double value = (double)jsonObject.get("value");
		 
		 
		 //int scale = (int) Math.pow(10, 1);
		 int intvalue  = (int) Math.round(value);
		 
		 humidity.setCommonStringValue(Integer.toString(intvalue));
		 String humunit = SharedUnits.getInstance().map.get("humidity");
		 humidity.setVmnUnit(UnitOntologyMap.getUnitOntology(humunit));
		 
		 report.setVmnHumidity(humidity);
		 
		 getHumidityRequest.setVmnHumidityReport(report);
		 getHumidityRequest.setIntResponseStatus(ThingFactoryIntegration.newStatusSuccess());
         
		 PresentEvent presentEvent = ThingFactoryMultimodal.newPresentEvent();
         presentEvent.setMmTimeToLive((long) 2000);
         
         AsyncServiceResponse asyncServiceResponse = ThingFactoryMultimodal.newAsyncServiceResponse();
         asyncServiceResponse.setIntResponse(getHumidityRequest);
         asyncServiceResponse.setMmCorrelationId(getHumidityRequest.getCommonId());
         
         presentEvent.setMmEvent(asyncServiceResponse);
         ipcFacade.invokeService(presentEvent,
                 String.format("%s.%s", definition.getIntNamespace(), definition.getCommonName()));
	}
	

}
