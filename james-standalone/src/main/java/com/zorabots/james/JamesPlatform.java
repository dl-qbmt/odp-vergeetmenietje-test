package com.zorabots.james;

import java.lang.reflect.Constructor;

import com.zorabots.socket.Client;

import de.semvox.odp.s3.S3CorePlatform;
import de.semvox.tts.bridge.TtsProvider;

public class JamesPlatform extends S3CorePlatform {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.semvox.odp.s3.S3CorePlatform#newTtsProvider()
	 */
	private final TtsProvider ttsProvider;

	public JamesPlatform() {
		this.ttsProvider = new com.zorabots.bridge.TtsProvider();
		Client.getInstance().setLanguage("nl-BE");
		Client.getInstance().setOriginalLanguage("nl-BE");
	}

	@Override
	protected TtsProvider newTtsProvider() throws Exception {
		// TODO Auto-generated method stub
		final Class<TtsProvider> providerCls = (Class<TtsProvider>) Class
				.forName(this.getTtsProviderClass());
		final Constructor<TtsProvider> cons = providerCls.getConstructor();
		return cons.newInstance();
	}

}
