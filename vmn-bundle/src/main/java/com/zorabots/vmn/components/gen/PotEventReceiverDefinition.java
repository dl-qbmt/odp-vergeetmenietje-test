/**
* GENERATED CODE
*/
package com.zorabots.vmn.components.gen;

public final class PotEventReceiverDefinition extends de.semvox.subcon.data.JavaThingLoader<de.semvox.types.subcon.integration.EventReceiverDefinition> implements de.semvox.commons.Generated {

	public static java.lang.String TYPE_NAME = "int#EventReceiverDefinition";

	public PotEventReceiverDefinition(de.semvox.commons.params.ParameterProvider provider, java.util.Map<java.lang.String, java.lang.String> entities) {
		super(provider, entities);
	}

	public PotEventReceiverDefinition(java.util.Map<java.lang.String, java.lang.String> entities) {
		super(entities);
	}

	public PotEventReceiverDefinition(de.semvox.commons.params.ParameterProvider provider) {
		super(provider);
	}

	public PotEventReceiverDefinition() {
		super();
	}

	@Override
	protected de.semvox.types.subcon.integration.EventReceiverDefinition createThing() {
		return getEventReceiverDefinition();
	}

	@Override
	protected void initialize(final de.semvox.types.subcon.integration.EventReceiverDefinition eventReceiverDefinition0, java.util.Map<String, Object> params) {

		String string0 = replaceEntities("PotEventReceiver");
		eventReceiverDefinition0.setCommonName(string0);
		String string1 = replaceEntities("com.zorabots.vmn.components");
		eventReceiverDefinition0.setIntNamespace(string1);
		com.zorabots.ontologies.vmn.NewPotUpdate newPotUpdate1 = getNewPotUpdate1();
		eventReceiverDefinition0.getIntSubscriptionsAsCommonEvent().put("NEW_POT_UPDATE", newPotUpdate1);
	}

	de.semvox.types.subcon.integration.EventReceiverDefinition getEventReceiverDefinition() {
		final de.semvox.types.subcon.integration.EventReceiverDefinition eventReceiverDefinition0 = de.semvox.types.subcon.integration.factory.PatternFactoryIntegration.newEventReceiverDefinition();
		return eventReceiverDefinition0;
	}


	com.zorabots.ontologies.vmn.NewPotUpdate getNewPotUpdate1() {
		final com.zorabots.ontologies.vmn.NewPotUpdate newPotUpdate1 = com.zorabots.ontologies.vmn.factory.PatternFactoryVmn.newNewPotUpdate();
		return newPotUpdate1;
	}
}